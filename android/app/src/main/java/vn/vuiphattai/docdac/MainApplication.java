package vn.vuiphattai.docdac;

import android.app.Application;

import com.reactnativenavigation.NavigationApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import android.support.annotation.Nullable;
import com.oblador.vectoricons.VectorIconsPackage;
import com.beefe.picker.PickerViewPackage;
import com.rt2zz.reactnativecontacts.ReactNativeContacts;

import java.util.Arrays;
import java.util.List;

import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.reactlibrary.RNEncryptToolPackage;
import com.imagepicker.ImagePickerPackage;
import com.projectseptember.RNGL.RNGLPackage;
import com.rnfs.RNFSPackage;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import com.microsoft.codepush.react.CodePush;
import com.zmxv.RNSound.RNSoundPackage;
// import com.smixx.fabric.FabricPackage;

import io.underscope.react.fbak.RNAccountKitPackage;


public class MainApplication extends NavigationApplication {

    @Override
    public boolean isDebug() {
        // Make sure you are using BuildConfig from your own application
        return BuildConfig.DEBUG;
    }

    // @Override
    // public String getJSBundleFile() {
    //     return CodePush.getJSBundleFile();
    // }

    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
        // Add the packages you require here.
        // No need to add RnnPackage and MainReactPackage
        return Arrays.<ReactPackage>asList(
                new CodePush(BuildConfig.CODEPUSH_KEY, MainApplication.this, BuildConfig.DEBUG),
                new VectorIconsPackage(),
                new PickerViewPackage(),
                new RNDeviceInfo(),
                new RNEncryptToolPackage(),
                new ImagePickerPackage(),
                new RNGLPackage(),
                new RNFSPackage(),
                new ReactNativeOneSignalPackage(),
                new RNSoundPackage(),
                // new FabricPackage(),
                new RNAccountKitPackage(),
                new ReactNativeContacts()
        );
    }


}
