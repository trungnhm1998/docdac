const formatMoney = (value) => {
  value = value.toString()
  const formatted = value.replace(new RegExp('\\,', 'g'), '')
  return formatted.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}
export default formatMoney
