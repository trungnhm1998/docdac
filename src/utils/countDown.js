import React from 'react'
import moment from 'moment'
import _ from 'lodash'
import { View, Text } from 'react-native'

/**
 * Count down module
 * A simple count down component.
 * */

const DateBetween = function (startDate, endDate) {
  const second = 1000
  const minute = second * 60
  const hour = minute * 60
  const day = hour * 24
  const distance = endDate - startDate

  if (distance < 0) {
    return false
  }

  const days = Math.floor(distance / day)
  const hours = Math.floor((distance % day) / hour)
  const minutes = Math.floor((distance % hour) / minute)
  const seconds = Math.floor((distance % minute) / second)

  const between = []

  days > 0 ? between.push(`${_.padStart(days, 2, 0)} ngày\n`) : false
  hours >= 0 ? between.push(` ${_.padStart(hours, 2, 0)}:`) : false
  minutes >= 0 ? between.push(`${_.padStart(minutes, 2, 0)}:`) : false
  seconds >= 0 ? between.push(`${_.padStart(seconds, 2, 0)}`) : false

  return between.join('')
}

export default class Countdown extends React.Component {

  constructor(props) {
    super(props)
    this.state = { remaining: null }
  }

  componentDidMount() {
    this.tick()
    this.interval = setInterval(this.tick.bind(this), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  tick() {
    const startDate = moment().valueOf()
    const endDate = moment(this.props.options.endDate, 'MM-DD-YYYY HH:mm').valueOf()
    const remaining = DateBetween(startDate, endDate)
    if (remaining === false) {
      clearInterval(this.interval)
      // this.props.options['cb'] ? this.props.options.cb() : false
    }

    this.setState({
      remaining: remaining || 'Vé đang xổ'
    })
  }

  render() {
    return (
      <Text style={{ marginTop: 5, color: '#fff' }}> {this.state.remaining}</Text>
    )
  }
}
