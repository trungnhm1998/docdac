// import store from '../configs/store.config'

const onReceived = (data) => {
  console.log('onReceived', data)
}

const onRegistered = (data) => {
  console.log('onRegistered', data) 
}

const onOpened = (data) => {
  const { payload } = data.notification
  if (payload.additionalData) {
    const { cmd, screen_name } = payload.additionalData
    switch (cmd) {
      case 'openScreen':
        console.log(screen_name)
        break
      case 'openUrl':
        console.log(screen_name)
        break
      default:
    }
  }
}

const onIds = () => {
  // console.log('onIds', ids)
}

export { onReceived, onRegistered, onOpened, onIds }
