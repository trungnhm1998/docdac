/**
 * Created by kami on 11/2/17.
 */

import AccountKit, { Color, StatusBarStyle } from 'react-native-facebook-account-kit'


const configureAccountKit = (phone, countryCode, countryWhitelist) => {
  AccountKit.configure({
    theme: {
      buttonBackgroundColor: Color.hex('#2B7FE0'),
      buttonDisabledBackgroundColor: Color.hex('#96BDEB'),
      buttonBorderColor:     Color.hex('#2B7FE0'),
      buttonTextColor:       Color.hex('#FFF'),
      headerBackgroundColor: Color.hex('#B10909'),
      headerTextColor:       Color.hex('#FFF'),
      iconColor:             Color.hex('#B10909'),
      inputBackgroundColor:  Color.hex('#FFF'),
      inputBorderColor:      Color.hex('#B1CFF0'),
      textColor:             Color.hex('#46586D'),
      inputTextColor:        Color.hex('#33475F'),
      //backgroundImage:       "background.png",
    },

    titleType: 'app_name',
    countryWhitelist: countryWhitelist,
    defaultCountry: 'VN',
    initialPhoneCountryPrefix: `+${countryCode}`,
    initialPhoneNumber: phone
  })
}

const verifyPhone = (phone, countryCode, countryWhitelist) => {
  configureAccountKit(phone, countryCode, countryWhitelist)
  return AccountKit.loginWithPhone()
}

export { configureAccountKit, verifyPhone }