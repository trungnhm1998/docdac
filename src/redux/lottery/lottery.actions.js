import * as types from './lottery.types'


export function getInitLotteryResult(data) {
  return {
    type: types.GET_INIT_LOTTERY_RESULT,
    payload: { ...data }
  }
}

export function getLottery645Result(data) {
  return {
    type: types.GET_LOTTERY_645_RESULT,
    payload: { ...data }
  }
}

export function getLottery655Result(data) {
  return {
    type: types.GET_LOTTERY_655_RESULT,
    payload: { ...data }
  }
}

export function getLotteryMax4dResult(data) {
  return {
    type: types.GET_LOTTERY_MAX4D_RESULT,
    payload: { ...data }
  }
}

export function get645PrizesList() {
  return {
    type: types.GET_645_PRIZE_LIST
  }
}

export function getMax4dPrizesList() {
  return {
    type: types.GET_MAX4D_PRIZE_LIST
  }
}

export function getTicketTypeList() {
  return {
    type: types.GET_TICKET_TYPE_LIST
  }
}

export function getUserLottery(data) {
  return {
    type: types.GET_USER_LOTTERY,
    payload: { ...data }
  }
}

export function getUserLotteryDetail(data) {
  return {
    type: types.GET_USER_LOTTERY_DETAIL,
    payload: { ...data }
  }
}

export function getListDeliveryAddress() {
  return {
    type: types.GET_LIST_DELIVERY_ADDRESS
  }
}

export function buyMegaPowerTicket(data, accessToken) {
  return {
    type: types.BUY_MEGA_POWER_TICKET,
    payload: { data, accessToken }
  }
}

export function buyMegaTicket(data, accessToken) {
  return {
    type: types.BUY_MEGA_TICKET,
    payload: { data, accessToken }
  }
}

export function buyMaxTicket(data, accessToken) {
  return {
    type: types.BUY_MEGA_TICKET,
    payload: { data, accessToken }
  }
}
export function userConfirmReceiveTicket(data) {
  return {
    type: types.USER_CONFIRM_RECEIVE_TICKET,
    payload: { ...data }
  }
}
export function getListPeriodTicket() {
  return {
    type: types.GET_LIST_PERIOD_TICKET
  }
}
export function getTicketByPreiod(data) {
  return {
    type: types.GET_TICKET_BY_PREIOD,
    payload: { ...data }
  }
}

export function getAgentList(data) {
  return {
    type: types.GET_AGENT_LIST,
    payload: { ...data }
  }
}

export function getAgentRewardList(data) {
  return {
    type: types.GET_AGENT_REWARD_LIST,
    payload: { ...data }
  }
}

export function postTicketScheduleReward(data) {
  return {
    type: types.POST_TICKET_SCHEDULE_REWARD,
    payload: { ...data }
  }
}

export function gotoResult(data) {
  return {
    type: types.GO_TO_RESULT_TAB,
    payload: data
  }
}

export function getBannerList() {
  return {
    type: types.GET_BANNER_LIST
  }
}
