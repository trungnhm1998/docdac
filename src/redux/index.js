import deviceReducer from './device/device.reducers'
import initReducer from './init/init.reducers'
import apiReducer from './api/api.reducers'
import accountReducer from './account/account.reducers'
import lotteryReducer from './lottery/lottery.reducers'
import paymentReducer from './payment/payment.reducers'

export {
  deviceReducer,
  initReducer,
  apiReducer,
  accountReducer,
  lotteryReducer,
  paymentReducer
}
