import * as types from './device.types'

const initialState = {
  passLock: false,
  version: '0.0.9'
}

export default function init(state = initialState, action) {
  switch (action.type) {
    case types.REGISTER_CLIENT_SUCCESS: {
      const { clientId } = action.payload
      return { ...state, clientId }
    }
    case types.REGISTER_PASS_LOCK: {
      const { passLock } = action.payload
      return {
        ...state,
        passLock
      }
    }
    case types.REMOVE_PASS_LOCK:
      return {
        ...state,
        passLock: false
      }
    case types.CLEAR_ALL_DEVICE:
      return {}
    case types.SET_ROUTING:
      return {
        ...state,
        routeName: action.payload.routeName
      }
    case types.UPGRADE_VERSION:
      return {
        ...state,
        version: action.payload.version
      }
    default:
      return state
  }
}
