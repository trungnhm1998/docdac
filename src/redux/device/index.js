import * as types from './device.types'
import * as actions from './device.actions'

export {
  types,
  actions,
}
