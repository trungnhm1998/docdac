import * as types from './account.types'

export function authorize(phone, countryCode, password, navigator) {
  return {
    type: types.AUTHORIZE_USER,
    payload: { phone, countryCode, password, navigator }
  }
}

export function verifyPhone(phone, countryCode, useAccountKit, navigator) {
  return {
    type: types.VERIFY_PHONE,
    payload: { phone, countryCode, useAccountKit, navigator }
  }
}

export function sendOTPRegister(phone, navigator) {
  return {
    type: types.SEND_OTP_REGISTER,
    payload: { phone, navigator }
  }
}

export function reSendOTPRegister(phone) {
  return {
    type: types.RE_SEND_OTP_REGISTER,
    payload: { phone }
  }
}

export function register(phone, password, otp, navigator) {
  return {
    type: types.REGISTER_USER,
    payload: { phone, password, otp, navigator }
  }
}

export function registerAccountKit(phone, countryCode, password, token, navigator) {
  return {
    type: types.REGISTER_USER_ACCOUNT_KIT,
    payload: { phone, countryCode, password, token, navigator }
  }
}

export function logout(navigator) {
  return {
    type: types.LOGOUT_USER,
    payload: { navigator }
  }
}

export function getUserInfo(accessToken) {
  return {
    type: types.GET_USERINFO,
    payload: { accessToken }
  }
}

export function getUserInfoDocdac(accessToken, os, env, deviceToken, navigator) {
  return {
    type: types.GET_USERINFO_DOCDAC,
    payload: { accessToken, os, env, deviceToken, navigator }
  }
}

export function updateUserInfo(accessToken, name, idNumber, birthday, gender, email, address, avatarUrl) {
  return {
    type: types.UPDATE_USERINFO,
    payload: { accessToken, name, idNumber, birthday, gender, email, address, avatarUrl }
  }
}

export function sendOTPForgotPassword(phone, navigator) {
  return {
    type: types.SEND_OTP_FORGOT_PASSWORD,
    payload: { phone, navigator }
  }
}

export function forgotPassword(phone, otp, password, rePassword, navigator) {
  return {
    type: types.FORGOT_PASS,
    payload: { phone, otp, password, rePassword, navigator }
  }
}

export function forgotPasswordAccountKit(phone, countryCode, token, password, rePassword, navigator) {
  return {
    type: types.FORGOT_PASS_ACCOUNT_KIT,
    payload: { phone, countryCode, token, password, rePassword, navigator }
  }
}

export function addNewAddress(data, accessToken) {
  return {
    type: types.ADD_NEW_ADDRESS,
    payload: { data, accessToken }
  }
}

export function getUserAddress(accessToken) {
  return {
    type: types.GET_USER_ADDRESS,
    payload: { accessToken }
  }
}

export function uploadAvatar(avatar, callback) {
  return {
    type: types.UPLOAD_AVATAR,
    payload: { avatar, callback }
  }
}

export function getTransferHistory(data, accessToken) {
  return {
    type: types.GET_TRANSFER_HISTORY,
    payload: { data, accessToken }
  }
}

export function rechargeHistory(data, accessToken) {
  return {
    type: types.RECHARGE_HISTORY,
    payload: { data, accessToken }
  }
}

export function withdrawHistory(data, accessToken) {
  return {
    type: types.WITHDRAW_HISTORY,
    payload: { data, accessToken }
  }
}

export function sendOrderWithdraw(data, accessToken) {
  return {
    type: types.SEND_ORDER_WITHDRAW_BANKING,
    payload: { data, accessToken }
  }
}

export function getMoneySetting() {
  return {
    type: types.GET_MONEY_SETTING
  }
}

export function getWithdrawHistory(data, accessToken) {
  return {
    type: types.GET_WITHDRAW_HISTORY,
    payload: { data, accessToken }
  }
}

export function moneyHistory(data, accessToken) {
  return {
      type: types.MONEY_HISTORY,
      payload: { data, accessToken }
  }
}

export function transferMoney(data, accessToken) {
  return {
    type: types.TRANSFER_MONEY,
    payload: { data, accessToken }
  }
}