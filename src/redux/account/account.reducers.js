import * as types from './account.types'
import _ from 'lodash'
const initialState = {
  rechargeHistory: {
    data: []
  },
  withDrawHistory: {
    data: []
  },
  moneyHistory: {
    data: []
  },
  transferHistory: {
    data: []
  },
  moneySetting: {
    data: []
  }
}

export default function api(state = initialState, action) {
  switch (action.type) {
    case types.SHOW_LOADING:
      return {
        ...initialState,
        showLoading: true
      }
    case types.HIDE_LOADING:
      return {
        ...initialState,
        showLoading: false
      }
    case types.STORE_API_MESSAGE: {
      const { type, message, isDefault } = action.payload
      return {
        ...state,
        type,
        message,
        isDefault
      }
    }
    case types.AUTHORIZE_USER_SUCCESS: {
      return {
        ...state,
        ...action.payload
      }
    }
    case types.STORE_API_PAYLOAD: {
      const { type, data } = action.payload
      return {
        ...state,
        type,
        data
      }
    }
    case types.SEND_OTP_REGISTER_SUCCESS: {
      const { phone } = action.payload
      return {
        ...state,
        phone
      }
    }
    case types.VERIFY_PHONE_SUCCESS: {
      const { phone, countryCode } = action.payload
      return {
        ...state,
        phone,
        countryCode
      }
    }
    case types.REGISTER_USER_SUCCESS: {
      return {
        ...state,
        ...action.payload
      }
    }
    case types.GET_USERINFO_SUCCESS: {
      return {
        ...state,
        ...action.payload
      }
    }
    case types.UPDATE_USERINFO_SUCCESS: {
      return {
        ...state,
        ...action.payload
      }
    }
    case types.SEND_OTP_FORGOT_PASSWORD_SUCCESS: {
      const { phone } = action.payload
      return {
        ...state,
        phone
      }
    }
    case types.LOGOUT_USER_SUCCESS: {
      return {}
    }
    case types.GET_USERINFO_DOCDAC_SUCCESS: {
      return {
        ...state,
        ...action.payload
      }
    }
    case types.GET_USERINFO_DOCDAC_FAIL: {
      return {}
    }
    case types.GET_USER_ADDRESS_SUCCESS:
      return {
        ...state,
        deliveryAddressList: [...action.payload]
      }
    case types.UPLOAD_AVATAR_SUCCESS:
      const { avatarUrl } = action.payload
      return {
        ...state,
        avatarUrl
      }
    case types.ADD_NEW_ADDRESS_SUCCESS: {
      const { deliveryAddressList } = state
      return {
        ...state,
        deliveryAddressList: [action.payload, ...deliveryAddressList]
      }
    }
    case types.GET_TRANSFER_HISTORY_SUCCESS: {
      const { last_page, data, nextPage } = action.payload
      const { transferHistory } = state
      if (nextPage === 2) {
        return {
          ...state,
          transferHistory: { data, last_page, nextPage }
        }
      }
      return {
        ...state,
        transferHistory: { data: [...transferHistory.data, ...data], last_page, nextPage }
      }
    }
    case types.GET_TRANSFER_HISTORY_EMPTY: {
      const { last_page, nextPage, data } = action.payload
      if (nextPage === 2 && data.length === 0) {
        return {
          ...state,
          transferHistory: { data: [], last_page, nextPage }
        }
      }
      const { transferHistory } = state
      return {
        ...state,
        transferHistory: { data: [...transferHistory.data, ...data], last_page, nextPage }
      }
    }
    case types.RECHARGE_HISTORY_SUCCESS: {
      const { last_page, data, nextPage } = action.payload
      const { rechargeHistory } = state
      if (nextPage === 2) {
        return {
          ...state,
          rechargeHistory: { data: { page_data: data }, last_page, nextPage }
        }
      }
      return {
        ...state,
        rechargeHistory: { data: { page_data: [...rechargeHistory.data, ...data] }, last_page, nextPage }
      }
    }
    case types.RECHARGE_HISTORY_EMPTY: {
      const { last_page, nextPage, data } = action.payload
      if (nextPage === 2 && data.length === 0) {
        return {
          ...state,
          rechargeHistory: { data: { page_data: [] }, last_page, nextPage }
        }
      }
      const { rechargeHistory } = state
      return {
        ...state,
        rechargeHistory: { data: { page_data: [...rechargeHistory.data, ...data] }, last_page, nextPage }
      }
    }
    case types.GET_WITHDRAW_HISTORY_SUCCESS: {
      const { last_page, data, nextPage } = action.payload
      const { withDrawHistory } = state
      if (nextPage === 2) {
        return {
          ...state,
          withDrawHistory: { data, last_page, nextPage }
        }
      }
      return {
        ...state,
        withDrawHistory: { data: [...withDrawHistory.data, ...data], last_page, nextPage }
      }
    }
    case types.GET_WITHDRAW_HISTORY_EMPTY: {
      const { last_page, nextPage, data } = action.payload
      if (nextPage === 2 && data.length === 0) {
        return {
          ...state,
          withDrawHistory: { data: [], last_page, nextPage }
        }
      }
      const { withDrawHistory } = state
      return {
        ...state,
        withDrawHistory: { data: [...withDrawHistory.data, ...data], last_page, nextPage }
      }
    }
    case types.WITHDRAW_HISTORY_SUCCESS: {
      const { last_page, data, nextPage } = action.payload
      const { withDrawHistory } = state
      if (nextPage === 2) {
        return {
          ...state,
          withDrawHistory: { data, last_page, nextPage }
        }
      }
      return {
        ...state,
        withDrawHistory: { data: [...withDrawHistory.data, ...data], last_page, nextPage }
      }
    }
    case types.WITHDRAW_HISTORY_EMPTY: {
      const { last_page, nextPage, data } = action.payload

      if (nextPage === 2 && data.length === 0) {
        return {
          ...state,
          withDrawHistory: { data: [], last_page, nextPage }
        }
      }
      const { withDrawHistory } = state
      return {
        ...state,
        withDrawHistory: { data: [...withDrawHistory.data, ...data], last_page, nextPage }
      }
    }
    case types.MONEY_HISTORY_SUCCESS: {
      const { last_page, data, nextPage } = action.payload
      const { moneyHistory } = state
      if (nextPage === 2) {
        return {
          ...state,
          moneyHistory: { data, last_page, nextPage }
        }
      }
      return {
        ...state,
        moneyHistory: { data: [...moneyHistory.data, ...data], last_page, nextPage }
      }
    }
    case types.MONEY_HISTORY_EMPTY: {
      const { last_page, nextPage, data } = action.payload
      if (nextPage === 2 && data.length === 0) {
        return {
          ...state,
          moneyHistory: { data: [], last_page, nextPage }
        }
      }
      const { moneyHistory } = state
      return {
        ...state,
        moneyHistory: { data: [...moneyHistory.data, ...data], last_page, nextPage }
      }
    }
    case types.GET_MONEY_SETTING_SUCCESS: {
      return {
        ...state,
        moneySetting: { data: action.payload.data}
      }
    }
    case types.TRANSFER_MONEY_SUCCESS: {
      return {
        ...state,
        ...action.payload
      }
    }
    default:
      return state
  }
}

