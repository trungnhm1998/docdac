import * as types from './payment.types'

export function getPaymentList(accessToken) {
  return {
    type: types.GET_PAYMENT_LIST,
    payload: { accessToken }
  }
}

export function getPaymentOrder(args, navigator) {
  return {
    type: types.GET_PAYMENT_ORDER,
    payload: { data: { ...args }, navigator }
  }
}

export function rechargeMomo(args, navigator) {
  return {
    type: types.RECHARGE_MOMO,
    payload: { data: { ...args }, navigator }
  }
}

export function paymentGate(args, navigator) {
  return {
    type: types.PAYMENT_GATE,
    payload: { data: { ...args }, navigator }
  }
}

export function paymentCard(args, navigator) {
  return {
    type: types.PAYMENT_CARD,
    payload: { data: { ...args }, navigator }
  }
}

export function withdrawMomo(args, navigator) {
  return {
    type: types.WITHDRAW_MOMO,
    payload: { data: { ...args }, navigator }
  }
}

export function withdrawMomoConfirm(args, navigator) {
  return {
    type: types.WITHDRAW_MOMO_CONFIRM,
    payload: { data: { ...args }, navigator }
  }
}

export function getNapasBankList(accessToken) {
  return {
    type: types.GET_NAPAS_BANK_LIST,
    payload: { accessToken }
  }
}
