import * as types from './payment.types'
const initialState = {}

export default function api(state = initialState, action) {
  switch (action.type) {
    case types.GET_PAYMENT_LIST_SUCCESS: {
      return {
        ...state,
        paymentDepositService: { ...action.payload }
      }
    }
    case types.GET_BANK_INFO_SUCCESS: {
      return {
        ...state,
        bankInfo: { ...action.payload }
      }
    }
    case types.GET_NAPAS_BANK_LIST_SUCCESS: {
      return {
        ...state,
        napasBankList: { ...action.payload }
      }
    }
    default:
      return state
  }
}

