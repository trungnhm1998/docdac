export const CLEAR_API_RESPONSE = 'CLEAR_API_RESPONSE'
export const SERVER_ERROR = 'SERVER_ERROR'
export const REQUIRE_LOGIN = 'REQUIRE_LOGIN'
export const STORE_API_MESSAGE = 'STORE_API_MESSAGE'
export const STORE_API_PAYLOAD = 'STORE_API_PAYLOAD'
export const API_RESPONSE = 'API_RESPONSE'
export const SHOW_LOADING = 'SHOW_LOADING'
export const HIDE_LOADING = 'HIDE_LOADING'
