import * as types from './init.types'

const initialState = {
  useAccountKit: true,
  countries: ["VN","US","KH"],
  countryInfos: [{
    name: 'Việt Nam',
    id: '84'
  }, {
    name: 'United States',
    id: '1'
  }, {
    name: 'Campuchia',
    id: '855'
  }]
}

export default function init(state = initialState, action) {

  switch (action.type) {
    case types.GET_INIT_CONFIG_SUCCESS:
      return {
        ...state,
        ...action.payload
      }
    case types.GET_INIT_CONFIG_FAIL:
      return { ...state, ...initialState }
    default:
      return state
  }
}

