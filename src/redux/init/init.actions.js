import * as types from './init.types'

export const initApplication = (accessToken) => {
  return {
    type: types.GET_INIT_APPLICATION,
    payload: { accessToken }
  }
}

export const getInitConfig = () => {
  return {
    type: types.GET_INIT_CONFIG
  }
}
