import React, { Component } from 'react'
import { Alert, Image, View, ScrollView, TouchableOpacity, Dimensions, Linking, Platform } from 'react-native'
import Carousel from 'react-native-snap-carousel'
import moment from 'moment'
import _ from 'lodash'
import numeral from 'numeral'
import { Button, LogoNav, TextFont, Themes, Loading, StatusBar } from '../../ui'
import Config from '../../screens/config'
import styles from './home.style'
import { helper } from '../../utils'
import Countdown from '../../utils/countDown'
import store from '../../configs/store.config'
import { VERSION } from '../../configs/version.config'
import { PURCHASE_WAP } from '../../configs/api.config'

const { width } = Dimensions.get('window')

class Home extends Component {

  constructor(props) {
    super(props)
  }

  getResource(ticket_type) {
    switch (ticket_type) {
      case '1':
        return {
          logo: Themes.Images.logo655,
          style: styles.higher655
        }
      case '2':
        return {
          logo: Themes.Images.logo645,
          style: styles.higherRowRed
        }
      case '4':
        return {
          logo: Themes.Images.logoMax4d,
          style: styles.higherRowPurple
        }
      default:
        return ''
    }
  }

  onPressBuyTicket(ticket_type) {
    const { accountInfo } = store.getState()
    if (Platform.OS === 'ios' && accountInfo.production_version === VERSION) {
      const purchaseUrl = `${PURCHASE_WAP}?token=${accountInfo.accessToken}`
      Linking.canOpenURL(purchaseUrl).then(() => {
        Linking.openURL(purchaseUrl)
      }).catch(err => console.warn('An unexpected error happened', err))
      return null
    }

    switch (ticket_type) {
      case '1':
        this.props.onPressMegaPower()
        break
      case '2':
        this.props.onPressMega()
        break
      case '4':
        this.props.onPressMax4d()
        break
      default:
        return ''
    }
    return null
  }
  
  renderBuyButton({ ticket_type, time_off_service }) {
    const { start, end } = time_off_service

    const timeStart = moment(start, 'HH:mm').format()
    const timeEnd = moment(end, 'HH:mm').format()
    const timeOffService = moment().isBetween(timeStart, timeEnd)


    if ((parseInt(start, 10) === -1 && parseInt(end, 10) === -1) || timeOffService === false) {
      return (
        <Button
          onPress={() => { this.onPressBuyTicket(ticket_type) }} style={styles.actionButton} btnColor="#fdb200"
        >MUA VÉ NGAY</Button>
      )
    }

    return <Button style={styles.actionButton} btnColor="#fdb200">TẠM NGƯNG BÁN VÉ</Button>
  }

  renderGame() {
    const { ticketPrizeInfo } = this.props

    return _.map(ticketPrizeInfo, (item, key) => {
      const { ticket_type, next_drawing, prize, enable } = item
      if (parseInt(enable, 10) !== 1) {
        return null
      }
      
      const options = { endDate: moment(next_drawing, 'HH:mm DD-MM-YYYY').format('MM-DD-YYYY HH:mm'), prefix: '', cb: () => {} }
      const resource = this.getResource(ticket_type)

      return (
        <View key={_.uniqueId()} style={styles.roundBox}>
          <View style={[styles.rowBox, resource.style]}>
            <View style={styles.leftCol}>
              <Image source={resource.logo} style={styles.logoStyle} />
              <Countdown options={options} />
            </View>
            <View style={[styles.rightCol]}>
              <TextFont style={styles.jackpotPrice}>{numeral(prize).format('0,0')}
                <TextFont style={styles.currency}> đ</TextFont>
              </TextFont>
              {this.renderBuyButton(item)}
            </View>
          </View>
          <View style={[styles.rowBox, styles.bottomRow]}>
            <View style={[styles.footerText, { alignItems: 'flex-start' }]}>
              <TextFont
                onPress={() => {
                  this.props.navigator.push({
                    ...Config.screen.settingHomeIntro,
                    title: 'Giới thiệu cách chơi',
                    passProps: { uri: 'https://docdac.vn/gioi-thieu.html' }
                  })
                }}
                style={styles.footerButton}
              >
              Hướng dẫn chơi
            </TextFont>
            </View>
            <View style={[styles.footerText, { alignItems: 'flex-end' }]}>
              <TextFont
                onPress={() => {
                  this.props.onPressLotteryResult(parseInt(key, 10))
                }}
                style={styles.footerButton}
              >
              Các kỳ quay số
            </TextFont>
            </View>
          </View>
        </View>
      )
    })
  }

  render() {
    if (this.props.isFetching === true) {
      return (<Loading />)
    }
    return (
      <View style={styles.container}>
        <StatusBar barStyle={'light-content'} backgroundColor={'rgba(0,0,0,0)'} translucent />
        <LogoNav imageSouce={Themes.Images.docdacLogo} />
        <ScrollView>
          {_.isNil(this.props.bannerList) || this.props.bannerList.length === 0 ? <TouchableOpacity
            activeOpacity={1}
            focusedOpacity={1}
            onPress={() => {
              this.props.navigator.push({
                ...Config.screen.settingHomeIntro,
                title: 'Giới thiệu cách chơi'
                // passProps: { uri: 'https://docdac.vn/gioi-thieu.html' }
              })
            }}
            style={{ flex: 1 }}
          >
            <Image
              source={Themes.Images.homeBanner}
              style={styles.headerBanner}
            />
          </TouchableOpacity> : <Carousel
            sliderWidth={width}
            itemWidth={width}
            inactiveSlideScale={1}
            autoplay
          >
            {_.map(this.props.bannerList, (data, index) => {
              return (
                <View
                  key={index}
                  style={styles.headerBanner}
                >
                  <TouchableOpacity
                    style={styles.headerBanner}
                    activeOpacity={1}
                    focusedOpacity={1}
                    onPress={() => {
                      if (_.startsWith(data.deeplink, 'http')) {
                        Linking.openURL(data.deeplink)
                        { /* this.props.navigator.push({
                          ...Config.screen.settingInfo,
                          title: data.description,
                          passProps: { uri: data.deeplink }
                        }) */ }
                      } else {
                        const page = helper.getParameterByName('page', data.deeplink)
                        this.props.navigator.push({
                          ...Config.screen[page]
                        })
                      }
                    }}
                  >
                    <Image
                      source={{ uri: this.props.mediaUrl + data.image_url }}
                      style={[styles.headerBanner, { resizeMode: 'contain' }]}
                    />
                  </TouchableOpacity>
                </View>

              )
            })}
          </Carousel>
          }
          <View style={styles.contentBox}>
            {this.renderGame()}
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default Home
