import { PixelRatio, StyleSheet, Dimensions } from 'react-native'
const { height, width } = Dimensions.get('window')
import { Metrics, Fonts } from '../../ui/themes'

export default StyleSheet.create({
  container: {
    flex: 1,
    width: '100%'
  },
  headerBanner: {
    width,
    height: width / 2.142857142857143
  },
  contentBox: {
    paddingHorizontal: Metrics.paddingHorizontal,
    paddingVertical: Metrics.paddingVertical,
    flex: 1
  },
  roundBox: {
    borderRadius: 10,
    width: '100%',
    backgroundColor: 'red',
    overflow: 'hidden',
    marginBottom: 15
  },
  higherRowRed: {
    backgroundColor: '#d02d47',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  higherRowPurple: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: '#9607a1',
    padding: 10
  },
  higher655: {
    backgroundColor: '#9b3515',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  rowBox: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  leftCol: {
    flexDirection: 'column',
    width: '35%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 90 * Metrics.ratioScreen,
    borderRightColor: '#ad2339',
    borderRightWidth: 1
  },
  rightCol: {
    flex: 1,
    alignItems: 'flex-end',
    paddingLeft: 10
  },
  logoStyle: {
    width: 95,
    height: 45,
    resizeMode: 'contain'
  },
  countDownTimer: {
    position: 'absolute',
    bottom: 0,
    color: '#fff'
  },
  jackpotPrice: {
    color: '#fff',
    fontSize: 24 * Metrics.ratioScreen,
    fontFamily: Fonts.type.UTMHel
  },
  currency: {
    fontSize: 10,
    color: '#fff',
    fontFamily: Fonts.type.UTMHel
  },
  footerText: {
    height: 40 * Metrics.ratioScreen,
    justifyContent: 'center',
    flex: 1,
    padding: 10
  },
  footerButton: {
    color: '#fff',
    fontSize: 14
  },
  bottomRow: {
    backgroundColor: '#464545',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  actionButton: {
    width: '100%',
    padding: '6%',
    marginTop: 10
  }

})
