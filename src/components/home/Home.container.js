import React, { Component } from 'react'
import { Alert, Linking } from 'react-native'
import OneSignal from 'react-native-onesignal'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import DeviceInfo from 'react-native-device-info'
import semver from 'semver'
import Home from './Home'
import Config from '../../screens/config'
import { Colors } from '../../ui/themes'
import { actions as AccountAction } from '../../redux/account'
import { actions as PaymentAction } from '../../redux/payment'
import { types as typesInit, actions as InitAction } from '../../redux/init'
import { actions as LotteryAction } from '../../redux/lottery/'

class HomeContainer extends Component {

  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE,
    drawUnderNavBar: true,
    navBarHidden: true,
    statusBarColor: Colors.navColor

  }

  constructor(props) {
    super(props)
    this.state = {
      isFetching: true
    }
    this.onPressMega = this.onPressMega.bind(this)
    this.onPressMax4d = this.onPressMax4d.bind(this)
    this.onPressMegaPower = this.onPressMegaPower.bind(this)
    this.onPressLotteryResult = this.onPressLotteryResult.bind(this)
  }

  componentWillMount() {
    const { accountAction, lotteryAction, initAction, payload } = this.props
    const accountInfo = payload.accountInfo
    lotteryAction.getBannerList()

    OneSignal.sendTags({ accountId: accountInfo.accountId, phone: accountInfo.phone })

    if (accountInfo.accessToken) {
      initAction.initApplication(accountAction.accessToken)
      lotteryAction.getListDeliveryAddress()
    }
  }

  componentWillReceiveProps(props) {
    const { apiResponse } = props.payload
    if (apiResponse.type === typesInit.GET_INIT_APPLICATION_SUCCESS) {
      this.setState({ isFetching: false })
    }
  }

  onPressMega() {
    this.props.navigator.push(Config.screen.megaPickup)
  }

  onPressMegaPower() {
    this.props.navigator.push(Config.screen.megaPowerPickup)
  }

  onPressMax4d() {
    this.props.navigator.push(Config.screen.megaPickupMax)
  }

  onPressLotteryResult(page) {
    const { lotteryAction } = this.props
    lotteryAction.gotoResult(page)
    this.props.navigator.switchToTab({
      tabIndex: 1,
      screen: 'LotteryResult',
      title: 'Kết quả xổ số',
      backButtonTitle: ''
    })
  }

  render() {
    const { lottery, apiResponse, accountInfo } = this.props.payload
    return (
      <Home
        onPressMega={this.onPressMega}
        onPressMegaPower={this.onPressMegaPower}
        onPressLotteryResult={this.onPressLotteryResult}
        ticketPrizeInfo={lottery.ticketPrizeInfo}
        isFetching={this.state.isFetching}
        apiResponse={apiResponse}
        onPressMax4d={this.onPressMax4d}
        navigator={this.props.navigator}
        bannerList={lottery.bannerList}
        mediaUrl={accountInfo.media_url}
      />
    )
  }

  componentDidMount() {
    const { accountInfo } = this.props.payload
    const { version_info } = accountInfo
    if (semver.lt(DeviceInfo.getVersion(), version_info.version_name)) {
      if (parseInt(version_info.force_update, 10) === 1) {
        Alert.alert('Thông báo cập nhật', `Độc đắc đã có bản mới ${version_info.version_name}`, [
          { text: 'Cập nhật ngay', onPress: () => Linking.openURL(version_info.link).catch(err => console.error('An error occurred', err)) }
        ],
        { cancelable: false })
      } else {
        Alert.alert('Thông báo cập nhật', `Độc đắc đã có bản mới ${version_info.version_name}`, [
          { text: 'Cập nhật ngay', onPress: () => Linking.openURL(version_info.link).catch(err => console.error('An error occurred', err)) },
          { text: 'Để sau', onPress: () => console.log('Cancel Pressed'), style: 'cancel' }
        ])
      }
    }
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch),
    paymentAction: bindActionCreators(PaymentAction, dispatch),
    lotteryAction: bindActionCreators(LotteryAction, dispatch),
    initAction: bindActionCreators(InitAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer)
