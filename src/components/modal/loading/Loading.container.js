import React, { Component } from 'react'
import { View, ActivityIndicator } from 'react-native'
import styles from './loading.styles'
import { hideKeyboard } from '../../../ui'

export default class LightBoxScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    navBarTranslucent: true,
    screenBackgroundColor: 'transparent',
    modalPresentationStyle: 'overCurrentContext'
  }

  constructor(props) {
    super(props)
  }

  componentWillMount() {
    hideKeyboard()
  }

  render() {
    return (
      <View style={styles.screen}>
        <ActivityIndicator
          size={'large'}
        />
      </View>
    )
  }
}
