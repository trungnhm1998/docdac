import { PixelRatio, StyleSheet, Dimensions, Platform } from 'react-native'
import { Metrics, Colors } from '../../../ui/themes'

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  container: {
    backgroundColor: 'white',
    width: Platform.OS === 'android' ? '85%' : '100%',
  },
  title: {
    paddingTop: 15,
    paddingBottom: 15,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderWidth: Metrics.borderWidth,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#CCC',
  },
  titleText: {
    fontSize: 15,
    fontWeight: '600',
    color: Colors.navColor
  },
  message: {
    padding: 20,
    paddingTop: 15,
    paddingBottom: 30,
    width: Metrics.screenWidth * 0.85,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textMessage: {
    textAlign: 'center',
  },
  button: {
    height: 38,
    backgroundColor: Metrics.buttonBackgroundColor,
    flexDirection: 'row',
  },
  buttonItem: {
    flex: 1,
    backgroundColor: Colors.navColor,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ok: {
    backgroundColor: Colors.navColor
  },
  cancel: {
    backgroundColor: Colors.black
  },
  buttonText: {
    color: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default styles
