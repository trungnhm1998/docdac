import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Dimensions } from 'react-native'
import _ from 'lodash'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as ApiAction from '../../../redux/api/api.actions'
import { actions as AccountAction } from '../../../redux/account'
import { Navigation } from '../../../screens/navigation'
import styles from './lightbox.styles'
import { hideKeyboard } from '../../../ui'

const ButtonMessage = (props) => {
  const { item, hideMessage } = props
  let text = 'ĐÓNG'
  let handleClick = hideMessage
  let style = 'ok'

  if (item) {
    text = item.text
    handleClick = () => {
      hideMessage()
      item.onPress()
    }
    style = item.style || 'ok'
  }

  return (
    <TouchableOpacity
      onPress={handleClick}
      style={[styles.buttonItem, styles[style]]}
    >
      <Text style={styles.buttonText}>{text}</Text>
    </TouchableOpacity>
  )
}

class LightBoxScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    navBarTranslucent: true,
    screenBackgroundColor: 'transparent',
    modalPresentationStyle: 'overCurrentContext'
  }

  constructor(props) {
    super(props)
    this.hideMessage = this.hideMessage.bind(this)
    this.navToLogin = this.navToLogin.bind(this)
  }

  componentWillMount() {
    hideKeyboard()
  }

  navToLogin() {
    Navigation.navToLogin()
  }

  hideMessage() {
    this.props.navigator.dismissLightBox()
    const { options } = this.props
    if (options && options.isLogout === true) {
      const { accountAction } = this.props
      accountAction.logout({
        nextScreen: this.navToLogin
      })
    }
    setTimeout(() => {
      this.props.apiAction.clearResponse()
    }, 250)
  }

  render() {
    const { message, buttons } = this.props
    return (
      <View style={styles.screen}>
        <View style={styles.container}>
          <View style={styles.title}>
            <Text style={styles.titleText}>THÔNG BÁO</Text>
          </View>
          <View style={styles.message}>
            <Text style={styles.textMessage}>{message}</Text>
          </View>
          <View style={styles.button}>
            {
              buttons
                ? _.map(buttons, (item) => {
                  return <ButtonMessage key={_.uniqueId()} item={item} hideMessage={this.hideMessage} />
                })
                : <ButtonMessage hideMessage={this.hideMessage} />
            }
          </View>
        </View>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    apiAction: bindActionCreators(ApiAction, dispatch),
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(LightBoxScreen)
