import { StyleSheet, Dimensions, Platform } from 'react-native'
import { Metrics, Colors } from '../../../ui/themes'

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    width: Platform.OS === 'ios' ? width : null,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  scrollView: {
    width,
  },
  modalContainer: {
    width: '90%',
    height: '80%',
    padding: 10,
    borderRadius: 10,
    backgroundColor: '#fff',
  },
  modalContainerNewAddress: {
    width: '90%',
    padding: 10,
    borderRadius: 10,
    backgroundColor: '#fff',
    marginTop: 50,
  },
  header: {
    height: 45,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',

  },
  buttonHeader: {
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  contentContainer: {
    borderStyle: 'solid',
    borderTopColor: '#eee',
    borderTopWidth: 1,
    flex: 1
  },
  label: {
    marginBottom: 10
  },

  ratioItem: {
    width: 20,
    height: 20,
    borderColor: '#c92033',
    borderWidth: 1,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10
  },
  selectRatio: {
    width: 8,
    height: 8,
    borderRadius: 4
  },
  selectedRatio: {
    backgroundColor: '#c92033'
  },
  ratioItemList: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 5,
    paddingRight: 10
  },
  ratioTextLabel: {
    marginBottom: 5,
    marginTop: 0,
    flex: 1
  },
  selectLocation: {
    flexDirection: 'row',
    marginTop: 10
  },
  buyerName: {
    fontSize: 15,
    marginBottom: 2
  },
  address: {
    fontSize: 12
  }

})

export default styles
