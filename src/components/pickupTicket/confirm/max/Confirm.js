import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Alert, Text, View, ScrollView, StatusBar } from 'react-native'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import styles from './max.styles'
import Config from '../../../../screens/config'
import { Colors } from '../../../../ui/themes'
import { types, actions as LotteryAction } from '../../../../redux/lottery'
import { actions as ApiAction } from '../../../../redux/api'
import { DeliveryType, TicketType, CurrencyType } from '../../../../configs/constant.config'
import { Button, TextFont } from '../../../../ui'

const numeral = require('numeral')

class ConfirmMax4d extends Component {

  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB,
    statusBarColor: Colors.navColor
  }

  constructor(props) {
    super(props)
    this.onPressBack = this.onPressBack.bind(this)
    this.onPressSubmit = this.onPressSubmit.bind(this)
  }

  onPressBack() {
    this.props.navigator.pop()
  }

  onPressSubmit() {
    const { lotteryAction, filledPrice, consecutiveBuy, easyPick, filledNumber, boardType, address_id, payload, agentId } = this.props
    const { accountInfo } = payload
    const listTicket = []
    _.forEach(filledNumber, (item, index) => {
      if (_.isEmpty(item) === false && _.isNull(item[0]) === false) {
        listTicket.push({
          easy_pick: easyPick[index],
          ticket_number: item.join(),
          board_price: CurrencyType[filledPrice[index]]
        })
      }
    })

    lotteryAction.buyMaxTicket({
      delivery_type: DeliveryType[boardType],
      address_id,
      agent_user_id: agentId,
      ticket_data: {
        ticket_type: 4,
        period_no: consecutiveBuy,
        list_ticket: listTicket
      }
    }, accountInfo.accessToken)
  }

  render() {
    const { consecutiveBuy, filledNumber, boardType, totalPrize, filledPrice, ticketPrizeInfo } = this.props
    const filled = _.countBy(filledNumber, (item) => {
      return _.isNil(item[0])
    })
    const nullItem = _.isNil(filled[0]) ? 0 : filled[0]
    let ticketPrize = totalPrize
    let deliveryPrize = 0
    if (boardType !== 'hold') {
      deliveryPrize = ticketPrizeInfo.delivery_fee
      ticketPrize = totalPrize - deliveryPrize
    }
    return (
      <ScrollView style={[styles.container]}>
        <StatusBar translucent={false} />
        <View style={styles.header}>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Trò chơi</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#c92033'} style={{ textAlign: 'right', flex: 1 }}>Max4D</TextFont>
          </View>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Số vé</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>{filled.false} vé</TextFont>
          </View>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Số kỳ tham gia</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>{consecutiveBuy}</TextFont>
          </View>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Loại vé</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>{TicketType[boardType]}</TextFont>
          </View>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Tiền vé</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>{numeral(ticketPrize).format('0,0')} đ</TextFont>
          </View>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Phí giao vé</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>{numeral(deliveryPrize).format('0,0')} đ</TextFont>
          </View>
        </View>

        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
          <TextFont color={'#6f6f6f'} font={'Bold'}>Dãy số mua vé</TextFont>
        </View>
        <View style={styles.contentContainer}>
          {_.map(filledNumber, (data, index) => {
            return (
              _.isNil(_.countBy(data, number => number).null) ? <View key={index} class="board" style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={[styles.boardNum, { borderWidth: 0 }]}>
                  <Text
                    style={{ color: '#6f6f6f' }}
                  >
                    #{index}
                  </Text>
                </View>
                <View style={styles.rowCover}>
                  <View style={styles.numberCover}>
                    {
                      _.map(data, (item, indexItem) => {
                        return (
                          <View key={indexItem} style={[styles.boardNum, { borderColor: '#C92033', marginRight: 8 }]}>
                            <Text style={styles.numberStyle}>{item}</Text>
                          </View>
                        )
                      })
                    }
                  </View>
                </View>
                <TextFont style={{ padding: 5, marginTop: 10, color: '#c92033', width: 60, textAlign: 'center' }}>{numeral(filledPrice[index]).format('0a')}</TextFont>
              </View>
                : null
            )
          })}

          <View style={styles.borderBottom} />
          <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <View style={{ flex: 1 }}>
              <TextFont color={'#8f8f8f'}>Tổng số tiền thanh toán:</TextFont>
            </View>
            <View>
              <TextFont font={'Bold'} color={'#c92033'}>{numeral(totalPrize).format('0,0')} đ</TextFont>
            </View>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <View style={{ flex: 1, padding: 10 }}>
              <Button style={[styles.roundButton, { backgroundColor: '#fff' }]} onPress={this.onPressBack}><TextFont font={'Bold'} color={'#c93022'}>CHỌN LẠI</TextFont></Button>
            </View>
            <View style={{ flex: 1, padding: 10 }}>
              <Button style={[styles.roundButton, { backgroundColor: '#c92033' }]} onPress={this.onPressSubmit}><TextFont font={'Bold'} color={'#fff'}>MUA VÉ</TextFont></Button>
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }

  componentDidUpdate() {
    const { apiResponse } = this.props.payload
    if (apiResponse.type === types.BUY_MEGA_TICKET_SUCCESS) {
      Alert.alert('Thông báo', 'Mua vé thành công.', [{
        text: 'Đồng ý',
        onPress: () => {
          this.props.navigator.popToRoot()
        }
      }])
    } else if (apiResponse.type === types.BUY_MEGA_TICKET_NOT_ENOUGH_MONEY) {
      Alert.alert('Thông báo', apiResponse.message, [{
        text: 'Đồng ý',
        onPress: () => {
          this.props.navigator.popToRoot()
        }
      }])
    }
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    lotteryAction: bindActionCreators(LotteryAction, dispatch),
    apiAction: bindActionCreators(ApiAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmMax4d)
