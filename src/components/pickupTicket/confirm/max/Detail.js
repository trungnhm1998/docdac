import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, StatusBar } from 'react-native'
import _ from 'lodash'
import numeral from 'numeral'

import styles from './max.styles'
import Config from '../../../../screens/config'
import { FieldSet, Icon, PickupTicketNav, SelectButton, TextFont, ThreeDButton, Button } from '../../../../ui'

class DetailMax4d extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  render() {
    const { consecutiveBuy, totalPrize, filledNumber, boardType } = this.props.ticketDetail
    const { ticketPrizeInfo } = this.props
    const filled = _.countBy(filledNumber, (item) => {
      return _.isNil(item[0])
    })

    let deliveryPrize = 0
    let ticketPrize = totalPrize
    if (boardType !== 'hold') {
      deliveryPrize = ticketPrizeInfo.delivery_fee
      ticketPrize = totalPrize - deliveryPrize
    }

    return (
      <View style={styles.container}>
        <StatusBar translucent={false} />
        <View style={styles.header}>
          <View style={styles.row}>

            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont font={'Bold'} color={'#656565'} style={{ flex: 1 }}>Số vé</TextFont>
              <TextFont color={'#656565'}>{filled.false} vé</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>{numeral(ticketPrize / consecutiveBuy).format('0,0')} đ</TextFont>
          </View>
          <View style={styles.row}>

            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont font={'Bold'} color={'#656565'} style={{ flex: 1 }}>Số kỳ</TextFont>
              <TextFont color={'#656565'}>{consecutiveBuy} kỳ</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>x {consecutiveBuy}</TextFont>
          </View>
        </View>

        <View style={styles.contentContainer}>
          <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <View style={{ width: 155 }}>
              <TextFont font={'Bold'} style={{ textAlign: 'right' }} color={'#8f8f8f'}>Tiền vé</TextFont>
            </View>
            <View style={{ flex: 1 }}>
              <TextFont style={{ textAlign: 'right' }} color={'#c92033'}>{numeral(ticketPrize).format('0,0')} đ</TextFont>
            </View>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <View style={{ width: 155 }}>
              <TextFont font={'Bold'} style={{ textAlign: 'right' }} color={'#8f8f8f'}>Phí giao vé</TextFont>
            </View>
            <View style={{ flex: 1 }}>
              <TextFont style={{ textAlign: 'right' }} color={'#c92033'}>{numeral(deliveryPrize).format('0,0')} đ</TextFont>
            </View>
          </View>
          <View style={styles.borderBottom} />
          <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <View style={{ width: 155 }}>
              <TextFont font={'Bold'} style={{ textAlign: 'right' }} color={'#8f8f8f'}>TỔNG THANH TOÁN</TextFont>
            </View>
            <View style={{ flex: 1 }}>
              <TextFont font={'Bold'} style={{ textAlign: 'right' }} color={'#c92033'}>{numeral(totalPrize).format('0,0')} đ</TextFont>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // lotteryAction: bindActionCreators(LotteryAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailMax4d)
