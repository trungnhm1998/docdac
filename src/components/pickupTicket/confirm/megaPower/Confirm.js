import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Alert, Text, View, ScrollView, StatusBar } from 'react-native'
import _ from 'lodash'
import numeral from 'numeral'
import Config from '../../../../screens/config'
import { Colors } from '../../../../ui/themes'
import { Button, TextFont } from '../../../../ui'
import { types, actions as LotteryAction } from '../../../../redux/lottery'
import { actions as ApiAction } from '../../../../redux/api'
import { DeliveryType, TicketType } from '../../../../configs/constant.config'
import styles from './max.styles'

class ConfirmMegaPower extends Component {

  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB,
    statusBarColor: Colors.navColor
  }

  constructor(props) {
    super(props)
    this.onPressBack = this.onPressBack.bind(this)
    this.onPressSubmit = this.onPressSubmit.bind(this)
  }

  onPressBack() {
    this.props.navigator.pop()
  }

  onPressSubmit() {
    const { lotteryAction, inclusive, consecutiveBuy, filledNumber, easyPick, boardType, address_id, payload, agentId } = this.props

    const { accountInfo } = payload
    const listTicket = []
    _.forEach(filledNumber, (item, index) => {
      if (_.isEmpty(item) === false) {
        listTicket.push({
          easy_pick: easyPick[index],
          ticket_number: item.join()
        })
      }
    })

    lotteryAction.buyMegaPowerTicket({
      delivery_type: DeliveryType[boardType],
      agent_user_id: agentId,
      address_id,
      ticket_data: {
        ticket_type: 1,
        bao: inclusive === 0 ? 1 : inclusive,
        period_no: consecutiveBuy,
        list_ticket: listTicket
      }
    }, accountInfo.accessToken)
  }

  render() {
    const { inclusive, consecutiveBuy, filledNumber, boardType, totalPrize, ticketPrizeInfo } = this.props
    const filled = _.countBy(filledNumber, (item) => {
      return item.length
    })
    const nullItem = _.isNil(filled[0]) ? 0 : filled[0]
    let ticketPrize = totalPrize
    let deliveryPrize = 0
    if (boardType !== 'hold') {
      deliveryPrize = ticketPrizeInfo.delivery_fee
      ticketPrize = totalPrize - deliveryPrize
    }

    return (
      <ScrollView style={[styles.container]}>
        <StatusBar translucent={false} />
        <View style={styles.header}>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Trò chơi</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#c92033'} style={{ textAlign: 'right', flex: 1 }}>Power 6/55</TextFont>
          </View>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Hình thức chơi</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>{inclusive === 0 ? 'Vé số tự chọn' : `Bao ${inclusive} số`}</TextFont>
          </View>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Số vé</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>{6 - nullItem} vé</TextFont>
          </View>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Số kỳ tham gia</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>{consecutiveBuy}</TextFont>
          </View>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Loại vé</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>{TicketType[boardType]}</TextFont>
          </View>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Tiền vé</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>{numeral(ticketPrize).format('0,0')} đ</TextFont>
          </View>
          <View style={styles.row}>
            <View style={styles.circleLine} />
            <View style={{ width: 150, paddingLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
              <TextFont color={'#656565'} style={{ flex: 1 }}>Phí giao vé</TextFont>
              <TextFont color={'#656565'}>:</TextFont>
            </View>
            <TextFont color={'#656565'} style={{ flex: 1, textAlign: 'right' }}>{numeral(deliveryPrize).format('0,0')} đ</TextFont>
          </View>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
          <TextFont color={'#6f6f6f'} font={'Bold'}>Dãy số mua vé</TextFont>
        </View>
        <View style={styles.contentContainer}>
          {
            _.map(filledNumber, (data, index) => {
              return (
                data.length > 0 ? <View key={index} class="board" style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                  <View style={[styles.boardNum, { borderWidth: 0 }]}>
                    <Text
                      style={{ color: '#6f6f6f' }}
                    >
                      #{index}
                    </Text>
                  </View>
                  <View style={styles.rowCover}>
                    <View style={[styles.numberCover, { justifyContent: 'flex-start' }]}>
                      {
                        _.map(data, (item, indexItem) => {
                          return (
                            <View key={indexItem} style={[styles.boardNum, { borderColor: '#C92033', marginRight: 8 }]}>
                              <Text style={styles.numberStyle}>{item}</Text>
                            </View>
                          )
                        })
                      }
                    </View>
                  </View>

                </View>
                  : null
              )
            })
          }
          <View style={styles.borderBottom} />
          <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <View style={{ flex: 1 }}>
              <TextFont color={'#8f8f8f'}>Tổng số tiền thanh toán:</TextFont>
            </View>
            <View>
              <TextFont font={'Bold'} color={'#c92033'}>{numeral(totalPrize).format('0,0')} đ</TextFont>
            </View>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <View style={{ flex: 1, padding: 10 }}>
              <Button style={[styles.roundButton, { backgroundColor: '#fff' }]} onPress={this.onPressBack}><TextFont font={'Bold'} color={'#c93022'}>CHỌN LẠI</TextFont></Button>
            </View>
            <View style={{ flex: 1, padding: 10 }}>
              <Button style={[styles.roundButton, { backgroundColor: '#c92033' }]} onPress={this.onPressSubmit}><TextFont font={'Bold'} color={'#fff'}>MUA VÉ</TextFont></Button>
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }

  componentDidUpdate() {
    const { apiResponse } = this.props.payload
    if (apiResponse.type === types.BUY_MEGA_POWER_TICKET_SUCCESS) {
      Alert.alert('Thông báo', 'Mua vé thành công.', [{
        text: 'Đồng ý',
        onPress: () => {
          this.props.navigator.popToRoot()
        }
      }])
    } else if (apiResponse.type === types.BUY_MEGA_TICKET_NOT_ENOUGH_MONEY) {
      Alert.alert('Thông báo', apiResponse.message, [{
        text: 'Đồng ý',
        onPress: () => {
          this.props.navigator.popToRoot()
        }
      }])
    }
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    lotteryAction: bindActionCreators(LotteryAction, dispatch),
    apiAction: bindActionCreators(ApiAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmMegaPower)
