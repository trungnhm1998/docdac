import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import PickupTicket from './PickupTicket'
import Config from '../../../screens/config'
import { types, actions as LotteryAction } from '../../../redux/lottery'

class PickupTicketContainer extends Component {
  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB,
    drawUnderNavBar: true,
    statusBarColor: '#c92033',
    navBarHidden: true
  }

  constructor(props) {
    super(props)
    this.showAddAddress = this.showAddAddress.bind(this)
    this.onBuyTicket = this.onBuyTicket.bind(this)
    this.getAgentList = this.getAgentList.bind(this)
  }

  componentWillReceiveProps(props) {
    const { apiResponse } = props.payload
    if (apiResponse.type === types.GET_USER_ADDRESS_SUCCESS) {
      this.showAddAddress()
    }
  }

  showAddAddress() {
    const { userAddressList } = this.props.payload.lottery
    this.props.navigator.showModal({
      screen: 'AddressList',
      passProps: { userAddressList },
      style: {
        backgroundBlur: 'none',
        backgroundColor: 'rgba(0,0,0,0.5)'
      }
    })
  }

  getAgentList(districtId) {
    const { lotteryAction } = this.props
    lotteryAction.getAgentList({ districtId })
  }

  onBuyTicket(value) {
    const { filledNumber, easyPick, consecutiveBuy, boardType, inclusive, totalPrize, address_id, agentId, ticketPrizeInfo } = value
    this.props.navigator.push({
      ...Config.screen.confirmMega,
      passProps: {
        address_id,
        inclusive,
        consecutiveBuy,
        filledNumber,
        easyPick,
        boardType,
        totalPrize,
        agentId,
        ticketPrizeInfo
      }
    }
    )
  }

  render() {
    const { lottery, accountInfo } = this.props.payload
    const filledNumber = this.props.filledNumber ? Object.assign({}, this.props.filledNumber) : undefined
    const ticketPrizeInfo = _.find(lottery.ticketPrizeInfo, item => parseInt(item.ticket_type, 10) === 2)

    return (
      <PickupTicket
        navigator={this.props.navigator}
        ticketPrizeInfo={ticketPrizeInfo}
        deliveryAddressList={accountInfo.deliveryAddressList}
        deliveryLocation={lottery.deliveryLocation}
        onGetAddressList={this.onGetAddressList}
        onBuyTicket={this.onBuyTicket}
        getAgentList={this.getAgentList}
        agentList={lottery.agentList}
        inclusive={this.props.bao}
        filledNumber={filledNumber}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    lotteryAction: bindActionCreators(LotteryAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PickupTicketContainer)
