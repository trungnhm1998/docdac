import React, { Component } from 'react'
import { Alert, Modal, ScrollView, TouchableOpacity, View, StatusBar, Platform } from 'react-native'
import _ from 'lodash'
import Picker from 'react-native-picker'
import Sound from 'react-native-sound'
import numeral from 'numeral'
import { FieldSet, Icon, Overlay, PickupTicketNav, SelectButton, TextFont, ThreeDButton } from '../../../ui'
import Board from './Board'
import Config from '../../../screens/config'
import { Images } from '../../../ui/themes'
import { Carousel } from '../../../ui/carousel'
import PickerBoard from './pickerBoard'
import styles from './pickupTicket.style'

const whoosh = new Sound('card.mp3', Sound.MAIN_BUNDLE)
const trashSound = new Sound('trash.mp3', Sound.MAIN_BUNDLE)

let totalPrize = 0
const minimumPrice = 20000
export default class PickupTicket extends Component {

  constructor(props) {
    super(props)
    this.state = {
      buyType: 'Tự chọn',
      filledNumber: _.cloneDeep(this.props.filledNumber),
      filledPrice: _.cloneDeep(this.props.filledPrice),
      easyPick: {
        A: false,
        B: false,
        C: false,
        D: false,
        E: false,
        F: false
      },
      filled: {},
      currentIndexBoard: 0,
      consecutiveBuy: 1,
      boardType: 'hold',
      buyLocation: null,
      storeName: null,
      isSelectingStore: false,
      modalVisible: false,
      initialBoard: 0,
      pickerShow: false,
      visibleOverlay: false,
      selectedAddress: null,
      agentName: null,
      agentId: null
    }
    this.inputBoardNumber = this.inputBoardNumber.bind(this)
    this.showAddAddress = this.showAddAddress.bind(this)
    this.onPressSelectedAddress = this.onPressSelectedAddress.bind(this)
    this.changePrice = this.changePrice.bind(this)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.showAgent = this.showAgent.bind(this)
    this.onCloseModalAddress = this.onCloseModalAddress.bind(this)
  }

  componentWillMount() {
    totalPrize = 0
  }

  componentDidMount() {
    this.setState({
      totalPrize: 0
    })
  }

  componentWillReceiveProps(nextProps) {
    if (_.isNil(nextProps.agentList)) {
      this.setState({
        agentName: null
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { consecutiveBuy } = nextState
    const { ticketPrizeInfo } = nextProps
    totalPrize = (this.getBoardPrice('A', nextState)
      + this.getBoardPrice('B', nextState)
      + this.getBoardPrice('C', nextState)
      + this.getBoardPrice('D', nextState)
      + this.getBoardPrice('E', nextState)
      + this.getBoardPrice('F', nextState)) * consecutiveBuy
    if (nextState.boardType !== 'hold') {
      totalPrize += parseInt(ticketPrizeInfo.delivery_fee, 10)
    }
    return true
  }

  onPressSubmit() {
    if (totalPrize > 0) {
      const { filledNumber, consecutiveBuy, easyPick, filledPrice, boardType, selectedAddress, agentId } = this.state
      const { ticketPrizeInfo } = this.props
      this.props.onBuyTicket({
        consecutiveBuy,
        easyPick,
        filledNumber,
        filledPrice,
        boardType,
        totalPrize,
        address_id: selectedAddress ? selectedAddress.address_id : null,
        agentId,
        ticketPrizeInfo
      })
    } else {
      Alert.alert('Thông báo', 'Vui lòng chọn vé trước khi thanh toán')
    }
  }

  isBoardNull(boardName, nextState) {
    const { filledNumber } = nextState
    const isFilled = _.countBy(filledNumber[boardName])
    return isFilled.null > 1
  }

  getBoardPrice(boardName, nextState) {
    const { filledPrice } = nextState
    if (this.isBoardNull(boardName, nextState) === false) {
      return filledPrice[boardName]
    }
    return 0
  }

  changePrice(boardKey) {
    const rawData = [
      // 10000,
      20000,
      50000,
      100000,
      200000,
      500000,
      1000000
    ]
    const data = [
      // '10,000 đ',
      '20,000 đ',
      '50,000 đ',
      '100,000 đ',
      '200,000 đ',
      '500,000 đ',
      '1,000,000 đ'
    ]
    this.setState({ visibleOverlay: true })
    Picker.init({
      pickerData: data,
      selectedValue: ['20,000 đ'],
      pickerTitleText: '',
      pickerToolBarBg: [127, 7, 145, 1],
      pickerBg: [255, 255, 255, 20],
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Hủy',
      onPickerCancel: () => {
        this.setState({
          visibleOverlay: false
        })
      },
      onPickerConfirm: (val) => {
        const indexPrice = data.indexOf(val[0])
        this.changeBoardPrice(boardKey, rawData[indexPrice])
      }
    })
    if (this.state.pickerShow === false) {
      Picker.show()
    } else {
      Picker.hide()
    }
  }

  changeBoardPrice(boardKey, price) {
    const { filledPrice } = this.state
    const newFilledPrice = Object.assign({}, filledPrice)
    newFilledPrice[boardKey] = price
    this.setState({ filledPrice: newFilledPrice, visibleOverlay: false })
  }

  showAddress() {
    const { deliveryLocation } = this.props
    const data = []
    _.forEach(deliveryLocation, (item) => {
      const provinceName = item.province_name
      const itemObj = {}
      itemObj[provinceName] = _.map(item.district, itemDistrict => `${itemDistrict.type} ${itemDistrict.name}`)
      data.push(itemObj)
    })
    const selectedItem = _.isNil(this.state.pickAddress) ? [] : [this.state.pickAddress.province, this.state.pickAddress.district]
    this.setState({ visibleOverlay: true })
    Picker.init({
      pickerData: data,
      selectedValue: selectedItem,
      pickerTitleText: '',
      pickerToolBarBg: [127, 7, 145, 1],
      pickerBg: [255, 255, 255, 20],
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Hủy',
      onPickerCancel: () => {
        this.setState({
          visibleOverlay: false
        })
      },
      onPickerConfirm: (value) => {
        const selectedProvince = _.find(deliveryLocation, (item) => {
          return item.province_name === value[0]
        })

        if (selectedProvince) {
          const selectedDistrict = _.find(selectedProvince.district, (item) => {
            return item.name === _.trim(value[1], [item.type, ' '])
          })
          this.props.getAgentList(selectedDistrict.district_id)

          this.setState({
            visibleOverlay: false,
            pickAddress: {
              ...this.state.pickAddress,
              province: value[0],
              district: value[1],
              districtId: selectedDistrict.district_id,
              provinceId: selectedProvince.province_id
            }

          })
        }
      }
    })
    Picker.show()
  }

  showAgent() {
    const error = _.isNil(this.state.pickAddress) || _.isNil(this.props.agentList) ? 'Bạn chưa chọn khu vực lấy vé hoặc không có đại lý tại nơi bạn đã chọn' : null
    if (!_.isNull(error)) {
      Alert.alert('Thông báo', error)
      return false
    }
    const { agentList } = this.props
    const data = []
    _.forEach(agentList, (item) => {
      const agentName = item.agent_name
      data.push(agentName)
    })

    const selectedItem = _.isNil(this.state.agentName) ? [] : [this.state.agentName]
    this.setState({ visibleOverlay: true })
    Picker.init({
      pickerData: data,
      selectedValue: selectedItem,
      pickerTitleText: '',
      pickerToolBarBg: [127, 7, 145, 1],
      pickerBg: [255, 255, 255, 20],
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Hủy',
      onPickerCancel: () => {
        this.setState({
          visibleOverlay: false
        })
      },
      onPickerConfirm: (value) => {
        const selectedAgent = _.find(agentList, (item) => {
          return item.agent_name === value[0]
        })
        this.setState({
          visibleOverlay: false,
          agentName: value[0],
          agentId: selectedAgent.user_id
        })
      }
    })
    Picker.show()
  }

  randomNumber() {
    const arr = []

    while (arr.length < 4) {
      const randomNumber = Math.floor(Math.random() * 10)
      let found = false
      let i = 0
      for (i; i < arr.length; i += 1) {
        if (arr[i] === randomNumber) {
          found = true
          break
        }
      }
      if (!found) arr[arr.length] = randomNumber
    }
    return _.sortBy(arr)
  }

  showNumber() {
    const { filledNumber, filledPrice, easyPick } = this.state
    if (this.state.currentIndexBoard >= 6) {
      return
    }
    whoosh.play()
    const filledNums = _.cloneDeep(filledNumber)
    const easyPickObject = _.cloneDeep(easyPick)
    const filledPriceObject = _.cloneDeep(filledPrice)

    _.forEach(Object.keys(filledNums), (value, index) => {
      const result = _.countBy(filledNums[value], item => _.isNumber(item))
      if (this.state.currentIndexBoard === index && result.true > 0) {
        this.state.currentIndexBoard += 1
      }
    })

    _.forEach(Object.keys(filledNums), (value, index) => {
      if (this.state.currentIndexBoard === index) {
        if (_.isNull(filledPriceObject[value])) {
          filledPriceObject[value] = minimumPrice
        }
        filledNums[value] = this.randomNumber()
        easyPickObject[value] = true
      }
    })
    this.setState({ filledNumber: filledNums, filledPrice: filledPriceObject, currentIndexBoard: this.state.currentIndexBoard + 1, easyPick: easyPickObject })
  }

  emptyBoard(boardKey) {
    trashSound.play()
    const filledNums = this.state.filledNumber
    const filledPrice = this.state.filledPrice
    filledPrice[boardKey] = null
    filledNums[boardKey] = [null, null, null, null]
    this.setState({ filledNums, filledPrice })
    this.setState({ currentIndexBoard: 0 })
  }

  emptyBoardPicker(boardKey) {
    const filledNums = this.state.filledNumber
    const filledPrice = this.state.filledPrice
    filledPrice[boardKey] = null
    filledNums[boardKey] = [null, null, null, null]
    this.setState({ filledNums, filledPrice })
  }

  selectBuyRepeat(number) {
    this.setState({
      consecutiveBuy: number
    })
  }

  onPressSelectedAddress(selectedAddress) {
    this.setState({ selectedAddress })
  }

  onCloseModalAddress(selectedAddress) {
    if (_.isNull(this.state.selectedAddress) && _.isNil(selectedAddress)) {
      this.setState({
        boardType: 'hold'
      })
    }
  }

  selectBoardType(type) {
    if (type === 'pickup') {
      this.setState({
        boardType: type,
        isSelectingStore: true
      })
    } else if (type === 'delivery') {
      const { deliveryAddressList, deliveryLocation } = this.props
      if (_.isEmpty(deliveryAddressList)) {
        this.props.navigator.showModal({
          screen: 'NewAddress',
          passProps: {
            deliveryLocation,
            onPressSubmit: this.onPressSelectedAddress,
            onCloseModalAddress: this.onCloseModalAddress
          }
        })
      } else {
        this.props.navigator.showModal({
          screen: 'AddressList',
          passProps: {
            userAddressList: deliveryAddressList,
            deliveryLocation,
            onPressSubmit: this.onPressSelectedAddress,
            onCloseModalAddress: this.onCloseModalAddress
          },
          animationType: 'none'
        })
      }
      this.setState({
        boardType: type,
        isSelectingStore: false
      })
    } else {
      this.setState({
        boardType: type,
        isSelectingStore: false,
        buyLocation: null,
        storeName: null
      })
    }
  }

  renderCards() {
    const Cards = [
      <PickerBoard
        key="1"
        onPress={() => this.setModalVisible(false)}
        generateNumber={() => this.generateNumberPicker('A')}
        emptyBoard={() => this.emptyBoardPicker('A')}
        onSelectNumber={this.inputBoardNumber}
        boardKey={'A'}
        filledNumber={this.state.filledNumber.A}
        boardPosition={1}
        currentIndexBoard={this.state.currentIndexBoard}
        filledPrice={this.state.filledPrice.A}
        changePrice={this.changePrice}
      />,
      <PickerBoard
        key="2"
        onPress={() => this.setModalVisible(false)}
        generateNumber={() => this.generateNumberPicker('B')}
        emptyBoard={() => this.emptyBoardPicker('B')}
        onSelectNumber={this.inputBoardNumber}
        boardKey={'B'}
        filledNumber={this.state.filledNumber.B}
        boardPosition={2}
        currentIndexBoard={this.state.currentIndexBoard}
        filledPrice={this.state.filledPrice.B}
        changePrice={this.changePrice}
      />,
      <PickerBoard
        key="3"
        onPress={() => this.setModalVisible(false)}
        generateNumber={() => this.generateNumberPicker('C')}
        emptyBoard={() => this.emptyBoardPicker('C')}
        onSelectNumber={this.inputBoardNumber}
        boardKey={'C'}
        filledNumber={this.state.filledNumber.C}
        boardPosition={3}
        currentIndexBoard={this.state.currentIndexBoard}
        filledPrice={this.state.filledPrice.C}
        changePrice={this.changePrice}
      />,
      <PickerBoard
        key="4"
        onPress={() => this.setModalVisible(false)}
        generateNumber={() => this.generateNumberPicker('D')}
        emptyBoard={() => this.emptyBoardPicker('D')}
        onSelectNumber={this.inputBoardNumber}
        boardKey={'D'}
        filledNumber={this.state.filledNumber.D}
        boardPosition={4}
        currentIndexBoard={this.state.currentIndexBoard}
        filledPrice={this.state.filledPrice.D}
        changePrice={this.changePrice}
      />,
      <PickerBoard
        key="5"
        onPress={() => this.setModalVisible(false)}
        generateNumber={() => this.generateNumberPicker('E')}
        emptyBoard={() => this.emptyBoardPicker('E')}
        onSelectNumber={this.inputBoardNumber}
        boardKey={'E'}
        filledNumber={this.state.filledNumber.E}
        boardPosition={5}
        currentIndexBoard={this.state.currentIndexBoard}
        filledPrice={this.state.filledPrice.E}
        changePrice={this.changePrice}
      />,
      <PickerBoard
        key="6"
        onPress={() => this.setModalVisible(false)}
        generateNumber={() => this.generateNumberPicker('F')}
        emptyBoard={() => this.emptyBoardPicker('F')}
        onSelectNumber={this.inputBoardNumber}
        boardKey={'F'}
        filledNumber={this.state.filledNumber.F}
        boardPosition={6}
        currentIndexBoard={this.state.currentIndexBoard}
        filledPrice={this.state.filledPrice.F}
        changePrice={this.changePrice}
      />
    ]
    return Cards
  }

  setModalVisible(visible, boardPos = 0) {
    Picker.hide()
    this.setState({
      modalVisible: visible,
      initialBoard: boardPos,
      currentIndexBoard: boardPos
    })
    const filledNums = this.state.filledNumber
    const errorBoard = []
    _.forEach(Object.keys(filledNums), (value, index) => {
      const isFilled = _.countBy(filledNums[value])
      if (isFilled.null > 0 && isFilled.null < 4) {
        errorBoard.push(index)
      }
    })
    if (errorBoard.length > 0) {
      Alert.alert('', 'Vui lòng chọn đủ 4 số cho những Board bạn đã chọn hoặc bỏ trống để tiếp tục')
      this.setState({
        modalVisible: true,
        initialBoard: errorBoard['0']
      })
    }
  }

  inputBoardNumber(boardKey, keyNumber, keyIndex) {
    const filledNums = this.state.filledNumber
    const filledPrice = this.state.filledPrice
    const easyPick = this.state.easyPick
    const filled = filledNums[boardKey][keyIndex] === keyNumber
    if (_.isNull(filledPrice[boardKey])) {
      filledPrice[boardKey] = minimumPrice
    }
    if (filled) {
      filledNums[boardKey][keyIndex] = null
    } else {
      filledNums[boardKey][keyIndex] = keyNumber
    }
    // filledNums[boardKey].sort((a, b) => a - b)
    easyPick[boardKey] = false
    this.setState({ filledNums, easyPick, filledPrice })
  }

  generateNumberPicker(boardKey) {
    const filledNums = this.state.filledNumber
    const filledPrice = this.state.filledPrice
    const easyPick = this.state.easyPick
    filledNums[boardKey] = this.randomNumber()
    if (_.isNull(this.state.filledPrice[boardKey])) {
      filledPrice[boardKey] = minimumPrice
      this.setState(filledPrice)
    }
    easyPick[boardKey] = true
    this.setState({ filledNums, easyPick })
  }

  showAddAddress() {
    this.props.navigator.showModal({
      screen: 'AddressList',
      passProps: {},
      style: {
        backgroundBlur: 'none',
        backgroundColor: 'rgba(0,0,0,0.5)'
      }
    })
  }

  renderAddressDelivery() {
    const { selectedAddress, boardType } = this.state
    if (!selectedAddress || boardType !== 'delivery') {
      return null
    }
    const { client_name, client_phone, address, ward_name, district_name, province_name } = selectedAddress

    return (
      <TouchableOpacity
        onPress={() => this.selectBoardType('delivery')}
        style={{
          height: this.state.boardType === 'deliveryBoard' ? 0 : null,
          overflow: 'hidden',
          opacity: this.state.boardType === 'deliveryBoard' ? 0 : 1,
          flexDirection: 'row',
          borderRadius: 5,
          borderWidth: 0.5,
          borderColor: '#860090',
          marginTop: 10,
          marginLeft: '9%',
          padding: 10
        }}
      >
        <View style={{ flex: 1, paddingRight: 10 }}>
          <TextFont font={'Bold'}>{client_name}</TextFont>
          <TextFont style={{ marginTop: 5 }}>{address}, P.{ward_name}, Q.{district_name}, {province_name}</TextFont>
          <TextFont style={{ marginTop: 5 }}>SĐT liên hệ: {client_phone}</TextFont>
        </View>
        <Icon name={'down'} size={10} color={'#860090'} />
      </TouchableOpacity>
    )
  }

  renderPickUpTicketType() {
    const { ticketPrizeInfo } = this.props
    const listTicketService = _.filter(ticketPrizeInfo.ticket_service, item => item.state === 1)

    return (
      <View style={{ marginTop: 20 }}>
        <FieldSet color={'#ff9417'} borderColor={'#860090'} fieldSetName="CHỌN LOẠI VÉ">
          <View class="ratio-input-group">
            {
              _.map(listTicketService, (item) => {
                return (
                  <View key={_.uniqueId()}>
                    <TouchableOpacity
                      onPress={() => this.selectBoardType(`${item.tag_name}`)}
                      style={styles.ratioItemList}
                    >
                      <View style={styles.ratioItem}>
                        <View
                          class="RatioSelected"
                          style={[styles.selectRatio, this.state.boardType === `${item.tag_name}` ? styles.selectedRatio : '']}
                        />
                      </View>
                      <TextFont
                        class="ratioTextLbabel"
                        style={styles.ratioTextLbabel}
                      >
                        {item.name}
                      </TextFont>
                    </TouchableOpacity>
                    {
                      item.tag_name === 'pickup' ? <View
                        style={{
                          height: this.state.isSelectingStore === false ? 0 : 100,
                          overflow: 'hidden',
                          opacity: this.state.isSelectingStore === false ? 0 : 1
                        }}
                      >
                        <View class="selectLocation" style={styles.selectLoation}>
                          <View style={{ width: 30 }} />
                          <View>
                            <View style={{ borderColor: '#c92033', borderRadius: 7, borderWidth: 1 }}>
                              <TextFont onPress={() => this.showAddress()} style={styles.pickPlayMethod}>
                                {_.isNil(this.state.pickAddress) ? 'Chọn khu vực lấy vé' : `${this.state.pickAddress.province} - ${this.state.pickAddress.district}` }
                              </TextFont>
                            </View>
                          </View>
                        </View>
                        <View style={styles.selectLoation}>
                          <View style={{ width: 30 }} />
                          <View>
                            <View style={{ borderColor: '#c92033', borderRadius: 7, borderWidth: 1 }}>
                              <TextFont onPress={this.showAgent} style={styles.pickPlayMethod}>{_.isNil(this.state.agentName) ? 'Chọn đại lý lấy vé' : this.state.agentName}</TextFont>
                            </View>
                          </View>
                        </View>
                      </View> : null
                    }
                  </View>

                )
              })
            }
            {this.renderAddressDelivery()}
          </View>
        </FieldSet>
      </View>
    )
  }

  render() {
    const { ticketPrizeInfo } = this.props
    return (
      <View style={styles.container}>
        {Platform.OS === 'android' ? <StatusBar /> : null}
        <PickupTicketNav backgroundColor={'#860090'} imageSource={Images.logoMax4d} onRightButton={() => this.props.navigator.pop()} jackpotNumber={parseInt(ticketPrizeInfo.prize)} />
        <View class="mainContainer" style={styles.mainContainer}>
          <ScrollView style={styles.scrollViewContainer}>
            <View style={{ marginTop: 20 }}>
              <FieldSet color={'#ff9417'} borderColor={'#860090'} fieldSetName="CHỌN DÃY SỐ DỰ THƯỞNG">
                <Board
                  onPress={() => this.setModalVisible(true, 0)}
                  onRemove={() => this.emptyBoard('A')}
                  filledNumber={this.state.filledNumber.A}
                  filledPrice={this.state.filledPrice.A}
                  changePrice={this.changePrice}
                  filled={this.state.filled.A}

                  boardName="A"
                />
                <Board
                  onPress={() => this.setModalVisible(true, 1)}
                  onRemove={() => this.emptyBoard('B')}
                  filledNumber={this.state.filledNumber.B}
                  filledPrice={this.state.filledPrice.B}
                  filled={this.state.filled.B}
                  changePrice={this.changePrice}
                  boardName="B"
                />
                <Board
                  onPress={() => this.setModalVisible(true, 2)}
                  onRemove={() => this.emptyBoard('C')}
                  filledNumber={this.state.filledNumber.C}
                  filledPrice={this.state.filledPrice.C}
                  filled={this.state.filled.C}
                  changePrice={this.changePrice}
                  boardName="C"
                />
                <Board
                  onPress={() => this.setModalVisible(true, 3)}
                  onRemove={() => this.emptyBoard('D')}
                  filledNumber={this.state.filledNumber.D}
                  filledPrice={this.state.filledPrice.D}
                  changePrice={this.changePrice}
                  filled={this.state.filled.D}
                  boardName="D"
                />
                <Board
                  onPress={() => this.setModalVisible(true, 4)}
                  onRemove={() => this.emptyBoard('E')}
                  filledNumber={this.state.filledNumber.E}
                  filledPrice={this.state.filledPrice.E}
                  changePrice={this.changePrice}
                  filled={this.state.filled.E}
                  boardName="E"
                />
                <Board
                  onPress={() => this.setModalVisible(true, 5)}
                  onRemove={() => this.emptyBoard('F')}
                  filledNumber={this.state.filledNumber.F}
                  filledPrice={this.state.filledPrice.F}
                  filled={this.state.filled.F}
                  changePrice={this.changePrice}
                  boardName="F"
                />
                <View style={{ marginTop: 20, alignItems: 'center' }}>
                  <TouchableOpacity
                    style={styles.randomBoard}
                    onPress={() => {
                      this.showNumber()
                    }}
                  ><TextFont style={styles.textButton}>Chọn nhanh</TextFont></TouchableOpacity>
                </View>
              </FieldSet>
            </View>

            <View style={{ marginTop: 20 }}>
              <FieldSet color={'#ff9417'} borderColor={'#860090'} fieldSetName="CHỌN SỐ KỲ THAM DỰ">
                <View class="groupSelect" style={styles.buttonGroup}>
                  <SelectButton
                    onPress={() => this.selectBuyRepeat(1)}
                    consecutiveBuy={this.state.consecutiveBuy}
                    buttonNum={1}
                    buttonColor="#7F0791"
                  />
                  <SelectButton
                    onPress={() => this.selectBuyRepeat(2)}
                    consecutiveBuy={this.state.consecutiveBuy}
                    buttonNum={2}
                    q
                    buttonColor="#7F0791"
                  />
                  <SelectButton
                    onPress={() => this.selectBuyRepeat(3)}
                    consecutiveBuy={this.state.consecutiveBuy}
                    buttonNum={3}
                    buttonColor="#7F0791"
                  />
                  <SelectButton
                    onPress={() => this.selectBuyRepeat(4)}
                    consecutiveBuy={this.state.consecutiveBuy}
                    buttonNum={4}
                    buttonColor="#7F0791"
                  />
                  <SelectButton
                    onPress={() => this.selectBuyRepeat(5)}
                    consecutiveBuy={this.state.consecutiveBuy}
                    buttonNum={5}
                    buttonColor="#7F0791"
                  />
                  <SelectButton
                    onPress={() => this.selectBuyRepeat(6)}
                    consecutiveBuy={this.state.consecutiveBuy}
                    buttonNum={6}
                    buttonColor="#7F0791"
                  />
                </View>
              </FieldSet>
            </View>
            {this.renderPickUpTicketType()}
            <View style={{ marginTop: 20, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}><TextFont style={{ color: '#222' }}>TỔNG SỐ TIỀN THANH TOÁN:</TextFont></View>
              <View style={{ alignItems: 'flex-end' }}><TextFont style={{ color: '#7F0791' }}>{ numeral(totalPrize).format('0,0') } đ</TextFont></View>
              <View style={{ marginLeft: 5 }}>
                <TextFont onPress={() => this.props.navigator.push({ ...Config.screen.detailMax4d, passProps: { ticketPrizeInfo, ticketDetail: { ...this.state, totalPrize } } })}>
                  <Icon name="question" color="#919191" size={16} />
                </TextFont>
              </View>
            </View>
            <View style={{ marginTop: 20, marginBottom: 20 }}>
              <ThreeDButton backgroundColor={'#7F0791'} borderedColor={'#AC1EC1'} onPress={this.onPressSubmit} />
            </View>
          </ScrollView>
        </View>
        <Overlay visible={this.state.visibleOverlay} onPressOverlay={() => Picker.hide()} />
        <Modal
          animationType="none"
          onRequestClose={() => {
            this.setState({ modalVisible: false })
          }}
          visible={this.state.modalVisible}
          style={styles.modalPickup}
          backdrop={false}
          transparent
          position={'top'}
        >
          <Carousel
            selectedIndex={this.state.initialBoard}
            style={{ backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'center', paddingTop: 35 }}
            viewPagerStyle={{ backgroundColor: 'transparent' }}
            cards={this.renderCards()}
            onSelectedIndexChange={(currentIndexBoard) => {
              this.setState({ currentIndexBoard })
            }}
          />
          <Overlay
            visible={this.state.visibleOverlay}
            onPressOverlay={() => {
              this.setState({
                visibleOverlay: false
              })
              Picker.hide()
            }}
          />
        </Modal>
      </View>
    )
  }
}
PickupTicket.defaultProps = {
  filledNumber: {
    A: [null, null, null, null],
    B: [null, null, null, null],
    C: [null, null, null, null],
    D: [null, null, null, null],
    E: [null, null, null, null],
    F: [null, null, null, null]
  },
  filledPrice: {
    A: null,
    B: null,
    C: null,
    D: null,
    E: null,
    F: null
  }

}
