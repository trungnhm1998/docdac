import React, { Component } from 'react'
import { ScrollView, Text, TouchableOpacity, View } from 'react-native'
import _ from 'lodash'
import { Icon, TextFont } from '../../../ui'
import styles from './pickupTicket.style'

const numeral = require('numeral')

export default class PickerBoard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showSymbol: false
    }
    this.showSymbol = this.showSymbol.bind(this)
    this.generateNumber = this.generateNumber.bind(this)
    this.onPressKey = this.onPressKey.bind(this)
  }

  shouldComponentUpdate(props) {
    const { boardPosition, currentIndexBoard } = props
    return boardPosition === currentIndexBoard || boardPosition === (currentIndexBoard + 1) || boardPosition === (currentIndexBoard + 2)
  }

  onPressKey(boardKey, keyNumber, keyIndex) {
    const { onSelectNumber } = this.props
    onSelectNumber(boardKey, keyNumber, keyIndex)
  }

  generateNumber() {
    this.props.generateNumber()
  }

  renderNumber(filledNumber, boardKey) {
    const { boardPosition, currentIndexBoard } = this.props
    if (boardPosition === currentIndexBoard || boardPosition === (currentIndexBoard + 1) || boardPosition === (currentIndexBoard + 2)) {
      const NumberList1 = []
      const NumberList2 = []
      const NumberList3 = []
      const NumberList4 = []
      for (let i = 0; i <= 9; i += 1) {
        const bgColor = filledNumber[0] === i ? '#f9a936' : '#fff'
        const color = filledNumber[0] === i ? '#fff' : '#000'

        NumberList1.push(
          <TouchableOpacity
            onPress={() => this.onPressKey(boardKey, i, 0)}
            key={i}
            style={[styles.pickNumberSymbol, { backgroundColor: bgColor }]}
            activeOpacity={0.8}
          >
            <View>
              <TextFont
                style={{ color, textAlign: 'center' }}
              >
                {i.toString()}
              </TextFont>
            </View>
          </TouchableOpacity>
        )
      }
      for (let i = 0; i <= 9; i += 1) {
        const bgColor = filledNumber[1] === i ? '#f9a936' : '#fff'
        const color = filledNumber[1] === i ? '#fff' : '#000'

        NumberList2.push(
          <TouchableOpacity
            onPress={() => this.onPressKey(boardKey, i, 1)}
            key={i}
            style={[styles.pickNumberSymbol, { backgroundColor: bgColor }]}
            activeOpacity={0.8}
          >
            <View>
              <TextFont
                style={{ color, textAlign: 'center' }}
              >
                {i.toString()}
              </TextFont>
            </View>
          </TouchableOpacity>
        )
      }
      for (let i = 0; i <= 9; i += 1) {
        const bgColor = filledNumber[2] === i ? '#f9a936' : '#fff'
        const color = filledNumber[2] === i ? '#fff' : '#000'

        NumberList3.push(
          <TouchableOpacity
            onPress={() => this.onPressKey(boardKey, i, 2)}
            key={i}
            style={[styles.pickNumberSymbol, { backgroundColor: bgColor }]}
            activeOpacity={0.8}
          >
            <View>

              <TextFont
                style={{ color, textAlign: 'center' }}
              >
                {i.toString()}
              </TextFont>
            </View>
          </TouchableOpacity>
        )
      }
      for (let i = 0; i <= 9; i += 1) {
        const bgColor = filledNumber[3] === i ? '#f9a936' : '#fff'
        const color = filledNumber[3] === i ? '#fff' : '#000'

        NumberList4.push(
          <TouchableOpacity
            onPress={() => this.onPressKey(boardKey, i, 3)}
            key={i}
            style={[styles.pickNumberSymbol, { backgroundColor: bgColor }]}
            activeOpacity={0.8}
          >
            <View>
              <TextFont
                style={{ color, textAlign: 'center' }}
              >
                {i.toString()}
              </TextFont>
            </View>
          </TouchableOpacity>
        )
      }
      return (
        <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>
          <View style={styles.ColumnNumber}>{NumberList1}</View>
          <View style={styles.ColumnNumber}>{NumberList2}</View>
          <View style={styles.ColumnNumber}>{NumberList3}</View>
          <View style={styles.ColumnNumber}>{NumberList4}</View>
        </View>
      )
    }
    return null
  }

  showSymbol() {
    this.setState({
      showSymbol: !this.state.showSymbol
    })
  }

  render() {
    const { boardPosition, onPress, filledNumber, boardKey } = this.props
    return (
      <View style={styles.slideNumberPicker}>
        <View style={styles.headerNumberPicker}>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <TextFont style={styles.boardPos}>BOARD {boardPosition}/6</TextFont>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => this.props.emptyBoard()}
              underlayColor="#e39d35"
              style={styles.buttonNumberPicker}
            >
              <Icon name="trash" color="#fff" size={20} />
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={this.generateNumber}
              activeOpacity={0.8}
              style={styles.buttonNumberPicker}
            >
              <Text style={{ color: '#fff' }}>
                Chọn nhanh
              </Text>
            </TouchableOpacity>
          </View>
          <View >
            <TouchableOpacity
              onPress={() => onPress()}
              activeOpacity={0.8}
              style={[
                styles.buttonNumberPicker, {
                  backgroundColor: 'transparent',
                  alignItems: 'flex-end'
                }]}
            >
              <Icon name="close" color="#fff" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.roundNumberPicker}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10, alignItems: 'center' }}>
            <TextFont
              style={styles.pickerCount}
            >Bạn đã chọn {filledNumber.length}/4 số</TextFont>
            <TouchableOpacity onPress={() => this.props.changePrice(this.props.boardKey)} style={styles.priceButton}>
              <TextFont color={'#F8AE40'} style={{ textAlign: 'center' }}>
                {_.isNull(this.props.filledPrice) ? 'Mệnh giá ' : numeral(this.props.filledPrice).format('0a')}
              </TextFont>
              <Text style={{ marginLeft: 10 }}><Icon name="down" color="#F8AE40" size={6} /></Text>
            </TouchableOpacity>
          </View>

          <ScrollView>
            <View style={styles.numberPicker}>
              { this.renderNumber(filledNumber, boardKey) }
            </View>
          </ScrollView>
        </View>
      </View>
    )
  }
}
PickerBoard.defaultProps = {
  inclusive: '6',
  showSymbol: false
}
