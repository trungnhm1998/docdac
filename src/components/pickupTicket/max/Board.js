import React from 'react'
import { Dimensions, StyleSheet, Text, TouchableOpacity, View, PixelRatio } from 'react-native'
import _ from 'lodash'
import { Icon, TextFont } from '../../../ui'

const numeral = require('numeral')
const { width } = Dimensions.get('window')
const styles = StyleSheet.create({
  boardNum: {
    width: width * 0.09,
    height: width * 0.09,
    borderWidth: 3,
    marginTop: 10,
    borderColor: '#7E0590',
    borderRadius: (width * 0.09) / 2,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden'
  },
  numberStyle: {
    color: '#FB940F'
  },
  randomGift: {
    width: width * 0.05,
    height: width * 0.05,
    borderRadius: (width * 0.05) / 2,
    borderColor: '#f15a22'
  },
  numberCover: {
    flex: 2,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start'
  },
  rowCover: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: 10
  },
  priceButton: {
    height: width * 0.09,
    borderWidth: 0.5 * PixelRatio.get(),
    borderColor: '#A1A1A1',
    justifyContent: 'center',
    borderRadius: 4,
    width: '60%',
    marginTop: width * 0.03
  },
  blockButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  }

})

const pad = (n) => {
  return (n < 10) ? `0${n}` : n
}

const Rendernumber = (props) => {
  if (!props.filledNumber) return <View />

  const rows = []
  for (let i = 0; i < 4; i += 1) {
    rows.push(
      <TouchableOpacity
        key={i}
        onPress={() => props.onPress()}
        style={[styles.boardNum, { borderColor: _.isNull(props.filledNumber[i]) ? '#7E0590' : '#FB940F', marginRight: 8 }]}
      >
        { _.isNull(props.filledNumber[i]) ? null : <Text style={styles.numberStyle}>{props.filledNumber[i]}</Text>}
      </TouchableOpacity>
    )
  }

  return (
    <View style={styles.rowCover}>
      <View style={styles.numberCover}>
        {rows}
      </View>
      <View style={{ flex: 1.2 }}>
        {_.isNull(props.filledNumber[0]) ? <View style={styles.blockButton}>
          <TouchableOpacity onPress={() => props.onPress()} style={[styles.priceButton, { width: '100%' }]}>
            <TextFont color={'#959595'} style={{ textAlign: 'center', fontSize: 10 }}>Chọn dãy số</TextFont>
          </TouchableOpacity>
        </View>
          : <View style={styles.blockButton}>
            <TouchableOpacity onPress={() => props.changePrice(props.boardName)} style={styles.priceButton}>
              <TextFont color={'#888'} style={{ textAlign: 'center' }}>{numeral(props.filledPrice).format('0a')}</TextFont>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => props.onRemove()} style={[styles.boardNum, { borderWidth: 0, justifyContent: 'center', alignItems: 'center' }]}>
              <Icon color={'#888'} name="trash" size={18} />
            </TouchableOpacity>
          </View>
        }

      </View>
    </View>
  )
}

const Board = (props) => {
  return (
    <View class="board" style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
      <View style={[styles.boardNum, { borderWidth: 0 }]}>
        <Text
          style={{ color: '#7E0590' }}
        >
          #{props.boardName}
        </Text>
      </View>
      <Rendernumber
        inclusive={props.inclusive}
        filledNumber={props.filledNumber}
        onRemove={props.onRemove}
        onPress={props.onPress}
        filled={props.filled}
        boardName={props.boardName}
        filledPrice={props.filledPrice}
        changePrice={props.changePrice}
      />
    </View>
  )
}
export default Board
