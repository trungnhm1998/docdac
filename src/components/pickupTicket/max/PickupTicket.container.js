import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import PickupTicket from './PickupTicket'
import Config from '../../../screens/config'
import { actions as LotteryAction } from '../../../redux/lottery/'

class PickupTicketContainer extends Component {
  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB,
    drawUnderNavBar: true,
    statusBarColor: '#860090',
    navBarHidden: true
  }
  
  constructor(props) {
    super(props)
    this.state = {
      pickNumber: 0 // play method
    }
    this.onBuyTicket = this.onBuyTicket.bind(this)
    this.getAgentList = this.getAgentList.bind(this)
  }

  getAgentList(districtId) {
    const { lotteryAction } = this.props
    lotteryAction.getAgentList({ districtId })
  }

  onBuyTicket(value) {
    const { lottery } = this.props.payload
    const { filledNumber, consecutiveBuy, easyPick, filledPrice, boardType, totalPrize, address_id, agentId, ticketPrizeInfo } = value
    this.props.navigator.push({
      ...Config.screen.confirmMax4d,
      passProps: {
        address_id,
        consecutiveBuy,
        easyPick,
        filledNumber,
        filledPrice,
        boardType,
        totalPrize,
        agentId,
        ticketPrizeInfo
      }
    }
    )
  }

  render() {
    const { lottery, accountInfo } = this.props.payload
    const { filledNumber, filledPrice } = this.props
    const ticketPrizeInfo = _.find(lottery.ticketPrizeInfo, item => parseInt(item.ticket_type, 10) === 4)
    
    return (
      <PickupTicket
        navigator={this.props.navigator}
        ticketPrizeInfo={ticketPrizeInfo}
        deliveryAddressList={accountInfo.deliveryAddressList}
        deliveryLocation={lottery.deliveryLocation}
        pickNumber={this.state.pickNumber + 4}
        onBuyTicket={this.onBuyTicket}
        getAgentList={this.getAgentList}
        agentList={lottery.agentList}
        filledNumber={filledNumber}
        filledPrice={filledPrice}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    lotteryAction: bindActionCreators(LotteryAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PickupTicketContainer)
