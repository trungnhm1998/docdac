import React from 'react'
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import _ from 'lodash'
import { Icon } from '../../../ui'

const { width } = Dimensions.get('window')
const styles = StyleSheet.create({
  boardNum: {
    width: width * 0.09,
    height: width * 0.09,
    borderWidth: 3,
    marginTop: 10,
    borderColor: '#f15a22',
    borderRadius: (width * 0.09) / 2,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden'
  },
  numberStyle: {
    color: '#06b606'
  },
  randomGift: {
    width: width * 0.05,
    height: width * 0.05,
    borderRadius: (width * 0.05) / 2,
    borderColor: '#f15a22'
  },
  numberCover: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start'
  },
  rowCover: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: 10
  }

})

const pad = (n) => {
  return (n < 10) ? `0${n}` : n
}

const Rendernumber = (props) => {
  const rows = []
  const LoopCircle = _.isNil((props.inclusive)) ? 6 : props.inclusive
  for (let i = 0; i < LoopCircle; i += 1) {
    rows.push(
      <TouchableOpacity
        key={i}
        onPress={() => props.onPress()}

        style={[styles.boardNum, { borderColor: _.isNil(props.filledNumber[i]) ? '#f15a22' : '#06b606', marginRight: '3%' }]}
      >
        {props.filled === 'filling' && props.filledNumber.length === 0 ? null : <Text style={styles.numberStyle}>{pad(props.filledNumber[i])}</Text>}
      </TouchableOpacity>
    )
  }

  if (_.isNil((props.inclusive))) {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1, paddingLeft: 10 }}>
        {rows}
        <TouchableOpacity onPress={() => props.onRemove()} style={[styles.boardNum, { borderWidth: 0 }]}>
          {props.filledNumber.length !== 0 ? <Text><Icon name="trash" size={18} /></Text> : null}
        </TouchableOpacity>
      </View>
    )
  }
  return (
    <View style={styles.rowCover}>
      <View style={styles.numberCover}>
        {rows}
      </View>
      <TouchableOpacity onPress={() => props.onRemove()} style={[styles.boardNum, { borderWidth: 0 }]}>
        {props.filledNumber.length !== 0 ? <Text><Icon name="trash" size={18} /></Text> : null}
      </TouchableOpacity>
    </View>
  )
}

const Board = (props) => {
  return (
    <View class="board" style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
      <View style={[styles.boardNum, { borderWidth: 0 }]}>
        <Text
          style={{ color: '#c92033' }}
        >
          #{props.boardName}
        </Text>
      </View>
      <Rendernumber
        inclusive={props.inclusive}
        filledNumber={props.filledNumber}
        onRemove={props.onRemove}
        onPress={props.onPress}
        filled={props.filled}
      />
    </View>
  )
}
export default Board
