import React, { Component } from 'react'
import { Alert, Modal, Platform, ScrollView, StatusBar, TouchableOpacity, View } from 'react-native'
import _ from 'lodash'
import Picker from 'react-native-picker'
import Sound from 'react-native-sound'
import numeral from 'numeral'
import Board from './Board'
import { FieldSet, Icon, Overlay, PickupTicketNav, SelectButton, TextFont, ThreeDButton } from '../../../ui'
import Config from '../../../screens/config'
import { Images } from '../../../ui/themes'
import { helper } from '../../../utils'
import { Carousel } from '../../../ui/carousel'
import PickerBoard from './pickerBoard'
import styles from './pickupTicket.style'

const whoosh = new Sound('card.mp3', Sound.MAIN_BUNDLE)
const trashSound = new Sound('trash.mp3', Sound.MAIN_BUNDLE)

let totalPrize = 0
export default class PickupTicket extends Component {
  constructor(props) {
    super(props)
    this.state = {
      filledNumber: _.cloneDeep(this.props.filledNumber),
      consecutiveBuy: 1,
      inclusive: this.props.inclusive === 1 ? 0 : this.props.inclusive, // props
      easyPick: {
        A: false,
        B: false,
        C: false,
        D: false,
        E: false,
        F: false
      },

      currentIndexBoard: 0,

      boardType: 'hold',
      buyLocation: null,
      storeName: null,
      isSelectingStore: false,
      modalVisible: false,
      initialBoard: 0,

      pickupSelectedIndex: 0,
      visibleOverlay: false,
      selectedAddress: null,
      agentName: null,
      agentId: null
    }
    this.inputBoardNumber = this.inputBoardNumber.bind(this)
    this.showPlayMethod = this.showPlayMethod.bind(this)
    this.onPressSelectedAddress = this.onPressSelectedAddress.bind(this)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.showAgent = this.showAgent.bind(this)
    this.onCloseModalAddress = this.onCloseModalAddress.bind(this)
  }

  componentWillMount() {
    totalPrize = 0
  }

  componentDidMount() {
    this.setState({
      totalPrize: 0
    })
  }

  componentWillReceiveProps(nextProps) {
    if (_.isNil(nextProps.agentList)) {
      this.setState({
        agentName: null
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { ticketPrizeInfo } = nextProps
    const { inclusive, filledNumber, consecutiveBuy, boardType } = nextState
    const { ticket_price, delivery_fee } = ticketPrizeInfo

    if (inclusive === 0) {
      const boards = _.countBy(filledNumber, item => !_.isEmpty(item))
      if (boardType !== 'hold') {
        totalPrize = (boards.true * parseInt(ticket_price, 10) * consecutiveBuy) + parseInt(delivery_fee, 10)
      } else {
        totalPrize = boards.true * parseInt(ticket_price, 10) * consecutiveBuy
      }
    } else if (_.isEmpty(filledNumber.A) === false) {
      if (boardType !== 'hold') {
        totalPrize = (helper.calculateCombinationPower6(inclusive) * parseInt(ticket_price, 10) * consecutiveBuy) + parseInt(delivery_fee, 10)
      } else {
        totalPrize = helper.calculateCombinationPower6(inclusive) * parseInt(ticket_price, 10) * consecutiveBuy
      }
    }
    return true
  }

  onPressSubmit() {
    if (totalPrize > 0) {
      const { ticketPrizeInfo } = this.props
      const { filledNumber, easyPick, consecutiveBuy, inclusive, boardType, selectedAddress, agentId } = this.state
      this.props.onBuyTicket({
        inclusive,
        easyPick,
        consecutiveBuy,
        filledNumber,
        boardType,
        totalPrize,
        address_id: selectedAddress ? selectedAddress.address_id : null,
        agentId,
        ticketPrizeInfo
      })
    } else {
      Alert.alert('Thông báo', 'Vui lòng chọn vé trước khi thanh toán')
    }
  }

  showPlayMethod() {
    const rawData = [0, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 18]
    const data = [
      'Vé số tự chọn',
      'Bao 5 số',
      'Bao 7 số',
      'Bao 8 số',
      'Bao 9 số',
      'Bao 10 số',
      'Bao 11 số',
      'Bao 12 số',
      'Bao 13 số',
      'Bao 14 số',
      'Bao 15 số',
      'Bao 18 số'
    ]
    this.setState({ visibleOverlay: true })
    Picker.init({
      pickerData: data,
      selectedValue: [data[this.state.pickupSelectedIndex]],
      pickerTitleText: '',
      pickerToolBarBg: [201, 32, 51, 1],
      pickerBg: [255, 255, 255, 20],
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Hủy',
      onPickerCancel: () => {
        this.setState({
          visibleOverlay: false
        })
      },
      onPickerConfirm: (val) => {
        const dataIndex = data.indexOf(val[0])
        const inclusive = rawData[dataIndex]
        this.setState({
          inclusive,
          filledNumber: {
            A: [],
            B: [],
            C: [],
            D: [],
            E: [],
            F: []
          },
          pickupSelectedIndex: dataIndex,
          currentIndexBoard: 0,
          visibleOverlay: false
        })
      }
    })
    Picker.show()
  }

  showAddress() {
    const { deliveryLocation } = this.props
    const data = []
    _.forEach(deliveryLocation, (item) => {
      const provinceName = item.province_name
      const itemObj = {}
      itemObj[provinceName] = _.map(item.district, itemDistrict => `${itemDistrict.type} ${itemDistrict.name}`)
      data.push(itemObj)
    })
    const selectedItem = _.isNil(this.state.pickAddress) ? [] : [this.state.pickAddress.province, this.state.pickAddress.district]
    this.setState({ visibleOverlay: true })
    Picker.init({
      pickerData: data,
      selectedValue: selectedItem,
      pickerTitleText: '',
      pickerToolBarBg: [201, 32, 51, 1],
      pickerBg: [255, 255, 255, 20],
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Hủy',
      onPickerCancel: () => {
        this.setState({
          visibleOverlay: false
        })
      },
      onPickerConfirm: (value) => {
        const selectedProvince = _.find(deliveryLocation, (item) => {
          return item.province_name === value[0]
        })

        if (selectedProvince) {
          const selectedDistrict = _.find(selectedProvince.district, (item) => {
            return item.name === _.trim(value[1], [item.type, ' '])
          })
          this.props.getAgentList(selectedDistrict.district_id)

          this.setState({
            visibleOverlay: false,
            pickAddress: {
              ...this.state.pickAddress,
              province: value[0],
              district: value[1],
              districtId: selectedDistrict.district_id,
              provinceId: selectedProvince.province_id
            }

          })
        }
      }
    })
    Picker.show()
  }

  showAgent() {
    const error = _.isNil(this.state.pickAddress) || _.isNil(this.props.agentList) ? 'Bạn chưa chọn khu vực lấy vé hoặc không có đại lý tại nơi bạn đã chọn' : null
    if (!_.isNull(error)) {
      Alert.alert('Thông báo', error)
      return
    }
    const { agentList } = this.props
    const data = []
    _.forEach(agentList, (item) => {
      const agentName = item.agent_name
      data.push(agentName)
    })

    const selectedItem = _.isNil(this.state.agentName) ? [] : [this.state.agentName]
    this.setState({ visibleOverlay: true })
    Picker.init({
      pickerData: data,
      selectedValue: selectedItem,
      pickerTitleText: '',
      pickerToolBarBg: [201, 32, 51, 1],
      pickerBg: [255, 255, 255, 20],
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Hủy',
      onPickerCancel: () => {
        this.setState({
          visibleOverlay: false
        })
      },
      onPickerConfirm: (value) => {
        const selectedAgent = _.find(agentList, (item) => {
          return item.agent_name === value[0]
        })
        this.setState({
          visibleOverlay: false,
          agentName: value[0],
          agentId: selectedAgent.user_id
        })
      }
    })
    Picker.show()
  }

  randomNumber() {
    const arr = []
    const inclusive = this.state.inclusive === 0 ? 6 : this.state.inclusive
    while (arr.length < inclusive) {
      // const randomNumber = Math.floor(Math.random() * (54)) + 1
      const randomNumber = _.random(1, 55)
      let found = false
      let i = 0
      for (i; i < arr.length; i += 1) {
        if (arr[i] === randomNumber) {
          found = true
          break
        }
      }
      if (!found) arr[arr.length] = randomNumber
    }
    return _.sortBy(arr)
  }

  showNumber() {
    const { filledNumber, easyPick } = this.state
    if (this.state.currentIndexBoard > 6) {
      return
    }
    whoosh.play()
    const filledNums = Object.assign({}, filledNumber)
    const easyPickObject = Object.assign({}, easyPick)

    _.forEach(Object.keys(filledNums), (value, index) => {
      if (this.state.currentIndexBoard === index && filledNums[value].length > 0) {
        this.state.currentIndexBoard += 1
      }
    })
    _.forEach(Object.keys(filledNums), (value, index) => {
      if (filledNums[value].length === 0 && this.state.currentIndexBoard === index) {
        filledNums[value] = this.randomNumber()
        easyPickObject[value] = true
      }
    })
    this.setState({ filledNumber: filledNums, easyPick: easyPickObject })
    if (this.state.currentIndexBoard <= 6) {
      this.setState({
        currentIndexBoard: this.state.currentIndexBoard + 1
      })
    }
  }

  emptyBoard(boardKey) {
    trashSound.play()
    const filledNums = this.state.filledNumber
    filledNums[boardKey] = []
    this.setState(filledNums)
    this.setState({ currentIndexBoard: 0 })
  }

  emptyBoardPicker(boardKey) {
    const filledNums = this.state.filledNumber
    filledNums[boardKey] = []
    this.setState(filledNums)
  }

  selectBuyRepeat(number) {
    this.setState({
      consecutiveBuy: number
    })
  }

  onPressSelectedAddress(selectedAddress) {
    this.setState({ selectedAddress })
  }

  onCloseModalAddress(selectedAddress) {
    if (_.isNull(this.state.selectedAddress) && _.isNil(selectedAddress)) {
      this.setState({
        boardType: 'hold'
      })
    }
  }

  selectBoardType(type) {
    if (type === 'pickup') {
      this.setState({
        boardType: type,
        isSelectingStore: true
      })
    } else if (type === 'delivery') {
      const { deliveryAddressList, deliveryLocation } = this.props
      if (_.isEmpty(deliveryAddressList)) {
        this.props.navigator.showModal({
          screen: 'NewAddress',
          passProps: {
            deliveryLocation,
            onPressSubmit: this.onPressSelectedAddress,
            onCloseModalAddress: this.onCloseModalAddress
          }
        })
      } else {
        this.props.navigator.showModal({
          screen: 'AddressList',
          passProps: {
            userAddressList: deliveryAddressList,
            deliveryLocation,
            onPressSubmit: this.onPressSelectedAddress,
            onCloseModalAddress: this.onCloseModalAddress
          },
          animationType: 'none'
        })
      }
      this.setState({
        boardType: type,
        isSelectingStore: false
      })
    } else {
      this.setState({
        boardType: type,
        isSelectingStore: false,
        buyLocation: null,
        storeName: null
      })
    }
  }

  renderCards() {
    const Cards1 = [<PickerBoard
      key="1"
      onPress={() => this.setModalVisible(false)}
      generateNumber={() => this.generateNumberPicker('A')}
      emptyBoard={() => this.emptyBoardPicker('A')}
      onSelectNumber={this.inputBoardNumber}
      boardKey={'A'}
      filledNumber={this.state.filledNumber.A}
      boardPosition={1}
      currentIndexBoard={this.state.currentIndexBoard}
      inclusive={this.state.inclusive}
    />]
    const Cards2 = [
      <PickerBoard
        key="1"
        onPress={() => this.setModalVisible(false)}
        generateNumber={() => this.generateNumberPicker('A')}
        emptyBoard={() => this.emptyBoardPicker('A')}
        onSelectNumber={this.inputBoardNumber}
        boardKey={'A'}
        filledNumber={this.state.filledNumber.A}
        boardPosition={1}
        currentIndexBoard={this.state.currentIndexBoard}
        inclusive={this.state.inclusive}
      />,
      <PickerBoard
        key="2"
        onPress={() => this.setModalVisible(false)}
        generateNumber={() => this.generateNumberPicker('B')}
        emptyBoard={() => this.emptyBoardPicker('B')}
        onSelectNumber={this.inputBoardNumber}
        boardKey={'B'}
        filledNumber={this.state.filledNumber.B}
        boardPosition={2}
        currentIndexBoard={this.state.currentIndexBoard}
        inclusive={this.state.inclusive}
      />,
      <PickerBoard
        key="3"
        onPress={() => this.setModalVisible(false)}
        generateNumber={() => this.generateNumberPicker('C')}
        emptyBoard={() => this.emptyBoardPicker('C')}
        onSelectNumber={this.inputBoardNumber}
        boardKey={'C'}
        filledNumber={this.state.filledNumber.C}
        boardPosition={3}
        currentIndexBoard={this.state.currentIndexBoard}
        inclusive={this.state.inclusive}
      />,
      <PickerBoard
        key="4"
        onPress={() => this.setModalVisible(false)}
        generateNumber={() => this.generateNumberPicker('D')}
        emptyBoard={() => this.emptyBoardPicker('D')}
        onSelectNumber={this.inputBoardNumber}
        boardKey={'D'}
        filledNumber={this.state.filledNumber.D}
        boardPosition={4}
        currentIndexBoard={this.state.currentIndexBoard}
        inclusive={this.state.inclusive}
      />,
      <PickerBoard
        key="5"
        onPress={() => this.setModalVisible(false)}
        generateNumber={() => this.generateNumberPicker('E')}
        emptyBoard={() => this.emptyBoardPicker('E')}
        onSelectNumber={this.inputBoardNumber}
        boardKey={'E'}
        filledNumber={this.state.filledNumber.E}
        boardPosition={5}
        currentIndexBoard={this.state.currentIndexBoard}
        inclusive={this.state.inclusive}
      />,
      <PickerBoard
        key="6"
        onPress={() => this.setModalVisible(false)}
        generateNumber={() => this.generateNumberPicker('F')}
        emptyBoard={() => this.emptyBoardPicker('F')}
        onSelectNumber={this.inputBoardNumber}
        boardKey={'F'}
        filledNumber={this.state.filledNumber.F}
        boardPosition={6}
        currentIndexBoard={this.state.currentIndexBoard}
        inclusive={this.state.inclusive}
      />
    ]

    if (this.state.inclusive === 0) {
      return Cards2
    }
    return Cards1
  }

  setModalVisible(visible, boardPos = 0) {
    const inclusive = this.state.inclusive === 0 ? 6 : this.state.inclusive
    this.setState({
      modalVisible: visible,
      initialBoard: boardPos,
      currentIndexBoard: boardPos
    })
    const filledNums = this.state.filledNumber
    const errorBoard = []
    _.forEach(Object.keys(filledNums), (value, index) => {
      if (filledNums[value].length > 0 && filledNums[value].length < inclusive) {
        errorBoard.push(index)
      }
    })
    if (errorBoard.length > 0) {
      Alert.alert('', `Vui lòng chọn đủ ${inclusive} số cho những Board bạn đã chọn hoặc bỏ trống để tiếp tục`)
      this.setState({
        modalVisible: true,
        initialBoard: errorBoard['0']
      })
    }
  }

  inputBoardNumber(boardKey, keyNumber) {
    const inclusive = this.state.inclusive === 0 ? 6 : this.state.inclusive
    const filledNums = this.state.filledNumber
    const easyPick = this.state.easyPick

    const index = filledNums[boardKey].indexOf(keyNumber)
    if (index > -1) {
      filledNums[boardKey] = filledNums[boardKey].filter(item => item !== keyNumber)
    } else if (filledNums[boardKey].length <= inclusive - 1) {
      filledNums[boardKey].push(keyNumber)
    }
    filledNums[boardKey].sort((a, b) => a - b)
    easyPick[boardKey] = false

    this.setState({ ...filledNums, ...easyPick })
  }

  generateNumberPicker(boardKey) {
    const filledNums = this.state.filledNumber
    const easyPick = this.state.easyPick
    filledNums[boardKey] = this.randomNumber()
    easyPick[boardKey] = true
    this.setState({ ...filledNums, ...easyPick })
  }

  renderAddressDelivery() {
    const { selectedAddress, boardType } = this.state
    if (!selectedAddress || boardType !== 'delivery') {
      return null
    }
    const { client_name, client_phone, address, ward_name, district_name, province_name } = selectedAddress

    return (
      <TouchableOpacity
        onPress={() => this.selectBoardType('delivery')}
        style={{
          height: this.state.boardType === 'deliveryBoard' ? 0 : null,
          overflow: 'hidden',
          opacity: this.state.boardType === 'deliveryBoard' ? 0 : 1,
          flexDirection: 'row',
          borderRadius: 5,
          borderWidth: 0.5,
          borderColor: '#be2122',
          marginTop: 10,
          marginLeft: '9%',
          padding: 10
        }}
      >
        <View style={{ flex: 1, paddingRight: 10 }}>
          <TextFont font={'Bold'}>{client_name}</TextFont>
          <TextFont style={{ marginTop: 5 }}>{address}, P.{ward_name}, Q.{district_name}, {province_name}</TextFont>
          <TextFont style={{ marginTop: 5 }}>SĐT liên hệ: {client_phone}</TextFont>
        </View>
        <Icon name={'down'} size={10} color={'#be2122'} />
      </TouchableOpacity>
    )
  }

  renderPickUpTicketType() {
    const { ticketPrizeInfo } = this.props

    const listTicketService = _.filter(ticketPrizeInfo.ticket_service, item => item.state === 1)

    return (
      <View style={{ marginTop: 20 }}>
        <FieldSet fieldSetName="CHỌN LOẠI VÉ">
          <View class="ratio-input-group">
            {
              _.map(listTicketService, (item) => {
                return (
                  <View key={_.uniqueId()}>
                    <TouchableOpacity
                      onPress={() => this.selectBoardType(`${item.tag_name}`)}
                      style={styles.ratioItemList}
                    >
                      <View style={styles.ratioItem}>
                        <View
                          class="RatioSelected"
                          style={[styles.selectRatio, this.state.boardType === `${item.tag_name}` ? styles.selectedRatio : '']}
                        />
                      </View>
                      <TextFont
                        class="ratioTextLbabel"
                        style={styles.ratioTextLbabel}
                      >
                        {item.name}
                      </TextFont>
                    </TouchableOpacity>
                    {
                      item.tag_name === 'pickup' ? <View
                        style={{
                          height: this.state.isSelectingStore === false ? 0 : 100,
                          overflow: 'hidden',
                          opacity: this.state.isSelectingStore === false ? 0 : 1
                        }}
                      >
                        <View class="selectLocation" style={styles.selectLoation}>
                          <View style={{ width: 30 }} />
                          <View>
                            <View style={{ borderColor: '#c92033', borderRadius: 7, borderWidth: 1 }}>
                              <TextFont onPress={() => this.showAddress()} style={styles.pickPlayMethod}>
                                {_.isNil(this.state.pickAddress) ? 'Chọn khu vực lấy vé' : `${this.state.pickAddress.province} - ${this.state.pickAddress.district}` }
                              </TextFont>
                            </View>
                          </View>
                        </View>
                        <View style={styles.selectLoation}>
                          <View style={{ width: 30 }} />
                          <View>
                            <View style={{ borderColor: '#c92033', borderRadius: 7, borderWidth: 1 }}>
                              <TextFont onPress={this.showAgent} style={styles.pickPlayMethod}>{_.isNil(this.state.agentName) ? 'Chọn đại lý lấy vé' : this.state.agentName}</TextFont>
                            </View>
                          </View>
                        </View>
                      </View> : null
                    }
                  </View>
                )
              })
            }
            {this.renderAddressDelivery()}
          </View>
        </FieldSet>
      </View>
    )
  }

  render() {
    const { ticketPrizeInfo } = this.props
    return (
      <View style={styles.container}>
        {Platform.OS === 'android' ? <StatusBar /> : <StatusBar barStyle={'light-content'} />}
        <PickupTicketNav onRightButton={() => this.props.navigator.pop()} imageSource={Images.logo655} jackpotNumber={parseInt(ticketPrizeInfo.prize, 10)} />
        <View class="mainContainer" style={styles.mainContainer}>
          <ScrollView style={styles.scrollViewContainer}>
            <View style={{ marginTop: 20 }}>
              <FieldSet fieldSetName="CHỌN CÁCH CHƠI">
                <View style={styles.rowContainer}>
                  <View style={{ justifyContent: 'center', paddingRight: 10 }}>
                    <TextFont style={{ color: '#222222' }}>Cách chơi</TextFont>
                  </View>
                  <View style={{ justifyContent: 'center' }}>
                    <View style={{ borderColor: '#c92033', borderRadius: 7, borderWidth: 1, flexDirection: 'row', alignItems: 'center' }}>
                      <TextFont
                        style={styles.pickPlayMethod}
                        onPress={() => {
                          this.showPlayMethod()
                        }}
                      >
                        {this.state.inclusive === 0 ? 'Vé số tự chọn' : `Bao ${this.state.inclusive} số`}
                      </TextFont>
                      <Icon name={'down'} size={8} style={{ paddingRight: 10 }} />
                    </View>
                  </View>
                </View>
                <View>
                  <TextFont style={{ color: '#5e5e5e', fontSize: 11, marginTop: 5, textAlign: 'center' }}>(*) Cơ hội
                    trúng
                    thưởng dành cho tất cả mọi người</TextFont>
                </View>
              </FieldSet>
            </View>

            <View style={{ marginTop: 20 }}>
              <FieldSet fieldSetName="CHỌN DÃY SỐ DỰ THƯỞNG">
                {this.state.inclusive === 0 ? <View>
                  <Board
                    onPress={() => this.setModalVisible(true, 0)}
                    onRemove={() => this.emptyBoard('A')}
                    filledNumber={this.state.filledNumber.A}
                    boardName="A"
                  />
                  <Board
                    onPress={() => this.setModalVisible(true, 1)}
                    onRemove={() => this.emptyBoard('B')}
                    filledNumber={this.state.filledNumber.B}
                    boardName="B"
                  />
                  <Board
                    onPress={() => this.setModalVisible(true, 2)}
                    onRemove={() => this.emptyBoard('C')}
                    filledNumber={this.state.filledNumber.C}
                    boardName="C"
                  />
                  <Board
                    onPress={() => this.setModalVisible(true, 3)}
                    onRemove={() => this.emptyBoard('D')}
                    filledNumber={this.state.filledNumber.D}
                    boardName="D"
                  />
                  <Board
                    onPress={() => this.setModalVisible(true, 4)}
                    onRemove={() => this.emptyBoard('E')}
                    filledNumber={this.state.filledNumber.E}
                    boardName="E"
                  />
                  <Board
                    onPress={() => this.setModalVisible(true, 5)}
                    onRemove={() => this.emptyBoard('F')}
                    filledNumber={this.state.filledNumber.F}
                    boardName="F"
                  />
                </View>
                  : <Board
                    onPress={() => this.setModalVisible(true, 0)}
                    onRemove={() => this.emptyBoard('A')}
                    filledNumber={this.state.filledNumber.A}
                    boardName="A"
                    inclusive={this.state.inclusive}
                  />
                }
                <View style={{ marginTop: 20, alignItems: 'center' }}>
                  <TouchableOpacity
                    style={styles.randomBoard}
                    onPress={() => {
                      this.showNumber()
                    }}
                  ><TextFont style={styles.textButton}>Chọn nhanh</TextFont></TouchableOpacity>
                </View>
              </FieldSet>
            </View>

            <View style={{ marginTop: 20 }}>
              <FieldSet fieldSetName="CHỌN SỐ KỲ THAM DỰ">
                <View class="groupSelect" style={styles.buttonGroup}>
                  <SelectButton
                    onPress={() => this.selectBuyRepeat(1)}
                    consecutiveBuy={this.state.consecutiveBuy}
                    buttonNum={1}
                  />
                  <SelectButton
                    onPress={() => this.selectBuyRepeat(2)}
                    consecutiveBuy={this.state.consecutiveBuy}
                    buttonNum={2}
                  />
                  <SelectButton
                    onPress={() => this.selectBuyRepeat(3)}
                    consecutiveBuy={this.state.consecutiveBuy}
                    buttonNum={3}
                  />
                  <SelectButton
                    onPress={() => this.selectBuyRepeat(4)}
                    consecutiveBuy={this.state.consecutiveBuy}
                    buttonNum={4}
                  />
                  <SelectButton
                    onPress={() => this.selectBuyRepeat(5)}
                    consecutiveBuy={this.state.consecutiveBuy}
                    buttonNum={5}
                  />
                  <SelectButton
                    onPress={() => this.selectBuyRepeat(6)}
                    consecutiveBuy={this.state.consecutiveBuy}
                    buttonNum={6}
                  />
                </View>
              </FieldSet>
            </View>
            {this.renderPickUpTicketType()}
            <View style={{ marginTop: 20, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}><TextFont style={{ color: '#222' }}>TỔNG SỐ TIỀN THANH TOÁN:</TextFont></View>
              <View style={{ alignItems: 'flex-end' }}>
                <TextFont style={{ color: '#c92033' }}>{numeral(totalPrize).format('0,0')} đ</TextFont>
              </View>
              <View style={{ marginLeft: 5 }}>
                <TextFont onPress={() => this.props.navigator.push({ ...Config.screen.detailMega, passProps: { ticketPrizeInfo, ticketDetail: { ...this.state, totalPrize } } })}>
                  <Icon name="question" color="#919191" size={16} />
                </TextFont>
              </View>
            </View>
            <View style={{ marginTop: 20, marginBottom: 20 }}>
              <ThreeDButton onPress={this.onPressSubmit} />
            </View>
          </ScrollView>
        </View>
        <Overlay
          visible={this.state.visibleOverlay}
          onPressOverlay={() => {
            Picker.hide()
            this.setState({ visibleOverlay: false })
          }}
        />
        <Modal
          animationType="none"
          onRequestClose={() => {
            this.setState({ modalVisible: false })
          }}
          visible={this.state.modalVisible}
          style={styles.modalPickup}
          backdrop={false}
          transparent
          position={'top'}
        >
          <Carousel
            selectedIndex={this.state.initialBoard}
            style={{ backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'center', paddingTop: 35 }}
            viewPagerStyle={{ backgroundColor: 'transparent' }}
            cards={this.renderCards()}
            onSelectedIndexChange={(currentIndexBoard) => {
              this.setState({ currentIndexBoard })
            }}
          />
        </Modal>
      </View>
    )
  }
}
PickupTicket.defaultProps = {
  filledNumber: { // props
    A: [],
    B: [],
    C: [],
    D: [],
    E: [],
    F: []
  },
  inclusive: 0 // props
}
