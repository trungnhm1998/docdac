import React, { Component } from 'react'
import { Image, ScrollView, Text, TouchableOpacity, View } from 'react-native'
import Icon2 from 'react-native-vector-icons/Ionicons'
import { Icon, TextFont } from '../../../ui'
import Symbol from '../../../ui/themes/symbol'
import styles from './pickupTicket.style'

export default class PickerBoard extends Component {
  static shouldComponentUpdate(props) {
    const { boardPosition, currentIndexBoard } = props
    return boardPosition === currentIndexBoard || boardPosition === (currentIndexBoard + 1) || boardPosition === (currentIndexBoard + 2)
  }

  constructor(props) {
    super(props)
    this.state = {
      showSymbol: false
    }
    this.showSymbol = this.showSymbol.bind(this)
    this.generateNumber = this.generateNumber.bind(this)
    this.onPressKey = this.onPressKey.bind(this)
  }

  onPressKey(boardKey, keyNumber) {
    const { onSelectNumber } = this.props
    onSelectNumber(boardKey, keyNumber)
  }

  generateNumber() {
    this.props.generateNumber()
  }

  renderNumber(filledNumber, boardKey) {
    const { boardPosition, currentIndexBoard } = this.props
    if (boardPosition === currentIndexBoard || boardPosition === (currentIndexBoard + 1) || boardPosition === (currentIndexBoard + 2)) {
      const NumberList = []
      for (let i = 1; i <= 55; i += 1) {
        const bgColor = filledNumber.indexOf(i) !== -1 ? '#f9a936' : '#fff'
        const color = filledNumber.indexOf(i) !== -1 ? '#fff' : '#000'
        const symbol = this.state.showSymbol ? <Image source={Symbol[`${i - 1}`]} style={styles.symbol} /> : null

        NumberList.push(
          <TouchableOpacity
            onPress={() => this.onPressKey(boardKey, i)}
            key={i}
            style={[styles.pickNumberSymbol, { backgroundColor: bgColor }]}
            activeOpacity={0.8}
          >
            <View>
              {symbol}
              <TextFont
                style={{ color, textAlign: 'center' }}
              >
                {i.toString()}
              </TextFont>
            </View>
          </TouchableOpacity>
        )
      }
      return NumberList
    }
    return null
  }

  showSymbol() {
    this.setState({
      showSymbol: !this.state.showSymbol
    })
  }

  render() {
    const { boardPosition, onPress, filledNumber, boardKey } = this.props
    return (
      <View style={styles.slideNumberPicker}>
        <View style={styles.headerNumberPicker}>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <TextFont style={styles.boardPos}>{this.props.inclusive === 0 ? `BOARD ${boardPosition}/6` : ''}</TextFont>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => this.props.emptyBoard()}
              underlayColor="#e39d35"
              style={styles.buttonNumberPicker}
            >
              <Icon name="trash" color="#fff" size={20} />
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={this.generateNumber}
              activeOpacity={0.8}
              style={styles.buttonNumberPicker}
            >
              <Text style={{ color: '#fff' }}>
                Chọn nhanh
              </Text>
            </TouchableOpacity>
          </View>
          <View >
            <TouchableOpacity
              onPress={() => onPress()}
              activeOpacity={0.8}
              style={[
                styles.buttonNumberPicker, {
                  backgroundColor: 'transparent',
                  alignItems: 'flex-end'
                }]}
            >
              <Icon name="close" color="#fff" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.roundNumberPicker}>
          <TextFont
            style={styles.pickerCount}

          >Bạn đã chọn {filledNumber.length}/{this.props.inclusive === 0 ? 6 : this.props.inclusive} số</TextFont>
          <ScrollView>
            <View style={styles.numberPicker}>
              { this.renderNumber(filledNumber, boardKey) }
            </View>
          </ScrollView>
          <TouchableOpacity style={styles.showSymbol} onPress={this.showSymbol} activeOpacity={0.8}>
            <View style={{ flexDirection: 'row' }}>
              <View style={styles.checkButton}>
                {this.state.showSymbol ? <Icon2 name="md-checkmark" size={18} /> : null}
              </View>
              <View style={{ justifyContent: 'center' }}>
                <TextFont style={styles.showSymbolLabel}>Hiển thị con vật dân gian</TextFont>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
PickerBoard.defaultProps = {
  inclusive: '6',
  showSymbol: false
}
