import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import RegisterAccountKit from './RegisterAccountKit'
import Config from '../../../screens/config'
import { Themes } from '../../../ui'
import * as AccountAction from '../../../redux/account/account.actions'

class RegisterAccountKitContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.navToProfile = this.navToProfile.bind(this)
  }

  componentWillMount() {
  }

  navToProfile() {
    this.props.navigator.resetTo({
      ...Config.screen.profile,
      passProps: { isFirstLogin: true }
    })
  }

  onPressSubmit(value) {
    const { accountAction, phone, countryCode, token } = this.props
    accountAction.registerAccountKit(phone, countryCode, value.password, token, {
      nextScreen: this.navToProfile
    })
  }

  render() {
    return (
      <RegisterAccountKit
        userPhone={this.props.phone}
        onPressSubmit={this.onPressSubmit}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterAccountKitContainer)
