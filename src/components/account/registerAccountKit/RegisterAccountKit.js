import React, { Component } from 'react'
import { View, Alert } from 'react-native'
import { Themes, TextFont, TcombForm, Button } from '../../../ui'
import styles from './registerAccountKit.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const userAccount = t.struct({
  passWord: t.String,
  repeatPassWord: t.String
})

class RegisterAccountKit extends Component {

  constructor(props) {
    super(props)
    this.state = {}
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.userPhone = this.props.userPhone
  }

  onPressSubmit = () => {
    const value = this.form.getValue()
    if (value) {
      if (value.passWord !== value.repeatPassWord) {
        Alert.alert('Thông báo', 'Mật khẩu xác nhận không khớp')
      } else {
        this.props.onPressSubmit({ password: value.passWord, repeatPassword: value.repeatPassWord })
      }
    }
  }

  renderForm = () => {
    const layout = (locals) => {
      return (
        <View style={{ marginTop: 15 }}>
          <TextFont style={{ marginBottom: 10 }}>Thiết lập mật khẩu cho TK {this.userPhone}</TextFont>
          <View>{locals.inputs.passWord}</View>
          <View>{locals.inputs.repeatPassWord}</View>
        </View>
      )
    }
    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        passWord: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập mật khẩu',
          placeholderTextColor: '#E7E7E7',
          secureTextEntry: true
        },
        repeatPassWord: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập lại mật khẩu',
          placeholderTextColor: '#E7E7E7',
          secureTextEntry: true
        }
      }
    }
    return (
      <Form ref={(form) => {
        this.form = form
      }} type={userAccount} options={options} />
    )
  }

  render() {
    return (
      <View style={[Themes.styleGB.container]}>
        {this.renderForm()}
        <Button onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Tiếp tục</Button>
      </View>
    )
  }

}
RegisterAccountKit.propTypes = {}
export default RegisterAccountKit
