import { StyleSheet, PixelRatio ,Platform} from 'react-native'
import { Themes } from '../../../ui'

const styles = StyleSheet.create({
  loginBg: {
    flex: 1,
    width: Themes.Metrics.screenWidth,
    backgroundColor: 'transparent',
    paddingHorizontal: Themes.Metrics.paddingHorizontal,
  },
  btnTouch: {
    backgroundColor: 'red',
    height: 100,
    width: Themes.Metrics.screenWidth
  },
  loginContentTop: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  txtForgetPass: {
    textDecorationLine: 'underline',
    alignSelf: 'center',
    marginTop: 20,
  },
  txtBottom: {
    position: 'absolute',
    bottom: 10,
    left: 0,
    right: 0,
    textAlign: 'center',
    color: '#8b8b8b',
  }
})

export default styles;
