import { StyleSheet, PixelRatio } from 'react-native'
import { Themes } from '../../../ui'

const styles = StyleSheet.create({
  loginBg: {
    flex: 1,
    width: null,
    height: null,
    backgroundColor: 'transparent',
    paddingHorizontal: Themes.Metrics.paddingHorizontal
  },
  btnTouch: {
    backgroundColor: 'red',
    height: 100,
    width: Themes.Metrics.screenWidth
  },
  loginContentTop: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  txtForgetPass: {
    textDecorationLine: 'underline',
    alignSelf: 'center',
    marginTop: 20
  },
  txtBottom: {
    position: 'absolute',
    bottom: 10,
    left: 0,
    right: 0,
    textAlign: 'center',
    color: '#8b8b8b'
  },
  modal: {
    width: '85%',
    height: null,
    maxHeight: '80%'
  },
  btnTouchModal: {
    padding: 15,
    backgroundColor: '#E7E7E7',
    borderWidth: 1 / PixelRatio.get(),
    borderColor: '#ccc'
  }
})

export default styles
