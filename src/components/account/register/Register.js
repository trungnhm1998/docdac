import React, { Component } from 'react'
import { View, Alert } from 'react-native'
import { Themes, TextFont, TcombForm, Button } from '../../../ui'
import styles from './register.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const userAccount = t.struct({
  pinCode: t.String,
  passWord: t.String,
  repeatPassWord: t.String
})

class Register extends Component {

  constructor(props) {
    super(props)
    this.state = {}
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onPressResendOTP = this.onPressResendOTP.bind(this)
    this.userPhone = this.props.userPhone
  }

  onPressSubmit = () => {
    const value = this.form.getValue()
    if (value) {
      if (value.passWord !== value.repeatPassWord) {
        Alert.alert('Thông báo', 'Mật khẩu xác nhận không khớp')
      } else {
        this.props.onPressSubmit({ otp: value.pinCode, password: value.passWord, repeatPassword: value.repeatPassWord })
      }
    }
  }

  onPressResendOTP(){
    this.props.onPressResendOTP()
  }

  renderForm = () => {
    const layout = (locals) => {
      return (
        <View style={{ marginTop: 15 }}>
          <TextFont style={{ marginBottom: 10 }}>Nhập mã PIN gửi về SĐT {this.userPhone}</TextFont>
          <View>{locals.inputs.pinCode}</View>
          <View>{locals.inputs.passWord}</View>
          <View>{locals.inputs.repeatPassWord}</View>
        </View>
      )
    }
    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        pinCode: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập mã pin',
          placeholderTextColor: '#E7E7E7',
          keyboardType: 'phone-pad',
          config: {
            label: ''
          }
        },
        passWord: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập mật khẩu',
          placeholderTextColor: '#E7E7E7',
          secureTextEntry: true
        },
        repeatPassWord: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập lại mật khẩu',
          placeholderTextColor: '#E7E7E7',
          secureTextEntry: true
        }
      }
    }
    return (
      <Form ref={(form) => {
        this.form = form
      }} type={userAccount} options={options} />
    )
  }

  render() {
    return (
      <View style={[Themes.styleGB.container]}>
        {this.renderForm()}
        <Button onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Tiếp tục</Button>
        <TextFont onPress={this.onPressResendOTP} style={[styles.txtForgetPass]}>Gửi lại PIN</TextFont>
      </View>
    )
  }

}
Register.propTypes = {}
export default Register
