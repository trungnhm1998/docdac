import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Register from './Register'
import Config from '../../../screens/config'
import { Themes } from '../../../ui'
import * as AccountAction from '../../../redux/account/account.actions'

class RegisterContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onPressResendOTP = this.onPressResendOTP.bind(this)
    this.navToProfile = this.navToProfile.bind(this)
  }

  componentWillMount() {
  }

  navToProfile() {
    this.props.navigator.resetTo({
      ...Config.screen.profile,
      passProps: { isFirstLogin: true }
    })
  }

  onPressSubmit(value) {
    const { accountAction, phone } = this.props
    accountAction.register(phone, value.password, value.otp, {
      nextScreen: this.navToProfile
    })
  }

  onPressResendOTP() {
    const { accountAction, phone } = this.props
    accountAction.reSendOTPRegister(phone)
  }

  render() {
    return (
      <Register
        userPhone={this.props.phone}
        onPressSubmit={this.onPressSubmit}
        onPressResendOTP={this.onPressResendOTP}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterContainer)
