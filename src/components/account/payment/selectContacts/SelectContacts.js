import React, { Component } from 'react'
import {
  View,
  ListView,
  TouchableNativeFeedback,
  TouchableHighlight,
  Platform,
  TextInput
} from 'react-native'
import { Themes, TextFont } from '../../../../ui'
import styles from './SelectContacts.style'

class SelectContacts extends Component {
  constructor(props) {
    super(props)
    this.onRenderRowContact = this.onRenderRowContact.bind(this)
  }

  componentWillMount() {
    this.createDataSource(this.props.contacts)
  }

  createDataSource(data) {
    const filteredData = data.filter(row => row.phoneNumbers.length > 0)
    const sortedData = filteredData.sort((a, b) => {
      let nameA = a.givenName.toUpperCase(); // ignore upper and lowercase
      let nameB = b.givenName.toUpperCase(); // ignore upper and lowercase

      return nameA > nameB ? 1 : -1
    })
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })

    const dataSource = ds.cloneWithRows(sortedData);
    this.setState({ dataSource: dataSource })
  }

  onContactPress(row) {
    this.props.onContactPress(row)
    this.props.navigator.pop()
  }

  onRenderRowContact(rowData) {
    // console.log(rowData)
    const { givenName, familyName, phoneNumbers } = rowData
    if (Platform.OS === 'android') {
      return (
        <TouchableNativeFeedback onPress={() => this.onContactPress(rowData)}>
          <View style={styles.rowContainer}>
            <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 5 }}>
              <TextFont color="#000" font={Themes.Fonts.type.Medium} size={16}>{`${givenName ? givenName : '' } ${familyName ? familyName : ''}`}</TextFont>
              
              <View style={{ paddingTop: 10, flexDirection: 'row' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <TextFont style={{ paddingLeft: 5 }} size={13} font={Themes.Fonts.type.Light} color="#6e6e6e">{phoneNumbers[0].number}</TextFont>
                </View>
              </View>
            </View>
          </View>
        </TouchableNativeFeedback>
      )
    }
    return (
      <TouchableHighlight onPress={() => this.onContactPress(rowData)}>
        <View style={styles.rowContainer}>
          <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 5 }}>
            <TextFont color="#000" font={Themes.Fonts.type.Medium} size={16}>{`${givenName ? givenName : '' } ${familyName ? familyName : ''}`}</TextFont>
            
            <View style={{ paddingTop: 10, flexDirection: 'row' }}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <TextFont style={{ paddingLeft: 5 }} size={13} font={Themes.Fonts.type.Light} color="#6e6e6e">{phoneNumbers[0].number}</TextFont>
              </View>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    )
  }

  renderContactList() {
    const { contacts } = this.props
    // console.log(this.props)
    // console.log(contacts)
    // console.log(this.dataSource)
    // if (contacts.length <= 1) {
    //   return (
    //     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
    //       <TextFont color={Themes.Colors.navColor} style={Themes.styleGB.centerContent}>Không có dữ liệu.</TextFont>
    //     </View>
    //   )
    // }

    return (
      <ListView
        dataSource={this.state.dataSource}
        scrollEventThrottle={16}
        enableEmptySections
        renderRow={this.onRenderRowContact}
      />
    )
  }

  onSearchInputChange(text) {
    const contacts = this.props.contacts
    const filterContacts = contacts.filter((row) => {
      const { givenName, familyName } = row
      let flag = false
      if (givenName) {
        flag = givenName.includes(text)
      }

      if (familyName && !flag) {
        flag = familyName.includes(text)
      }

      return flag
    })

    // console.log(filterContacts)
    this.createDataSource(filterContacts)
  }

  renderSearchInput() {
    return (
      <TextInput
        style={styles.searchInput}
        placeholder="Tìm kiếm"
        onChangeText={(text) => this.onSearchInputChange(text) }
      />
    )
  }

  render() {
    return (
      <View style={[styles.container]}>
        {this.renderSearchInput()}
        {this.renderContactList()}
      </View>
    )
  }
}

export default SelectContacts