import React, { Component } from 'react'
import { ActivityIndicator } from 'react-native'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import { connect } from 'react-redux'
import moment from 'moment'
import PaymentHistory from './PaymentHistory'
import Config from '../../../../screens/config'
import { Colors } from '../../../../ui/themes'
import * as AccountAction from '../../../../redux/account/account.actions'
import { dateFormatList } from '../../../../utils'

const PREVIOUS_DAY_THRESHOLD = 60

class PaymentHistoryContainer extends Component {
  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB,
    statusBarColor: Colors.navColor
  }

  constructor(props) {
    super(props)

    const endDate = moment()

    this.state = {
      isFirstLoad: true,
      isLastPage: false,
      endDate: endDate.format(dateFormatList.SERVER_DATE),
      startDate: endDate.day(PREVIOUS_DAY_THRESHOLD * -1).format(dateFormatList.SERVER_DATE)
    }

    this.loadMoreHistoryMoney = this.loadMoreHistoryMoney.bind(this)
    this.refreshingHistory = this.refreshingHistory.bind(this)
  }

  componentWillMount() {
    this.refreshingHistory()
  }

  componentWillReceiveProps(props) {
    const { accountInfo } = props.payload
    const { moneyHistory } = accountInfo

    this.setState({ isFirstLoad: false, isLastPage: moneyHistory.last_page })
  }


  loadMoreHistoryMoney() {
    const { accountAction, payload } = this.props
    const { accountInfo } = payload
    const { startDate, endDate } = this.state

    if (accountInfo.moneyHistory.last_page === false ||
        accountInfo.transferHistory.last_page === false ||
        accountInfo.rechargeHistory.last_page === false) {
      const data = {
        page: this.props.currentPage === 0 ?
          accountInfo.rechargeHistory.nextPage :
          this.props.currentPage === 1 ?
            accountInfo.moneyHistory.nextPage :
            accountInfo.transferHistory.nextPage,
        from_date: startDate,
        to_date: endDate
      }
      switch (this.props.currentPage) {
        case 0:
          accountAction.rechargeHistory(data, accountInfo.accessToken)
          break
        case 1:
          accountAction.getWithdrawHistory(data, accountInfo.accessToken)
          break
        case 2:
          accountAction.getTransferHistory(data, accountInfo.accessToken)
          break
        default:
          break
      }
    }
  }

  refreshingHistory() {
    const { accountAction, payload } = this.props
    const { startDate, endDate } = this.state
    const accountInfo = payload.accountInfo

    const data = {
      page: 1,
      from_date: startDate,
      to_date: endDate
    }
    switch (this.props.currentPage) {
      case 0:
        accountAction.rechargeHistory(data, accountInfo.accessToken)
        break
      case 1:
        accountAction.getWithdrawHistory(data, accountInfo.accessToken)
        break
      case 2:
        accountAction.getTransferHistory(data, accountInfo.accessToken)
        break
      default:
        break
    }
  }

  render() {

    if (this.state.isFirstLoad) {
      return (<ActivityIndicator style={{ marginTop: 10, marginBottom: 10 }} />)
    }

    const { accountInfo } = this.props.payload
    // const { data: moneyHistoryData } = accountInfo.moneyHistory
    console.log('this.props', this.props)
    let historyData = accountInfo.withDrawHistory.data
    switch (this.props.currentPage) {
      case 0:
        historyData = accountInfo.rechargeHistory.data
        break
      case 1:
        historyData = accountInfo.withDrawHistory.data
        break
      case 2:
        historyData = accountInfo.transferHistory.data
        break
      default:
        break
    }

    return (
      <PaymentHistory
        navigator={this.props.navigator}
        currentPage={this.props.currentPage}
        listMoneyHistory={historyData}
        loadMoreHistoryMoney={this.loadMoreHistoryMoney}
        refreshingHistory={this.refreshingHistory}
        { ...this.state }
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

PaymentHistoryContainer.defaultProps = {
  payload: {
    accountInfo: {
      moneyHistory: {
        data: []
      },
      rechargeHistory: {
        data: []
      },
      withDrawHistory: {
        data: []
      }
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentHistoryContainer)
