import React, { Component } from 'react'
import { View, ListView, Image, ActivityIndicator, Platform, RefreshControl } from 'react-native'
import _ from 'lodash'
import numeral from 'numeral'
import { DefaultTabBar } from 'react-native-scrollable-tab-view'
import { Themes, TextFont, Loading } from '../../../../ui'
import styles from './paymenthistory.style'
import { formatServerTimeToShow } from '../../../../utils'

class PaymentHistory extends Component {

  constructor(props) {
    super(props)
    this.state = {}
    this.renderMoneyHistory = this.renderMoneyHistory.bind(this)
    this.onRenderRowHistory = this.onRenderRowHistory.bind(this)
    this.onHistoryEndReached = this.onHistoryEndReached.bind(this)
    this.onRenderRechargeRowHistory = this.onRenderRechargeRowHistory.bind(this)
    this.onRenderTransferRowHistory = this.onRenderTransferRowHistory.bind(this)
  }

  onRenderRowHistory(rowData) {
    const { withdraw_id, money, bank_account_name, created_date, state } = rowData
    return (
      <View style={styles.rowContainer}>
        <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 5 }}>
          <TextFont color="#000" font={Themes.Fonts.type.Medium} size={16}>{bank_account_name}</TextFont>

          <View style={{ paddingTop: 10, flexDirection: 'row'}}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image source={Themes.Images.transIdIcon} />
              <TextFont style={{ paddingLeft: 5 }} size={13} font={Themes.Fonts.type.Light} color="#6e6e6e">{withdraw_id}</TextFont>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 20}}>
              <Image source={Themes.Images.timeIcon} />
              <TextFont style={{ paddingLeft: 5 }} size={13} font={Themes.Fonts.type.Light} color="#6e6e6e">{formatServerTimeToShow(created_date)}</TextFont>
            </View>
          </View>
        </View>
        <View style={{ alignItems: 'flex-end', flexDirection: 'column' }}>
          <TextFont style={styles.txtNumMoney} size={16} color={money > 0 ? '#399F2E' : Themes.Colors.navColor}>{numeral(money).format('0,0')}đ</TextFont>
          <TextFont style={styles.txtNumMoney} size={13} color={state === "2" ? '#3498db' : '#e74c3c'}>{state === "2" ? 'đã duyệt' : 'chờ duyệt'}</TextFont>
        </View>
      </View>
    )
  }

  onRenderRechargeRowHistory(rowData) {
    const { trans_id, money, reason_name, time } = rowData
    return (
      <View style={styles.rowContainer}>
        <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 5 }}>
          <TextFont color="#000" font={Themes.Fonts.type.Medium} size={16}>{reason_name}</TextFont>

          <View style={{ paddingTop: 10, flexDirection: 'row'}}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image source={Themes.Images.transIdIcon} />
              <TextFont style={{ paddingLeft: 5 }} size={13} font={Themes.Fonts.type.Light} color="#6e6e6e">{trans_id}</TextFont>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 20}}>
              <Image source={Themes.Images.timeIcon} />
              <TextFont style={{ paddingLeft: 5 }} size={13} font={Themes.Fonts.type.Light} color="#6e6e6e">{formatServerTimeToShow(time)}</TextFont>
            </View>
          </View>
        </View>
        <View style={{ alignItems: 'flex-end'}}>
          <TextFont style={styles.txtNumMoney} size={16} color={money > 0 ? '#399F2E' : Themes.Colors.navColor}>{numeral(money).format('0,0')}đ</TextFont>
        </View>
      </View>
    )
  }

  onRenderTransferRowHistory(rowData) {
    const { trans_id, money, reason, created_date, receiver_phone } = rowData
    return (
      <View style={styles.rowContainer}>
        <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 5 }}>
          <TextFont color="#000" font={Themes.Fonts.type.Medium} size={16}>{receiver_phone}</TextFont>

          <View style={{ paddingTop: 10, flexDirection: 'row'}}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image source={Themes.Images.transIdIcon} />
              <TextFont style={{ paddingLeft: 5 }} size={13} font={Themes.Fonts.type.Light} color="#6e6e6e">{trans_id}</TextFont>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 20}}>
              <Image source={Themes.Images.timeIcon} />
              <TextFont style={{ paddingLeft: 5 }} size={13} font={Themes.Fonts.type.Light} color="#6e6e6e">{formatServerTimeToShow(created_date)}</TextFont>
            </View>
          </View>
        </View>
        <View style={{ alignItems: 'flex-end'}}>
          <TextFont style={styles.txtNumMoney} size={16} color={money > 0 ? '#399F2E' : Themes.Colors.navColor}>{numeral(money).format('0,0')}đ</TextFont>
        </View>
      </View>
    )
  }

  onHistoryEndReached = () => {
    if (!this.props.isLastPage) {
        this.props.loadMoreHistoryMoney()
    }
  }

  renderMoneyHistory = () => {
    const { listMoneyHistory, isFirstLoad, refreshingHistory, currentPage } = this.props

    if ((listMoneyHistory.page_data && listMoneyHistory.page_data.length === 0) || listMoneyHistory.length === 0 ) {
      if (isFirstLoad === true) {
        return (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <TextFont color={Themes.Colors.navColor} style={Themes.styleGB.centerContent}>Bạn có giao dịch nào</TextFont>
          </View>
        )
      }
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <TextFont color={Themes.Colors.navColor} style={Themes.styleGB.centerContent}>Không có dữ liệu.</TextFont>
        </View>
      )
    }
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const dataSource = ds.cloneWithRows(listMoneyHistory.page_data ? listMoneyHistory.page_data : [])
    return (
      <View style={{ flex: 1 }}>

        <View style={{ flexDirection: 'row', marginTop: 10 }}>
          <View style={{ flex: 1, alignItems: 'flex-start' }}>
            <TextFont style={{ paddingLeft: 10 }} color="#000" size={22} font={Themes.Fonts.type.Bold}>Thông tin</TextFont>
          </View>
          <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <TextFont style={{ paddingRight: 10 }} color="#000" size={22} font={Themes.Fonts.type.Bold}>Số tiền</TextFont>
          </View>
        </View>
        <ListView
          dataSource={dataSource}
          scrollEventThrottle={16}
          enableEmptySections
          refreshControl={
            <RefreshControl
              refreshing={false}
              onRefresh={refreshingHistory}
            />
          }
          renderRow={currentPage === 0 ? this.onRenderRechargeRowHistory : currentPage === 1 ? this.onRenderRowHistory : this.onRenderTransferRowHistory}
          onEndReached={this.onHistoryEndReached}
          onEndReachedThreshold={50}
        />
      </View>
    )
  }

  render() {
    return (
      <View style={[styles.container]}>
        {this.renderMoneyHistory()}
      </View>
    )
  }
}
PaymentHistory.propTypes = {}
export default PaymentHistory
