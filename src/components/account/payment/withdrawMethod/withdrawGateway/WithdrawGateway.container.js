import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import WithdrawGateway from './WithdrawGateway'
import Config from '../../../../../screens/config'
import { actions as PaymentAction } from '../../../../../redux/payment'

class WithdrawGatewayContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.withdrawConfirm = this.withdrawConfirm.bind(this)
  }

  withdrawConfirm() {
    this.props.navigator.push(Config.screen.withdrawGatewayConfirm)
  }

  onPressSubmit(value) {
    const { paymentAction } = this.props
    paymentAction.withdrawMomo({ momo_id: value.account, money: value.money }, { nextScreen: this.withdrawConfirm })
  }

  render() {
    const { payment_id, payment_name, prices, fee_description } = this.props
    return (
      <WithdrawGateway
        onPressSubmit={this.onPressSubmit}
        listPrice={prices}
        paymentId={payment_id}
        paymentName={payment_name}
        feeDescription={fee_description}
        navigator={this.props.navigator}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    paymentAction: bindActionCreators(PaymentAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WithdrawGatewayContainer)
