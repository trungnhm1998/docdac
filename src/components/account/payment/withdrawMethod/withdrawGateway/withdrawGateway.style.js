import { StyleSheet, PixelRatio } from 'react-native'
import { Themes } from '../../../../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: Themes.Metrics.paddingHorizontal
  },
  momoImage: {
    width: 70,
    height: 70,
    alignSelf: 'center',
    marginVertical: 10
  },
  txtNote: {
    marginBottom: 10,
    marginTop: -5
  },
  modal: {
    width: '85%',
    height: null,
    maxHeight: '80%'
  },
  btnTouchModal: {
    padding: 15,
    backgroundColor: '#E7E7E7',
    borderWidth: 1 / PixelRatio.get(),
    borderColor: '#ccc'
  }
})

export default styles
