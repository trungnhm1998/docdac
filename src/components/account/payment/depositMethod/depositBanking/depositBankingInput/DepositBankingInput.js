import React, { Component } from 'react'
import Accordion from 'react-native-collapsible/Accordion'
import { View, Image, ScrollView } from 'react-native'
import { Themes, TextFont } from '../../../../../../ui'
import styles from './depositbankinginput.style'

class DepositBankingInput extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isDateTimePickerVisible: false,
      activeSection: false,
      collapsed: true,
      formValue: {
        cash: '',
        sdt: ''
      }
    }
    this.renderContent = this.renderContent.bind(this)
  }

  setSection(section) {
    this.setState({ activeSection: section })
  }

  renderHeader(section, i, isActive) {
    return (
      <View duration={400} style={[styles.header, isActive ? styles.active : styles.inactive]} transition="backgroundColor">
        <TextFont style={styles.headerText}>{section.bank_name}</TextFont>
      </View>
    )
  }

  renderContent(section, i, isActive) {
    const { sms_syntax } = this.props.bankInfo
    return (
      <View duration={400} style={[styles.content, isActive ? styles.active : styles.inactive]} transition="backgroundColor">
        <View style={[styles.rowBank]}>
          <TextFont font={Themes.Fonts.type.Light}>Chi nhánh:</TextFont>
          <TextFont style={[styles.txtStore]}>{section.branch_name}</TextFont>
        </View>
        <View style={[styles.rowBank]}>
          <TextFont font={Themes.Fonts.type.Light}>Chủ tài khoản:</TextFont>
          <TextFont font={Themes.Fonts.type.Bold} style={[styles.txtStore]}>{section.account_name}</TextFont>
        </View>
        <View style={[styles.rowBank]}>
          <TextFont font={Themes.Fonts.type.Light}>Số tài khoản:</TextFont>
          <TextFont font={Themes.Fonts.type.Bold} style={[styles.txtStore]}>{section.account_numbers}</TextFont>
        </View>
        <View style={[styles.rowBank]}>
          <TextFont font={Themes.Fonts.type.Light}>Nội dung chuyển tiền:</TextFont>
          <TextFont font={Themes.Fonts.type.Bold} style={[styles.txtStore]}>{sms_syntax}</TextFont>
        </View>
      </View>
    )
  }

  render() {
    return (
      <ScrollView>
        <View style={[styles.container]}>
          <Image source={Themes.Images.netBankingIcon} style={styles.momoImage} />
          <TextFont font={Themes.Fonts.type.Bold} style={Themes.styleGB.textCenter} color={Themes.Colors.navColor}>Chuyển khoản ngân hàng</TextFont>
          {/* <TextFont style={[styles.txtTax]} color={Themes.Colors.navColor}>Phí dịch vụ là 10% khi nạp dưới 1.000.000đ</TextFont> */}
          <TextFont style={[styles.txtTitleBank]}>Chọn ngân hàng: </TextFont>
          <Accordion
            underlayColor="transparent"
            activeSection={this.state.activeSection}
            sections={this.props.bankInfo.banks}
            renderHeader={this.renderHeader}
            renderContent={this.renderContent}
            duration={400}
            onChange={this.setSection.bind(this)}
          />
        </View>
      </ScrollView>
    )
  }


}
DepositBankingInput.propTypes = {}
export default DepositBankingInput
