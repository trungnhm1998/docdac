import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { NativeModules } from 'react-native'
import DepositGatewayCard from './DepositGatewayCard'
import { PaymentGateway } from '../../../../../../configs/constant.config'
import Config from '../../../../../../screens/config'
import { Navigation } from '../../../../../../screens/navigation'
import { actions as PaymentAction } from '../../../../../../redux/payment'

class DepositGatewayCardContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.depositSuccess = this.depositSuccess.bind(this)
  }

  depositSuccess(data) {
    this.props.navigator.resetTo({
      ...Config.screen.depositGatewaySuccess,
      passProps: { amount: data.amount, paymentType: 'card' }
    })
  }

  async onPressSubmit(value) {
    const { serial, pin, telcoId } = value
    if (telcoId === PaymentGateway.CARD_GATE) {
      this.props.paymentAction.paymentGate({
        card_serial: serial,
        card_pin: pin
      }, {
        nextScreen: this.depositSuccess
      })
    } else {
      this.props.paymentAction.paymentCard({
        card_serial: serial,
        card_pin: pin,
        payment_id: telcoId
      }, {
        nextScreen: this.depositSuccess
      })
    }
  }

  render() {
    const { payment_id, payment_name, telco, fee_description } = this.props

    return (
      <DepositGatewayCard
        onPressSubmit={this.onPressSubmit}
        listTelco={telco}
        paymentId={payment_id}
        paymentName={payment_name}
        feeDescription={fee_description}
        navigator={this.props.navigator}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    paymentAction: bindActionCreators(PaymentAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DepositGatewayCardContainer)
