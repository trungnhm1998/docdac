import React, { Component } from 'react'
import { View, Platform, Image, ScrollView, TouchableWithoutFeedback, Keyboard, TouchableOpacity, Alert } from 'react-native'
import Modal from 'react-native-modalbox'
import KeyboardSpacer from 'react-native-keyboard-spacer'
import { Themes, TextFont, Button, TcombForm } from '../../../../../../ui'
import styles from './depositGatewayCard.style'
import _ from 'lodash'

const t = require('tcomb-form-native')
const Form = t.form.Form
const payment = t.struct({
  telco: t.String,
  serial: t.String,
  pin: t.String
})

class DepositGatewayCard extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isDateTimePickerVisible: false,
      value: {
        telco: null,
        serial: null,
        pin: null
      }
    }
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  onChange(value) {
    this.setState({ value })
  }

  onPressSubmit = () => {
    const value = this.form.getValue()
    if (value) {
      this.props.onPressSubmit({ ...value, telcoId: this.state.telcoId })
    } else {
      Alert.alert('Thông báo', 'Bạn vui lòng nhập đầy đủ thông tin !')
    }
  }

  setValueForm = (key, value, id, modal) => {
    const newState = {}
    newState[`${key}`] = value

    this.setState({
      value: {
        ...this.state.value,
        ...newState
      },
      telcoId: id
    })
    this.refs[`${modal}`].close()
  }

  renderModalMoney = () => {
    const dataSelect = this.props.listTelco
    return (
      <Modal animationDuration={0} swipeToClose={false} style={[styles.modal]} position={'center'} ref={'modalMoney'}>
        <ScrollView bounces={false} contentContainerStyle={{ justifyContent: 'center' }}>
          {
            _.map(dataSelect, (data, index) => {
              const textValue = `${data.payment_name} (Phí ${parseFloat(data.fee) * 100}%)`
              return (
                <TouchableOpacity key={index} activeOpacity={0.7} onPress={() => this.setValueForm('telco', textValue, data.payment_id, 'modalMoney')} style={[styles.btnTouchModal]}>
                  <TextFont color={data.payment_id === this.state.telcoId ? Themes.Colors.navColor : null}>{textValue}</TextFont>
                </TouchableOpacity>
              )
            })
          }
        </ScrollView>
      </Modal>
    )
  }

  onFocusForm = (value) => {
    this.refs[`${value}`].open()
  }

  renderForm = () => {
    const { paymentName, feeDescription } = this.props

    const layout = (locals) => {
      return (
        <View style={{ marginTop: 10 }}>
          <View style={{ marginBottom: 15 }}>{locals.inputs.telco}</View>
          {/* <TextFont style={styles.txtNote} size={12} color={Themes.Colors.navColor}>* {feeDescription}</TextFont>*/}
          <View>{locals.inputs.serial}</View>
          <View style={{ marginBottom: 10 }}>{locals.inputs.pin}</View>
        </View>
      )
    }
    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        telco: {
          factory: TcombForm.InputModalRightIcon,
          placeholder: 'Chọn loại thẻ cào',
          onFocus: () => this.onFocusForm('modalMoney'),
          placeholderTextColor: '#E7E7E7',
          config: {
            currency: 'đ',
            nameIcon: 'down',
            IconSize: 7,
            IconColor: '#bcbcbc',
            labelStyle: {
              marginLeft: 0,
              color: '#222424'
            }
          }
        },
        serial: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập số serial'
        },
        pin: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập số pin'
        }
      }
    }
    return (
      <Form
        ref={(comp) => {
          this.form = comp
        }}
        type={payment}
        options={options}
        value={this.state.value}
        onChange={this.onChange}
      />
    )
  }

  render() {
    const { paymentId } = this.props
    const iconName = _.toLower(`icon${paymentId}`)

    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={[styles.container]}>
          <Image source={Themes.Images[iconName]} style={styles.momoImage} />
          {this.renderForm()}
          <Button onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Nạp Tiền</Button>
          {this.renderModalMoney()}
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
DepositGatewayCard.propTypes = {}
export default DepositGatewayCard
