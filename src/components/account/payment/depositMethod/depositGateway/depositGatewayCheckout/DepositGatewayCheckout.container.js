import React from 'react'
import { View, WebView, Platform } from 'react-native'
import Config from '../../../../../../screens/config'
import styles from './depositGatewayCheckout.styles'

class DepositGatewayCheckoutContainer extends React.Component {
  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  constructor(props) {
    super(props)
    this.state = {
      value: {}
    }
    this.onMessage = this.onMessage.bind(this)
  }

  onMessage(event) {
    try {
      const body = event.nativeEvent.data
      if (body.indexOf('{"code"') === 0) {
        const result = JSON.parse(body)
        if (result.code === 1) {
          this.props.navigator.resetTo({
            ...Config.screen.depositGatewaySuccess,
            passProps: { amount: result.data.amount, paymentType: '123pay' }
          })
        } else {
          this.props.navigator.resetTo(Config.screen.depositGatewayFail)
        }
      }
    } catch (error) {
      if (__DEV__) {
        console.log(error)
      }
    }
  }

  render() {
    const { uri } = this.props
    
    let jsCode = "window.postMessage(document.getElementsByTagName('body')[0].textContent);"
    if (Platform.OS === 'android') {
      jsCode = "window.__REACT_WEB_VIEW_BRIDGE.postMessage(document.getElementsByTagName('body')[0].textContent);"
    }

    console.log('----- Gateway Checkout: ', uri)

    return (
      <View style={styles.container}>
        <WebView
          ref={(component) => {
            this.webView = component
          }}
          automaticallyAdjustContentInsets={false}
          javaScriptEnabled
          decelerationRate="normal"
          startInLoadingState
          scalesPageToFit
          source={{ uri }}
          onMessage={this.onMessage}
          injectedJavaScript={jsCode}
          javaScriptEnabledAndroid
        />
      </View>
    )
  }
}

export default DepositGatewayCheckoutContainer
