import React, { Component } from 'react'
import { Platform } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import DepositGatewaySuccess from './DepositGatewaySuccess'
import Config from '../../../../../../screens/config'
import { actions as AccountAction } from '../../../../../../redux/account'
import { Navigation } from '../../../../../../screens/navigation'

class DepositGatewaySuccessContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
  }

  onPressSubmit() {
    const { accountAction, payload } = this.props
    const accountInfo = payload.accountInfo
    accountAction.getUserInfoDocdac(accountInfo.accessToken, Platform.OS, 1, '', () => {
      Navigation.navToHome()
    })
  }

  render() {
    const { amount, paymentType } = this.props
    return (
      <DepositGatewaySuccess
        onPressSubmit={this.onPressSubmit}
        paymentType={paymentType}
        amount={amount}
        navigator={this.props.navigator}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DepositGatewaySuccessContainer)
