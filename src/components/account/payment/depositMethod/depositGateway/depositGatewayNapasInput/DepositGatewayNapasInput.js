import React, { Component } from 'react'
import { View, Image, ScrollView, TouchableOpacity, TouchableWithoutFeedback, Keyboard, Alert } from 'react-native'
import Modal from 'react-native-modalbox'
import numeral from 'numeral'
import { Themes, TextFont, Button, Icon, TcombForm } from '../../../../../../ui'
import styles from './depositGatewayNapasInput.style'
import _ from 'lodash'
import formatMoney from '../../../../../../utils/formatMoney'

const bankTypes = [
  { id: 1, name: 'Thẻ nội địa'},
  { id: 2, name: 'Thẻ quốc tế'},
]
const amountThreshold = 10000
const bankTypeLocal = 1

const t = require('tcomb-form-native')
const Form = t.form.Form
const payment = t.struct({
  amount: t.Number,
  bank: t.String,
  bankType: t.String,
})

class DepositGatewayNapasInput extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isDateTimePickerVisible: false,
      value: {
        amount: null,
        bank: null,
        bankId: null,
        bankType: null,
        bankTypeId: null,
      }
    }
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
    this.renderModalBanks = this.renderModalBanks.bind(this)
    this.renderModalBankType = this.renderModalBankType.bind(this)
    this.setValueForm = this.setValueForm.bind(this)
    this.calculateTotalPay = this.calculateTotalPay.bind(this)
  }

  onChange(value) {
    this.setState({ value })
  }

  onPressSubmit = () => {
    const { bankId, bankTypeId, amount } = this.state.value
    let realAmount = (amount === null || amount === '') ? 0 : amount.replace(/,/g, '')

    if(bankTypeId === null)  {
      Alert.alert('Thông báo', 'Bạn vui lòng chọn loại thẻ !')
      return
    }

    if(bankTypeId === bankTypeLocal && bankId === null)  {
      Alert.alert('Thông báo', 'Bạn vui lòng chọn ngân hàng thanh toán!')
      return
    }

    if(realAmount < amountThreshold || realAmount % amountThreshold !== 0) {
      Alert.alert('Thông báo', `Số tiền nạp phải bội số của ${numeral(amountThreshold).format('0,0')} !`)
      return
    }

    realAmount = this.calculateTotalPay(realAmount)

    this.props.onPressSubmit(bankTypeId, realAmount, bankId)
  }

  setValueForm = (key, id, name, modal) => {
    const newState = {}
    newState[`${key}`] = name
    newState[`${key}Id`] = id

    this.setState({
      value: {
        ...this.state.value,
        ...newState
      }
    })

    this.refs[`${modal}`].close()
  }

  renderModalBanks = () => {
    const { napasBankList } = this.props
    const { bank: bankId } = this.state.value

    return (
      <Modal animationDuration={0} swipeToClose={false} style={[styles.modal]} position={'center'} ref={'modalBanks'}>
        <ScrollView bounces={false} contentContainerStyle={{ justifyContent: 'center' }}>
          {
            _.map(napasBankList, (data, index) => {
              const textValue = `${data.name}`
              return (
                <TouchableOpacity key={index} activeOpacity={0.7} onPress={() => this.setValueForm('bank', data.id, textValue, 'modalBanks')} style={[styles.btnTouchModal]}>
                  <TextFont color={data.id === bankId ? Themes.Colors.navColor : null}>{textValue}</TextFont>
                </TouchableOpacity>
              )
            })
          }
        </ScrollView>
      </Modal>
    )
  }

  renderModalBankType = () => {

    const { bankType: bankTypeId } = this.state.value

    return (
      <Modal animationDuration={0} swipeToClose={false} style={[styles.modal]} position={'center'} ref={'modalBankType'}>
        <ScrollView bounces={false} contentContainerStyle={{ justifyContent: 'center' }}>
          {
            _.map(bankTypes, (data, index) => {
              const textValue = `${data.name}`
              return (
                <TouchableOpacity key={index} activeOpacity={0.7} onPress={() => this.setValueForm('bankType', data.id, textValue, 'modalBankType')} style={[styles.btnTouchModal]}>
                  <TextFont color={data.id === this.state.bankTypeId ? Themes.Colors.navColor : null}>{textValue}</TextFont>
                </TouchableOpacity>
              )
            })
          }
        </ScrollView>
      </Modal>
    )
  }

  onFocusForm = (value) => {
    this.refs[`${value}`].open()
    Keyboard.dismiss()
  }

  calculateTotalPay(amount) {

    if(isNaN(amount)) {
      return 0
    }
    const { fee, minApplyFee } = this.props
    const payAmount = parseInt(amount)
    let totalPay = payAmount + (payAmount * fee)

    if(minApplyFee > 0) {
      totalPay = payAmount < minApplyFee ? totalPay : payAmount
    }

    return totalPay
  }

  renderForm = () => {
    const { paymentName, feeDescription } = this.props

    const { amount } = this.state.value
    const realAmount = (amount === null || amount === '') ? 0 : amount.replace(/,/g, '')

    const totalPay = this.calculateTotalPay(realAmount)

    const layout = (locals) => {
      const bankList = this.state.value.bankTypeId === 1 ? (<View style={{ marginTop: 10 }}>{locals.inputs.bank}</View>) : null

      return (
        <View style={{ marginTop: 10 }}>
          <View style={{ marginTop: 10 }}>{locals.inputs.bankType}</View>
          {bankList}
          <View style={{ marginTop: 10 }}>{locals.inputs.amount}</View>
          <TextFont style={styles.txtNote} size={12} color={Themes.Colors.navColor}>
            {totalPay > 0 ? `Tổng tiền thanh toán: ${formatMoney(totalPay)}` : null}
          </TextFont>
        </View>
      )
    }
    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        bank: {
          factory: TcombForm.InputModalRightIcon,
          placeholder: 'Chọn ngân hàng',
          onFocus: () => this.onFocusForm('modalBanks'),
          placeholderTextColor: '#E7E7E7',
          config: {
            nameIcon: 'down',
            IconSize: 7,
            IconColor: '#bcbcbc',
            labelStyle: {
              marginLeft: 0,
              color: '#222424'
            }
          }
        },
        bankType: {
          factory: TcombForm.InputModalRightIcon,
          placeholder: 'Chọn loại thẻ',
          onFocus: () => this.onFocusForm('modalBankType'),
          placeholderTextColor: '#E7E7E7',
          config: {
            nameIcon: 'down',
            IconSize: 7,
            IconColor: '#bcbcbc',
            labelStyle: {
              marginLeft: 0,
              color: '#222424'
            }
          }
        },
        amount: {
          template: TcombForm.InputTextFormatMoney,
          placeholder: 'Nhập số tiền cần nạp'
        },
      }
    }
    return (
      <Form
        ref={(comp) => {
          this.form = comp
        }}
        type={payment}
        options={options}
        value={this.state.value}
        onChange={this.onChange}
      />
    )
  }

  render() {

    const { paymentId, paymentName } = this.props
    const iconName = _.toLower(`icon${paymentId}`)

    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={[styles.container]}>
          <Image source={Themes.Images[iconName]} style={styles.napasImage} />
          {this.renderForm()}
          <Button onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Nạp Tiền</Button>
          {this.renderModalBanks()}
          {this.renderModalBankType()}
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
DepositGatewayNapasInput.propTypes = {}
export default DepositGatewayNapasInput
