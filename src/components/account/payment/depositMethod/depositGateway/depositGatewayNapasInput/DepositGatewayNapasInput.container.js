import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import DepositGatewayNapasInput from './DepositGatewayNapasInput'
import Config from '../../../../../../screens/config'
import { actions as PaymentAction } from '../../../../../../redux/payment'

class DepositGatewayNapasInputContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.depositSuccess = this.depositSuccess.bind(this)
    this.rechargeNapas = this.rechargeNapas.bind(this)
  }

  componentWillMount() {
    const { paymentAction } = this.props;
    paymentAction.getNapasBankList();
  }

  depositSuccess(data) {
    this.props.navigator.resetTo({
      ...Config.screen.depositGatewaySuccess,
      passProps: { amount: data.amount, paymentType: 'napas' }
    })
  }

  rechargeNapas(data) {
    this.props.navigator.push({
      ...Config.screen.depositGatewayCheckout,
      passProps: { uri: data.paymentURL, amount: data.amount }
    })
  }

  async onPressSubmit(bankTypeId, amount, bankId) {
    const { payment_id, paymentAction } = this.props

    paymentAction.getPaymentOrder({
      payment_type: payment_id,
      amount: amount,
      client_ip: '127.0.0.1',
      bank_type: bankTypeId,
      bank_code: bankId,
    }, {
      callback: this.rechargeNapas
    })
  }

  render() {

    const { payment_id, payment_name, prices, fee, min_apply_fee, fee_description, payload } = this.props
    const { paymentInfo } = payload
    const { napasBankList } = paymentInfo

    return (
      <DepositGatewayNapasInput
        onPressSubmit={this.onPressSubmit}
        listPrice={prices}
        paymentId={payment_id}
        paymentName={payment_name}
        feeDescription={fee_description}
        napasBankList={napasBankList}
        fee={fee}
        minApplyFee={min_apply_fee}
        navigator={this.props.navigator}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    paymentAction: bindActionCreators(PaymentAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DepositGatewayNapasInputContainer)
