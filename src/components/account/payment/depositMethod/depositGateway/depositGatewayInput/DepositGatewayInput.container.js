import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Platform, Alert, NativeModules } from 'react-native'
import DeviceInfo from 'react-native-device-info'
import DepositGatewayInput from './DepositGatewayInput'
import { PaymentGateway } from '../../../../../../configs/constant.config'
import { ZaloPayMessageError } from '../../../../../../configs/error.config'
import { PaymentGatewayMomo, PaymentGatewayZalo } from '../../../../../../configs/payment.config'
import Config from '../../../../../../screens/config'
import { Navigation } from '../../../../../../screens/navigation'
import { actions as PaymentAction } from '../../../../../../redux/payment'

class DepositGatewayInputContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.depositSuccess = this.depositSuccess.bind(this)
    this.rechargeZaloPay = this.rechargeZaloPay.bind(this)
    this.recharge123Pay = this.recharge123Pay.bind(this)
    this.rechargeMomo = this.rechargeMomo.bind(this)
    this.rechargeWebMoney = this.rechargeWebMoney.bind(this)
  }

  depositSuccess(data) {
    this.props.navigator.resetTo({
      ...Config.screen.depositGatewaySuccess,
      passProps: { amount: data.amount, paymentType: 'momo' }
    })
  }

  async rechargeZaloPay(data) {
    let ZaloPaymentSDK
    if (Platform.OS === 'android') {
      ZaloPaymentSDK = NativeModules.RNPaymentSdk
    } else {
      ZaloPaymentSDK = NativeModules.ZaloPaymentSDK
    }

    try {
      const resultPayment = await ZaloPaymentSDK.rechargeZaloPay(PaymentGatewayZalo.APP_ID, data.token)
      if (resultPayment === true) {
        this.props.navigator.resetTo({
          ...Config.screen.depositGatewaySuccess,
          passProps: { amount: data.amount, paymentType: 'zaloPay' }
        })
      } else {
        Alert.alert('Thông báo', 'Thanh toán thất bại')
      }
    } catch (error) {
      Alert.alert('Thông báo', ZaloPayMessageError.getMessage(error.message))
    }
  }

  recharge123Pay(data) {
    this.props.navigator.resetTo({
      ...Config.screen.depositGatewayCheckout,
      passProps: { uri: data.paymentURL, amount: data.amount }
    })
  }

  rechargeMomo(data) {
    this.props.navigator.push({
      ...Config.screen.depositGatewayCheckout,
      passProps: { uri: data.paymentURL, amount: data.amount }
    })
  }

  rechargeWebMoney(data) {
    this.props.navigator.resetTo({
      ...Config.screen.depositGatewayWebmoneyCheckout,
      passProps: { uri: data.paymentURL, amount: data.amount }
    })
  }

  async onPressSubmit(value) {
    const { payment_id, fee, paymentAction, payload } = this.props
    const { accountInfo } = payload

    const { amount } = value
    const { phone } = accountInfo
    const totalAmount = amount + (amount * fee)

    switch (payment_id) {
      // case PaymentGateway.MOMO: {
      //   const { RNPaymentSdk } = NativeModules
      //   try {
      //     const resultPayment = await RNPaymentSdk.rechargeMomo(totalAmount, 0, phone, PaymentGatewayMomo.BUNDLE_ID, PaymentGatewayMomo.MERCHANT_CODE, PaymentGatewayMomo.MERCHANT_ID, PaymentGatewayMomo.MERCHANT_LABEL, PaymentGatewayMomo.MERCHANT_LABEL, PaymentGatewayMomo.PUBLIC_KEY)
      //     const { app_data } = resultPayment

      //     paymentAction.rechargeMomo({
      //       username: accountInfo.full_name,
      //       amount: totalAmount,
      //       phone,
      //       app_data
      //     }, {
      //       nextScreen: this.depositSuccess
      //     })
      //   } catch (error) {
      //     Navigation.showMessage(error.message)
      //   }
      //   break
      // }
      case PaymentGateway.MOMO: {
        paymentAction.getPaymentOrder({
          payment_type: payment_id,
          amount: totalAmount,
          client_ip: '127.0.0.1'
        }, {
          callback: this.rechargeMomo
        })
        break
      }
      case PaymentGateway.ZALO_PAY: {
        paymentAction.getPaymentOrder({
          payment_type: payment_id,
          amount: totalAmount,
          client_ip: '127.0.0.1'
        }, {
          callback: this.rechargeZaloPay
        })
        break
      }
      case PaymentGateway.PAY_123: {
        paymentAction.getPaymentOrder({
          payment_type: payment_id,
          amount: totalAmount,
          client_ip: '127.0.0.1'
        }, {
          callback: this.recharge123Pay
        })
        break
      }
      case PaymentGateway.WEB_MONEY: {
        paymentAction.getPaymentOrder({
          payment_type: payment_id,
          amount: totalAmount,
          client_ip: '127.0.0.1',
          user_agent: DeviceInfo.getUserAgent()
        }, {
          callback: this.rechargeWebMoney
        })
        break
      }
      default:
    }
  }

  render() {
    const { payment_id, payment_name, prices, fee_description } = this.props

    return (
      <DepositGatewayInput
        onPressSubmit={this.onPressSubmit}
        listPrice={prices}
        paymentId={payment_id}
        paymentName={payment_name}
        feeDescription={fee_description}
        navigator={this.props.navigator}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    paymentAction: bindActionCreators(PaymentAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DepositGatewayInputContainer)
