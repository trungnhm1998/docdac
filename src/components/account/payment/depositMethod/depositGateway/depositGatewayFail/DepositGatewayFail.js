import React, { Component } from 'react'
import { View, Image } from 'react-native'
import { Themes, TextFont, Button } from '../../../../../../ui'
import styles from './depositGatewayFail.style'

class DepositGatewayFail extends Component {

  constructor(props) {
    super(props)
    this.state = {}
    this.onPressSubmit = this.onPressSubmit.bind(this)
  }

  onPressSubmit = () => {
    this.props.onPressSubmit()
  }

  render() {
    return (
      <View style={[styles.container]}>
        <Image style={[styles.imgSuccess]} source={Themes.Images.depositSuccess} />
        <View style={Themes.styleGB.centerContent}>
          <TextFont>Giao dịch nạp tiền thất bại!</TextFont>
        </View>
        <Button style={{ marginTop: 20 }} onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Thử lại</Button>
      </View>
    )
  }
}
DepositGatewayFail.propTypes = {}
export default DepositGatewayFail
