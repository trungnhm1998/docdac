import React, { Component } from 'react'
import { connect } from 'react-redux'
import DepositGatewayFail from './DepositGatewayFail'
import Config from '../../../../../../screens/config'
import { Navigation }from '../../../../../../screens/navigation'

class DepositGatewayFailContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
  }

  onPressSubmit() {
    this.props.navigator.resetTo(Config.screen.paymentHome)
  }

  render() {
    const { amount } = this.props
    return (
      <DepositGatewayFail
        onPressSubmit={this.onPressSubmit}
        amount={amount}
        navigator={this.props.navigator}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = () => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(DepositGatewayFailContainer)
