import { StyleSheet, PixelRatio, Platform } from 'react-native'
import { Themes } from '../../../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  moneyBox: {
    borderWidth: 1 / PixelRatio.get(),
    marginTop: 10,
    marginHorizontal: 10,
    borderColor: '#dedede',
    borderRadius: 5,
  },
  headBox: {
    backgroundColor: '#f7f7f7',
    padding: 7,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5
  },
  bodyBox: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10
  },
  bodyBoxColumn: {
    flex: 1
  },
  txtCol: {
    paddingRight: 30
  },
  iconBox: {
    //paddingHorizontal: 15,
    paddingLeft: 15,
    paddingRight: 7,
    borderLeftWidth: 2 / PixelRatio.get(),
    borderColor: '#e7e7e7',
    paddingVertical: 5
  },
  rowPay: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: 2 / PixelRatio.get(),
    borderColor: '#f2f2f2',
    marginHorizontal: 5,
    paddingRight: 30
  },
  imgRow: {
    width: 55,
    height: 55,
    marginRight: 10,
  },
  modal: {
    width: '85%',
    height: null,
    maxHeight: '80%'
  },
  btnTouchModal: {
    padding: 15,
    backgroundColor: '#E7E7E7',
    borderWidth: 1 / PixelRatio.get(),
    borderColor: '#ccc'
  }
})

export default styles
