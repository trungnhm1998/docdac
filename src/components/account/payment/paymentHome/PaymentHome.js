import React, { Component } from 'react'
import {
  View,
  TouchableOpacity,
  ListView,
  Image,
  Platform,
  ScrollView,
  Keyboard,
  TouchableWithoutFeedback,
  Alert
} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view'
import _ from 'lodash'
import Modal from 'react-native-modalbox'
import numeral from 'numeral'
import Contacts from 'react-native-contacts';
import Permissions from 'react-native-permissions'
import { Themes, TextFont, TcombForm, Icon, Button } from '../../../../ui'

import styles from './paymenthome.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
/*
  user_info
  bank_account
  bank_name
  bank_branch
  money
*/
const withdrawForm = t.struct({
  userInfo: t.String,
  bankAccountNumber: t.String,
  bankName: t.String,
  bankBranch: t.String,
  money: t.String
})
const userAccount = t.struct({
  country: t.String,
  phone: t.String,
  money: t.String
})
class PaymentHome extends Component {

  constructor(props) {
    super(props)
    this.state = {
      currentPage: 0,
      value: {
        country: 'Việt Nam (+84)',
        countryCode: '84',
        phone: ''
      },
      withdrawValue: {

      }
    }
    this.gotoHistory = this.gotoHistory.bind(this)
    this.gotoSelectContacts = this.gotoSelectContacts.bind(this)
    this.onPressHistory = _.debounce(this.gotoHistory, 100)
    this.onPressContact = _.debounce(this.gotoSelectContacts, 100)
    this.onChangeTab = this.onChangeTab.bind(this)

    this.onChange = this.onChange.bind(this)
    this.onWithdrawFormChange = this.onWithdrawFormChange.bind(this)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.renderModalCountry = this.renderModalCountry.bind(this)
    this.setCountry = this.setCountry.bind(this)
    this.onFocusCountry = this.onFocusCountry.bind(this)
    this.onPressContactIcon = this.onPressContactIcon.bind(this)
    this.requestContactsPermission = this.requestContactsPermission.bind(this)
  }

  onChange(value) {
    this.setState({ value })
    // console.log(value)
  }

  onWithdrawFormChange(value) {
    this.setState({ withdrawValue: value })
  }

  componentWillReceiveProps(props) {
    if (props.selectedContact !== undefined) {
      this.setState({ value: {
        phone: props.selectedContact.phoneNumbers[0].number,
        country: 'Việt Nam (+84)',
        countryCode: '84'
      }})
    }
  }

  onPressSubmit = (min, max) => {
    console.log('this.props',this.props)
    const { accountInfo } = this.props
    const value = this.form.getValue()
    if (value) {
      let { money } = value
      money = money.replace(new RegExp('\\,', 'g'), '')
      if (parseInt(money, 10) < parseInt(min.value, 10) || parseInt(money, 10) > parseInt(max.value, 10)) {
        Alert.alert('Thông báo', 'Số tiền để chuyển chưa đủ mức tối thiểu hoặc đã vượt quá mức cho phép.')
      } else if (accountInfo.money < parseInt(money, 10)) {
        Alert.alert('Thông báo', 'Tài khoản của bạn không đủ')
      } else {
        Alert.alert(
          'Xác nhận',
          `Bạn muốn chuyển tiền tới ${this.state.value.phone} số tiền ${numeral(money).format('0,0') + ' đ?'}`,
          [
            {text: 'Huỷ', onPress: () => {}, style: 'cancel'},
            {text: 'Xác nhận', onPress: () => this.props.onPressTransferMoney({ ...this.state.value, phoneNumber: this.state.value.phone, money })},
          ]
        )
      }
    }
  }

  onWithdrawSubmit(min) {
    const { userInfo, bankAccountNumber, bankName, bankBranch } = this.state.withdrawValue
    const { accountInfo } = this.props
    const value = this.withdrawForm.getValue()
    if (value) {
      let { money } = value
      money = money.replace(new RegExp('\\,', 'g'), '')
      money = parseInt(money, 10)
      if (parseInt(money, 10) < parseInt(min.value, 10)) {
        Alert.alert('Thông báo', `Số tiền để rút phải tối thiểu là ${numeral(min.value).format('0,0')} đ.`)
      }
      else if (accountInfo.money < parseInt(money, 10)) {
        Alert.alert('Thông báo', 'Tài khoản của bạn không đủ')
      } else {
        Alert.alert(
          'Xác nhận rút tiền',
          `Vui lòng xác nhận việc rút tiền:${'\ntên: '}${userInfo}${'\nsố tài khoản: '+bankAccountNumber+'\ntên ngân hàng: '+bankName+'\nchi nhánh ngân hàng: '+bankBranch}${'\n'}số tiền: ${numeral(money).format('0,0') + ' đ'}`,
          [
            {text: 'Huỷ', onPress: () => {}, style: 'cancel'},
            {text: 'Xác nhận', onPress: () => this.props.onPressOrderWithdrawSubmit({ ...this.state.withdrawValue, money})},
          ]
        )
      }
    }
  }

  onPressContactIcon = () => {
    Permissions.check('contacts').then(response => {
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      if (response === 'authorized') {
        console.log(this.state)
        Contacts.getAll((err, contacts) => {
          if (err) console.log(err);
        
          // contacts returned
          console.log(contacts)
          this.onPressContact(contacts)
        })
      }

      if (response === 'denied') {

      } else {
        console.log('requesting permisison')
        // Permissions.request('contacts').then(response => {
        //   // Returns once the user has chosen to 'allow' or to 'not allow' access
        //   // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
        //   console.log(response)
        // })
        this.requestContactsPermission()
      }
    })
    
  }

  requestContactsPermission = () => {
    Permissions.request('contacts', {
      rationale: {
        title: 'Yêu cầu quyền truy cập danh bạ',
        message:
          'Để có thể dễ dàng chọn số điện thoại ' +
          'hãy chọn \"Allow" để cho phép quyền truy cập danh bạ.',
      },
    }).then(response => {
      console.log(response)
      // this.setState({ cameraPermission: response })
    })
  }

  onPressPayment = (rowData) => {
    this.props.onPressPayment(rowData)
  }

  gotoHistory = () => {
    this.props.onPressHistory(this.state.currentPage)
  }

  gotoSelectContacts = (data) => {
    this.props.onPressContact(data)
  }

  onPressPaymentWithdraw = (rowData) => {
    this.props.onPressPaymentWithdraw(rowData)
  }

  renderListDeposit = () => {
    if (!this.props.listData) {
      return null
    }

    const listData = _.filter(this.props.listData, item => item.state === "1")
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const dataSource = ds.cloneWithRows(listData)

    return (
      <ListView
        dataSource={dataSource}
        renderRow={(rowData) => {
          const iconName = _.toLower(`icon${rowData.payment_id}`)
          return (
            <TouchableOpacity onPress={() => this.onPressPayment(rowData)} activeOpacity={0.7} style={[styles.rowPay]}>
              <Image style={[styles.imgRow]} source={Themes.Images[iconName]} />
              <View style={styles.txtCol}>
                <TextFont style={{ fontWeight: '700' }} color="#6e6e6e" font={Themes.Fonts.type.Medium} size={16}>{rowData.payment_name}</TextFont>
                <TextFont font={Themes.Fonts.type.Light} color="#6e6e6e">{rowData.description}</TextFont>
              </View>
            </TouchableOpacity>
          )
        }}
        scrollEventThrottle={16}
      />
    )
  }

  onChangeTab(e) {
    this.setState({
      currentPage: e.i
    })
  }

  renderListWithdraw() {
    if (!this.props.listData) {
      return null
    }

    const { listData } = this.props
    const withdrawList = _.find(listData, item => item.payment_id === '2')
    if (!withdrawList) {
      return null
    }

    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const dataSource = ds.cloneWithRows([withdrawList])

    return (
      <ListView
        dataSource={dataSource}
        renderRow={(rowData) => {
          const iconName = _.toLower(`icon${rowData.payment_id}`)
          return (
            <TouchableOpacity onPress={() => this.onPressPaymentWithdraw(rowData)} activeOpacity={0.7} style={[styles.rowPay]}>
              <Image style={[styles.imgRow]} source={Themes.Images[iconName]} />
              <View style={styles.txtCol}>
                <TextFont style={{ fontWeight: '700' }} color="#6e6e6e" font={Themes.Fonts.type.Medium} size={16}>{rowData.payment_name}</TextFont>
                <TextFont font={Themes.Fonts.type.Light} color="#6e6e6e">{rowData.description}</TextFont>
              </View>
            </TouchableOpacity>
          )
        }}
        scrollEventThrottle={16}
      />
    )
  }

  onFocusCountry = () => {
    this.refs['country'].open()
    Keyboard.dismiss()
  }

  setCountry = (countryCode, country) => {
    this.setState({
      value: {
        ...this.state.value,
        countryCode,
        country
      }
    })

    this.refs['country'].close()
  }

  renderModalCountry() {

    const { countryInfos } = this.props
    console.log('countryInfos', countryInfos)

    return (
      <Modal animationDuration={0} swipeToClose={false} style={[styles.modal]} position={'center'} ref={'country'}>
        <ScrollView bounces={false} contentContainerStyle={{ justifyContent: 'center' }}>
          {
            _.map(countryInfos, (data, index) => {
              const textValue = `${data.name} (+${data.id})`
              return (
                <TouchableOpacity key={index} activeOpacity={0.7} onPress={() => this.setCountry(data.id, textValue)} style={[styles.btnTouchModal]}>
                  <TextFont color={data.id === this.state.value.countryCode ? Themes.Colors.navColor : null}>{textValue}</TextFont>
                </TouchableOpacity>
              )
            })
          }
        </ScrollView>
      </Modal>
    )
  }

  renderWithdraw() {
    const withdrawSetting = this.props.accountInfo.moneySetting.data.find((element) => {
      return element.name === 'min_withdraw'
    })
    const withdrawFee = this.props.accountInfo.moneySetting.data.find((element) => {
      return element.name === 'fee_withdraw'
    })
    /*
      user_info
      bank_account
      bank_name
      bank_branch
      money
    */
    const layout = (locals) => {
      return(
        <View style={{ marginTop: 10 }}>
          {locals.inputs.userInfo}
          {locals.inputs.bankAccountNumber}
          {locals.inputs.bankName}
          {locals.inputs.bankBranch}
          {locals.inputs.money}
        </View>
      )
    }

    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        userInfo: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nguyen Van A',
          autoCapitalize: 'characters',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: 'Tên tài khoản'
          }
        },
        bankAccountNumber: {
          template: TcombForm.InputTextNormal,
          placeholder: '22222222222222222',
          placeholderTextColor: '#E7E7E7',
          keyboardType: 'phone-pad',
          config: {
            label: 'Số tài khoản'
          }
        },
        bankName: {
          template: TcombForm.InputTextNormal,
          placeholder: 'ACB',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: 'Tên ngân hàng'
          }
        },
        bankBranch: {
          template: TcombForm.InputTextNormal,
          placeholder: 'ho chi minh',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: 'Chi nhánh ngân hàng'
          }
        },
        money: {
          template: TcombForm.InputTextFormatMoney,
          placeholder: `${numeral(withdrawSetting.value).format('0,0')} đ`,
          placeholderTextColor: '#E7E7E7',
          keyboardType: 'numeric',
          config: {
            label: 'Số tiền cần rút'
          }
        }
      }
    }

    return (
      <KeyboardAwareScrollView>

        <View
          onStartShouldSetResponder={() => true}
          style={{ flex: 1, marginHorizontal: 10 * Themes.Metrics.ratioScreen, paddingTop: 10 * Themes.Metrics.ratioScreen }}
        >
          <Form
            ref={(form) => {
              this.withdrawForm = form
            }}
            onChange={this.onWithdrawFormChange}
            value={this.state.withdrawValue}
            type={withdrawForm}
            options={options}
          />

          <View style={{ flex: 1, marginBottom: 10 }}>
            <TextFont font={Themes.Fonts.type.Italic} color="#e74c3c">
              {withdrawSetting ? '(Tối thiểu: ' + numeral(withdrawSetting.value).format('0,0') + 'đ' : '('}
              {withdrawFee ? ', phí: ' + numeral(withdrawFee.value).format('0,0') + 'đ )' : ')'}
            </TextFont>
          </View>
          <View style={{ flex: 1 }}>
            <Button onPress={() => this.onWithdrawSubmit(withdrawSetting)} btnColor={Themes.Colors.btnColor1}>Tiếp tục</Button>
          </View>
        </View>
      </KeyboardAwareScrollView>
    )
  }

  renderTransfer() {
    const transferMin = this.props.accountInfo.moneySetting.data.find((element) => {
      return element.name === 'min_transfer'
    })
    const transferMax = this.props.accountInfo.moneySetting.data.find((element) => {
      return element.name === 'max_transfer'
    })
    const layout = (locals) => {
      return (
        <View style={{ marginTop: 10}}>
          {locals.inputs.country}
          {locals.inputs.phone}
          {locals.inputs.money}
        </View>
      )
    }

    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        country: {
          factory: TcombForm.InputModalRightIcon,
          placeholder: 'Chọn quốc gia',
          onFocus: () => this.onFocusCountry(),
          placeholderTextColor: '#E7E7E7',
          config: {
            nameIcon: 'down',
            IconSize: 7,
            IconColor: '#bcbcbc',
            labelStyle: {
              marginLeft: 0,
              color: '#222424'
            },
            label: 'Nhập số điện thoại được chuyển tiền'
          },
          value: this.state.value.country,
          initState: true
        },
        phone: {
          template: TcombForm.InputRightIcon,
          placeholder: '0904143147',
          placeholderTextColor: '#E7E7E7',
          keyboardType: 'phone-pad',
          config: {
            useVectorIcon: true,
            rightIconName: 'contacts',
            rightIconSize: 20,
            onIconPress: () => this.onPressContactIcon()
          }
        },
        money: {
          template: TcombForm.InputTextFormatMoney,
          placeholder: `${numeral(transferMin.value).format('0,0')} đ`,
          placeholderTextColor: '#E7E7E7',
          keyboardType: 'numeric',
          config: {
            label: 'Số tiền cần chuyển'
          }
        }
      }
    }
    return (
      <View style={{ flex: 1, marginHorizontal: 10 * Themes.Metrics.ratioScreen, paddingTop: 10 * Themes.Metrics.ratioScreen }}>
        <Form
          ref={(form) => {
            this.form = form
          }}
          onChange={this.onChange}
          value={this.state.value}
          type={userAccount}
          options={options}
        />
        <View style={{ flex: 1, marginBottom: 10 }}>
          <TextFont font={Themes.Fonts.type.Italic} color="#e74c3c">
            {transferMin ? '(Tối thiểu: ' + numeral(transferMin.value).format('0,0') + 'đ' : '('}
            {transferMax ? ', tối đa: ' + numeral(transferMax.value).format('0,0') + 'đ )' : ')'}
          </TextFont>
        </View>
        <View style={{ flex: 1 }}>
          <Button onPress={() => this.onPressSubmit(transferMin, transferMax)} btnColor={Themes.Colors.btnColor1}>Tiếp tục</Button>
        </View>
      </View>
    )
  }

  renderTabView = () => {
    if (Platform.OS === 'ios') {
      const reviewStore = this.props.accountInfo.moneySetting.data.find((element) => {
        return element.name === 'review_store'
      })
      if (reviewStore !== undefined) {
        if (this.props.renderScrollTab === 1 && parseInt(reviewStore.value, 10) === 1 ) {
          return (
            <ScrollableTabView
              tabBarUnderlineStyle={{ backgroundColor: Themes.Colors.navColor, height: 3 }}
              tabBarTextStyle={{ fontFamily: Themes.Fonts.type.Bold, paddingTop: 5, fontWeight: 'bold' }}
              tabBarActiveTextColor={Themes.Colors.navColor}
              renderTabBar={() => <DefaultTabBar />}
              locked={Platform.OS === 'android'}
              onChangeTab={this.onChangeTab}
            >
              <View style={Themes.styleGB.flexOne} tabLabel="NẠP TIỀN">
                {this.renderListDeposit()}
              </View>
              <View style={[Themes.styleGB.flexOne]} tabLabel="RÚT TIỀN">
                <ScrollView>
                  { this.renderWithdraw() }
                </ScrollView>
              </View>
              <View style={[Themes.styleGB.flexOne]} tabLabel="CHUYỂN TIỀN">
                <ScrollView>
                  {this.renderTransfer()}
                </ScrollView>
              </View>
            </ScrollableTabView>
          )
        }
      }
    } else {
      return (
        <ScrollableTabView
          tabBarUnderlineStyle={{ backgroundColor: Themes.Colors.navColor, height: 3 }}
          tabBarTextStyle={{ fontFamily: Themes.Fonts.type.Bold, paddingTop: 5, fontWeight: 'bold' }}
          tabBarActiveTextColor={Themes.Colors.navColor}
          renderTabBar={() => <DefaultTabBar />}
          locked={Platform.OS === 'android'}
          onChangeTab={this.onChangeTab}
        >
          <View style={Themes.styleGB.flexOne} tabLabel="NẠP TIỀN">
            {this.renderListDeposit()}
          </View>
          <View style={[Themes.styleGB.flexOne]} tabLabel="RÚT TIỀN">
            <ScrollView>
              { this.renderWithdraw() }
            </ScrollView>
          </View>
          <View style={[Themes.styleGB.flexOne]} tabLabel="CHUYỂN TIỀN">
            <ScrollView>
              {this.renderTransfer()}
            </ScrollView>
          </View>
        </ScrollableTabView>
      )
    }
    return null
  }

  render() {
    const { money, game_money } = this.props.accountInfo
    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={[styles.container]}>
          <View style={[styles.moneyBox]}>
            <View style={[styles.headBox]}>
              <TextFont color="#6e6e6e" size={13}>Tài khoản hiện có</TextFont>
            </View>
            <View style={[styles.bodyBox]}>
              <View style={[styles.bodyBoxColumn]}>
                <View>
                  <TextFont color={Themes.Colors.navColor} size={24} font={Themes.Fonts.type.Bold}>{numeral(money).format('0,0')} đ</TextFont>
                </View>
                <View>
                  <TextFont color="#6e6e6e" size={14} font={Themes.Fonts.type.Regular}>{`(Tiền khoá: ${numeral(game_money).format('0,0')} đ)`}</TextFont>
                </View>

              </View>
              <View>
                <TouchableOpacity onPress={this.onPressHistory} activeOpacity={0.7} style={[styles.iconBox]}>
                  <Icon size={23} name="history" />
                </TouchableOpacity>
              </View>

            </View>
          </View>
          {this.renderTabView()}
          {this.renderModalCountry()}
          {/* {this.renderModalPermission()} */}
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
PaymentHome.propTypes = {}
export default PaymentHome
