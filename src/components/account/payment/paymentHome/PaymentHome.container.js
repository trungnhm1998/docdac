import React, { Component } from 'react'
import { Platform } from 'react-native'
import { connect } from 'react-redux'
import _ from 'lodash'
import PaymentHome from './PaymentHome'
import Config from '../../../../screens/config'
import { PaymentGateway } from '../../../../configs/constant.config'
import { Icon, Themes } from '../../../../ui'
import * as AccountAction from '../../../../redux/account/account.actions'
import { bindActionCreators } from 'redux';


class PaymentHomeContainer extends Component {

  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE,
    statusBarColor: Themes.Colors.navColor
  }

  constructor(props) {
    super(props)
    this.state = {
      renderScrollTab: 1
    }
    this.onPressPayment = _.debounce(this.onPressPayment.bind(this), 100)
    this.onPressPaymentWithdraw = _.debounce(this.onPressPaymentWithdraw.bind(this), 100)
    this.onPressHistory = this.onPressHistory.bind(this)
    this.onPressTransferMoney = this.onPressTransferMoney.bind(this)
    this.onPressSendOrderWithdrawSubmit = this.onPressSendOrderWithdrawSubmit.bind(this)
    this.onPressContact = this.onPressContact.bind(this)
    this.onNavigatorEvent = this.onNavigatorEvent.bind(this)
    this.props.navigator.setOnNavigatorEvent(_.debounce(this.onNavigatorEvent, 100))
    this.refreshAccountInfo = this.refreshAccountInfo.bind(this)
  }

  componentWillMount() {
    this.renderLeftIcon()
    this.renderRightIcon()
  }

  renderLeftIcon = () => {
    Promise.all([
      Icon.getImageSource('setting', 24)
    ]).then((values) => {
      if (Platform.OS === 'ios') {
        this.props.navigator.setButtons({
          leftButtons: [
            { id: 'setting', icon: values[0] }
          ]
        })
      } else {
        this.props.navigator.setButtons({
          leftButtons: [
            { id: 'setting', icon: values[0] }
          ]
        })
      }
    })
  }

  renderRightIcon = () => {
    Promise.all([
      Icon.getImageSource('profile', 24),
      Icon.getImageSource('setting', 24)
    ]).then((values) => {
      if (Platform.OS === 'ios') {
        this.props.navigator.setButtons({
          rightButtons: [
            { id: 'profile', icon: values[0] }
          ]
        })
      } else {
        this.props.navigator.setButtons({
          rightButtons: [
            { id: 'profile', icon: values[0] }
          ]
        })
      }
    })
  }

  onNavigatorEvent(event) {
    if (event.type === 'NavBarButtonPress') {
      if (event.id === 'setting') {
        this.props.navigator.push(Config.screen.settingHome)
      }
      if (event.id === 'profile') {
        this.props.navigator.push(Config.screen.profile)
      }
    }
    if (event.id === 'willAppear') {
      // to DO
    }
  }

  onPressPayment(value) {
    switch (value.payment_id) {
      case PaymentGateway.ZALO_PAY:
      case PaymentGateway.PAY_123:
      case PaymentGateway.WEB_MONEY:
      case PaymentGateway.MOMO: {
        this.props.navigator.push({
          ...Config.screen.depositGatewayInput,
          passProps: { ...value }
        })
        break
      }
      case PaymentGateway.INTERNET_BANKING: {
        this.props.navigator.push(Config.screen.depositBankingInput)
        break
      }
      case PaymentGateway.CARD_TELCO: {
        this.props.navigator.push({
          ...Config.screen.depositGatewayCard,
          passProps: { ...value }
        })
        break
      }
      case PaymentGateway.NAPAS: {
        this.props.navigator.push({
          ...Config.screen.depositGatewayNapasInput,
          passProps: { ...value }
        })
        break
      }
      default:
    }
  }

  onPressPaymentWithdraw(value) {
    this.props.navigator.push({
      ...Config.screen.withdrawGateway,
      passProps: { ...value }
    })
  }

  onPressTransferMoney(value) {
    const { accountInfo } = this.props.payload
    // const accessToken = 'eyJoYXNoIjoiZGUzNDk4Y2E1NWNiZTBhZTE3ZDUxNDQxZWFlNGZjMjQiLCJhY2NvdW50X2lkIjo1NDA3NDQ2MTIsImFjY291bnQiOiIwOTczNjgwNDgzIiwiZW1haWwiOiJuaGF0a2hhbmguZGluaEBnbWFpbC5jb20iLCJkZXZpY2VfaWQiOiJpUGhvbmU4LDIiLCJjaGFubmVsIjoiMTAwMXwxLjAuMHxWUFR8U1RPUkUiLCJ0ZWxjbyI6IlZpZXR0ZWwiLCJwbGF0Zm9ybSI6ImlvcyIsInBhcnRuZXJJZCI6NTQwNzQ0NjEyLCJwYXJ0bmVyIjoiVlBUIn0='
    this.props.accountAction.transferMoney(value, accountInfo.accessToken)
    this.refreshAccountInfo()
  }

  onPressSendOrderWithdrawSubmit(value) {
    const { accountInfo } = this.props.payload
    this.props.accountAction.sendOrderWithdraw(value, accountInfo.accessToken)
  }

  refreshAccountInfo() {
    // const accessToken = 'eyJoYXNoIjoiZGUzNDk4Y2E1NWNiZTBhZTE3ZDUxNDQxZWFlNGZjMjQiLCJhY2NvdW50X2lkIjo1NDA3NDQ2MTIsImFjY291bnQiOiIwOTczNjgwNDgzIiwiZW1haWwiOiJuaGF0a2hhbmguZGluaEBnbWFpbC5jb20iLCJkZXZpY2VfaWQiOiJpUGhvbmU4LDIiLCJjaGFubmVsIjoiMTAwMXwxLjAuMHxWUFR8U1RPUkUiLCJ0ZWxjbyI6IlZpZXR0ZWwiLCJwbGF0Zm9ybSI6ImlvcyIsInBhcnRuZXJJZCI6NTQwNzQ0NjEyLCJwYXJ0bmVyIjoiVlBUIn0='
    const { accountInfo } = this.props.payload
    this.props.accountAction.getUserInfoDocdac(accountInfo.accessToken, Platform.OS, 1, '')
  }

  onPressHistory(pageIndex) {
    const historyTitle = pageIndex === 0 ? 'Lịch sử nạp tiền' : pageIndex === 1 ? 'Lịch sử rút tiền' : 'Lịch sử chuyển tiền'
    this.props.navigator.push({ ...Config.screen.paymentHistory, passProps: { currentPage: pageIndex }, title:  historyTitle })
  }

  onPressContact(data) {
    this.props.navigator.push({ ...Config.screen.selectContacts, passProps: {
      contacts: data,
      selectContact: (contact) => {
        this.setState({ selectedContact: contact })
      }
    }})
  }

  render() {
    console.log(this.props.payload)
    const { accountInfo, paymentInfo } = this.props.payload
    const { countryInfos } = this.props.payload.initConfig
    return (
      <PaymentHome
        onPressPayment={this.onPressPayment}
        onPressPaymentWithdraw={this.onPressPaymentWithdraw}
        onPressTransferMoney={this.onPressTransferMoney}
        onPressHistory={this.onPressHistory}
        onPressContact={this.onPressContact}
        onPressOrderWithdrawSubmit={this.onPressSendOrderWithdrawSubmit}
        selectedContact={this.state.selectedContact}
        accountInfo={accountInfo}
        refreshAccountInfo={this.refreshAccountInfo}
        navigator={this.props.navigator}
        listData={paymentInfo.paymentDepositService}
        renderScrollTab={this.state.renderScrollTab}
        countryInfos={ countryInfos }
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentHomeContainer)
