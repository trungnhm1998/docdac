import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'
import { Themes, TextFont, TcombForm, Button } from '../../../ui'
import styles from './welcome.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const userAccount = t.struct({
  password: t.String
})

class Welcome extends Component {

  constructor(props) {
    super(props)
    this.state = {}
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onPressForgotPassword = this.onPressForgotPassword.bind(this)
    this.onPressChangePhone = this.onPressChangePhone.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  onPressSubmit = () => {
    const value = this.form.getValue()
    if (value) {
      this.props.onPressSubmit(value.password)
    }
  }

  onPressChangePhone() {
    this.props.onPressChangePhone()
  }

  onPressForgotPassword = () => {
    this.props.onPressForgotPassword()
  }

  onChange(value) {
    this.setState({ value })
  }

  renderForm = () => {
    const { userPhone } = this.props
    const layout = (locals) => {
      return (
        <View style={{ marginTop: 15 }}>
          <View>{locals.inputs.password}</View>
        </View>
      )
    }
    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        password: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập mật khẩu',
          placeholderTextColor: '#E7E7E7',
          secureTextEntry: true,
          autoFocus: true,
          config: {
            label: `Chào mừng ${userPhone} quay lại`
          }
        }
      }
    }
    return (
      <Form
        ref={(form) => {
          this.form = form
        }}
        value={this.state.value}
        onChange={this.onChange}
        type={userAccount}
        options={options}
      />
    )
  }

  render() {
    return (
      <View style={[Themes.styleGB.container]}>
        <StatusBar barStyle="light-content" />
        {this.renderForm()}
        <Button onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Tiếp tục</Button>
        <View style={{ flexDirection: 'row' }}>
          <TextFont onPress={this.onPressChangePhone} style={[styles.txtChangePhone]}>Đổi số điện thoại.</TextFont>
          <TextFont onPress={this.onPressForgotPassword} style={[styles.txtForgetPass]}>Quên mật khẩu ?</TextFont>
        </View>
      </View>
    )
  }
}
Welcome.propTypes = {}

export default Welcome
