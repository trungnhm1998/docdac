import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import store from '../../../configs/store.config'
import Welcome from './Welcome'
import Config from '../../../screens/config'
import { Navigation } from '../../../screens/navigation'
import * as AccountAction from '../../../redux/account/account.actions'
import { AccountKitHandler } from '../../../handler'

// const Fabric = require('react-native-fabric')

// const { Answers } = Fabric

class WelcomeContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onPressForgotPassword = this.onPressForgotPassword.bind(this)
    this.onPressChangePhone = this.onPressChangePhone.bind(this)
    this.navToHome = this.navToHome.bind(this)
    this.navToForgotPassword = this.navToForgotPassword.bind(this)
  }

  navToHome() {
    // Answers.logLogin('USER_LOGIN', true)
    Navigation.navToHome()
  }

  navToForgotPassword(phone) {
    this.props.navigator.push({
      ...Config.screen.forgotPassword,
      passProps: { phone }
    })
  }

  onPressSubmit(pass) {
    const { accountAction, payload } = this.props
    const accountInfo = payload.accountInfo
    const { phone, countryCode } = accountInfo
    accountAction.authorize(phone, countryCode, pass, { nextScreen: this.navToHome })
  }

  onPressChangePhone() {
    const { accountAction } = this.props
    accountAction.logout({
      nextScreen: () => Navigation.navToLogin()
    })
  }

  onPressForgotPassword() {
    const { accountAction, payload } = this.props
    const { phone, countryCode } = payload.accountInfo
    const { useAccountKit, countries } = payload.initConfig

    if(useAccountKit) {
      AccountKitHandler.verifyPhone(phone, countryCode, countries)
        .then(verifyData => {
          const { token } = verifyData
          this.props.navigator.resetTo({
            ...Config.screen.forgotPasswordAccountKit,
            passProps: { phone, countryCode, token }
          })
        }).catch(error => {
        if(__DEV__) {
          console.log('Forgot password fail', error)
        }
      })
    } else {
        accountAction.sendOTPForgotPassword(phone, {
          forgotPasswordScreen: this.navToForgotPassword
        })
    }
  }

  render() {
    const { payload } = this.props
    const { phone } = payload.accountInfo
    return (
      <Welcome
        userPhone={phone}
        onPressSubmit={this.onPressSubmit}
        onPressForgotPassword={this.onPressForgotPassword}
        onPressChangePhone={this.onPressChangePhone}
        navigator={this.props.navigator}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeContainer)
