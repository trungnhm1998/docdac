import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { VERSION } from '../../../../configs/version.config'
import { Themes, TextFont, Icon } from '../../../../ui'
import styles from './settinghome.style'

const RowSetting = ({ children, iconNamne, iconSize, iconColor, txtColor, rightIconColor, onPress, lastItem }) => {
  return (
    <TouchableOpacity activeOpacity={0.6} onPress={onPress} style={lastItem ? styles.lastRowSetting : styles.rowSetting}>
      <View style={styles.iconBox}>
        <Icon color={iconColor} size={iconSize} name={iconNamne} />
      </View>
      <TextFont color={txtColor} style={[styles.txtTitle]} size={17}>{children}</TextFont>
      <Icon color={rightIconColor} name="next" />
    </TouchableOpacity>
  )
}
RowSetting.defaultProps = {
  iconColor: Themes.Colors.navColor,
  txtColor: '#1c1c1c',
  rightIconColor: '#1c1c1c'
}

class SettingHome extends Component {

  constructor(props) {
    super(props)
    this.state = {}
    this.onPressIntroduce = this.onPressIntroduce.bind(this)
    this.onPressCompany = this.onPressCompany.bind(this)
    this.onPressProcedure = this.onPressProcedure.bind(this)
    this.onPressInformation = this.onPressInformation.bind(this)
    this.onPressWithdraw = this.onPressWithdraw.bind(this)
    this.onPressSubmit = this.onPressSubmit.bind(this)
  }

  onPressIntroduce = () => {
    this.props.onPressIntroduce()
  }

  onPressCompany = () => {
    this.props.onPressCompany()
  }

  onPressProcedure = () => {
    this.props.onPressProcedure()
  }

  onPressInformation = () => {
    this.props.onPressInformation()
  }

  onPressSubmit = () => {
    this.props.onPressSubmit()
  }

  onPressWithdraw() {
    this.props.onPressWithdraw()
  }

  render() {
    return (
      <View style={[Themes.styleGB.container]}>
        <View style={[styles.bannerImg]}>
          <Image source={Themes.Images.settingBanner} style={{ width: '100%' }} />
        </View>
        <RowSetting onPress={this.onPressIntroduce} iconSize={20} iconNamne="info">Giới thiệu và cách chơi</RowSetting>
        <RowSetting onPress={this.onPressProcedure} iconSize={20} iconNamne="dolla">Quy trình trả thưởng</RowSetting>
        <RowSetting onPress={this.onPressWithdraw} iconSize={18} iconNamne="info">Quy định rút tiền</RowSetting>
        <RowSetting onPress={this.onPressInformation} iconSize={15} iconNamne="support">Thông tin liên hệ</RowSetting>
        {
          /*
          <RowSetting onPress={this.onPressCompany} iconSize={18}
                      iconNamne="board">Đơn vị chủ quản</RowSetting>
          */
        }
        <RowSetting onPress={this.onPressSubmit} lastItem rightIconColor="transparent" txtColor="red" iconColor="transparent" iconSize={18} iconNamne="board">Đăng xuất</RowSetting>
        <View style={{ position: 'absolute', right: 15, bottom: 10 }}>
          <Text>Phiên bản {VERSION}</Text>
        </View>
      </View>
    )
  }
}
SettingHome.propTypes = {}
export default SettingHome
