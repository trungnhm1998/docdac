import React, { Component } from 'react'
import { WebView } from 'react-native'
import Config from '../../../../screens/config'
import { Colors } from '../../../../ui/themes'

class SettingInfo extends Component {

  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB,
    statusBarColor: Colors.navColor
  }

  render() {
    const { uri } = this.props
    return (
      <WebView
        source={{ uri }}
        automaticallyAdjustContentInsets={false}
        decelerationRate="normal"
        startInLoadingState
        scalesPageToFit
      />
    )
  }
}

export default SettingInfo
