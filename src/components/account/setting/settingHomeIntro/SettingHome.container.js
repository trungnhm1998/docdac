import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import SettingHome from './SettingHome'
import Config from '../../../../screens/config'
import { Colors } from '../../../../ui/themes'
import { Navigation } from '../../../../screens/navigation'
import * as AccountAction from '../../../../redux/account/account.actions'

class SettingHomeContainer extends Component {

  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB,
    statusBarColor: Colors.navColor
  }

  constructor(props) {
    super(props)
    this.onPressIntroduce = this.onPressIntroduce.bind(this)
    this.onPressCompany = this.onPressCompany.bind(this)
    this.onPressProcedure = this.onPressProcedure.bind(this)
    this.onPressInformation = this.onPressInformation.bind(this)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onPressWithdraw = this.onPressWithdraw.bind(this)
    this.navToLogin = this.navToLogin.bind(this)
  }

  onPressIntroduce() {
    this.props.navigator.push({ 
      ...Config.screen.settingInfo,
      title: 'Giới thiệu cách chơi',
      passProps: { uri: 'https://docdac.vn/gioi-thieu.html' }
    })
  }

  onPressProcedure() {
    this.props.navigator.push({ 
      ...Config.screen.settingInfo,
      title: 'Quy trình trả thưởng',
      passProps: { uri: 'https://docdac.vn/quy-trinh-tra-thuong.html' }
    })
  }

  onPressCompany() {
    this.props.navigator.push({ 
      ...Config.screen.settingInfo,
      title: 'Đơn vị chủ quản',
      passProps: { uri: 'https://docdac.vn/don-vi-chu-quan.html' }
    })
  }

  onPressInformation() {
    this.props.navigator.push({ 
      ...Config.screen.settingInfo,
      title: 'Thông tin liên hệ',
      passProps: { uri: 'https://docdac.vn/thong-tin-lien-he.html' }
    })
  }

  onPressWithdraw() {
    this.props.navigator.push({ 
      ...Config.screen.settingInfo,
      title: 'Quy định rút tiền',
      passProps: { uri: 'https://docdac.vn/quy-dinh-rut-tien.html' }
    })
  }

  navToLogin() {
    Navigation.navToLogin()
  }

  onPressSubmit() {
    const { accountAction } = this.props
    accountAction.logout({
      nextScreen: this.navToLogin
    })
  }

  render() {
    return (
      <SettingHome
        onPressIntroduce={this.onPressIntroduce}
        onPressCompany={this.onPressCompany}
        onPressProcedure={this.onPressProcedure}
        onPressInformation={this.onPressInformation}
        onPressSubmit={this.onPressSubmit}
        onPressWithdraw={this.onPressWithdraw}
        navigator={this.props.navigator}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingHomeContainer)
