import React, { Component } from 'react'
import { Alert } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ForgotPasswordAccountKit from './ForgotPasswordAccountKit'
import Config from '../../../screens/config'
import * as AccountAction from '../../../redux/account/account.actions'

class ForgotPasswordAccountKitContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.navToLogin = this.navToLogin.bind(this)
  }

  navToLogin() {
    Alert.alert('Thông báo', 'Cập nhật mật khẩu thành công', [{
      text: 'Đăng nhập lại',
      onPress: () => {
        this.props.navigator.resetTo(Config.screen.login)
      }
    }])
  }

  onPressSubmit(value) {
    const { accountAction, phone, countryCode, token } = this.props
    accountAction.forgotPasswordAccountKit(phone, countryCode, token, value.passWord, value.repeatPassWord, {
      loginScreen: this.navToLogin
    })
  }

  render() {
    return (
      <ForgotPasswordAccountKit
        onPressSubmit={this.onPressSubmit}
        phone={this.props.phone}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordAccountKitContainer)
