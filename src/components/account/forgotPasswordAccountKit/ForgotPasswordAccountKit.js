import React, { Component } from 'react'
import { Alert, View } from 'react-native'
import { Themes, TextFont, TcombForm, Button } from '../../../ui'
import styles from './forgotpasswordAccountKit.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const userAccount = t.struct({
  passWord: t.String,
  repeatPassWord: t.String
})

class ForgotPasswordAccountKit extends Component {

  constructor(props) {
    super(props)
    this.state = { value: '' }
    this.onChange = this.onChange.bind(this)
    this.onPressSubmit = this.onPressSubmit.bind(this)
  }

  onChange(value) {
    this.setState({ value })
  }

  onPressSubmit = () => {
    const value = this.form.getValue()
    if (value) {
      if (value.passWord !== value.repeatPassWord) {
        Alert.alert('Thông báo', 'Mật khẩu xác nhận không khớp')
      } else {
        this.props.onPressSubmit(value)
      }
    }
  }

  renderForm = () => {
    const layout = (locals) => {
      return (
        <View style={{ marginTop: 15 }}>
          <View>{locals.inputs.passWord}</View>
          <View>{locals.inputs.repeatPassWord}</View>
        </View>
      )
    }
    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        passWord: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập mật khẩu',
          placeholderTextColor: '#E7E7E7',
          secureTextEntry: true
        },
        repeatPassWord: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập lại mật khẩu',
          placeholderTextColor: '#E7E7E7',
          secureTextEntry: true
        }
      }
    }
    return (
      <Form 
        ref={(form) => {
          this.form = form
        }} 
        onChange={this.onChange}
        value={this.state.value}
        type={userAccount} 
        options={options} 
      />
    )
  }

  render() {
    return (
      <View style={[Themes.styleGB.container]}>
        <TextFont style={{ textAlign: 'center', marginTop: 15, fontSize: 16 }}>Thiết lập mật khẩu mới</TextFont>
        {this.renderForm()}
        <Button onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Tiếp tục</Button>
      </View>
    )
  }

}
ForgotPasswordAccountKit.propTypes = {}
export default ForgotPasswordAccountKit
