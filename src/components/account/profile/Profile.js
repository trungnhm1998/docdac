import React, { Component } from 'react'
import { View, Image, ScrollView, Alert, TouchableOpacity, Dimensions, Keyboard } from 'react-native'
import KeyboardSpacer from 'react-native-keyboard-spacer'
import DateTimePicker from 'react-native-modal-datetime-picker'
import Picker from 'react-native-picker'
import ImagePicker from 'react-native-image-picker'
import RNFS from 'react-native-fs'
import md5 from 'md5'
import _ from 'lodash'
import moment from 'moment'
import { Navigation } from '../../../screens/navigation'
import ImageCrop from '../../../ui/Cropper/ImageCropper'
import { Themes, Icon, TextFont, TcombForm, Button, Loading, Overlay } from '../../../ui'
import { types } from '../../../redux/account/'
import styles from './profile.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const userAccount = t.struct({
  name: t.String,
  email: t.maybe(t.String),
  birthday: t.maybe(t.String),
  sex: t.maybe(t.String),
  idNumber: t.String,
  address: t.maybe(t.String)
})
const options = {
  title: 'Chọn ảnh từ',
  cancelButtonTitle: 'Thoát',
  takePhotoButtonTitle: 'Máy ảnh',
  chooseFromLibraryButtonTitle: 'Bộ sưu tập',
  maxWidth: 400,
  maxHeight: 400,
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
}

const { width } = Dimensions.get('window')
const path = `${RNFS.DocumentDirectoryPath}/${md5(Date.now())}.png`

class Profile extends Component {

  constructor(props) {
    super(props)
    this.state = {
      visibleOverlay: false,
      isDateTimePickerVisible: false,
      gender: {
        1: 'Nam',
        2: 'Nữ'
      },
      pickedUri: null
    }
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.showDateTimePicker = this.showDateTimePicker.bind(this)
    this.hideDateTimePicker = this.hideDateTimePicker.bind(this)
    this.onChange = this.onChange.bind(this)
    this.showImagePicker = this.showImagePicker.bind(this)
    this.capture = this.capture.bind(this)
  }
  
  componentWillMount() {
    const { accountInfo } = this.props
    this.setState({
      value: {
        name: accountInfo.name,
        email: accountInfo.email,
        birthday: accountInfo.birthday !== '' ? moment(accountInfo.birthday).format('DD/MM/YYYY') : moment().format('DD/MM/YYYY'),
        sex: (parseInt(accountInfo.gender, 10) === 1) ? 'Nam' : 'Nữ',
        idNumber: accountInfo.idNumber,
        address: accountInfo.address,
        avatarUrl: accountInfo.avatarUrl
      }
    })
  }

  onPressSubmit = () => {
    const value = this.form.getValue()
    if (_.isNull(value)) {
      Alert.alert('Thông báo', 'Vui lòng nhập đầy đủ thông tin')
    } else if (moment(this.state.value.birthday, 'DD/MM/YYYY').isAfter(moment().subtract(3, 'year'), 'year')) {
      Alert.alert('Thông báo', 'Bạn chưa đủ độ tuổi quy định')
    } else if(_.isNull(value.email)) {
      Alert.alert('Thông báo', 'Vui lòng nhập email để nhận biên nhận khi mua vé')
    } else {
      this.props.onPressSubmit({
        name: value.name,
        idNumber: value.idNumber,
        birthday: moment(this.state.value.birthday, 'DD/MM/YYYY').format('YYYY-MM-DD'),
        gender: (value.sex === 'Nam') ? 1 : 2,
        email: value.email,
        address: value.address,
        avatarUrl: this.state.value.avatarUrl
      })
    }
  }

  showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true })

  hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false })

  handleDatePicked = (date) => {
    this.setState({
      ...this.state,
      value: {
        ...this.state.value,
        birthday: moment(date).format('DD/MM/YYYY')
      }
    })
    this.hideDateTimePicker()
  }

  onChange(value) {
    this.setState({ value })
  }

  renderForm = () => {
    const layout = (locals) => {
      return (
        <View style={{ marginTop: 10 }}>
          <View>{locals.inputs.name}</View>
          <View>{locals.inputs.idNumber}</View>
          <View>{locals.inputs.email}</View>
          <View style={[Themes.styleGB.flexRow]}>
            <View style={[Themes.styleGB.flexOne, { marginRight: 5, flex: 3 }]}>{locals.inputs.birthday}</View>
            <View style={[Themes.styleGB.flexOne, { marginLeft: 5, flex: 2 }]}>{locals.inputs.sex}</View>
          </View>
          <View>{locals.inputs.address}</View>
        </View>
      )
    }
    const optionsForm = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        name: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập họ và tên',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: 'Họ và tên'
          }
        },
        email: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập email',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: 'Email'
          }
        },
        birthday: {
          template: TcombForm.InputRightIcon,
          placeholder: 'DD/MM/YY',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: 'Ngày sinh',
            rightIcon: 'birdthday',
            rightIconSize: 20
          },
          onFocus: () => {
            Picker.hide()
            Keyboard.dismiss()
            this.showDateTimePicker()
          }
        },
        sex: {
          template: TcombForm.InputRightIcon,
          placeholder: 'Nam',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: 'Giới tính',
            rightIcon: 'dropdown',
            rightIconSize: 10
          },
          onFocus: () => {
            Keyboard.dismiss()
            this.showGenderPicker()
          }
        },
        idNumber: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập CMND',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: 'CMND'
          }
        },
        address: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập địa chỉ',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: 'Địa chỉ'
          }
        }
      }
    }
    return (
      <Form
        ref={(form) => {
          this.form = form
        }}
        type={userAccount}
        options={optionsForm}
        value={this.state.value}
        onChange={this.onChange}
      />
    )
  }

  showGenderPicker() {
    const data = ['Nam', 'Nữ']
    this.setState({ ...this.state, visibleOverlay: true })

    Picker.init({
      pickerData: data,
      selectedValue: [this.state.value.sex],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Thoát',
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerToolBarBg: [201, 32, 51, 1],
      pickerBg: [255, 255, 255, 1],
      pickerTitleText: '',
      onPickerConfirm: (selectValue) => {
        this.setState({
          ...this.state,
          visibleOverlay: false,
          value: {
            ...this.state.value,
            sex: selectValue[0]
          }
        })
      },
      onPickerCancel: () => {
        this.setState({
          ...this.state,
          visibleOverlay: false
        })
      }
    })
    Picker.show()
  }

  showImagePicker() {
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {

      } else if (response.error) {
        
      } else {
        this.props.onUploadAvatar(response.uri, (avatarUrl) => {
          this.setState({ value: { ...this.state.value, avatarUrl } })
        })
      }
    })
  }

  async capture() {
    const pathCrop = await this.cropper.crop()
    this.setState({
      pickedUri: null
    })
    this.props.onUploadAvatar(pathCrop, (avatarUrl) => {
      this.setState({ value: { ...this.state.value, avatarUrl } })
    })
  }

  render() {
    if (this.props.isFetching === true || (this.props.accountInfo.name === undefined && this.props.isFirstLogin === false)) {
      return <Loading />
    }

    const avatar = <Image style={[styles.avatarImage]} source={{ uri: this.state.value.avatarUrl || Themes.Images.avatar }} />

    if (_.isNull(this.state.pickedUri) === false) {
      return (
        <View style={styles.containerPicker}>
          <View style={styles.imagePicker}>
            <ImageCrop
              ref={(comp) => {
                this.cropper = comp
              }}
              image={this.state.pickedUri}
              cropHeight={width - 20}
              cropWidth={width - 20}
              zoom={20}
              maxZoom={80}
              minZoom={20}
              panToMove
              pinchToZoom
              format={'file'}
              filePath={path}
            />
          </View>
          <View style={styles.buttonGroup}>
            <Button textStyle={styles.buttonExitStyle} onPress={() => this.setState({ pickedUri: null })} style={styles.actionExitButton}>Thoát</Button>
            <Button textStyle={styles.buttonAcceptStyle} onPress={() => this.capture()} style={styles.actionAcceptButton}>Chọn</Button>
          </View>
        </View>
      )
    }

    return (
      <ScrollView
        bounces={false}
        automaticallyAdjustContentInsets={false}
        keyboardShouldPersistTaps={'always'}
      >
        <View style={[styles.container]}>
          <View style={[styles.wrapperImg]}>
            <TouchableOpacity onPress={() => this.showImagePicker()} style={[styles.btnAvatar]}>
              {avatar}
              <Icon style={[styles.iconAvatar]} name="edit" color="#000" size={22} />
            </TouchableOpacity>
            <View style={[styles.boxAvatar]}>
              <TextFont size={19} style={[styles.txtPhone]}>SĐT: {this.props.accountInfo.phone}</TextFont>
            </View>
          </View>
          {this.renderForm()}
          <DateTimePicker
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this.handleDatePicked}
            onCancel={this.hideDateTimePicker}
            confirmTextIOS="Ok"
            date={moment(this.state.value.birthday, 'DD/MM/YYYY').toDate()}  
          />
          <Button
            style={{ marginBottom: 10 }}
            onPress={this.onPressSubmit}
            btnColor={Themes.Colors.btnColor1}
          >
            Lưu
          </Button>
        </View>
        <Overlay
          visible={this.state.visibleOverlay}
          onPressOverlay={() => {
            Picker.hide()
            this.setState({ visibleOverlay: false })
          }}
        />
        <KeyboardSpacer />
      </ScrollView>
    )
  }

  componentDidUpdate() {
    const { apiResponse, navigator, isFirstLogin } = this.props
    if (apiResponse.type === types.UPDATE_USERINFO_SUCCESS) {
      Alert.alert('Thông báo', 'Cập nhật hồ sơ thành công', [{
        text: 'Đóng',
        onPress: () => {
          if (isFirstLogin === true) {
            Navigation.navToHome()
          } else {
            navigator.pop()
          }
        }
      }])
    }
  }

}
Profile.propTypes = {}
export default Profile
