import React, { Component } from 'react'
import { Platform } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Picker from 'react-native-picker'
import Profile from './Profile'
import Config from '../../../screens/config'
import { Colors } from '../../../ui/themes'
import { Loading } from '../../../ui/'
import { types, actions as AccountAction } from '../../../redux/account/'

class ProfileContainer extends Component {
  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB,
    statusBarColor: Colors.navColor
  }

  constructor(props) {
    super(props)
    this.state = {
      isFetching: true
    }
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onUploadAvatar = this.onUploadAvatar.bind(this)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
  }

  componentWillMount() {
    const { accountAction, isFirstLogin, payload } = this.props
    const accountInfo = payload.accountInfo
    if (isFirstLogin === true) {
      accountAction.getUserInfoDocdac(accountInfo.accessToken, Platform.OS, 1)
    } else {
      accountAction.getUserInfo(accountInfo.accessToken)
    }
  }

  componentWillReceiveProps(props) {
    const { apiResponse } = props.payload
    console.log(apiResponse)
    if (apiResponse.type === types.GET_USERINFO_SUCCESS || apiResponse.type === types.GET_USERINFO_DOCDAC_SUCCESS) {
      console.log('SET STATE')
      this.setState({ isFetching: false })
    }
  }

  onPressSubmit(value) {
    const { accountAction, payload } = this.props
    const accountInfo = payload.accountInfo
    accountAction.updateUserInfo(accountInfo.accessToken, value.name, value.idNumber, value.birthday, value.gender, value.email, value.address, value.avatarUrl)
  }

  onNavigatorEvent(event) {
    switch (event.id) {
      case 'willDisappear':
        Picker.hide()
        break
      default:
        break
    }
  }

  onUploadAvatar(avatar, callback) {
    const { accountAction } = this.props
    accountAction.uploadAvatar(avatar, callback)
  }

  render() {
    const { accountInfo, apiResponse } = this.props.payload
    const { isFetching } = this.state
    console.log('RENDER', isFetching)
    if (isFetching === true) {
      return <Loading />
    }

    return (
      <Profile
        onPressSubmit={this.onPressSubmit}
        navigator={this.props.navigator}
        accountInfo={accountInfo}
        isFirstLogin={this.props.isFirstLogin}
        isFetching={this.state.isFetching}
        apiResponse={apiResponse}
        onUploadAvatar={this.onUploadAvatar}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer)
