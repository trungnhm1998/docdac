import React, { Component } from 'react'
import { Alert } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ForgotPassword from './ForgotPassword'
import Config from '../../../screens/config'
import * as AccountAction from '../../../redux/account/account.actions'

class ForgotPasswordContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onPressResendOTP = this.onPressResendOTP.bind(this)
    this.navToLogin = this.navToLogin.bind(this)
  }

  navToLogin() {
    Alert.alert('Thông báo', 'Cập nhật mật khẩu thành công', [{
      text: 'Đăng nhập lại',
      onPress: () => {
        this.props.navigator.resetTo(Config.screen.login)
      }
    }])
  }

  onPressSubmit(value) {
    const { accountAction, phone } = this.props
    accountAction.forgotPassword(phone, value.pinCode, value.passWord, value.repeatPassWord, {
      loginScreen: this.navToLogin
    })
  }

  onPressResendOTP() {
    const { accountAction, payload } = this.props
    const accountInfo = payload.accountInfo
    accountAction.sendOTPForgotPassword(accountInfo.phone)
  }

  render() {
    return (
      <ForgotPassword
        onPressSubmit={this.onPressSubmit}
        onPressResendOTP={this.onPressResendOTP}
        phone={this.props.phone}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordContainer)
