import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'
import { Themes, TextFont, TcombForm, Button } from '../../../ui'
import styles from './login.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const userAccount = t.struct({
  password: t.String
})

class Login extends Component {

  constructor(props) {
    super(props)
    this.state = {}
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onPressForgotPassword = this.onPressForgotPassword.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  onPressSubmit = () => {
    const value = this.form.getValue()
    if (value) {
      this.props.onPressSubmit(value.password)
    }
  }

  onPressForgotPassword = () => {
    this.props.onPressForgotPassword()
  }

  onChange(value) {
    this.setState({ value })
  }

  renderForm = () => {
    const layout = (locals) => {
      return (
        <View style={{ marginTop: 15 }}>
          <View>{locals.inputs.password}</View>
        </View>
      )
    }
    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        password: {
          template: TcombForm.InputTextNormal,
          placeholder: '******',
          placeholderTextColor: '#E7E7E7',
          secureTextEntry: true,
          autoFocus: true,
          config: {
            label: 'Mời bạn nhập mật khẩu'
          }
        }
      }
    }
    return (
      <Form
        ref={(form) => {
          this.form = form
        }}
        value={this.state.value}
        onChange={this.onChange}
        type={userAccount}
        options={options}
      />
    )
  }

  render() {
    return (
      <View style={[Themes.styleGB.container]}>
        <StatusBar barStyle="light-content" />
        {this.renderForm()}
        <Button onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Tiếp tục</Button>
        <TextFont onPress={this.onPressForgotPassword} style={[styles.txtForgetPass]}>Quên mật khẩu ?</TextFont>
      </View>
    )
  }
}
Login.propTypes = {}

export default Login
