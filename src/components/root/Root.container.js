import React, { Component } from 'react'
import { AsyncStorage, Platform, View, Image, Alert, Linking } from 'react-native'
import { persistStore } from 'redux-persist'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import OneSignal from 'react-native-onesignal'
// import codePush from 'react-native-code-push'
import DeviceInfo from 'react-native-device-info'
import semver from 'semver'
import store from '../../configs/store.config'
import { PushNotificationHandler } from '../../handler'
import { actions as AccountAction, types } from '../../redux/account'
import { actions as LotteryAction } from '../../redux/lottery/'
import { actions as InitAction } from '../../redux/init/'
import { Navigation } from '../../screens/navigation'
import Config from '../../screens/config'
import { Colors, Images } from '../../ui/themes'

// const codePushOptions = { checkFrequency: codePush.CheckFrequency.ON_APP_RESUME }

class RootContainer extends Component {
  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE,
    statusBarColor: Colors.navColor,
    navBarHidden: true
  }

  constructor(props) {
    super(props)
  }

  componentWillMount() {

    persistStore(store, {
      storage: AsyncStorage,
      blacklist: ['apiResponse', 'lottery', 'paymentInfo', 'initConfig']
    }, () => {
      const { accountAction, payload } = this.props
      const accountInfo = payload.accountInfo
      if (accountInfo.accessToken) {
        accountAction.getUserInfoDocdac(accountInfo.accessToken, Platform.OS, 1, '')
      } else {
        Navigation.navToLogin()
      }
    })

    const { initAction } = this.props
    initAction.getInitConfig()

  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.navColor }}>
        <Image source={Images.docdacLogo} />
      </View>
    )
  }

  componentDidUpdate() {
    const { payload } = this.props
    const { apiResponse } = payload
    if (apiResponse.type === types.GET_USERINFO_DOCDAC_SUCCESS) {
      // Answers.logLogin('USER_RESUME', true)
      if(__DEV__) {
        Navigation.navToHome()
      } else{
        Navigation.navToWelcome()
      }
    }
  }

  componentDidMount() {
    // const { deviceAction } = this.props    
    // deviceAction.setTimeSleepApp(false)

    OneSignal.addEventListener('received', PushNotificationHandler.onReceived)
    OneSignal.addEventListener('opened', PushNotificationHandler.onOpened)
    // OneSignal.addEventListener('registered', PushNotificationHandler.onRegistered)
    OneSignal.addEventListener('ids', PushNotificationHandler.onIds)

    this.props.accountAction.getMoneySetting()
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch),
    lotteryAction: bindActionCreators(LotteryAction, dispatch),
    initAction: bindActionCreators(InitAction, dispatch),
  }
}

// export default codePush(codePushOptions)(connect(mapStateToProps, mapDispatchToProps)(RootContainer))
export default connect(mapStateToProps, mapDispatchToProps)(RootContainer)
