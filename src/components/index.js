import Root from './root/Root.container'
import SendOTPRegister from './account/sendOTPRegister/SendOTPRegister.container'
import Register from './account/register/Register.container'
import RegisterAccountKit from './account/registerAccountKit/RegisterAccountKit.container'
import Login from './account/login/Login.container'
import Welcome from './account/welcome/Welcome.container'
import Home from './home/Home.container'
import ForgotPassword from './account/forgotPassword/ForgotPassword.container'
import ForgotPasswordAccountKit from './account/forgotPasswordAccountKit/ForgotPasswordAccountKit.container'
import Profile from './account/profile/Profile.container'
import PickupTicket from './pickupTicket/mega/PickupTicket.container'
import PickupTicketMegaPower from './pickupTicket/megaPower/PickupTicket.container'
import PickupTicketMax from './pickupTicket/max/PickupTicket.container'
import PaymentHome from './account/payment/paymentHome/PaymentHome.container'
import DepositGatewayInput from './account/payment/depositMethod/depositGateway/depositGatewayInput/DepositGatewayInput.container'
import DepositGatewayNapasInput from './account/payment/depositMethod/depositGateway/depositGatewayNapasInput/DepositGatewayNapasInput.container'
import DepositGatewayCard from './account/payment/depositMethod/depositGateway/depositGatewayCard/DepositGatewayCard.container'
import DepositGatewaySuccess from './account/payment/depositMethod/depositGateway/depositGatewaySuccess/DepositGatewaySuccess.container'
import DepositGatewayFail from './account/payment/depositMethod/depositGateway/depositGatewayFail/DepositGatewayFail.container'
import DepositGatewayCheckout from './account/payment/depositMethod/depositGateway/depositGatewayCheckout/DepositGatewayCheckout.container'
import DepositGatewayWebmoneyCheckout from './account/payment/depositMethod/depositGateway/depositGatewayWebmoneyCheckout/DepositGatewayWebmoneyCheckout.container'
import DepositBankingInput from './account/payment/depositMethod/depositBanking/depositBankingInput/DepositBankingInput.container'
import WithdrawGateway from './account/payment/withdrawMethod/withdrawGateway/WithdrawGateway.container'
import WithdrawGatewayConfirm from './account/payment/withdrawMethod/withdrawGatewayConfirm/WithdrawGatewayConfirm.container'
import PaymentHistory from './account/payment/paymentHistory/PaymentHistory.container'
import SelectContacts from './account/payment/selectContacts/SelectContacts.container'
import SettingHome from './account/setting/settingHome/SettingHome.container'
import SettingHomeIntro from './account/setting/settingHomeIntro/SettingHome.container'
import SettingInfo from './account/setting/settingInfo/SettingInfo'
import LotteryResult from './lottery/lotteryResult/LotteryResult.container'
import LightBox from './modal/lightbox/LightBox.container'
import Loading from './modal/loading/Loading.container'
import LotteryHome from './lottery/lotteryHome/LotteryHome.container'
import Lottery655Delivery from './lottery/lottery655/lotteryDeliveryDetail/LotteryDeliveryDetail.container'
import Lottery655Pick from './lottery/lottery655/lotteryPickDetail/LotteryPickDetail.container'
import Lottery655Stored from './lottery/lottery655/lotteryStoredDetail/LotteryStoredDetail.container'
import Lottery655WinForm from './lottery/lottery655/lotteryStoredWinForm/LotteryStoredWinForm.container'
import Lottery645Delivery from './lottery/lottery645/lotteryDeliveryDetail/LotteryDeliveryDetail.container'
import Lottery645Pick from './lottery/lottery645/lotteryPickDetail/LotteryPickDetail.container'
import Lottery645Stored from './lottery/lottery645/lotteryStoredDetail/LotteryStoredDetail.container'
import Lottery645WinForm from './lottery/lottery645/lotteryStoredWinForm/LotteryStoredWinForm.container'
import LotteryMax4dDelivery from './lottery/lotteryMax4d/lotteryDeliveryDetail/LotteryDeliveryDetail.container'
import LotteryMax4dPick from './lottery/lotteryMax4d/lotteryPickDetail/LotteryPickDetail.container'
import LotteryMax4dStored from './lottery/lotteryMax4d/lotteryStoredDetail/LotteryStoredDetail.container'
import AddAddress from './modal/pickupModal/newAddress.container'
import AddressList from './modal/pickupModal/addressList.container'
import DetailMax4d from './pickupTicket/confirm/max/Detail'
import DetailMega from './pickupTicket/confirm/mega/Detail'
import ConfirmMax4d from './pickupTicket/confirm/max/Confirm'
import ConfirmMega from './pickupTicket/confirm/mega/Confirm'
import ConfirmMegaPower from './pickupTicket/confirm/megaPower/Confirm'


export {
  Root,
  SendOTPRegister,
  Register,
  RegisterAccountKit,
  Login,
  Welcome,
  Home,
  Profile,
  ForgotPassword,
  ForgotPasswordAccountKit,
  PickupTicket,
  PickupTicketMegaPower,
  PickupTicketMax,
  PaymentHome,
  DepositGatewayInput,
  DepositGatewayNapasInput,
  DepositGatewayCard,
  DepositGatewaySuccess,
  DepositGatewayFail,
  DepositGatewayCheckout,
  DepositGatewayWebmoneyCheckout,
  WithdrawGateway,
  WithdrawGatewayConfirm,
  PaymentHistory,
  SelectContacts,
  SettingHome,
  SettingHomeIntro,
  SettingInfo,
  LotteryResult,
  Loading,
  LightBox,
  LotteryHome,
  AddAddress,
  AddressList,
  Lottery645Pick,
  Lottery645Stored,
  Lottery645WinForm,
  Lottery645Delivery,
  Lottery655Pick,
  Lottery655WinForm,
  Lottery655Stored,
  Lottery655Delivery,
  DepositBankingInput,
  DetailMax4d,
  DetailMega,
  ConfirmMax4d,
  ConfirmMega,
  ConfirmMegaPower,
  LotteryMax4dDelivery,
  LotteryMax4dPick,
  LotteryMax4dStored,
}
