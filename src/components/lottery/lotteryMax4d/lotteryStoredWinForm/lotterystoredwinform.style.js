import { StyleSheet, PixelRatio } from 'react-native'
import { Themes } from '../../../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  rowWinForm: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 12,
    backgroundColor: '#f7f7f7'
  },
  txtWinForm: {
    marginLeft: 10
  },
  btnConfirm: {
    backgroundColor: Themes.Colors.navColor,
    borderRadius: 50,
    marginTop: 10,
    marginBottom: 10,
    marginHorizontal: 10
  }

})

export default styles
