import React, { Component } from 'react'
import { View, ScrollView, Image, TouchableOpacity } from 'react-native'
import { Themes, TextFont, Icon, Button, TcombForm } from '../../../../ui'
import styles from './lotterystoredwinform.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const userAccount = t.struct({
  location: t.String,
  store: t.String,
  date: t.String,
  hour: t.String
})

class LotteryStoredWinForm extends Component {

  constructor(props) {
    super(props)
    this.state = {}
  }

  renderFormLocation = () => {
    const layout = (locals) => {
      return (
        <View>
          <View style={{ backgroundColor: '#fff', padding: 10 }}>
            <View style={{ marginBottom: 10 }}>{locals.inputs.location}</View>
            <View>{locals.inputs.store}</View>
          </View>
          <View style={[styles.rowWinForm]}>
            <Icon name="circle" color={Themes.Colors.navColor} />
            <TextFont style={[styles.txtWinForm]}>Thời gian nhận thưởng ca</TextFont>
          </View>
          <View style={{ flexDirection: 'row', padding: 10 }}>
            <View style={{ flex: 1, paddingRight: 5 }}>{locals.inputs.date}</View>
            <View style={{ flex: 1, paddingLeft: 5 }}>{locals.inputs.hour}</View>
          </View>
        </View>
      )
    }
    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        location: {
          template: TcombForm.InputLeftIcon,
          placeholder: 'Chọn khu vực',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: '',
            leftIcon: 'location',
            leftIconSize: 17,
            leftIconColor: '#9f9f9f',
            inputStyle: {
              marginBottom: 0,
              borderRadius: 24
            }
          }
        },
        store: {
          template: TcombForm.InputLeftIcon,
          placeholder: 'Đại lý',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: '',
            leftIcon: 'store',
            leftIconSize: 17,
            leftIconColor: '#9f9f9f',
            inputStyle: {
              marginBottom: 0,
              borderRadius: 24
            }
          }
        },
        date: {
          template: TcombForm.InputModalLeftIcon,
          placeholder: 'Ngày nhận vé',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: '',
            leftIcon: 'list',
            leftIconSize: 17,
            leftIconColor: '#9f9f9f',
            inputStyle: {
              marginBottom: 0,
              borderRadius: 24
            }
          }
        },
        hour: {
          template: TcombForm.InputModalLeftIcon,
          placeholder: 'Giờ nhận vé',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: '',
            leftIcon: 'history',
            leftIconSize: 17,
            leftIconColor: '#9f9f9f',
            inputStyle: {
              marginBottom: 0,
              borderRadius: 24
            }
          }
        }
      }
    }
    return (
      <Form ref={(form) => { this.form = form }} type={userAccount} options={options} />
    )
  }

  render() {
    return (
      <View style={[styles.container]}>
        <View style={[styles.rowWinForm]}>
          <Icon name="circle" color={Themes.Colors.navColor} />
          <TextFont style={[styles.txtWinForm]}>Vé trúng trưởng.</TextFont>
        </View>
        <View style={[styles.rowWinForm]}>
          <Icon name="circle" color={Themes.Colors.navColor} />
          <TextFont style={[styles.txtWinForm]}>Chọn đại lý: </TextFont>
        </View>
        {this.renderFormLocation()}
        <Button onPress={this.onPressWinForm} style={[styles.btnConfirm]}>ĐĂNG KÝ NHẬN THƯỞNG</Button>
        <TextFont color="#727272" style={Themes.styleGB.textCenter} size={12}>* Không được thấp hơn 2 ngày không bao gồm ngày nghỉ</TextFont>
      </View>
    )
  }

}
LotteryStoredWinForm.propTypes = {}
export default LotteryStoredWinForm
