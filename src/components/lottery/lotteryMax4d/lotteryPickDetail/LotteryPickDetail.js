import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'
import { View, ScrollView, Image, TouchableOpacity } from 'react-native'
import { Themes, TextFont, Icon, Button } from '../../../../ui'
import styles from './lotterypickdetail.style'
import * as Utils from '../../../../utils'

class LotteryPickDetail extends Component {

  constructor(props) {
    super(props)
    this.state = {}
    this.onPressReceiveTicket = this.onPressReceiveTicket.bind(this)
  }

  onPressReceiveTicket = () => {
    this.props.onPressReceiveTicket()
  }

  renderNumber = (data, winData) => {
    const arrayBoard = []
    let winNumberList = []

    if (_.isEmpty(winData) === false) {
      winNumberList = winData.win_number.split(',')
      _.forEach(winNumberList, (item, key) => {
        winNumberList[key] = '00'.substring(0, 2 - item.length) + item
      })
    }

    _.forEach(data.ticket_number.split(','), (item, index) => {
      const colorWin = winNumberList.indexOf(item) !== -1 ? 'red' : 'black'
      arrayBoard.push(
        <View key={index} style={[styles.rowNumber]}>
          <TextFont color={colorWin}>{item}</TextFont>
        </View>
      )
    })
    return arrayBoard
  }

  renderListBoard = () => {
    const board = []
    const { ticket_data, win_data } = this.props.lotteryDetail
    _.forEach(ticket_data.list_ticket, (data, dataIndex) => {
      const string = String.fromCharCode(65 + dataIndex)

      const winData = _.find(win_data, item => item.board_order === dataIndex)

      board.push(
        <View key={dataIndex} style={[styles.rowBoard]}>
          <View style={{ alignItems: 'center', paddingHorizontal: Themes.Metrics.screenWidth * 0.015 }}>
            <TextFont>{string} :</TextFont>
          </View>
          <View style={[styles.groupNumber]}>
            {this.renderNumber(data, winData)}
          </View>
          <TextFont>{data.board_price}</TextFont>
        </View>
      )
    })
    return board
  }

  renderTicketInfo = () => {
    const data = this.props.lotteryDetail
    return (
      <View>
        <View style={[styles.rowTypeTicket]}>
          <View style={[styles.colType]}>
            <Icon color={Themes.Colors.navColor} name="circle" size={9} />
            <TextFont style={[styles.txtType]}>LOẠI VÉ :</TextFont>
          </View>
          <TextFont style={[styles.txtTypeTicket]}>Vé cứng (lấy tại đại lý)</TextFont>
        </View>
        <View style={[styles.boxTicket]}>
          <View style={[styles.boxTicketUI]}>
            <View style={[styles.TicketWhite]}>
              <TextFont style={[styles.txtTitleTicket]} size={16} font={Themes.Fonts.type.Bold}>MAX 4D</TextFont>
              <View style={[styles.lineBoxTicket]} />
              <View style={[styles.lineBoxTicket, styles.marginTop5]} />

              {this.renderListBoard()}

              <View style={[styles.lineBoxTicket, { marginTop: 10 }]} />
              <View style={[styles.lineBoxTicket, styles.marginTop5]} />
              <View style={{ paddingHorizontal: 5 }}>
                <View style={[styles.rowTotal, { marginTop: 7, justifyContent: 'space-between' }]}>
                  <TextFont size={13}>Tổng</TextFont>
                  <TextFont font={Themes.Fonts.type.Bold} size={13} style={[styles.txtNumberTotal]}>{Utils.formatMoney(data.money)} đ</TextFont>
                </View>
                <View style={[styles.rowTotal, { marginTop: 3, justifyContent: 'space-between' }]}>
                  <TextFont size={13}>Kỳ QSMT</TextFont>
                  <TextFont size={13} style={[styles.txtNumberTotal]}>{data.period}</TextFont>
                </View>
                <TextFont size={13} style={[styles.txtTextRight, { marginTop: 3 }]}>{data.created_date}</TextFont>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }

  renderUITypeConfirm = () => {
    const data = this.props.lotteryDetail
    return (
      <ScrollView>

        {this.renderTicketInfo()}

        <View style={[styles.viewInfo]}>
          <View style={[styles.rowInfoUser]}>
            <Icon color={Themes.Colors.navColor} name="circle" size={9} />
            <TextFont style={[styles.txtType]}>ĐẠI LÝ NHẬN VÉ CHO BẠN :</TextFont>
          </View>

          <View style={[styles.rowInforName]}>
            <Image source={Themes.Images.avtStoreVietllot} />
            <View style={[styles.colInforName]}>
              <TextFont size={18} style={[styles.txtTitleName]}>{data.agent_name}</TextFont>
              <TextFont style={{ marginTop: 5 }} color={Themes.Colors.navColor} size={11}>Vui lòng đến nhận vé trước 16:00 {moment(data.drawing_date).format('DD/MM/YYYY')}</TextFont>
              <View style={[styles.rowLocation]}>
                <View style={[styles.colLocation]}>
                  <Icon color="#585858" size={13} name="location" />
                  <TextFont color="#585858" style={[styles.txtLocation]} size={12}>{data.agent_address}</TextFont>
                </View>
                <View style={[styles.colLocation, { marginTop: 5 }]}>
                  <Icon color="#585858" size={13} name="phone" />
                  <TextFont color="#585858" style={[styles.txtPhone]} size={12}>{data.agent_phone}</TextFont>
                </View>
              </View>
            </View>
          </View>

          {data.delivery_date === null ?
            <View>
              <Button onPress={this.onPressReceiveTicket} style={[styles.btnConfirm]}>XÁC NHẬN LẤY VÉ</Button>
              <TextFont style={[Themes.styleGB.textCenter]} color="#727272" size={12}>* Bạn nhớ bấm <TextFont color={Themes.Colors.navColor} size={12}>XÁC NHẬN</TextFont> trong lịch sử khi nhân viên giao vé cho bạn Cám ơn !!!</TextFont>
            </View>
            :
            <Button style={[styles.btnConfirmSuccess]}>ĐÃ LẤY VÉ (15:00 16/09/2016)</Button>
          }

        </View>
      </ScrollView>
    )
  }

  fetchNumberRow = (obj, special) => {
    const number = []
    const resultList = obj.split(',')
    const backgroundColor = special === true ? Themes.Colors.max4dColor : '#fff'
    const color = special === true ? '#fff' : Themes.Colors.max4dColor
    _.map(resultList, (value, key) => {
      number.push(
        <View key={key} style={[styles.numberResultMax, { backgroundColor }]}>
          <TextFont color={color}>{value}</TextFont>
        </View>
      )
    })
    return number
  }

  renderRowNumber = (data) => {
    return (
      <View style={[Themes.styleGB.centerContent]}>
        <TextFont size={16} style={[styles.txtTitle4d, { color: Themes.Colors.max4dColor }]}>Giải Nhất</TextFont>
        <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[0][0], true)}</View>
        <TextFont size={16} style={[styles.txtTitle4d, { color: '#6e6e6e' }]}>Giải Nhì</TextFont>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[1][0])}</View>
          <View style={[styles.lineResultMax4d]} />
          <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[1][1])}</View>
        </View>
        <TextFont size={16} style={[styles.txtTitle4d, { color: '#6e6e6e' }]}>Giải Ba</TextFont>
        <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[2][0])}</View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[2][1])}</View>
          <View style={[styles.lineResultMax4d]} />
          <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[2][2])}</View>
        </View>
        <TextFont size={16} style={[styles.txtTitle4d, { color: '#6e6e6e' }]}>Giải Khuyến Khích</TextFont>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[3][0])}</View>
          <View style={[styles.lineResultMax4d]} />
          <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[4][0])}</View>
        </View>
      </View>
    )
  }

  renderUITypeWin = () => {
    const data = this.props.lotteryDetail
    return (
      <ScrollView>

        {this.renderTicketInfo()}

        <View style={[styles.viewInfo]}>
          <View style={[styles.rowInfoUser]}>
            <Icon color={Themes.Colors.navColor} name="circle" size={9} />
            <TextFont style={[styles.txtType]}>KẾT QUẢ KỲ QUAY THƯỞNG #{data.period}</TextFont>
          </View>

          <View style={[styles.rowNumberWin]}>
            {this.renderRowNumber(JSON.parse(data.draw_number))}
          </View>

          <Image style={[styles.winnerImg]} source={Themes.Images.winImage} />
          <View style={[styles.rowWin]}>
            <TextFont color="#666666">Bạn đã trúng: </TextFont>
            <TextFont color={Themes.Colors.navColor} size={18} font={Themes.Fonts.type.Bold} style={[styles.txtMoneyWin]}>{Utils.formatMoney(data.total_win)} đ</TextFont>
            <TouchableOpacity><Icon color="#666666" size={22} name="question" /></TouchableOpacity>
          </View>
          <TextFont color="#666" size={11} style={[Themes.styleGB.textCenter]}><TextFont color={Themes.Colors.navColor} size={12}>Chúc mừng bạn</TextFont> - Hãy nhanh chóng đến đại lý gần nhất để nhận thưởng</TextFont>

        </View>
      </ScrollView>
    )
  }

  renderUITypeLose = () => {
    const data = this.props.lotteryDetail
    return (
      <ScrollView>

        {this.renderTicketInfo()}

        <View style={[styles.viewInfo]}>
          <View style={[styles.rowInfoUser]}>
            <Icon color={Themes.Colors.navColor} name="circle" size={9} />
            <TextFont style={[styles.txtType]}>KẾT QUẢ KỲ QUAY THƯỞNG #{data.period}</TextFont>
          </View>

          <View style={[styles.rowNumberWin]}>
            {this.renderRowNumber(JSON.parse(data.draw_number))}
          </View>

          <Image style={[styles.winnerImg]} source={Themes.Images.closeImage} />
          <TextFont color="#666" size={12} style={[Themes.styleGB.textCenter]}>Vé không trúng thưởng - Chúc bạn may mắn lần sau</TextFont>

        </View>
      </ScrollView>
    )
  }

  checkUI = () => {
    if (this.props.lotteryTypeUI === 1) {
      return (this.renderUITypeConfirm())
    } else if (this.props.lotteryTypeUI === 2) {
      return (this.renderUITypeWin())
    } else if (this.props.lotteryTypeUI === 3) {
      return (this.renderUITypeLose())
    }
    return null
  }

  render() {
    return (
      <View style={[styles.container]}>
        {this.checkUI()}
      </View>
    )
  }

}
LotteryPickDetail.propTypes = {}
export default LotteryPickDetail
