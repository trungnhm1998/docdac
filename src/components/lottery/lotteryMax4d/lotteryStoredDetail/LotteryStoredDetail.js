import React, { Component } from 'react'
import _ from 'lodash'
import { View, ScrollView, Image, TouchableOpacity, Linking } from 'react-native'
import { Themes, TextFont, Icon, Button } from '../../../../ui'
import styles from './lotterystoreddetail.style'
import * as Utils from '../../../../utils'
import Config from '../../../../screens/config'
import { CurrencyType } from '../../../../configs/constant.config'

class LotteryStoredDetail extends Component {

  constructor(props) {
    super(props)
    this.state = {
      filledNumber: {
        A: [null, null, null, null],
        B: [null, null, null, null],
        C: [null, null, null, null],
        D: [null, null, null, null],
        E: [null, null, null, null],
        F: [null, null, null, null]
      },
      filledPrice: {
        A: null,
        B: null,
        C: null,
        D: null,
        E: null,
        F: null
      },
      easyPick: {
        A: false,
        B: false,
        C: false,
        D: false,
        E: false,
        F: false
      }
    }
    this.onPressWinForm = this.onPressWinForm.bind(this)
  }

  onPressWinForm() {
    this.props.onPressWinForm()
  }

  renderNumber = (data, winData) => {
    const arrayBoard = []
    let winNumberList = []

    if (_.isEmpty(winData) === false) {
      winNumberList = winData.win_number.split(',')
      _.forEach(winNumberList, (item, key) => {
        winNumberList[key] = item
      })
    }

    _.forEach(data.ticket_number.split(','), (item, index) => {
      const colorWin = winNumberList.indexOf(item) !== -1 ? 'red' : 'black'
      arrayBoard.push(
        <View key={index} style={[styles.rowNumber]}>
          <TextFont color={colorWin}>{item}</TextFont>
        </View>
      )
    })
    return arrayBoard
  }

  renderListBoard = () => {
    const board = []
    const { ticket_data, win_data } = this.props.lotteryDetail
    _.forEach(ticket_data.list_ticket, (data, dataIndex) => {
      const string = String.fromCharCode(65 + dataIndex)

      const winData = _.find(win_data, item => item.board_order === dataIndex)

      board.push(
        <View key={dataIndex} style={[styles.rowBoard]}>
          <View style={{ alignItems: 'center', paddingHorizontal: Themes.Metrics.screenWidth * 0.015 }}>
            <TextFont>{string} :</TextFont>
          </View>
          <View style={[styles.groupNumber]}>
            {this.renderNumber(data, winData)}
          </View>
          <TextFont>{data.board_price}</TextFont>
        </View>
      )
    })
    return board
  }
  LaunchURL = (url) => {
    Linking.canOpenURL(url).then(() => {
      Linking.openURL(url)
    }).catch(err => console.warn('An unexpected error happened', err))
  }

  renderTicketInfo = () => {
    const data = this.props.lotteryDetail
    return (
      <View>
        <View style={[styles.rowTypeTicket]}>
          <View style={[styles.colType]}>
            <Icon color={Themes.Colors.navColor} name="circle" size={9} />
            <TextFont style={[styles.txtType]}>LOẠI VÉ :</TextFont>
          </View>
          <TextFont style={[styles.txtTypeTicket]}>Vé điện tử</TextFont>
        </View>
        <View style={[styles.boxTicket]}>
          <View style={[styles.boxTicketUI]}>
            <View style={[styles.TicketWhite]}>
              <TextFont style={[styles.txtTitleTicket]} size={16} font={Themes.Fonts.type.Bold}>MAX 4D</TextFont>
              <View style={[styles.lineBoxTicket]} />
              <View style={[styles.lineBoxTicket, styles.marginTop5]} />

              {this.renderListBoard()}

              <View style={[styles.lineBoxTicket, { marginTop: 10 }]} />
              <View style={[styles.lineBoxTicket, styles.marginTop5]} />
              <View style={{ paddingHorizontal: 5 }}>
                <View style={[styles.rowTotal, { marginTop: 7, justifyContent: 'space-between' }]}>
                  <TextFont size={13}>Tổng</TextFont>
                  <TextFont font={Themes.Fonts.type.Bold} size={13} style={[styles.txtNumberTotal]}>{Utils.formatMoney(data.money)} đ</TextFont>
                </View>
                <View style={[styles.rowTotal, { marginTop: 3, justifyContent: 'space-between' }]}>
                  <TextFont size={13}>Kỳ QSMT</TextFont>
                  <TextFont size={13} style={[styles.txtNumberTotal]}>{data.period}</TextFont>
                </View>
                <TextFont size={13} style={[styles.txtTextRight, { marginTop: 3 }]}>{data.created_date}</TextFont>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }

  renderStatus(state) {
    if (state === '1') {
      return <TextFont style={[styles.txtType]}>Vé số Vietlott đã được xử lý bên thiết bị đầu cuối và lưu trữ tại Công ty. Biên nhận điện tử đã được gửi về email của bạn, đồng thời ghi nhận tại lịch sử giao dịch.</TextFont>
    } else if (parseInt(state, 10) < 0) {
      return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
          <TextFont style={[styles.txtType]}>Trạng thái</TextFont><TextFont style={[styles.txtType, { color: 'red', paddingRight: 10 }]}>Vé đã hủy</TextFont>
        </View>
      )
    }
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
        <TextFont style={[styles.txtType]}>Trạng thái</TextFont><TextFont style={[styles.txtType, { color: 'green', paddingRight: 10 }]}>Vé đang xử lý</TextFont>
      </View>
    )
  }

  renderUITypeConfirm = () => {
    const { state } = this.props.lotteryDetail
    return (
      <ScrollView>

        {this.renderTicketInfo()}

        <View style={[styles.viewInfo]}>
          <View style={[styles.rowInfoUser, { alignItems: 'flex-start' }]}>
            {this.renderStatus(state)}
          </View>
          <TextFont style={[Themes.styleGB.textCenter, { marginTop: 10 }]}>Thắc mắc xin liên hệ<TextFont onPress={() => this.LaunchURL(`tel:${this.props.hotLine.hot_line}`)} color={Themes.Colors.navColor}> {this.props.hotLine.hot_line}</TextFont></TextFont>
        </View>
      </ScrollView>
    )
  }

  fetchNumberRow = (obj, special) => {
    const number = []
    const resultList = obj.split(',')
    const backgroundColor = special === true ? Themes.Colors.max4dColor : '#fff'
    const color = special === true ? '#fff' : Themes.Colors.max4dColor
    _.map(resultList, (value, key) => {
      number.push(
        <View key={key} style={[styles.numberResultMax, { backgroundColor }]}>
          <TextFont color={color}>{value}</TextFont>
        </View>
      )
    })
    return number
  }

  renderRowNumber = (data) => {
    return (
      <View style={[Themes.styleGB.centerContent]}>
        <TextFont size={16} style={[styles.txtTitle4d, { color: Themes.Colors.max4dColor }]}>Giải Nhất</TextFont>
        <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[0][0], true)}</View>
        <TextFont size={16} style={[styles.txtTitle4d, { color: '#6e6e6e' }]}>Giải Nhì</TextFont>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[1][0])}</View>
          <View style={[styles.lineResultMax4d]} />
          <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[1][1])}</View>
        </View>
        <TextFont size={16} style={[styles.txtTitle4d, { color: '#6e6e6e' }]}>Giải Ba</TextFont>
        <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[2][0])}</View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[2][1])}</View>
          <View style={[styles.lineResultMax4d]} />
          <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[2][2])}</View>
        </View>
        <TextFont size={16} style={[styles.txtTitle4d, { color: '#6e6e6e' }]}>Giải Khuyến Khích</TextFont>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[3][0])}</View>
          <View style={[styles.lineResultMax4d]} />
          <View style={{ flexDirection: 'row' }}>{this.fetchNumberRow(data[4][0])}</View>
        </View>
      </View>
    )
  }

  checkButtonWin() {
    const data = this.props.lotteryDetail
    if (this.props.scheduled === '0' && data.must_scheduled === 1) {
      return (
        <Button onPress={this.onPressWinForm} style={[styles.btnConfirm]}>ĐĂNG KÝ NHẬN THƯỞNG</Button>
      )
    } else if (this.props.scheduled === '0' && data.must_scheduled === 0) {
      return (
        <TextFont color="#666" size={14} style={[Themes.styleGB.textCenter]}>Tiền thưởng đã được chuyển vào tài khoản của bạn</TextFont>
      )
    }
    return <TextFont color="#666" size={14} style={[Themes.styleGB.textCenter]}>Tiền thưởng đã được chuyển vào tài khoản của bạn</TextFont>
  }

  renderUITypeWin = () => {
    const data = this.props.lotteryDetail
    return (
      <ScrollView>

        {this.renderTicketInfo()}

        <View style={[styles.viewInfo]}>
          <View style={[styles.rowInfoUser]}>
            <Icon color={Themes.Colors.navColor} name="circle" size={9} />
            <TextFont style={[styles.txtType]}>KẾT QUẢ KỲ QUAY THƯỞNG #{data.period}</TextFont>
          </View>

          <View style={[styles.rowNumberWin]}>
            {this.renderRowNumber(JSON.parse(data.draw_number))}
          </View>

          <Image style={[styles.winnerImg]} source={Themes.Images.winImage} />
          <View style={[styles.rowWin]}>
            <TextFont color="#666666">Bạn đã trúng: </TextFont>
            <TextFont color={Themes.Colors.navColor} size={18} font={Themes.Fonts.type.Bold} style={[styles.txtMoneyWin]}>{Utils.formatMoney(data.total_win)} đ</TextFont>
          </View>
          {this.checkButtonWin()}
        </View>
      </ScrollView>
    )
  }

  btnBuyTicket = () => {
    const data = this.props.lotteryDetail.ticket_data.list_ticket

    const filledNumber = _.cloneDeep(this.state.filledNumber)
    const filledPrice = _.cloneDeep(this.state.filledPrice)
    _.map(data, (newData, index) => {
      const string = String.fromCharCode(65 + index)
      const newArray = newData.ticket_number.split(',')
      const numberArray = _.map(newArray, i => parseInt(i, 0))
      filledPrice[string] = parseInt(_.findKey(CurrencyType, (i) => { return i === newData.board_price }))
      filledNumber[string] = numberArray
    })
    this.props.navigator.push({ ...Config.screen.megaPickupMax, passProps: { filledNumber, filledPrice } })
  }

  renderUITypeLose = () => {
    const data = this.props.lotteryDetail
    return (
      <ScrollView>

        {this.renderTicketInfo()}

        <View style={{ alignItems: 'center', marginTop: 20, marginBottom: 10 }}>
          <TouchableOpacity activeOpacity={0.8} onPress={() => this.btnBuyTicket()} style={{ backgroundColor: Themes.Colors.navColor, width: 120, justifyContent: 'center', alignItems: 'center' }}>
            <TextFont style={{ color: '#fff', paddingVertical: 10 }}>Mua vé lại</TextFont>
          </TouchableOpacity>
        </View>

        <View style={[styles.viewInfo]}>
          <View style={[styles.rowInfoUser]}>
            <Icon color={Themes.Colors.navColor} name="circle" size={9} />
            <TextFont style={[styles.txtType]}>KẾT QUẢ KỲ QUAY THƯỞNG #{data.period}</TextFont>
          </View>

          <View style={[styles.rowNumberWin]}>
            {this.renderRowNumber(JSON.parse(data.draw_number))}
          </View>

          <Image style={[styles.winnerImg]} source={Themes.Images.closeImage} />
          <TextFont color="#666" size={12} style={[Themes.styleGB.textCenter]}>Vé không trúng thưởng - Chúc bạn may mắn lần sau</TextFont>

        </View>
      </ScrollView>
    )
  }

  renderUITypeReceiveWin = () => {
    const data = this.props.lotteryDetail
    return (
      <ScrollView>
        {this.renderTicketInfo()}
        <View style={[styles.viewInfo]}>
          <View style={[styles.rowInfoUser]}>
            <Icon color={Themes.Colors.navColor} name="circle" size={9} />
            <TextFont style={[styles.txtType]}>KẾT QUẢ KỲ QUAY THƯỞNG #{data.period}</TextFont>
          </View>

          <View style={[styles.rowNumberWin]}>
            {this.renderRowNumber(JSON.parse(data.draw_number))}
          </View>

          <Image style={[styles.winnerImg]} source={Themes.Images.imgTicketWin} />
          <View style={[styles.rowWin]}>
            <TextFont color="#666666">Bạn đã trúng: </TextFont>
            <TextFont color={Themes.Colors.navColor} size={18} font={Themes.Fonts.type.Bold} style={[styles.txtMoneyWin]}>{Utils.formatMoney(data.total_win)} đ</TextFont>
            <TouchableOpacity><Icon color="#666666" size={22} name="question" /></TouchableOpacity>
          </View>

          <Button style={[styles.btnConfirmSuccess]}>ĐÃ NHẬN THƯỞNG</Button>

        </View>
      </ScrollView>
    )
  }

  checkUI = () => {
    if (this.props.lotteryTypeUI === 1) {
      return (this.renderUITypeConfirm())
    } else if (this.props.lotteryTypeUI === 2) {
      return (this.renderUITypeWin())
    } else if (this.props.lotteryTypeUI === 3) {
      return (this.renderUITypeLose())
    } else if (this.props.lotteryTypeUI === 4) {
      return (this.renderUITypeReceiveWin())
    }
    return null
  }

  render() {
    return (
      <View style={[styles.container]}>
        {this.checkUI()}
      </View>
    )
  }

}
LotteryStoredDetail.propTypes = {}
export default LotteryStoredDetail
