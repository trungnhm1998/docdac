import React, { Component } from 'react'
import { Platform, PixelRatio, Alert } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as LotteryAction, types } from '../../../../redux/lottery'
import LotteryStoredWinForm from './LotteryStoredWinForm'
import Config from '../../../../screens/config'
import Picker from 'react-native-picker'

class LotteryStoredWinFormContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  constructor(props) {
    super(props)
    this.state = {}
    this.onPressWinForm = this.onPressWinForm.bind(this)
    this.getAgentList = this.getAgentList.bind(this)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
  }

  componentWillMount() {

  }

  onNavigatorEvent(event) {
    switch (event.id) {
      case 'willDisappear':
        Picker.hide()
        break
    }
  }

  onPressWinForm = (value) => {
    const { lotteryAction } = this.props
    lotteryAction.postTicketScheduleReward(value)
  }

  getAgentList(districtId) {
    const { lotteryAction } = this.props
    lotteryAction.getAgentList({ districtId })
  }

  render() {
    const { lottery } = this.props.payload

    return (
      <LotteryStoredWinForm
        ticketId={this.props.ticketID}
        agentList={lottery.agentList}
        deliveryLocation={lottery.deliveryLocation}
        getAgentList={this.getAgentList}
        onPressWinForm={this.onPressWinForm}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    lotteryAction: bindActionCreators(LotteryAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LotteryStoredWinFormContainer)
