import React, { Component } from 'react'
import _ from 'lodash'
import { View, ScrollView, Image, TouchableOpacity, Linking, Platform } from 'react-native'
import { Themes, TextFont, Icon, Button } from '../../../../ui'
import styles from './lotterystoreddetail.style'
import * as Utils from '../../../../utils'
import Config from '../../../../screens/config'

class LotteryStoredDetail extends Component {

  constructor(props) {
    super(props)
    this.state = {
      filledNumber: {
        A: [],
        B: [],
        C: [],
        D: [],
        E: [],
        F: []
      }
    }
    this.onPressWinForm = this.onPressWinForm.bind(this)
  }

  LaunchURL = (url) => {
    Linking.canOpenURL(url).then(() => {
      Linking.openURL(url)
    }).catch(err => console.warn('An unexpected error happened', err))
  }

  onPressWinForm() {
    this.props.onPressWinForm()
  }

  renderNumber = (data, winData, jackpot2) => {
    const arrayBoard = []

    const numbers = data.ticket_number.split(',').map(value => parseInt(value, 10))
    _.forEach(numbers, (item, index) => {
      let colorWin = winData.indexOf(item) !== -1 ? 'red' : 'black'
      if (item === jackpot2) {
        colorWin = '#d6a720'
      }

      arrayBoard.push(
        <View key={index} style={[styles.rowNumber]}>
          <TextFont color={colorWin}>{item}</TextFont>
        </View>
      )
    })
    return arrayBoard
  }

  renderListBoard = () => {
    const board = []
    const { ticket_data, draw_number } = this.props.lotteryDetail
    let drawNumber = []
    if (draw_number) {
      drawNumber = JSON.parse(draw_number)
    }

    _.forEach(ticket_data.list_ticket, (data, dataIndex) => {
      const string = String.fromCharCode(65 + dataIndex)
      let winData = []
      if (draw_number) {
        const numbers = data.ticket_number.split(',').map(value => parseInt(value, 10))
        winData = _.intersection(numbers, drawNumber.numbers)
      }

      board.push(
        <View key={dataIndex} style={[styles.rowBoard]}>
          <View style={{ alignItems: 'center', paddingHorizontal: Themes.Metrics.screenWidth * 0.015 }}>
            <TextFont>{string} :</TextFont>
          </View>
          <View style={[styles.groupNumber]}>
            {this.renderNumber(data, winData, drawNumber.jackpot2)}
          </View>
        </View>
      )
    })
    return board
  }

  renderTicketInfo = () => {
    const data = this.props.lotteryDetail
    return (
      <View>
        <View style={[styles.rowTypeTicket]}>
          <View style={[styles.colType]}>
            <Icon color={Themes.Colors.navColor} name="circle" size={9} />
            <TextFont style={[styles.txtType]}>LOẠI VÉ :</TextFont>
          </View>
          <TextFont style={[styles.txtTypeTicket]}>Vé điện tử</TextFont>
        </View>
        <View style={[styles.boxTicket]}>
          <View style={[styles.boxTicketUI]}>
            <View style={[styles.TicketWhite]}>
              <TextFont style={[styles.txtTitleTicket]} size={16} font={Themes.Fonts.type.Bold}>POWER 655</TextFont>
              <View style={[styles.lineBoxTicket]} />
              <View style={[styles.lineBoxTicket, styles.marginTop5]} />

              {this.renderListBoard()}

              <View style={[styles.lineBoxTicket, { marginTop: 10 }]} />
              <View style={[styles.lineBoxTicket, styles.marginTop5]} />
              <View style={{ paddingHorizontal: 5 }}>
                <View style={[styles.rowTotal, { marginTop: 7, justifyContent: 'space-between' }]}>
                  <TextFont size={13}>Tổng</TextFont>
                  <TextFont font={Themes.Fonts.type.Bold} size={13} style={[styles.txtNumberTotal]}>{Utils.formatMoney(data.money)} đ</TextFont>
                </View>
                <View style={[styles.rowTotal, { marginTop: 3, justifyContent: 'space-between' }]}>
                  <TextFont size={13}>Kỳ QSMT</TextFont>
                  <TextFont size={13} style={[styles.txtNumberTotal]}>{data.period}</TextFont>
                </View>
                <TextFont size={13} style={[styles.txtTextRight, { marginTop: 3 }]}>{data.created_date}</TextFont>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }

  renderStatus(state) {
    if (state === '0' || state === '1') {
      return <TextFont style={[styles.txtType]}>Vé số Vietlott đã được xử lý bên thiết bị đầu cuối và lưu trữ tại Công ty. Biên nhận điện tử đã được gửi về email của bạn, đồng thời ghi nhận tại lịch sử giao dịch.</TextFont>
    } else if (parseInt(state, 10) < 0) {
      return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
          <TextFont style={[styles.txtType]}>Trạng thái</TextFont><TextFont style={[styles.txtType, { color: 'red', paddingRight: 10 }]}>Vé đã hủy</TextFont>
        </View>
      )
    }
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
        <TextFont style={[styles.txtType]}>Trạng thái</TextFont><TextFont style={[styles.txtType, { color: 'green', paddingRight: 10 }]}>Vé đang xử lý</TextFont>
      </View>
    )
  }

  renderUITypeConfirm = () => {
    const { state } = this.props.lotteryDetail
    return (
      <ScrollView>

        {this.renderTicketInfo()}

        <View style={[styles.viewInfo]}>
          <View style={[styles.rowInfoUser, { alignItems: 'flex-start' }]}>
            {this.renderStatus(state)}
          </View>
          <TextFont style={[Themes.styleGB.textCenter, { marginTop: 10 }]}>
            Thắc mắc xin liên hệ 
            <TextFont onPress={() => this.LaunchURL(`tel:${this.props.hotLine.hot_line}`)} color={Themes.Colors.navColor}> {this.props.hotLine.hot_line}</TextFont>
          </TextFont>
        </View>
      </ScrollView>
    )
  }

  renderRowNumber = (data) => {
    const array = []
    const ticket = JSON.parse(data)
    _.forEach(ticket.numbers, (number, index) => {
      array.push(
        <View style={[styles.txtNumberWin]} key={index}>
          <TextFont color={Themes.Colors.navColor}>{number}</TextFont>
        </View>
      )
    })
    array.push(
      <View style={[styles.txtNumberJackpot]} key={_.uniqueId()}>
        <TextFont color={'#d6a720'}>{ticket.jackpot2}</TextFont>
      </View>
    )
    return array
  }

  checkButtonWin() {
    const data = this.props.lotteryDetail
    if (this.props.scheduled === '0' && data.must_scheduled === 1) {
      return (
        <Button onPress={this.onPressWinForm} style={[styles.btnConfirm]}>ĐĂNG KÝ NHẬN THƯỞNG</Button>
      )
    } else if (this.props.scheduled === '0' && data.must_scheduled === 0) {
      return (
        <TextFont color="#666" size={14} style={[Themes.styleGB.textCenter]}>Tiền thưởng đã được chuyển vào tài khoản của bạn</TextFont>
      )
    }
    return <TextFont color="#666" size={14} style={[Themes.styleGB.textCenter]}>Tiền thưởng đã được chuyển vào tài khoản của bạn</TextFont>
  }

  renderUITypeWin = () => {
    const data = this.props.lotteryDetail
    return (
      <ScrollView>

        {this.renderTicketInfo()}

        <View style={[styles.viewInfo]}>
          <View style={[styles.rowInfoUser]}>
            <Icon color={Themes.Colors.navColor} name="circle" size={9} />
            <TextFont style={[styles.txtType]}>KẾT QUẢ KỲ QUAY THƯỞNG #{data.period}</TextFont>
          </View>

          <View style={[styles.rowNumberWin]}>
            {this.renderRowNumber(data.draw_number)}
          </View>

          <Image style={[styles.winnerImg]} source={Themes.Images.winImage} />
          <View style={[styles.rowWin]}>
            <TextFont color="#666666">Bạn đã trúng: </TextFont>
            <TextFont color={Themes.Colors.navColor} size={18} font={Themes.Fonts.type.Bold} style={[styles.txtMoneyWin]}>{Utils.formatMoney(data.total_win)} đ</TextFont>
          </View>
          {this.checkButtonWin()}
        </View>
      </ScrollView>
    )
  }

  btnBuyTicket = () => {
    const data = this.props.lotteryDetail.ticket_data.list_ticket
    const filledNumber = _.cloneDeep(this.state.filledNumber)
    _.map(data, (newData, index) => {
      const string = String.fromCharCode(65 + index)
      const newArray = newData.ticket_number.split(',')
      const numberArray = _.map(newArray, i => parseInt(i, 0))
      filledNumber[string] = numberArray
    })
    this.props.navigator.push({ ...Config.screen.megaPowerPickup, passProps: { filledNumber, bao: this.props.lotteryDetail.ticket_data.bao } })
  }

  renderUITypeLose = () => {
    const data = this.props.lotteryDetail
    return (
      <ScrollView>

        {this.renderTicketInfo()}

        <View style={{ alignItems: 'center', marginTop: 20, marginBottom: 10 }}>
          <TouchableOpacity activeOpacity={0.8} onPress={() => this.btnBuyTicket()} style={{ backgroundColor: Themes.Colors.navColor, width: 120, justifyContent: 'center', alignItems: 'center' }}>
            <TextFont style={{ color: '#fff', paddingVertical: 10 }}>Mua vé lại</TextFont>
          </TouchableOpacity>
        </View>

        <View style={[styles.viewInfo]}>
          <View style={[styles.rowInfoUser]}>
            <Icon color={Themes.Colors.navColor} name="circle" size={9} />
            <TextFont style={[styles.txtType]}>KẾT QUẢ KỲ QUAY THƯỞNG #{data.period}</TextFont>
          </View>

          <View style={[styles.rowNumberWin]}>
            {this.renderRowNumber(data.draw_number)}
          </View>

          <Image style={[styles.winnerImg]} source={Themes.Images.closeImage} />
          <TextFont color="#666" size={12} style={[Themes.styleGB.textCenter]}>Vé không trúng thưởng - Chúc bạn may mắn lần sau</TextFont>

        </View>
      </ScrollView>
    )
  }

  checkUI = () => {
    if (this.props.lotteryTypeUI === 1) {
      return (this.renderUITypeConfirm())
    } else if (this.props.lotteryTypeUI === 2) {
      return (this.renderUITypeWin())
    } else if (this.props.lotteryTypeUI === 3) {
      return (this.renderUITypeLose())
    }
    return null
  }

  render() {
    return (
      <View style={[styles.container]}>
        {this.checkUI()}
      </View>
    )
  }

}
LotteryStoredDetail.propTypes = {}
export default LotteryStoredDetail
