import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'
import Modal from 'react-native-modalbox'
import { ActivityIndicator, Image, ListView, RefreshControl, ScrollView, TouchableOpacity, View, StatusBar } from 'react-native'
import { TcombForm, TextFont, Themes } from '../../../ui'
import styles from './lotteryhome.style'
import * as Utils from '../../../utils'

const t = require('tcomb-form-native')
const Form = t.form.Form
const userAccount = t.struct({
  filter: t.maybe(t.String)
})

class LotteryHome extends Component {

  constructor(props) {
    super(props)
    this.state = {
      formValue: {
        filter: null
      }
    }
    this.onTicketRefreshing = this.onTicketRefreshing.bind(this)
    this.refreshingList = this.refreshingList.bind(this)
  }

  setValueForm = (key, date, value, modal) => {
    this.setState({
      formValue: {
        ...this.state,
        [key]: date
      }
    }, () => {
      this.refs[`${modal}`].close()
      this.props.onPressFilter(parseInt(value, 0))
    })
  }

  renderModalMoney = () => {
    const dataSelect = this.props.listPeriodTicket
    return (
      <Modal animationDuration={0} swipeToClose={false} style={[styles.modal]} position={'center'} ref={'modalMoney'}>
        <TouchableOpacity activeOpacity={0.7} onPress={this.onTicketRefreshing} style={[styles.btnTouchModal]}>
          <TextFont>Xem tất cả</TextFont>
        </TouchableOpacity>
        <ScrollView bounces={false} contentContainerStyle={{ justifyContent: 'center' }}>
          {
            _.map(dataSelect, (data, index) => {
              const date = moment(data.drawing_date).format('DD/MM/YYYY')
              return (
                <TouchableOpacity key={index} activeOpacity={0.7} onPress={() => this.setValueForm('filter', date, data.calendar_id, 'modalMoney')} style={[styles.btnTouchModal]}>
                  <TextFont color={date === this.state.formValue.filter ? 'red' : null}>Ngày {date}</TextFont>
                  <View style={[styles.tags, { backgroundColor: data.name === 'Max 4D' ? '#8b028d' : Themes.Colors.navColor }]}><TextFont style={styles.tagsText}>{data.name}</TextFont></View>
                </TouchableOpacity>
              )
            })
          }
        </ScrollView>
      </Modal>
    )
  }

  onFocusForm = (value) => {
    this.refs[`${value}`].open()
  }

  renderFormFilter = () => {
    const layout = (locals) => {
      return (
        <View style={[styles.boxFilter]}>
          <TextFont>Lọc theo: </TextFont>
          <View style={{ flex: 1 }}>{locals.inputs.filter}</View>
        </View>
      )
    }
    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        filter: {
          factory: TcombForm.InputModalRightIconTicket,
          placeholder: 'Chọn ngày xổ',
          onFocus: () => this.onFocusForm('modalMoney'),
          placeholderTextColor: '#E7E7E7',
          config: {
            nameIcon: 'down',
            IconSize: 7,
            IconColor: '#bcbcbc'
          }
        }
      }
    }
    return (
      <Form ref={(form) => { this.form = form }} type={userAccount} options={options} value={this.state.formValue} />
    )
  }

  checkImage(rowData) {
    if (rowData.ticket_type === '2') {
      return <Image source={Themes.Images.icon645} />
    } else if (rowData.ticket_type === '1') {
      return <Image source={Themes.Images.icon655} />
    }
    return <Image source={Themes.Images.iconMax4d} />
  }

  checkTicketType(rowData) {
    const typeTicket = parseInt(rowData.delivery_type, 0)

    const checkClub = () => {
      if (rowData.club_id !== '0') {
        return `Vé mua từ Club #${rowData.club_id}`
      } return 'Vé điện tử'
    }

    switch (typeTicket) {
      case 0: {
        return (
          <TextFont font={Themes.Fonts.type.Bold} color="#fe9a00">{checkClub()}</TextFont>
        )
      }
      case 1: {
        return (
          <TextFont font={Themes.Fonts.type.Bold} color="#78bde8">Vé cứng (lấy tại đại lý)</TextFont>
        )
      }
      case 2: {
        return (
          <TextFont font={Themes.Fonts.type.Bold} color="#27be04">Vé cứng (giao tận nơi)</TextFont>
        )
      }
      default: {
        return null
      }
    }
  }

  checkMoney(rowData) {
    const formatMoney = Utils.formatMoney(rowData.money)
    return (
      <TextFont style={{ textAlign: 'right' }} color="#d63733">{formatMoney} đ</TextFont>
    )
  }

  checkStatus(rowData) {
    if (rowData.delivery_state === '0' && (rowData.delivery_type === '1' || rowData.delivery_type === '2') && rowData.have_result === '0') {
      return (
        <View style={[styles.boxStatus]}><TextFont color="#999" size={12} style={styles.txtStatus}>Chưa nhận vé ...</TextFont></View>
      )
    } else if (rowData.delivery_state === '1' && (rowData.delivery_type === '1' || rowData.delivery_type === '2') && rowData.have_result === '0') {
      return (
        <View style={[styles.boxStatus, { backgroundColor: '#aaa9a9' }]}><TextFont color="#fff" size={12} style={styles.txtStatus}>Đã nhận vé</TextFont></View>
      )
    } else if (rowData.delivery_state === '-1' && (rowData.delivery_type === '1' || rowData.delivery_type === '2' || rowData.delivery_type === '0') && rowData.have_result === '0') {
      return (
        <View style={[styles.boxStatus, { backgroundColor: '#00b72c' }]}><TextFont color="#fff" size={12} style={styles.txtStatus}>Giữ vé hộ</TextFont></View>
      )
    } else if (rowData.delivery_type === '0' && rowData.state === '0' && rowData.have_result === '0') {
      return (
        <View style={[styles.boxStatus, { backgroundColor: '#b7a406' }]}>
          { rowData.ticket_type === '2' || rowData.ticket_type === '1'
           ? <TextFont color="#fff" size={12} style={styles.txtStatus}>Đã xử lý</TextFont>
           : <TextFont color="#fff" size={12} style={styles.txtStatus}>Đang xử lý</TextFont>
          }
        </View>
      )
    } else if (rowData.delivery_type === '0' && rowData.state === '1' && rowData.have_result === '0') {
      return (
        <View style={[styles.boxStatus, { backgroundColor: '#00b72c' }]}><TextFont color="#fff" size={12} style={styles.txtStatus}>Vé đã in</TextFont></View>
      )
    } else if (rowData.have_result === '1' && rowData.state_win === '0') {
      return (
        <View style={[styles.boxStatus, { backgroundColor: '#aaa9a9' }]}><TextFont color="#fff" size={12} style={styles.txtStatus}>Vé đã xổ</TextFont></View>
      )
    } else if (rowData.have_result === '0' && rowData.delivery_state === '1') {
      return (
        <View style={[styles.boxStatus, { backgroundColor: '#fe9a00' }]}><TextFont color="#fff" size={12} style={styles.txtStatus}>Chờ xổ</TextFont></View>
      )
    } else if (rowData.state_win === '1') {
      return (
        <View style={[styles.boxStatus, { backgroundColor: '#00b72c' }]}><TextFont color="#fff" size={10} style={styles.txtStatus}>Trúng thưởng</TextFont></View>
      )
    } else if (parseInt(rowData.state, 10) < 0) {
      return <View style={[styles.boxStatus, { backgroundColor: 'red' }]}><TextFont color="#fff" size={10} style={styles.txtStatus}>Vé đã hủy</TextFont></View>
    }
    return ''
  }

  onTicketRefreshing = () => {
    this.setState({
      formValue: {
        filter: null
      }
    })
    this.props.getListTicketResult(true)
    this.refs.modalMoney.close()
  }

  refreshingList = () => {
    this.setState({
      formValue: {
        filter: null
      }
    })
    this.props.refreshingList()
    this.refs.modalMoney.close()
  }

  onTicketListEndReached = () => {
    if (this.props.isTicketLoadingMore === false && this.props.isTicketLastPage === false) {
      this.props.getListTicketResult(false)
    }
  }

  renderTicketFooter = () => {
    const { isTicketLoadingMore, isTicketLastPage } = this.props
    return isTicketLoadingMore === true && isTicketLastPage === false ? <ActivityIndicator style={{ marginTop: 10, marginBottom: 10 }} /> : null
  }

  renderListTicket = () => {
    const { listTicket, isFirstLoading, loadingPreiod } = this.props
    if (!listTicket) {
      return (<ActivityIndicator style={{ marginTop: 10, marginBottom: 10 }} />)
    }
    if (!loadingPreiod) {
      return (
        <ActivityIndicator style={{ marginTop: 10, marginBottom: 10 }} />
      )
    }
    if (listTicket.length === 0) {
      if (isFirstLoading === true) {
        return (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <TextFont color={Themes.Colors.navColor} style={Themes.styleGB.centerContent}>Bạn chưa mua vé nào</TextFont>
          </View>
        )
      }
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <TextFont color={Themes.Colors.navColor} style={Themes.styleGB.centerContent}>Không có dữ liệu.</TextFont>
          {/* <TextFont onPress={this.onTicketRefreshing} color={Themes.Colors.navColor} style={[Themes.styleGB.centerContent, { marginTop: 10 }]}>TẢI LẠI</TextFont> */}
        </View>
      )
    }
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const dataSource = ds.cloneWithRows(this.props.listTicket)
    return (
      <ListView
        dataSource={dataSource}
        scrollEventThrottle={16}
        enableEmptySections
        refreshControl={
          <RefreshControl
            refreshing={this.props.isRefresh}
            onRefresh={this.refreshingList}
          />
        }
        renderRow={(rowData) => {
          return (
            <TouchableOpacity
              // onPress={() => alert(rowData.ticket_id)}
              onPress={() => this.props.onPressDetailScreen(rowData)}
              activeOpacity={0.6}
              style={styles.rowTouch}
            >
              {this.checkImage(rowData)}
              <View style={[styles.colTitle]}>
                {this.checkTicketType(rowData)}
                <TextFont color="#999" size={15} style={[styles.txtDes]}>Kỳ quay số #{rowData.period}</TextFont>
              </View>
              <View style={[styles.colEnd]}>
                {this.checkMoney(rowData)}
                {this.checkStatus(rowData)}
              </View>
            </TouchableOpacity>
          )
        }}
        onEndReached={this.onTicketListEndReached}
        renderFooter={this.renderTicketFooter}
        onEndReachedThreshold={50}
      />
    )
  }

  render() {
    return (
      <View style={[styles.container]} tabLabel="Vé điện tử">
        <StatusBar />
        {this.renderFormFilter()}
        {this.renderListTicket()}
        {this.renderModalMoney()}
      </View>
    )
  }

}
LotteryHome.propTypes = {}
export default LotteryHome
