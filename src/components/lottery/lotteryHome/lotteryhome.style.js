import { StyleSheet, PixelRatio, Platform } from 'react-native'
import { Themes } from '../../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  boxFilter: {
    padding: 10,
    backgroundColor: '#f1f1f1',
    flexDirection: 'row',
    alignItems: 'center'
  },
  rowTouch: {
    paddingHorizontal: 10,
    paddingVertical: 15,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1 / PixelRatio.get(),
    borderColor: '#e0e4e7',
    backgroundColor: '#fff'
  },
  colTitle: {
    flex: 1,
    marginHorizontal: 10
  },
  txtDes: {
    marginTop: 5
  },
  boxStatus: {
    padding: 5,
    backgroundColor: '#eae9e9',
    borderRadius: 10,
    marginTop: 5
  },
  txtStatus: {
    textAlign: 'center'
  },
  colEnd: {
  },
  modal: {
    width: '85%',
    height: null,
    maxHeight: '80%'
  },
  btnTouchModal: {
    padding: 15,
    backgroundColor: '#E7E7E7',
    borderWidth: 1 / PixelRatio.get(),
    borderColor: '#ccc',
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  tags: {
    backgroundColor: Themes.Colors.navColor,
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 2
  },
  tagsText: {
    color: '#fff',
    fontSize: 12
  }
})

export default styles
