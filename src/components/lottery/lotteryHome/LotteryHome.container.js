import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import LotteryHome from './LotteryHome'
import Config from '../../../screens/config'
import { actions as LotteryAction, types } from '../../../redux/lottery'
import { Colors } from '../../../ui/themes'

class LotteryHomeContainer extends Component {

  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE,
    statusBarColor: Colors.navColor
  }

  constructor(props) {
    super(props)
    this.state = {
      firstAppear: true,
      isFirstLoading: true,
      loadingPreiod: true,
      isFetchingTicket: true,
      isTicketLastPage: false,
      isTicketLoadingMore: false,
      isTicketRefreshing: false,
      ticketCurrentPage: 1,
      ticketID: 0
    }
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
    this.onPressDetailScreen = this.onPressDetailScreen.bind(this)
    this.getListTicketResult = this.getListTicketResult.bind(this)
    this.onPressFilter = this.onPressFilter.bind(this)
    this.refreshingList = this.refreshingList.bind(this)
  }

  componentWillMount() {
    this.props.lotteryAction.getListPeriodTicket()
  }

  componentWillReceiveProps(props) {
    // console.log(this.props.payload)
    const { apiResponse } = props.payload
    const { data } = apiResponse
    if (apiResponse.type === types.GET_TICKET_BY_PREIOD_SUCCESS) {
      this.setState({ loadingPreiod: true, isFetchingTicket: false, isTicketLastPage: data.lastPage || false, isTicketLoadingMore: false, isTicketRefreshing: false })
    } else {
      this.setState({ loadingPreiod: true })
    }
  }

  onNavigatorEvent(event) {
    if (event.id === 'willAppear') {
      if (this.state.firstAppear === true) {
        this.props.lotteryAction.getTicketByPreiod({ calendar_id: this.state.ticketID, page: this.state.ticketCurrentPage })
        this.setState({ firstAppear: false })
      }
    }
  }

  onPressFilter(data) {
    this.setState({
      isFirstLoading: false,
      loadingPreiod: false,
      ticketCurrentPage: 1,
      ticketID: data
    })
    this.props.lotteryAction.getTicketByPreiod({ calendar_id: data, page: 1 })
  }

  onPressDetailScreen(item) {
    const ticketID = item.ticket_id
    const scheduled = item.is_scheduled
    switch (item.ticket_type) {
      case '1': {
        if (item.delivery_type === '0') {
          this.props.navigator.push({ ...Config.screen.lottery655Stored, passProps: { ticketID, scheduled } })
        } else if (item.delivery_type === '1') {
          this.props.navigator.push({ ...Config.screen.lottery655Pick, passProps: { ticketID, scheduled } })
        } else if (item.delivery_type === '2') {
          this.props.navigator.push({ ...Config.screen.lottery655Delivery, passProps: { ticketID, scheduled } })
        }
        break
      }
      case '2': {
        if (item.delivery_type === '0') {
          this.props.navigator.push({ ...Config.screen.lottery645Stored, passProps: { ticketID, scheduled } })
        } else if (item.delivery_type === '1') {
          this.props.navigator.push({ ...Config.screen.lottery645Pick, passProps: { ticketID, scheduled } })
        } else if (item.delivery_type === '2') {
          this.props.navigator.push({ ...Config.screen.lottery645Delivery, passProps: { ticketID, scheduled } })
        }
        break
      }
      case '4': {
        if (item.delivery_type === '0') {
          this.props.navigator.push({ ...Config.screen.lotteryMax4dStored, passProps: { ticketID, scheduled } })
        } else if (item.delivery_type === '1') {
          this.props.navigator.push({ ...Config.screen.lotteryMax4dPick, passProps: { ticketID, scheduled } })
        } else if (item.delivery_type === '2') {
          this.props.navigator.push({ ...Config.screen.lotteryMax4dDelivery, passProps: { ticketID, scheduled } })
        }
        break
      }
      default:
    }

    // if (item.ticket_type === '2' || item.ticket_type === '1') {
    //   if (item.delivery_type === '0') {
    //     this.props.navigator.push({ ...Config.screen.lottery645Stored, passProps: { ticketID, scheduled } })
    //   } else if (item.delivery_type === '1') {
    //     this.props.navigator.push({ ...Config.screen.lottery645Pick, passProps: { ticketID, scheduled } })
    //   } else if (item.delivery_type === '2') {
    //     this.props.navigator.push({ ...Config.screen.lottery645Delivery, passProps: { ticketID, scheduled } })
    //   }
    // } else if (item.delivery_type === '0') {
    //   this.props.navigator.push({ ...Config.screen.lotteryMax4dStored, passProps: { ticketID, scheduled } })
    // } else if (item.delivery_type === '1') {
    //   this.props.navigator.push({ ...Config.screen.lotteryMax4dPick, passProps: { ticketID, scheduled } })
    // } else if (item.delivery_type === '2') {
    //   this.props.navigator.push({ ...Config.screen.lotteryMax4dDelivery, passProps: { ticketID, scheduled } })
    // }
  }

  getListTicketResult(isRefresh) {
    const { lotteryAction } = this.props
    if (isRefresh === true) {
      lotteryAction.getTicketByPreiod({ calendar_id: 0, page: 1 })
      this.setState({ isFirstLoading: false, ticketCurrentPage: 1, isTicketRefreshing: false, ticketID: 0, loadingPreiod: false })
    } else {
      const currentPage = this.state.ticketCurrentPage + 1
      lotteryAction.getTicketByPreiod({ calendar_id: this.state.ticketID, page: currentPage })
      this.setState({ isFirstLoading: false, ticketCurrentPage: currentPage, isTicketLoadingMore: true })
    }
  }

  refreshingList() {
    const { lotteryAction } = this.props
    lotteryAction.getTicketByPreiod({ calendar_id: 0, page: 1 })
    this.setState({ isFirstLoading: false, ticketCurrentPage: 1, isTicketRefreshing: false, ticketID: 0, loadingPreiod: true })
  }

  render() {
    return (
      <LotteryHome
        onPressFilter={this.onPressFilter}
        listPeriodTicket={this.props.payload.lottery.listPeriodTicket}
        isRefresh={this.state.isTicketRefreshing}
        listTicket={this.props.payload.lottery.lotteryBuyList}
        onPressDetailScreen={this.onPressDetailScreen}
        getListTicketResult={this.getListTicketResult}
        refreshingList={this.refreshingList}
        {...this.state}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    lotteryAction: bindActionCreators(LotteryAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LotteryHomeContainer)
