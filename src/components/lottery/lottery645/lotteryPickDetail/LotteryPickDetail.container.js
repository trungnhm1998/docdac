import React, { Component } from 'react'
import { Platform, PixelRatio, Alert, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import LotteryPickDetail from './LotteryPickDetail'
import Config from '../../../../screens/config'
import { actions as LotteryAction, types } from '../../../../redux/lottery'


class LotteryPickDetailContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  constructor(props) {
    super(props)
    this.state = {
      isFetching: true
    }
    this.onPressReceiveTicket = this.onPressReceiveTicket.bind(this)
  }

  componentWillMount() {
    const ticket = { ticketID: this.props.ticketID }
    this.props.lotteryAction.getUserLotteryDetail(ticket)
  }

  componentWillReceiveProps(nextProps) {
    const { apiResponse } = nextProps.payload
    if (apiResponse.type === types.GET_USER_LOTTERY_DETAIL_SUCCESS) {
      this.setState({ isFetching: false })
    }
  }

  onPressReceiveTicket = () => {
    const ticket = { ticketID: this.props.ticketID }
    this.props.lotteryAction.userConfirmReceiveTicket(ticket)
  }

  renderView = () => {
    if (!this.props.payload.lottery.LotteryDetail || this.state.isFetching === true) {
      return (
        <ActivityIndicator style={{ marginTop: 10, marginBottom: 10 }} />
      )
    }
    const { delivery_date } = this.props.payload.lottery.LotteryDetail
    let TypeUI
    if (delivery_date === null || delivery_date !== null) {
      TypeUI = 1
    } else {
      TypeUI = 2
    }
    return (
      <LotteryPickDetail
        onPressReceiveTicket={this.onPressReceiveTicket}
        lotteryTypeUI={TypeUI}
        lotteryDetail={this.props.payload.lottery.LotteryDetail}
      />
    )
  }

  render() {
    return (
      this.renderView()
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    lotteryAction: bindActionCreators(LotteryAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LotteryPickDetailContainer)
