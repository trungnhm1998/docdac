import { StyleSheet, PixelRatio } from 'react-native'
import { Themes } from '../../../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  rowTypeTicket: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#fff'
  },
  colType: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  txtType: {
    marginLeft: 5,
    color: '#444444'
  },
  txtTypeTicket: {
    flex: 1,
    textAlign: 'right'
  },
  boxTicket: {
    backgroundColor: '#f7f7f7',
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  boxTicketUI: {
    width: Themes.Metrics.screenWidth * 0.70,
    backgroundColor: Themes.Colors.navColor,
    paddingHorizontal: Themes.Metrics.screenWidth * 0.03
  },
  TicketWhite: {
    backgroundColor: '#fff',
    paddingVertical: 10
  },
  txtTitleTicket: {
    textAlign: 'center',
    paddingVertical: 10
  },
  lineBoxTicket: {
    height: 1 / PixelRatio.get(),
    borderWidth: 1 / PixelRatio.get(),
    borderStyle: 'dashed',
    borderColor: '#535353'
  },
  marginTop5: {
    marginTop: 2
  },
  rowBoard: {
    flexDirection: 'row',
    marginTop: 10,
  },
  rowNumber: {
    width: Themes.Metrics.screenWidth * 0.09,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  groupNumber: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    width: Themes.Metrics.screenWidth * 0.55
  },
  rowTotal: {
    flexDirection: 'row'
  },
  txtNumber: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    // paddingHorizontal: 12
  },
  txtNumberTotal: {
    flex: 1,
    textAlign: 'right'
  },
  txtTextRight: {
    textAlign: 'right'
  },
  viewInfo: {
    backgroundColor: '#fff',
    padding: 10
  },
  rowInfoUser: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rowInforName: {
    flexDirection: 'row',
    marginTop: 10
  },
  txtIcon: {
    marginTop: 5
  },
  txtTitleName: {
    color: Themes.Colors.navColor
  },
  colInforName: {
    paddingLeft: 7,
    flex: 1
  },
  rowLocation: {
    borderTopWidth: 1 / PixelRatio.get(),
    borderColor: '#f2f2f2',
    marginTop: 7,
    paddingVertical: 5,
    paddingTop: 7
  },
  colLocation: {
    flexDirection: 'row',
    flex: 1,
    paddingRight: 20
  },
  colLocationStore: {
    flex: 1,
    paddingRight: 20
  },
  colPhone: {
    flexDirection: 'row'
  },
  txtLocation: {
    marginLeft: 5
  },
  txtPhone: {
    marginLeft: 5
  },
  btnConfirm: {
    backgroundColor: Themes.Colors.navColor,
    borderRadius: 50,
    marginTop: 10,
    marginBottom: 10
  },
  btnConfirmSuccess: {
    backgroundColor: '#fb9511',
    borderRadius: 50,
    marginTop: 10,
    marginBottom: 10
  },
  iconSuccess: {
    position: 'absolute',
    right: 0,
    top: 0
  },
  rowNumberWin: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 40,
    marginTop: 15
  },
  txtNumberWin: {
    height: Themes.Metrics.screenWidth * 0.09,
    width: Themes.Metrics.screenWidth * 0.09,
    borderRadius: (Themes.Metrics.screenWidth * 0.09) / 2,
    borderWidth: 3 / PixelRatio.get(),
    borderColor: Themes.Colors.navColor,
    justifyContent: 'center',
    alignItems: 'center'
  },
  winnerImg: {
    alignSelf: 'center',
    marginVertical: 15
  },
  rowWin: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 7
  },
  txtMoneyWin: {
    paddingRight: 7
  }

})

export default styles
