import React, { Component } from 'react'
import _ from 'lodash'
import { ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as LotteryAction, types } from '../../../../redux/lottery'
import LotteryStoredDetail from './LotteryStoredDetail'
import Config from '../../../../screens/config'


class LotteryStoredDetailContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  constructor(props) {
    super(props)
    this.state = {
      isFetching: true
    }
    this.onPressWinForm = this.onPressWinForm.bind(this)
  }

  componentWillMount() {
    const ticket = { ticketID: this.props.ticketID }
    this.props.lotteryAction.getUserLotteryDetail(ticket)
  }

  componentWillReceiveProps(props) {
    const { apiResponse } = props.payload
    if (apiResponse.type === types.GET_USER_LOTTERY_DETAIL_SUCCESS) {
      this.setState({ isFetching: false })
    }
  }

  onPressWinForm() {
    const { ticketID } = this.props
    this.props.navigator.push({ ...Config.screen.lottery645WinForm, passProps: { ticketID } })
  }

  renderView = () => {
    if (!this.props.payload.lottery.LotteryDetail || this.state.isFetching === true) {
      return (<ActivityIndicator style={{ marginTop: 10, marginBottom: 10 }} />)
    }

    const { LotteryDetail } = this.props.payload.lottery
    let TypeUI
    if ((LotteryDetail.delivery_date === null || LotteryDetail.delivery_date !== null) && LotteryDetail.draw_number === '') {
      TypeUI = 1
    } else if (LotteryDetail.draw_number !== '' && LotteryDetail.state_win === '1') {
      TypeUI = 2
    } else if (LotteryDetail.draw_number !== '' && LotteryDetail.state_win === '0') {
      TypeUI = 3
    }
    return (
      <LotteryStoredDetail
        onPressWinForm={this.onPressWinForm}
        scheduled={this.props.scheduled}
        lotteryTypeUI={TypeUI}
        lotteryDetail={this.props.payload.lottery.LotteryDetail}
        hotLine={this.props.payload.accountInfo}
        navigator={this.props.navigator}
      />
    )
  }

  render() {
    return (
      this.renderView()

    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    lotteryAction: bindActionCreators(LotteryAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LotteryStoredDetailContainer)
