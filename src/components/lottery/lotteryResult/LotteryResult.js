import React, { Component } from 'react'
import _ from 'lodash'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import { View, Image, RefreshControl, ListView, ActivityIndicator } from 'react-native'
import { Themes, TextFont, TabbarLottery, Loading, StatusBar } from '../../../ui'
import { helper } from '../../../utils'
import styles from './lotteryResult.style'

const moment = require('moment')
const esLocale = require('moment/locale/vi')
moment.updateLocale('vi', esLocale)

class LotteryResultsHome extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isLoading: true
    }
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onPowerRefreshing = this.onPowerRefreshing.bind(this)
    this.onMegaRefreshing = this.onMegaRefreshing.bind(this)
    this.onMax4dRefreshing = this.onMax4dRefreshing.bind(this)
    this.renderPowerFooter = this.renderPowerFooter.bind(this)
    this.renderMegaFooter = this.renderMegaFooter.bind(this)
    this.renderMax4dFooter = this.renderMax4dFooter.bind(this)
    this.onPowerListEndReached = this.onPowerListEndReached.bind(this)
    this.onMegaListEndReached = this.onMegaListEndReached.bind(this)
    this.onMax4dListEndReached = this.onMax4dListEndReached.bind(this)
  }

  onPressSubmit = () => {
    this.props.onPressSubmit()
  }

  renderMegaItem = (rowData, section, rowIndex) => {
    const numberWin = rowData.winning_numbers
    const formattedDate = _.upperFirst(moment(rowData.drawing_date, 'DD-MM-YYYY').format('dddd, DD-MM-YYYY'))
    const backgroundColor = rowIndex === '0' ? Themes.Colors.navColor : '#fff'
    const color = rowIndex === '0' ? '#fff' : Themes.Colors.navColor

    const renderNumberResult = () => {
      const number = []
      _.forEach(numberWin.split(', '), (data, index) => {
        number.push(
          <View style={[styles.numberResult, { backgroundColor }]} key={index}>
            <TextFont color={color} style={Themes.styleGB.textCenter}>{helper.pad('00', data, true)}</TextFont>
          </View>
        )
      })
      return number
    }

    return (
      <View style={[styles.rowMega]}>
        <TextFont color="#6e6e6e">{formattedDate}</TextFont>
        <View style={styles.rowResultMega}>
          {renderNumberResult(rowData, rowIndex)}
        </View>
      </View>
    )
  }

  renderMegaPowerItem = (rowData, section, rowIndex) => {
    const numberWin = JSON.parse(rowData.winning_numbers)
    const formattedDate = _.upperFirst(moment(rowData.drawing_date, 'DD-MM-YYYY').format('dddd, DD-MM-YYYY'))
    const backgroundColor = rowIndex === '0' ? Themes.Colors.navColor : '#fff'
    const color = rowIndex === '0' ? '#fff' : Themes.Colors.navColor

    const renderNumberResult = () => {
      const number = []
      _.forEach(numberWin.numbers, (data, index) => {
        number.push(
          <View style={[styles.numberResult, { backgroundColor }]} key={index}>
            <TextFont color={color} style={Themes.styleGB.textCenter}>{helper.pad('00', data, true)}</TextFont>
          </View>
        )
      })
      number.push(
        <View key={_.uniqueId()} style={[styles.numberPowerResult]}>
          <TextFont color={'white'} style={Themes.styleGB.textCenter}>{helper.pad('00', numberWin.jackpot2, true)}</TextFont>
        </View>
      )
      return number
    }

    return (
      <View style={[styles.rowMega]}>
        <TextFont color="#6e6e6e">{formattedDate}</TextFont>
        <View style={styles.rowResultMega}>
          {renderNumberResult(rowData, rowIndex)}
        </View>
      </View>
    )
  }

  onPowerRefreshing() {
    this.props.getListPowerResult(true)
  }

  onMegaRefreshing() {
    this.props.getListMegaResult(true)
  }

  onMax4dRefreshing() {
    this.props.getListMax4dResult(true)
  }

  renderMax4dFooter() {
    const { isMax4dLoadingMore, isMax4dLastPage } = this.props
    return isMax4dLoadingMore === true && isMax4dLastPage === false ? <ActivityIndicator style={{ marginTop: 10, marginBottom: 10 }} /> : null
  }

  onMax4dListEndReached() {
    if (this.props.isMax4dLoadingMore === false) {
      this.props.getListMax4dResult()
    }
  }

  renderMegaFooter() {
    const { isMegaLoadingMore, isMegaLastPage } = this.props
    return isMegaLoadingMore === true && isMegaLastPage === false ? <ActivityIndicator style={{ marginTop: 10, marginBottom: 10 }} /> : null
  }
  
  renderPowerFooter() {
    const { isPowerLoadingMore, isPowerLastPage } = this.props
    return isPowerLoadingMore === true && isPowerLastPage === false ? <ActivityIndicator style={{ marginTop: 10, marginBottom: 10 }} /> : null
  }

  onMegaListEndReached() {
    if (this.props.isMegaLoadingMore === false) {
      this.props.getListMegaResult()
    }
  }

  onPowerListEndReached() {
    if (this.props.isPowerLoadingMore === false) {
      this.props.getListPowerResult()
    }
  }

  renderListMega = () => {
    const { listResultMega, isMegaRefreshing } = this.props
    if (_.isEmpty(listResultMega) === true) {
      return null
    }
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const dataSource = ds.cloneWithRows(listResultMega)
    return (
      <ListView
        refreshControl={
          <RefreshControl
            refreshing={isMegaRefreshing}
            onRefresh={this.onMegaRefreshing}
          />
        }
        dataSource={dataSource}
        renderRow={(rowData, section, rowIndex) => this.renderMegaItem(rowData, section, rowIndex)}
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps={'always'}
        showsVerticalScrollIndicator
        onEndReached={this.onMegaListEndReached}
        renderFooter={this.renderMegaFooter}
        onEndReachedThreshold={50}
      />
    )
  }

  renderListMegaPower = () => {
    const { listResultPower, isPowerRefreshing } = this.props
    if (_.isEmpty(listResultPower) === true) {
      return null
    }
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const dataSource = ds.cloneWithRows(listResultPower)
    return (
      <ListView
        refreshControl={
          <RefreshControl
            refreshing={isPowerRefreshing}
            onRefresh={this.onPowerRefreshing}
          />
        }
        dataSource={dataSource}
        renderRow={(rowData, section, rowIndex) => this.renderMegaPowerItem(rowData, section, rowIndex)}
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps={'always'}
        showsVerticalScrollIndicator
        onEndReached={this.onPowerListEndReached}
        renderFooter={this.renderPowerFooter}
        onEndReachedThreshold={50}
      />
    )
  }

  renderImageNew = (rowIndex) => {
    if (rowIndex === 0) {
      return (
        <Image source={Themes.Images.newLabelLottery} />
      )
    }
    return null
  }

  fetchNumber = (obj, special) => {
    const number = []
    const resultList = obj.split(',')
    const backgroundColor = special === true ? Themes.Colors.max4dColor : '#fff'
    const color = special === true ? '#fff' : Themes.Colors.max4dColor

    _.map(resultList, (value, key) => {
      number.push(
        <View key={key} style={[styles.numberResultMax, { backgroundColor }]}>
          <TextFont color={color}>{value}</TextFont>
        </View>
      )
    })


    return number
  }

  renderLayout4d = (obj) => {
    return (
      <View style={Themes.styleGB.centerContent}>
        <TextFont size={16} style={[styles.txtTitle4d, { color: Themes.Colors.max4dColor }]}>Giải Nhất</TextFont>
        <View style={{ flexDirection: 'row' }}>{this.fetchNumber(obj[0][0], true)}</View>
        <TextFont size={16} style={[styles.txtTitle4d, { color: '#6e6e6e' }]}>Giải Nhì</TextFont>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>{this.fetchNumber(obj[1][0])}</View>
          <View style={[styles.lineResultMax4d]} />
          <View style={{ flexDirection: 'row' }}>{this.fetchNumber(obj[1][1])}</View>
        </View>
        <TextFont size={16} style={[styles.txtTitle4d, { color: '#6e6e6e' }]}>Giải Ba</TextFont>
        <View style={{ flexDirection: 'row' }}>{this.fetchNumber(obj[2][0])}</View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>{this.fetchNumber(obj[2][1])}</View>
          <View style={[styles.lineResultMax4d]} />
          <View style={{ flexDirection: 'row' }}>{this.fetchNumber(obj[2][2])}</View>
        </View>
        <TextFont size={16} style={[styles.txtTitle4d, { color: '#6e6e6e' }]}>Giải Khuyến Khích</TextFont>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>{this.fetchNumber(obj[3][0])}</View>
          <View style={[styles.lineResultMax4d]} />
          <View style={{ flexDirection: 'row' }}>{this.fetchNumber(obj[4][0])}</View>
        </View>
      </View>
    )
  }

  renderMax4dItem = (item, section, rowIndex) => {
    const obj = JSON.parse(item.winning_numbers)
    const formattedDate = _.upperFirst(moment(item.drawing_date, 'DD-MM-YYYY').format('dddd, DD-MM-YYYY'))
    return (
      <View style={styles.rowMax4d}>
        <View style={[Themes.styleGB.flexRow, { alignItems: 'center' }]}>
          <TextFont style={[styles.txtTextDate]} color="#6e6e6e">{formattedDate}</TextFont>
          {this.renderImageNew(rowIndex)}
        </View>
        <View style={styles.rowResultMax4d}>
          {this.renderLayout4d(obj)}
        </View>
      </View>
    )
  }

  renderListMax4d = () => {
    const { listResultMax4d, isMax4dRefreshing } = this.props
    if (_.isEmpty(listResultMax4d) === true) {
      return null
    }

    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const dataSource = ds.cloneWithRows(listResultMax4d)

    return (
      <ListView
        refreshControl={
          <RefreshControl
            refreshing={isMax4dRefreshing}
            onRefresh={this.onMax4dRefreshing}
          />
        }
        dataSource={dataSource}
        renderRow={(rowData, section, rowIndex) => this.renderMax4dItem(rowData, section, rowIndex)}
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps={'always'}
        showsVerticalScrollIndicator
        onEndReached={this.onMax4dListEndReached}
        renderFooter={this.renderMax4dFooter}
        onEndReachedThreshold={50}
      />
    )
  }

  renderTabView = () => {
    return (
      <ScrollableTabView
        tabBarUnderlineStyle={{ backgroundColor: '#fff', height: 0 }}
        tabBarTextStyle={{ fontFamily: Themes.Fonts.type.Bold, paddingTop: 5, fontWeight: 'bold' }}
        tabBarActiveTextColor={Themes.Colors.navColor}
        tabBarInactiveTextColor="#949494"
        paddingContent={20}
        locked
        prerenderingSiblingsNumber={1}
        renderTabBar={() => <TabbarLottery />}
        page={this.props.initialPage}
      >
        <View tabLabel="MEGA 645">
          {this.renderListMega()}
        </View>
        <View tabLabel="MAX 4D">
          {this.renderListMax4d()}
        </View>
        <View tabLabel="POWER 655">
          {this.renderListMegaPower()}
        </View>
      </ScrollableTabView>
    )
  }

  render() {
    if (this.props.isFetchingMega === true || this.props.isFetchingMax4d === true || this.props.isFetchingPower === true) {
      return <Loading />
    }

    return (
      <View style={[styles.container]}>
        <StatusBar />
        <View style={[styles.tabView]}>
          {this.renderTabView()} 
        </View>
      </View>
    )
  }

}
LotteryResultsHome.propTypes = {}
export default LotteryResultsHome
