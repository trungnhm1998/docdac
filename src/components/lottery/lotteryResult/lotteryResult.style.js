import { StyleSheet, PixelRatio } from 'react-native'
import { Themes } from '../../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f6f6f6',
    padding: 10,
    overflow: 'scroll'
  },
  tabView: {
    backgroundColor: '#fff',
    overflow: 'scroll',
    // marginLeft: 10,
    // marginRight: 10,
    // marginTop: 10,
    flex: 1
  },
  rowMega: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderBottomWidth: 1 / PixelRatio.get(),
    borderColor: '#d7d7d7',
    justifyContent: 'center'
  },
  rowResultMega: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: Themes.Metrics.screenWidth * 0.07
  },
  numberResult: {
    height: Themes.Metrics.screenWidth * 0.09,
    width: Themes.Metrics.screenWidth * 0.09,
    borderRadius: (Themes.Metrics.screenWidth * 0.09) / 2,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    backgroundColor: Themes.Colors.navColor,
    borderWidth: 2 / PixelRatio.get(),
    borderColor: Themes.Colors.navColor
  },
  numberPowerResult: {
    height: Themes.Metrics.screenWidth * 0.09,
    width: Themes.Metrics.screenWidth * 0.09,
    borderRadius: (Themes.Metrics.screenWidth * 0.09) / 2,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    backgroundColor: '#d6a720',
    borderWidth: 2 / PixelRatio.get(),
    borderColor: '#d6a720'
  },
  rowMax4d: {
    // backgroundColor: '#f2f1f1'
  },
  txtTextDate: {
    padding: 10
  },
  rowResultMax4d: {
    backgroundColor: '#fff',
    padding: 5,
    alignItems: 'center',
    paddingBottom: 20
  },
  rowMax: {
    flexDirection: 'row'
  },
  numberResultMax: {
    height: Themes.Metrics.screenWidth * 0.09,
    width: Themes.Metrics.screenWidth * 0.09,
    borderRadius: (Themes.Metrics.screenWidth * 0.09) / 2,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    backgroundColor: Themes.Colors.max4dColor,
    borderWidth: 2 / PixelRatio.get(),
    borderColor: Themes.Colors.max4dColor,
    margin: Themes.Metrics.screenWidth * 0.007
  },
  resultGroup4d: {
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
    width: null,
    flexDirection: 'row'
  },
  lineResultMax4d: {
    width: 1 / PixelRatio.get(),
    backgroundColor: '#6e6e6e',
    height: Themes.Metrics.screenWidth * 0.08,
    marginHorizontal: 10,
    marginTop: 7
  },
  txtTitle4d: {
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 5,
    color: '#6e6e6e'
  }
})

export default styles
