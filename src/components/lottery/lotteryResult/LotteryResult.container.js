import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as LotteryAction, types } from '../../../redux/lottery'
import { Colors } from '../../../ui/themes'
import LotteryResult from './LotteryResult'
import Config from '../../../screens/config'

class LotteryResultsHomeContainer extends Component {
  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE,
    statusBarColor: Colors.navColor
  }

  constructor(props) {
    super(props)
    this.state = {
      firstAppear: true,
      
      isFetchingMega: true,
      isFetchingPower: true,
      isFetchingMax4d: true,

      isPowerLastPage: false,
      isPowerLoadingMore: false,
      isPowerRefreshing: false,
      powerCurrentPage: 1,

      isMegaLastPage: false,
      isMegaLoadingMore: false,
      isMegaRefreshing: false,
      megaCurrentPage: 1,

      isMax4dLastPage: false,
      isMax4dLoadingMore: false,
      isMax4dRefreshing: false,
      max4dCurrentPage: 1
    }
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.getListPowerResult = this.getListPowerResult.bind(this)
    this.getListMegaResult = this.getListMegaResult.bind(this)
    this.getListMax4dResult = this.getListMax4dResult.bind(this)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
  }

  componentWillReceiveProps(props) {
    const { apiResponse } = props.payload
    const { data } = apiResponse
    if (apiResponse.type === types.GET_LOTTERY_645_RESULT_SUCCESS) {
      this.setState({ isFetchingMega: false, isMegaLastPage: data.lastPage || false, isMegaLoadingMore: false, isMegaRefreshing: false })
    } else if (apiResponse.type === types.GET_LOTTERY_MAX4D_RESULT_SUCCESS) {
      this.setState({ isFetchingMax4d: false, isMax4dLastPage: data.lastPage || false, isMax4dLoadingMore: false, isMax4dRefreshing: false })
    } else if (apiResponse.type === types.GET_LOTTERY_655_RESULT_SUCCESS) {
      this.setState({ isFetchingPower: false, isPowerLastPage: data.lastPage || false, isPowerLoadingMore: false, isPowerRefreshing: false })
    } else if (apiResponse.type === types.GET_INIT_LOTTERY_RESULT_SUCCESS) {
      this.setState({ isFetchingMega: false, isFetchingMax4d: false, isFetchingPower: false })
    }
  }

  onNavigatorEvent(event) {
    if (event.id === 'willAppear') {
      if (this.state.firstAppear === true) {
        const { lotteryAction } = this.props
        lotteryAction.getInitLotteryResult({ pagePower: this.state.powerCurrentPage, pageMega: this.state.megaCurrentPage, pageMax: this.state.max4dCurrentPage })
        this.setState({ firstAppear: false })
      }
    }
  }

  onPressSubmit() {
    this.props.navigator.push(Config.screen.login)
  }

  getListPowerResult(isRefresh) {
    const { lotteryAction } = this.props
    if (isRefresh === true) {
      lotteryAction.getLottery655Result({ page: 1 })
      this.setState({ powerCurrentPage: 1, isMegaRefreshing: true })
    } else {
      const currentPage = this.state.powerCurrentPage + 1
      lotteryAction.getLottery655Result({ page: currentPage })
      this.setState({ powerCurrentPage: currentPage, isMegaLoadingMore: true })
    }
  }

  getListMegaResult(isRefresh) {
    const { lotteryAction } = this.props
    if (isRefresh === true) {
      lotteryAction.getLottery645Result({ page: 1 })
      this.setState({ megaCurrentPage: 1, isMegaRefreshing: true })
    } else {
      const currentPage = this.state.megaCurrentPage + 1
      lotteryAction.getLottery645Result({ page: currentPage })
      this.setState({ megaCurrentPage: currentPage, isMegaLoadingMore: true })
    }
  }

  getListMax4dResult(isRefresh) {
    const { lotteryAction } = this.props
    if (isRefresh === true) {
      lotteryAction.getLotteryMax4dResult({ page: 1 })
      this.setState({ max4dCurrentPage: 1, isMax4dRefreshing: true })
    } else {
      const currentPage = this.state.max4dCurrentPage + 1
      lotteryAction.getLotteryMax4dResult({ page: currentPage })
      this.setState({ max4dCurrentPage: currentPage, isMax4dLoadingMore: true })
    }
  }

  render() {
    const { lottery } = this.props.payload
    return (
      <LotteryResult
        onPressSubmit={this.onPressSubmit}
        navigator={this.props.navigator}
        initialPage={lottery.initialPage}
        getListPowerResult={this.getListPowerResult}
        getListMegaResult={this.getListMegaResult}
        getListMax4dResult={this.getListMax4dResult}
        listResultMega={lottery.megaLotteryResult}
        listResultPower={lottery.megaPowerLotteryResult}
        listResultMax4d={lottery.max4dLotteryResult}
        {...this.state}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    lotteryAction: bindActionCreators(LotteryAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LotteryResultsHomeContainer)
