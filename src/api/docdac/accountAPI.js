import md5 from 'md5'
import Request from './middleware/request'
import { API_USER } from '../../configs/api.config'

const accountAPI = {
  authorize(data) {
    const { account, country_code, password } = data
    return Request.callAPI(`${API_USER}/user/authorize`, { account, country_code, password: md5(password) })
  },
  authorizeFacebook(data) {
    const { fbAccessToken } = data
    return Request.callAPI(`${API_USER}/user/authorize_facebook`, { fb_access_token: fbAccessToken })
  },
  sendOTP(data) {
    const { account } = data
    return Request.callAPI(`${API_USER}/user/send_otp`, { account })
  },
  register(data) {
    const { account, password, otp } = data
    return Request.callAPI(`${API_USER}/user/register`, { account, otp, password: md5(password) })
  },
  registerAccountKit(data) {
    const { account, country_code, password, token } = data
    return Request.callAPI(`${API_USER}/user/register_account_kit`, { account, country_code, fb_access_token: token, password: md5(password) })
  },
  logout(data) {
    const { accessToken } = data
    return Request.callAPI(`${API_USER}/user/logout`, { access_token: accessToken })
  },
  verifyAccessToken(data) {
    const { accessToken } = data
    return Request.callAPI(`${API_USER}/user/verify_access_token`, { access_token: accessToken })
  },
  verifyAccount(data) {
    const { account, password } = data
    return Request.callAPI(`${API_USER}/user/verify_account`, { account, password: md5(password) })
  },
  verifyPhone(data) {
    const { account, country_code } = data
    return Request.callAPI(`${API_USER}/user/verify_phone`, { account, country_code })
  },
  getInfo(data) {
    const { accessToken } = data
    return Request.callAPI(`${API_USER}/user/get_info`, { access_token: accessToken })
  },
  updateInfo(data) {
    return Request.callAPI(`${API_USER}/user/update_info`, data)
  },
  changePassword(data) {
    const { access_token, new_password, old_password } = data
    return Request.callAPI(`${API_USER}/user/change_password`, { access_token, new_password: md5(new_password), old_password: md5(old_password) })
  },
  forgetPassword(data) {
    const { account, otp, password, password_retype } = data
    return Request.callAPI(`${API_USER}/user/forget_password`, { account, otp, password: md5(password), password_retype: md5(password_retype) })
  },
  forgetPasswordAccountKit(data) {
    const { account, country_code, token, password, password_retype } = data
    return Request.callAPI(`${API_USER}/user/forget_password_account_kit`, { account, country_code, fb_access_token: token, password: md5(password), password_retype: md5(password_retype) })
  },
  insideAccounts(data) {
    const { date } = data
    return Request.callAPI(`${API_USER}/user/inside_accounts`, { date })
  },
  uploadAvatar(data) {
    const { avatar } = data
    return Request.callPostAPI(`${API_USER}/avatar/upload`, { avatar })
  },
  getInitConfig() {
    return Request.callAPI(`${API_USER}/init/information`)
  }
}

export default accountAPI
