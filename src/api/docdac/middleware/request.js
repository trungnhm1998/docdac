import md5 from 'md5'
import deviceInfo from 'react-native-device-info'
import { NativeModules, Platform } from 'react-native'
import _ from 'lodash'
import { API_GRAPH_PRIVATE_KEY, API_GRAPH_APP, API_DOCDAC_CHANNEL } from '../../../configs/api.config'

const generateToken = function (encryptedData, otp, privateKey) {
  return md5(encryptedData + otp + privateKey)
}

const generalParams = {
  user_agent: deviceInfo.getUserAgent(),
  platform: Platform.OS,
  channel: API_DOCDAC_CHANNEL,
  device_id: deviceInfo.getDeviceId(),
  telco: 'Viettel',
  version: deviceInfo.getVersion(),
  lang: 'vi',
  app: API_GRAPH_APP
}

const generateParams = async (data) => {
  const { RNEncryptTool } = NativeModules
  const params = _.merge(data, generalParams)
  const otp = await RNEncryptTool.generateOTP(API_GRAPH_PRIVATE_KEY, Date.now())
  const encryptedData = await RNEncryptTool.tripleDesEncrypt(API_GRAPH_PRIVATE_KEY, JSON.stringify(params))
  const validToken = generateToken(encryptedData, otp, API_GRAPH_PRIVATE_KEY)

  return {
    headers: {
      app: API_GRAPH_APP,
      otp,
      token: validToken
    },
    params: {
      q: encodeURIComponent(encryptedData)
    }
  }
}

const generateUploadParams = async (data) => {
  const avatar = {
    uri: data.avatar,
    type: 'image/jpeg',
    name: `${md5(Date.now())}.jpg`
  }
  const body = new FormData()
  body.append('avatar', avatar)
  return {
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    body
  }
}

const decodeResponse = async (response) => {
  try {
    const { RNEncryptTool } = NativeModules
    return await RNEncryptTool.tripleDesDecrypt(API_GRAPH_PRIVATE_KEY, response)
  } catch (e) {
    return response
  }
}

const Request = {
  callAPI(api, data) {
    return new Promise(async (resolve, reject) => {
      try {
        const generateData = await generateParams(data)
        const response = await fetch(`${api}?q=${generateData.params.q}`, { headers: generateData.headers })

        if(__DEV__) {
          console.log('[RESPONSE]: ', api, data, response)
        }

        const responseText = await response.text()
        const jsonString = await decodeResponse(responseText)
        const json = JSON.parse(jsonString)

        if (__DEV__) {
          console.log('[REQUEST]', api, data, generateData, json)
        }
        // setTimeout(() => {
        //   resolve(json)
        // }, 500)
        resolve(json)
      } catch (error) {
        if (__DEV__) {
          console.log(error)
        }
        reject(error)
      }
    })
  },
  callPostAPI(api, data) {
    return new Promise(async (resolve, reject) => {
      try {
        const generateData = await generateUploadParams(data)
        const response = await fetch(api, generateData)

        if (__DEV__) {
          console.log('[RESPONSE]', api, data, response)
        }

        const responseText = await response.text()
        const json = JSON.parse(responseText)
        if (__DEV__) {
          console.log('[REQUEST]', api, data, generateData, json)
        }

        // setTimeout(() => {
        //   resolve(json)
        // }, 1000)
        resolve(json)
      } catch (error) {
        if (__DEV__) {
          console.log(error)
        }
        reject(error)
      }
    })
  }
}
export default Request
