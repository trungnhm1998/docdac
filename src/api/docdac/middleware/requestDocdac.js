import { Platform } from 'react-native'
import _ from 'lodash'

const jsonToFormEncoded = (data) => {
  const str = []
  for (const p in data) {
    if (_.isObject(data[p])) {
      const d = data[p]
      for (const o in d) {
        if (d.hasOwnProperty(o) && d[o]) {
          str.push(p + '[' + encodeURIComponent(o) + ']=' + encodeURIComponent(d[o]))
        }
      }
    } else {
      if (data.hasOwnProperty(p) && data[p]) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(data[p]))
      }
    }
  }
  return str.join('&')
}

const generateParams = (params, method) => {
  const data = JSON.stringify(params.data)
  return {
    method: method,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
    body: jsonToFormEncoded({ ...params, data })
  }
}

const RequestDocdac = {
  callAPI(api, data, method = 'post') {
    return new Promise(async(resolve, reject) => {
      try {
        const generateData = generateParams(data, method)
        const response = await fetch(api, generateData)
        const json = await response.json()

        if (__DEV__) {
          console.log('[REQUEST]', api, data, generateData, json)
        }
        setTimeout(() => {
          resolve(json)
        }, 500)
      } catch (error) {
        if (__DEV__) {
          console.log(error)
        }
        reject(error)
      }
    })
  }
}
export default RequestDocdac
