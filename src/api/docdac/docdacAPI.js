import deviceInfo from 'react-native-device-info'
import Request from './middleware/requestDocdac'
import { API_DOCDAC, API_DOCDAC_CHANNEL } from '../../configs/api.config'

const deviceId = deviceInfo.getDeviceId()

const docdacAPI = {
  getUserInfo(data, accessToken) {
    const command = 'U_GET_MY_INFO_NOT_ENCRYPTED'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken, device_id: deviceId, channel: API_DOCDAC_CHANNEL })
  },
  updateUserInfo(data, accessToken) {
    const command = 'U_UPDATE_INFO'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  userLogout(accessToken) {
    const command = 'U_LOGOUT'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, token: accessToken, device_id: deviceId })
  },
  getPrizesList(data) {
    const command = 'L_PRIZES_LIST'
    const { ticketType } = data
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, data: { ticket_type: ticketType, channel: API_DOCDAC_CHANNEL } })
  },
  getAgentNearest(data) {
    const command = 'A_AGENT_NEAREST'
    return Request.callAPI(`${API_DOCDAC}/agent/`, { command, data })
  },
  getUserTicket(data, accessToken) {
    const command = 'L_TICKET_BOUGHT'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, data, token: accessToken })
  },
  getUserTicketDetail(data, accessToken) {
    const command = 'L_TICKET_DETAIL'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, data, token: accessToken })
  },
  getPaymentOrder(data, accessToken) {
    const command = 'P_GENERATE_PAYMENT_ORDER'
    data.from_react_app = 1
    return Request.callAPI(`${API_DOCDAC}/payment/`, { command, data, token: accessToken })
  },
  getRechargeHistory(data, accessToken) {
    const command = 'U_RECHARGE_HISTORY_LOAD_MORE'
    // const command = 'U_RECHARGE_HISTORY'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  withdrawHistory(data, accessToken) {
    const command = 'U_WITHDRAW_HISTORY_LOAD_MORE'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  paymentMOMORecharge(data, accessToken) {
    const command = 'P_PAYMENT_MOMO_RECHARGE'
    return Request.callAPI(`${API_DOCDAC}/payment/`, { command, data, token: accessToken })
  },
  getDrawNumberByPage(data) {
    const command = 'L_DRAW_NUMBER_PAGE'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, data })
  },
  getDrawNumberByDate(data) {
    const command = 'L_DRAW_NUMBER_DATE'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, data })
  },
  ratingAgency(data, accessToken) {
    const command = 'L_RATING_AGENCY'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, data, token: accessToken })
  },
  provinceListCustom() {
    const command = 'G_PROVINCE_LIST_CUSTOM'
    return Request.callAPI(`${API_DOCDAC}/general/`, { command })
  },
  districtListCustom(data) {
    const command = 'G_DISTRICT_LIST_CUSTOM'
    return Request.callAPI(`${API_DOCDAC}/general/`, { command, data })
  },
  provinceList() {
    const command = 'G_PROVINCE_LIST'
    return Request.callAPI(`${API_DOCDAC}/general/`, { command })
  },
  districtList(data) {
    const command = 'G_DISTRICT_LIST'
    return Request.callAPI(`${API_DOCDAC}/general/`, { command, data })
  },
  getWardList(data) {
    const command = 'G_WARD_LIST'
    return Request.callAPI(`${API_DOCDAC}/general/`, { command, data })
  },
  userAddressList(accessToken) {
    const command = 'U_ADDRESS_LIST'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, token: accessToken })
  },
  userAddressAdd(args) {
    const command = 'U_ADDRESS_ADD'
    const { data, accessToken } = args
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  userAddressUpdate(data, accessToken) {
    const command = 'U_ADDRESS_UPDATE'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  userConfirmReceivedTicket(data, accessToken) {
    const command = 'U_CONFIRM_RECEIVED_TICKET'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  getProvinceListCustom() {
    const command = 'G_PROVINCE_LIST_CUSTOM'
    return Request.callAPI(`${API_DOCDAC}/general/`, { command })
  },
  getDistrictListCustom(data) {
    const command = 'G_DISTRICT_LIST_CUSTOM'
    return Request.callAPI(`${API_DOCDAC}/general/`, { command, data })
  },
  userWithdraw(data, accessToken) {
    const command = 'U_WITHDRAW'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  userWithdrawConfirm(data, accessToken) {
    const command = 'U_WITHDRAW_CONFIRM'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  userWithdrawHistory(data, accessToken) {
    const command = 'U_WITHDRAW_HISTORY'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  paymentGateRecharge(data, accessToken) {
    const command = 'P_PAYMENT_GATE_RECHARGE'
    return Request.callAPI(`${API_DOCDAC}/payment/`, { command, data, token: accessToken })
  },
  paymentMobileCard(data, accessToken) {
    const command = 'P_PAYMENT_MOBILE_CARD_RECHARGE'
    return Request.callAPI(`${API_DOCDAC}/payment/`, { command, data, token: accessToken })
  },
  buyTicket(data, accessToken) {
    const command = 'L_BUY_TICKET'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, data, token: accessToken })
  },
  getTicketRewardScheduleConfig(accessToken) {
    const command = 'L_TICKET_SCHEDULE_REWARD_CONFIG'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, token: accessToken })
  },
  schedulerRewardTicket(data, accessToken) {
    const command = 'L_TICKET_SCHEDULE_REWARD'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, data, token: accessToken })
  },
  getTicketDrawStatistics(data) {
    const command = 'L_TICKET_DRAW_STATISTICS'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, data })
  },
  getPaymentServiceState(accessToken) {
    const command = 'P_PAYMENT_SERVICE_STATE_V2'
    return Request.callAPI(`${API_DOCDAC}/payment/`, { command, token: accessToken })
  },
  max4dSoldoutNumbers(accessToken) {
    const command = 'L_SOLDOUT_NUMBER_MAX4D'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, token: accessToken })
  },
  bankInfoPayment(accessToken) {
    const command = 'G_BANKING_INFO'
    return Request.callAPI(`${API_DOCDAC}/general/`, { command, token: accessToken })
  },
  getTicketTypeList() {
    const command = 'L_TICKET_TYPE_LIST'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, data: { channel: API_DOCDAC_CHANNEL } })
  },
  getCalendarPrevious() {
    const command = 'L_DRAW_CALENDAR_PREVIOUS'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command })
  },
  getUserTicketHistoryByPeriod(data, accessToken) {
    const command = 'L_USER_TICKET_HISTORY'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, data: { page: data.page, calendar_id: data.calendar_id }, token: accessToken })
  },
  getListAgentByDistrict(districtId) {
    const command = 'A_AGENT_DISTRICT'
    return Request.callAPI(`${API_DOCDAC}/agent/`, { command, data: { district_id: districtId } })
  },
  postTicketScheduleReward(data, accessToken) {
    const command = 'L_TICKET_SCHEDULE_REWARD'
    return Request.callAPI(`${API_DOCDAC}/lottery/`, { command, data, token: accessToken })
  },
  getBannerList() {
    const command = 'G_BANNER_LIST'
    return Request.callAPI(`${API_DOCDAC}/general/`, { command })
  },
  getNapasBankList() {
    const command = 'P_PAYMENT_NAPAS_BANK_LIST'
    return Request.callAPI(`${API_DOCDAC}/payment/`, { command })
  },
  getMoneyHistory(data, accessToken) {
    const command = 'U_MONEY_HISTORY_FILTER_DATE'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  getwithdrawHistory(data, accessToken) {
    const command = 'U_BANKING_WITHDRAW_HISTORY'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  sendOrderWithdraw(data, accessToken) {
    const command = 'U_SEND_ORDER_BANKING_WITHDRAW'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  getMoneySetting() {
    const command = 'U_GET_MONEY_SETTINGS'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command })
  },
  transferMoney(data, accessToken) {
    const command = 'U_TRANSFER_MONEY'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command, data, token: accessToken })
  },
  getTransferHistory(data, accessToken) {
    const command = 'U_TRANSFER_HISTORY'
    return Request.callAPI(`${API_DOCDAC}/user/`, { command , data, token: accessToken })
  }
}

export default docdacAPI
