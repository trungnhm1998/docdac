import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/lottery'
import { docdacAPI } from '../../api/'

function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(docdacAPI.getListAgentByDistrict, action.payload.districtId)
    yield put({ type: apiTypes.HIDE_LOADING })
    switch (code) {
      case responseCode.GET_LIST_AGENT_BY_DISTRICT_SUCCESS : {
        yield put({ type: types.GET_AGENT_LIST_SUCCESS, payload: { data, ...action.payload } })
        break
      }
      case responseCode.GET_LIST_AGENT_BY_DISTRICT_EMPTY : {
        yield put({ type: types.GET_AGENT_LIST_EMPTY })
    //    yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, message: 'Không tìm thấy đại lý tại điểm đã chọn. Vui lòng chọn khu vực khác', isDefault: true }))
        break
      }
      default: {
        return null
      }
    }
  } catch (error) {
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_AGENT_LIST, doAction)
}
