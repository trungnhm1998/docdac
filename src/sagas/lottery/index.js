import GetInitLotteryResult from './getInitLotteryResult.saga'
import GetListDeliveryAddress from './getListDeliveryAddress.saga'
import GetMegaLotteryResult from './getMegaLotteryResult.saga'
import GetMegaPowerLotteryResult from './getMegaPowerLotteryResult.saga'
import GetMax4DLotteryResult from './getMax4dLotteryResult.saga'
import GetMegaPrizeList from './getMegaPrizeList.saga'
import GetMax4DPrizeList from './getMax4DPrizeList.saga'
import GetTicketTypeList from './getTicketTypeList.saga'
import GetUserLottery from './getUserLottery.saga'
import GetUserLotteryDetail from './getUserLotteryDetail.saga'
import BuyMegaTicket from './buyMegaTicket.saga'
import BuyMegaPowerTicket from './buyMegaPowerTicket.saga'
import BuyMaxTicket from './buyMaxTicket.saga'
import UserConfirmReceiveTicket from './userConfirmReceiveTicket.saga'
import GetListPeriodTicket from './getListPeriodTicket.saga'
import GetTicketByPreiod from './getTicketByPreiod.saga'
import GetAgentList from './getAgentList.saga'
import GetAgentRewardList from './getAgentRewardList.saga'
import PostTicketScheduleReward from './postTicketScheduleReward.saga'
import GetBannerList from './getBannerList.saga'

export {
  GetInitLotteryResult,
  GetListDeliveryAddress,
  GetMegaLotteryResult,
  GetMegaPowerLotteryResult,
  GetMax4DLotteryResult,
  GetMegaPrizeList,
  GetMax4DPrizeList,
  GetTicketTypeList,
  GetUserLottery,
  GetUserLotteryDetail,
  BuyMegaTicket,
  BuyMegaPowerTicket,
  BuyMaxTicket,
  UserConfirmReceiveTicket,
  GetListPeriodTicket,
  GetTicketByPreiod,
  GetAgentList,
  GetAgentRewardList,
  PostTicketScheduleReward,
  GetBannerList
}
