import { takeLatest, call, put, select } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/lottery'
import { docdacAPI } from '../../api/'
const accessToken = state => state.accountInfo.accessToken

function* doAction(action) {
  const token = yield select(accessToken)
  const { page, calendar_id } = action.payload
  try {
    const { code, msg, data, last_page } = yield call(docdacAPI.getUserTicketHistoryByPeriod, { page, calendar_id }, token)
    switch (code) {
      case responseCode.REQUEST_SUCCESS: {
        yield put({ type: types.GET_TICKET_BY_PREIOD_SUCCESS, payload: { data, page } })
        yield put(apiActions.storeResponsePayload({ type: types.GET_TICKET_BY_PREIOD_SUCCESS, payload: { lastPage: last_page } }))
        break
      }
      case responseCode.DATA_EMPTY: {
        if (page === 1) {
          yield put({ type: types.GET_TICKET_BY_PREIOD_EMPTY })
        } else {
          yield put({ type: types.GET_TICKET_BY_PREIOD_SUCCESS, payload: { data, page } })
          yield put(apiActions.storeResponsePayload({ type: types.GET_TICKET_BY_PREIOD_SUCCESS, payload: { lastPage: last_page } }))
        }
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, message: msg, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_TICKET_BY_PREIOD, doAction)
}
