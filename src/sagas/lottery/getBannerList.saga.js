import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/lottery'
import { docdacAPI } from '../../api/'

function* doAction(action) {

  try {
    const { code, data } = yield call(docdacAPI.getBannerList)
    switch (code) {
      case responseCode.GET_BANNER_LIST_SUCCESS : {
        yield put({ type: types.GET_BANNER_LIST_SUCCESS, payload: { data } })
        break
      }
      case responseCode.GET_LIST_AGENT_BY_DISTRICT_EMPTY : {
        yield put({ type: types.GET_AGENT_LIST_EMPTY })
        break
      }
      default: {
        return null
      }
    }
  } catch (error) {
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_BANNER_LIST, doAction)
}
