import { takeLatest, call, put, all } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import _ from 'lodash'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/lottery'
import { docdacAPI } from '../../api/'

function* doAction() {
  try {
    const { data } = yield call(docdacAPI.getProvinceListCustom)

    let listRaceDistrict = []
    let listRaceWard = []
    _.forEach(data, (item) => {
      listRaceDistrict.push(call(docdacAPI.getDistrictListCustom, { province_id: item.province_id }))
    })

    const resultDistrictRace = yield all([...listRaceDistrict])

    let resultDistrict = []
    _.map(resultDistrictRace, (item) => {
      resultDistrict.push(item.data)

      _.forEach(item.data, (itemDistrict) => {
        listRaceWard.push(call(docdacAPI.getWardList, { district_id: itemDistrict.district_id }))
      })
    })

    const resultWardRace = yield all([...listRaceWard])

    let keyDistrict = 0
    let keyDistrictItem = 0

    _.map(resultWardRace, (item) => {
      resultDistrict[keyDistrict][keyDistrictItem].ward = item.data
      keyDistrictItem++
      if (resultDistrict[keyDistrict][keyDistrictItem] === undefined) {
        keyDistrict++
        keyDistrictItem = 0
      }
    })

    _.map(data, (item, key) => {
      data[key]['district'] = resultDistrict[key]
    })

    yield put({ type: types.GET_LIST_DELIVERY_ADDRESS_SUCCESS, payload: { data } })
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_LIST_DELIVERY_ADDRESS, doAction)
}
