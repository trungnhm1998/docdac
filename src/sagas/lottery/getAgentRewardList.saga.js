import { takeLatest, call, select, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/lottery'
import { docdacAPI } from '../../api/'
const accessToken = state => state.accountInfo.accessToken

function* doAction(action) {
  try {
    const token = yield select(accessToken)
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(docdacAPI.getTicketRewardScheduleConfig, token)
    yield put({ type: apiTypes.HIDE_LOADING })

    switch (code) {
      case responseCode.REQUEST_SUCCESS : {
        yield put({ type: types.GET_AGENT_REWARD_LIST_SUCCESS, payload: data })
        break
      }
      default: {
        return null
      }
    }
  } catch (error) {
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_AGENT_REWARD_LIST, doAction)
}
