import { takeLatest, call, put, all } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import * as responseCodeDocdac from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/lottery'
import { docdacAPI } from '../../api/'

function* doAction(action) {
  try {

    const result = yield all([
      call(docdacAPI.getDrawNumberByPage, { ticket_type: 2, page: action.payload.pageMega }),
      call(docdacAPI.getDrawNumberByPage, { ticket_type: 4, page: action.payload.pageMax }),
      call(docdacAPI.getDrawNumberByPage, { ticket_type: 1, page: action.payload.pagePower })
    ])

    if (result[0].code !== responseCodeDocdac.REQUEST_SUCCESS
      || result[1].code !== responseCodeDocdac.REQUEST_SUCCESS
      || result[2].code !== responseCodeDocdac.REQUEST_SUCCESS) {
      yield put(apiActions.storeResponseMessage({ type: types.GET_INIT_LOTTERY_RESULT_FAIL, code: -1, message: 'Lấy kết quả thất bại', isDefault: true }))
    } else {
      yield put({ type: types.GET_LOTTERY_645_RESULT_SUCCESS, payload: { data: result[0].data, page: action.payload.pageMega } })
      yield put({ type: types.GET_LOTTERY_MAX4D_RESULT_SUCCESS, payload: { data: result[1].data, page: action.payload.pageMax } })
      yield put({ type: types.GET_LOTTERY_655_RESULT_SUCCESS, payload: { data: result[2].data, page: action.payload.pagePower } })
      yield put(apiActions.storeResponseMessage({ type: types.GET_INIT_LOTTERY_RESULT_SUCCESS }))
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_INIT_LOTTERY_RESULT, doAction)
}
