import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/lottery'
import { docdacAPI } from '../../api/'

function* doAction(action) {
  try {
    const { code, data, msg } = yield call(docdacAPI.getPrizesList, { ticketType: 4 })
    switch (code) {
      case responseCode.REQUEST_SUCCESS: {
        const dispatchAction = {
          type: types.GET_MAX4D_PRIZE_LIST_SUCCESS,
          payload: data
        }
        yield put(dispatchAction)
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, message: msg, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_MAX4D_PRIZE_LIST, doAction)
}
