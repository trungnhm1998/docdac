import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/lottery'
import { docdacAPI } from '../../api/'

function* doAction(action) {
  try {
    const { code, data, last_page } = yield call(docdacAPI.getDrawNumberByPage, { ticket_type: 2, ...action.payload })
    switch (code) {
      case responseCode.GET_LOTTERY_RESULT : {
        yield put({ type: types.GET_LOTTERY_645_RESULT_SUCCESS, payload: { data, ...action.payload } })
        yield put(apiActions.storeResponsePayload({ type: types.GET_LOTTERY_645_RESULT_SUCCESS, payload: { lastPage: last_page } }))
        break
      }
      default: {
        return null
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_LOTTERY_645_RESULT, doAction)
}
