import { Platform } from 'react-native'
import { delay } from 'redux-saga'
import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types as accountTypes } from '../../redux/account'
import { types } from '../../redux/lottery'
import { docdacAPI } from '../../api/'

function* doAction(action) {
  try {
    const { accessToken } = action.payload
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, msg } = yield call(docdacAPI.buyTicket, action.payload.data, accessToken)
    yield put({ type: apiTypes.HIDE_LOADING })

    switch (code) {
      case responseCode.REQUEST_SUCCESS: {
        yield put({
          type: accountTypes.GET_USERINFO_DOCDAC,
          payload: { accessToken, os: Platform.OS, env: 1, deviceToken: '' }
        })
        yield put(apiActions.storeResponseMessage({ type: types.BUY_MEGA_TICKET_SUCCESS }))
        break
      }
      case responseCode.BUY_TICKET_CLOSED:
      case responseCode.BUY_TICKET_PENDING: {
        yield put(apiActions.storeResponseMessage({ type: types.BUY_MEGA_TICKET_NOT_ENOUGH_MONEY, message: msg }))
        break
      }
      case responseCode.BUY_TICKET_ERROR_MONEY: {
        yield put(apiActions.storeResponseMessage({ type: types.BUY_MEGA_TICKET_NOT_ENOUGH_MONEY, message: msg }))
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, code, message: msg, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.BUY_MEGA_TICKET, doAction)
}
