import { takeLatest, call, put, select } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/lottery'
import { docdacAPI } from '../../api/'
const accessToken = state => state.accountInfo.accessToken

function* doAction(action) {
  const token = yield select(accessToken)
  const { page } = action.payload
  try {
    const { code, msg, data, last_page } = yield call(docdacAPI.getUserTicket, { page, from_date: '2016-01-01', to_date: '2017-12-30' }, token)
    switch (code) {
      case responseCode.REQUEST_SUCCESS: {
        yield put({ type: types.GET_USER_LOTTERY_SUCCESS, payload: { data, ...action.payload } })
        yield put(apiActions.storeResponsePayload({ type: types.GET_USER_LOTTERY_SUCCESS, payload: { lastPage: last_page } }))
        break
      }
      case responseCode.DATA_EMPTY: {
        yield put({ type: types.GET_USER_LOTTERY_EMPTY })
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, message: msg, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_USER_LOTTERY, doAction)
}
