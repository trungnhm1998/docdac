import { takeLatest, call, put, select } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/lottery'
import { docdacAPI } from '../../api/'
const accessToken = state => state.accountInfo.accessToken

function* doAction(action) {
  const token = yield select(accessToken)
  const { ticketID } = action.payload
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, msg, data } = yield call(docdacAPI.userConfirmReceivedTicket, { ticket_id: ticketID }, token)
    yield put({ type: apiTypes.HIDE_LOADING })
    switch (code) {
      case responseCode.REQUEST_SUCCESS: {
        alert("Bạn đã lấy vé thành công !")
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, code, message: msg, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.USER_CONFIRM_RECEIVE_TICKET, doAction)
}
