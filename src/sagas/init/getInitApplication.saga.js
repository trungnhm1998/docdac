import { takeLatest, call, put, select, all } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types as paymentTypes } from '../../redux/payment'
import { types as accountTypes } from '../../redux/account'
import { types as lotteryTypes } from '../../redux/lottery'
import { types } from '../../redux/init'
import { docdacAPI } from '../../api/'

function* doAction() {
  try {
    const accessToken = yield select(state => state.accountInfo.accessToken)
    const resultTicketTypeList = yield call(docdacAPI.getTicketTypeList)

    if (resultTicketTypeList) {
      yield put({
        type: lotteryTypes.GET_TICKET_TYPE_LIST_SUCCESS,
        payload: resultTicketTypeList.data
      })
      yield put(apiActions.storeResponseMessage({ type: types.GET_INIT_APPLICATION_SUCCESS }))
    }

    const result = yield all([
      call(docdacAPI.userAddressList, accessToken),
      call(docdacAPI.getPaymentServiceState, accessToken),
      call(docdacAPI.bankInfoPayment, accessToken)
    ])

    if ((result[0].code !== responseCode.REQUEST_SUCCESS && result[0].code !== responseCode.DATA_EMPTY)
      || result[1].code !== responseCode.REQUEST_SUCCESS
      || result[2].code !== responseCode.REQUEST_SUCCESS) {
      yield put(apiActions.storeResponseMessage({ type: types.INIT_GET_INFORMATION_FAIL, code: -1, isDefault: true }))
    } else {
      yield put({
        type: accountTypes.GET_USER_ADDRESS_SUCCESS,
        payload: result[0].data || []
      })

      yield put({
        type: paymentTypes.GET_PAYMENT_LIST_SUCCESS,
        payload: result[1].data
      })

      yield put({
        type: paymentTypes.GET_BANK_INFO_SUCCESS,
        payload: result[2].data
      })
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_INIT_APPLICATION, doAction)
}
