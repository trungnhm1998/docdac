import { takeLatest, call, put, select } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/payment'
import { docdacAPI } from '../../api/'

function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const accessToken = yield select(state => state.accountInfo.accessToken)
    const { code, data, msg } = yield call(docdacAPI.userWithdrawConfirm, action.payload.data, accessToken)
    yield put({ type: apiTypes.HIDE_LOADING })

    switch (code) {
      case responseCode.REQUEST_SUCCESS: {
        action.payload.navigator.nextScreen(msg)
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: types.WITHDRAW_MOMO_CONFIRM_FAIL, code, message: msg, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.WITHDRAW_MOMO_CONFIRM, doAction)
}
