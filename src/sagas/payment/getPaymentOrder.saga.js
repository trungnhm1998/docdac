import { delay } from 'redux-saga'
import { takeLatest, call, put, select } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { PaymentGateway } from '../../configs/constant.config'
import { types } from '../../redux/payment'
import { docdacAPI } from '../../api/'

function* doAction(action) {
  try {
    const accessToken = yield select(state => state.accountInfo.accessToken)
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data, msg } = yield call(docdacAPI.getPaymentOrder, action.payload.data, accessToken)

    switch (code) {
      case responseCode.REQUEST_SUCCESS: {
        if (action.payload.data.payment_type === PaymentGateway.ZALO_PAY) {
          action.payload.navigator.callback({ token: data.zptranstoken, amount: action.payload.data.amount })
        }

        if (action.payload.data.payment_type === PaymentGateway.PAY_123 
          || action.payload.data.payment_type === PaymentGateway.WEB_MONEY
          || action.payload.data.payment_type === PaymentGateway.MOMO
          || action.payload.data.payment_type === PaymentGateway.NAPAS
        ) {
          action.payload.navigator.callback({ paymentURL: data.payment_url, paymentOrder: data.payment_order, amount: action.payload.data.amount })
        }

        yield delay(500)
        yield put({ type: apiTypes.HIDE_LOADING })
        break
      }
      default: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put(apiActions.storeResponseMessage({ type: types.GET_PAYMENT_ORDER_FAIL, code, message: msg, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_PAYMENT_ORDER, doAction)
}
