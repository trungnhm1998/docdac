import { fork, all } from 'redux-saga/effects'
import * as Init from './init'
import * as Account from './account'
import * as Lottery from './lottery'
import * as Payment from './payment'

function* rootSagas() {

  yield all([
    fork(Init.GetInitApplication),
    fork(Init.GetInitConfig),

    fork(Account.Authorize),
    fork(Account.SendOTPRegister),
    fork(Account.SendOTPForgotPassword),
    fork(Account.ReSendOTPRegister),
    fork(Account.VerifyPhone),
    fork(Account.Register),
    fork(Account.RegisterAccountKit),
    fork(Account.GetUserInfo),
    fork(Account.GetUserInfoDocdac),
    fork(Account.Logout),
    fork(Account.UpdateUserInfo),
    fork(Account.ForgotPassword),
    fork(Account.ForgotPasswordAccountKit),
    fork(Account.GetUserAddress),
    fork(Account.UploadAvatar),
    fork(Account.AddNewAddress),
    fork(Account.RechargeHistory),
    fork(Account.WithdrawHistory),
    fork(Account.MoneyHistory),
    fork(Account.GetWithdrawHistory),
    fork(Account.SendOrderWithdraw),
    fork(Account.GetMoneySetting),
    fork(Account.TransferMoney),
    fork(Account.GetTransferHistory),

    fork(Lottery.GetInitLotteryResult),
    fork(Lottery.GetListDeliveryAddress),
    fork(Lottery.GetMegaLotteryResult),
    fork(Lottery.GetMegaPowerLotteryResult),
    fork(Lottery.GetMax4DLotteryResult),
    fork(Lottery.GetMegaPrizeList),
    fork(Lottery.GetMax4DPrizeList),
    fork(Lottery.GetTicketTypeList),
    fork(Lottery.GetUserLottery),
    fork(Lottery.GetUserLotteryDetail),
    fork(Lottery.BuyMegaTicket),
    fork(Lottery.BuyMaxTicket),
    fork(Lottery.BuyMegaPowerTicket),
    fork(Lottery.UserConfirmReceiveTicket),
    fork(Lottery.GetListPeriodTicket),
    fork(Lottery.GetTicketByPreiod),
    fork(Lottery.GetAgentList),
    fork(Lottery.GetAgentRewardList),
    fork(Lottery.PostTicketScheduleReward),
    fork(Lottery.GetBannerList),

    fork(Payment.GetPaymentList),
    fork(Payment.RechargeMomo),
    fork(Payment.GetPaymentOrder),
    fork(Payment.PaymentGate),
    fork(Payment.PaymentCard),
    fork(Payment.WithdrawMomo),
    fork(Payment.WithdrawMomoConfirm),
    fork(Payment.GetNapasBankList),

  ])
}
export default rootSagas
