import { takeLatest, call, put, select } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { accountAPI } from '../../api/'

function* doAction(action) {
  try {
    const { navigator } = action.payload
    const accessToken = yield select(state => state.accountInfo.accessToken)

    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data, desc, message } = yield call(accountAPI.logout, { accessToken })
    yield put({ type: apiTypes.HIDE_LOADING })

    switch (code) {
      case responseCode.LOGOUT_SUCCESS:
      case responseCode.ACCESS_TOKEN_INVALID: {
        yield put({ type: types.LOGOUT_USER_SUCCESS })
        navigator.nextScreen()
        break
      }

      default: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put(apiActions.storeResponseMessage({ type: types.LOGOUT_USER_FAIL, message, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.LOGOUT_USER, doAction)
}
