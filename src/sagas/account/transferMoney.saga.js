import { takeLatest, call, put } from 'redux-saga/effects'
import { Alert } from 'react-native'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { docdacAPI } from '../../api/'

function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { accessToken } = action.payload
    const { phoneNumber, money } = action.payload.data
    const { code, data, msg } = yield call(docdacAPI.transferMoney, { receiver_phone: phoneNumber, money }, accessToken)
    switch (code) {
      case responseCode.TRANSFER_MONEY_SUCCESS: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put({ type: types.TRANSFER_MONEY_SUCCESS, payload: { data } })
        yield Alert.alert('Thông báo', 'Chuyển tiền thành công.')
        break
      }
      default: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, message, isDefault: true }))
        yield Alert.alert('Thông báo', msg)
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.TRANSFER_MONEY, doAction)
}
