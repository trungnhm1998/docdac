import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { docdacAPI } from '../../api/'

const mock_data = {
  "code": 1,
  "msg": "Thành công",
  "data": {
      "total_page": "1",
      "page_data": [
          {
              "withdraw_id": "4111",
              "user_id": "540744612",
              "bank_name": "ACB",
              "bank_branch": "hcm",
              "bank_account_name": "Khanh Dinh",
              "bank_account_number": "22223333",
              "money": "200000",
              "fee": "10000",
              "total_money": "210000",
              "state": "1",
              "created_date": "2018-11-27 09:34:12"
          },
          {
              "withdraw_id": "4042",
              "user_id": "540744612",
              "bank_name": "TMCP An Bình - AB Bank",
              "bank_branch": "q1",
              "bank_account_name": "Khanh Dinh",
              "bank_account_number": "0088997766",
              "money": "1000000",
              "fee": "10000",
              "total_money": "1000000",
              "state": "1",
              "created_date": "2017-11-01 15:30:42"
          },
          {
              "withdraw_id": "4041",
              "user_id": "540744612",
              "bank_name": "TMCP Á châu - ACB",
              "bank_branch": "Tan Dinh",
              "bank_account_name": "Khanh Dinh",
              "bank_account_number": "00942264001",
              "money": "1000000",
              "fee": "10000",
              "total_money": "1000000",
              "state": "1",
              "created_date": "2017-11-01 15:25:29"
          },
          {
              "withdraw_id": "3457",
              "user_id": "540744612",
              "bank_name": "TMCP Á châu - ACB",
              "bank_branch": "q3",
              "bank_account_name": "Khanh Dinh",
              "bank_account_number": "123456789",
              "money": "1000000",
              "fee": "10000",
              "total_money": "950000",
              "state": "1",
              "created_date": "2017-07-17 10:06:42"
          }
      ]
  }
}

function* doAction(action) {
  try {
    const { accessToken } = action.payload
    const passData = action.payload.data
    // const { code, last_page, data, message } = yield call(docdacAPI.getMoneyHistory, passData, accessToken)
    const { code, last_page, data, message } = mock_data

    switch (code) {
      case responseCode.GET_MONEY_HISTORY_SUCCESS: {
        yield put({ type: types.MONEY_HISTORY_SUCCESS, payload: { last_page, data, nextPage: passData.page + 1 } })
        break
      }
      case responseCode.GET_MONEY_HISTORY_EMPTY: {
        yield put({ type: types.MONEY_HISTORY_EMPTY, payload: { last_page, data, nextPage: passData.page + 1 } })
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, message, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.MONEY_HISTORY, doAction)
}
