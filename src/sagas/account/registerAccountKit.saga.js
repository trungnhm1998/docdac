import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { accountAPI } from '../../api/'

function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { phone, countryCode, password, token, navigator } = action.payload
    const { code, data, desc, message } = yield call(accountAPI.registerAccountKit, { account: phone, country_code: countryCode, password, token })
    yield put({ type: apiTypes.HIDE_LOADING })

    switch (code) {
      case responseCode.REGISTER_SUCCESS: {
        yield put({
          type: types.REGISTER_USER_SUCCESS,
          payload: {
            accessToken: data.access_token,
            accountId: data.account_id,
            isProfileUpdated: true
          }
        })
        navigator.nextScreen()
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: types.REGISTER_USER_FAIL, code, message, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.REGISTER_USER_ACCOUNT_KIT, doAction)
}
