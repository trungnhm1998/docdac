import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { accountAPI } from '../../api/'

function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { phone, countryCode, token, password, rePassword, navigator } = action.payload
    const { code, data, desc, message } = yield call(accountAPI.forgetPasswordAccountKit, { account: phone, country_code: countryCode, token, password, password_retype: rePassword })
    switch (code) {
      case responseCode.CHANGE_PASSWORD_SUCCESS: {
        yield put({ type: apiTypes.HIDE_LOADING })
        navigator.loginScreen()
        break
      }
      default: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, code, message, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.FORGOT_PASS_ACCOUNT_KIT, doAction)
}
