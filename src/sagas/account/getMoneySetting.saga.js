import { takeLatest, call, put, actionChannel } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { docdacAPI } from '../../api'

function* doAction(action) {
  try {
    console.log('saga called')
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, msg, data } = yield call(docdacAPI.getMoneySetting)
    switch (code) {
      case responseCode.GET_MONEY_SETTING_SUCCESS: {
        // todo update reducer here
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put({ type: types.GET_MONEY_SETTING_SUCCESS, payload: { data }})
        break
      }
      default: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, msg, isDefault: true }))
        break
      }
    }
  } catch (error) {
    console.log(error)
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_MONEY_SETTING, doAction)
}