import { delay } from 'redux-saga'
import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { docdacAPI } from '../../api/'

function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, msg } = yield call(docdacAPI.updateUserInfo, { avatar_url: action.payload.avatarUrl, id_number: action.payload.idNumber, ...action.payload }, action.payload.accessToken)

    switch (code) {
      case responseCode.REQUEST_SUCCESS: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put({
          type: types.UPDATE_USERINFO_SUCCESS,
          payload: { ...action.payload }
        })
        yield put(apiActions.storeResponseMessage({ type: types.UPDATE_USERINFO_SUCCESS, message: msg }))
        yield call(delay, 500)
        yield put({ type: apiTypes.CLEAR_API_RESPONSE })
        break
      }

      default: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, code: -1, message: msg, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.UPDATE_USERINFO, doAction)
}
