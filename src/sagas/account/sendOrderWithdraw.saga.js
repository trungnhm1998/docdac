import { takeLatest, call, put, actionChannel } from 'redux-saga/effects'
import { Alert } from 'react-native'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { docdacAPI } from '../../api'

function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { accessToken } = action.payload
    const { userInfo, bankAccountNumber, bankName, bankBranch, money } = action.payload.data
    const { code, msg, data } = yield call(docdacAPI.sendOrderWithdraw, {
        user_info: userInfo,
        bank_account: bankAccountNumber,
        bank_name: bankName,
        bank_branch: bankBranch,
        money
      },
      accessToken
    )
    switch (code) {
      case responseCode.SEND_ORDER_WITHDRAW_BANKING: {
        yield put({ type: types.SEND_ORDER_WITHDRAW_BANKING_SUCCESS, payload: {}})
        break
      }
      default: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, msg, isDefault: true }))
        yield Alert.alert('Thông báo', msg)
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.SEND_ORDER_WITHDRAW_BANKING, doAction)
}