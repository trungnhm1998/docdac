import { delay } from 'redux-saga'
import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { accountAPI } from '../../api/'

function* doAction(action) {
  try {

    yield put({ type: apiTypes.SHOW_LOADING })
    const { phone, countryCode, useAccountKit, navigator } = action.payload
    const { code, data, desc, message } = yield call(accountAPI.verifyPhone, { account: phone, country_code: countryCode })

    switch (code) {
      case responseCode.VERIFY_PHONE_SUCCESS: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put({ type: types.VERIFY_PHONE_SUCCESS, payload: { phone, countryCode } })
        navigator.loginScreen()
        break
      }
      case responseCode.INVALID_PHONE: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, code, message, isDefault: true }))
        break
      }
      case responseCode.VERIFY_PHONE_FAIL: {
        yield put({ type: apiTypes.HIDE_LOADING })
        if(useAccountKit) {
          navigator.sendActiveCodeScreen()
        } else {
          yield put({ type: types.SEND_OTP_REGISTER, payload: { phone, navigator } })
        }

        yield put({ type: apiTypes.HIDE_LOADING })
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, code, message, isDefault: true }))
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.VERIFY_PHONE, doAction)
}
