import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { docdacAPI } from '../../api/'

function* doAction(action) {
  try {
    const { accessToken } = action.payload
    const passData = action.payload.data
    const { code, last_page, data, message } = yield call(docdacAPI.getRechargeHistory, passData, accessToken)
    switch (code) {
      case responseCode.GET_RECHARGE_HISTORY_SUCCESS: {
        yield put({ type: types.RECHARGE_HISTORY_SUCCESS, payload: { last_page, data, nextPage: passData.page + 1 } })
        break
      }
      case responseCode.GET_RECHARGE_HISTORY_EMPTY: {
        yield put({ type: types.RECHARGE_HISTORY_EMPTY, payload: { last_page, data, nextPage: passData.page + 1 } })
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, message, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.RECHARGE_HISTORY, doAction)
}
