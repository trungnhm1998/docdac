import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { accountAPI } from '../../api/'

function* doAction(action) {
  try {
    const { phone, navigator } = action.payload
    const { code, data, desc, message } = yield call(accountAPI.sendOTP, { account: phone })

    switch (code) {
      case responseCode.SEND_OTP_SUCCESS: {
        yield put({ type: apiTypes.HIDE_LOADING })
        navigator.registerScreen(phone)
        break
      }
      default: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put(apiActions.storeResponseMessage({ type: types.SEND_OTP_REGISTER_FAIL, code, message, isDefault: true }))
        return null
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.SEND_OTP_REGISTER, doAction)
}
