import { Platform } from 'react-native'
import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import * as responseCodeDocdac from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { accountAPI, docdacAPI } from '../../api/'

function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { phone, countryCode, navigator, password } = action.payload
    const { code, data, desc, message } = yield call(accountAPI.authorize, { account: phone, country_code: countryCode, password })
    switch (code) {
      case responseCode.AUTHORIZE_SUCCESS: {
        yield put({
          type: types.AUTHORIZE_USER_SUCCESS,
          payload: {
            accessToken: data.access_token,
            accountId: data.account_id,
            isProfileUpdated: data.profile_update
          }
        })

        const resultAccountInfo = yield call(docdacAPI.getUserInfo, { os: Platform.OS, env: 1, device_token: '' }, data.access_token)
        yield put({ type: apiTypes.HIDE_LOADING })
        if (resultAccountInfo.code === responseCodeDocdac.REQUEST_SUCCESS) {
          yield put({
            type: types.GET_USERINFO_DOCDAC_SUCCESS,
            payload: { ...resultAccountInfo.data }
          })
          navigator.nextScreen()
        } else {
          yield put(apiActions.storeResponseMessage({ type: types.AUTHORIZE_USER_FAIL, code, message, isDefault: true }))
        }
        break
      }
      case responseCode.AUTHORIZE_FAIL: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put(apiActions.storeResponseMessage({ type: types.AUTHORIZE_USER_FAIL, code, message, isDefault: true }))
        break
      }
      default: {
        return null
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.AUTHORIZE_USER, doAction)
}
