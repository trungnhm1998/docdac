const Fonts = {
  R: 'SFUIText-Regular',
  L: 'SFUIText-Light',
  M: 'SFUIText-Medium',
  I: 'SFUIText-Italic',
  B: 'SFUIText-Semibold',
}
export {
  Fonts
}
