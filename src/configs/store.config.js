import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { autoRehydrate } from 'redux-persist'
import { composeWithDevTools } from 'remote-redux-devtools'
import rootReducers from './reducers.config'
import rootSaga from '../sagas'

const enhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
console.log(enhancer);
const configureStore = (initialState) => {
  const sagaMiddleware = createSagaMiddleware()
  if (__DEV__) {
    const store = createStore(
      rootReducers,
      initialState,
      enhancer(
      //composeWithDevTools(
        applyMiddleware(sagaMiddleware),
        autoRehydrate(),
      )
    )
    sagaMiddleware.run(rootSaga)
    return store
  }

  const store = createStore(
    rootReducers,
    initialState,
    compose(
      applyMiddleware(sagaMiddleware),
      autoRehydrate(),
    )
  )
  sagaMiddleware.run(rootSaga)
  return store
}

const store = configureStore()
export default store