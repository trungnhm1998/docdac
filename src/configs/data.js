import { Themes } from '../ui'

let data
data = {
  ListPayment: [
    {
      title: 'MOMO',
      images: Themes.Images.momoIcon,
      description: 'Dịch vụ thanh toán trên di động'
    },
    {
      title: 'MOPAY',
      images: Themes.Images.mopayIcon,
      description: 'Dịch vụ thanh toán qua ngân hàng'
    },
    {
      title: 'INTERNET BANKING',
      images: Themes.Images.netBankingIcon,
      description: 'Các dịch vụ thanh toán qua ngân hàng'
    },
    {
      title: 'TOPPAY',
      images: Themes.Images.topPayIcon,
      description: 'Tiện ích tiêu dùng, tài chính cá nhân'
    },
    {
      title: 'THẺ CÀO',
      images: Themes.Images.cardIcon,
      description: 'Các nhà mạng di động'
    }
  ],
  HistoryDeposit: [
    {
      title: 'VÍ MOMO',
      time: '12:00 (05-12-2016)',
      cash: '+ 500.000 đ',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Thẻ Gate',
      time: '12:00 (05-12-2016)',
      cash: '+ 500.000 đ',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Thẻ Gate',
      time: '12:00 (05-12-2016)',
      cash: '+ 500.000 đ',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Thẻ Gate',
      time: '12:00 (05-12-2016)',
      cash: '+ 500.000 đ',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Thẻ Gate',
      time: '12:00 (05-12-2016)',
      cash: '+ 500.000 đ',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Thẻ Gate',
      time: '12:00 (05-12-2016)',
      cash: '+ 500.000 đ',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Thẻ Gate',
      time: '12:00 (05-12-2016)',
      cash: '+ 500.000 đ',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Thẻ Gate',
      time: '12:00 (05-12-2016)',
      cash: '+ 500.000 đ',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Thẻ Gate',
      time: '12:00 (05-12-2016)',
      cash: '+ 500.000 đ',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Thẻ Gate',
      time: '12:00 (05-12-2016)',
      cash: '+ 500.000 đ',
      images: './images/mobayIcon.png'
    }
  ],
  HistoryWithdraw: [
    {
      title: 'Rút - 200.000 đ',
      tk: 'TK nhận 093 777198',
      status: 0,
      time: '12:00 (05-12-2016)',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Rút - 200.000 đ',
      tk: 'TK nhận 093 777198',
      status: 0,
      time: '12:00 (05-12-2016)',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Rút - 200.000 đ',
      tk: 'TK nhận 093 777198',
      status: 'Hoàn thành',
      time: '12:00 (05-12-2016)',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Rút - 200.000 đ',
      tk: 'TK nhận 093 777198',
      status: 0,
      time: '12:00 (05-12-2016)',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Rút - 200.000 đ',
      tk: 'TK nhận 093 777198',
      status: 0,
      time: '12:00 (05-12-2016)',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Rút - 200.000 đ',
      tk: 'TK nhận 093 777198',
      status: 0,
      time: '12:00 (05-12-2016)',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Rút - 200.000 đ',
      tk: 'TK nhận 093 777198',
      status: 1,
      time: '12:00 (05-12-2016)',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Rút - 200.000 đ',
      tk: 'TK nhận 093 777198',
      status: 1,
      time: '12:00 (05-12-2016)',
      images: './images/mobayIcon.png'
    },
    {
      title: 'Rút - 200.000 đ',
      tk: 'TK nhận 093 777198',
      status: 1,
      time: '12:00 (05-12-2016)',
      images: './images/mobayIcon.png'
    }
  ],
  lotteryMega: [
    {
      id: 1,
      date: 'Thứ Tư - 7/11/2016',
      result: [1, 12, 25, 23, 12, 22]
    },
    {
      id: 2,
      date: 'Thứ Sáu - 7/11/2016',
      result: [1, 22, 25, 23, 12, 22]
    },
    {
      id: 3,
      date: 'Thứ Tư - 7/11/2016',
      result: [1, 12, 25, 23, 12, 22]
    },
    {
      id: 4,
      date: 'Thứ Sáu - 7/11/2016',
      result: [1, 22, 25, 23, 12, 22]
    },
    {
      id: 5,
      date: 'Thứ Tư - 7/11/2016',
      result: [1, 12, 25, 23, 12, 22]
    },
    {
      id: 6,
      date: 'Thứ Sáu - 7/11/2016',
      result: [1, 22, 25, 23, 12, 22]
    },
    {
      id: 7,
      date: 'Thứ Tư - 7/11/2016',
      result: [1, 12, 25, 23, 12, 22]
    },
    {
      id: 8,
      date: 'Thứ Sáu - 7/11/2016',
      result: [1, 22, 25, 23, 12, 22]
    },
    {
      id: 9,
      date: 'Thứ Tư - 7/11/2016',
      result: [1, 12, 25, 23, 12, 22]
    },
    {
      id: 10,
      date: 'Thứ Sáu - 7/11/2016',
      result: [1, 22, 25, 23, 12, 22]
    }
  ],
  lotteryMax4d: [
    {
      id: 1,
      date: 'Thứ Tư - 7/11/2016',
      winNumber: [
        [[25, 28, 33, 40]],
        [[9, 19, 49, 68], [12, 13, 14, 15]],
        [[16, 17, 18, 19], [20, 21, 22, 23], [24, 25, 26, 27]],
        [[28, 29, 30, 31], [32, 33, 34, 35]]
      ]
    },
    {
      id: 2,
      date: 'Thứ Năm - 8/12/2017',
      winNumber: [
        [[1, 2, 3, 4]],
        [[35, 36, 37, 38], [39, 40, 41, 42]],
        [[43, 44, 45, 46], [47, 48, 49, 50], [51, 52, 53, 54]],
        [[55, 56, 57, 58], [59, 60, 61, 62]]
      ]
    },
    {
      id: 3,
      date: 'Thứ Năm - 8/12/2017',
      winNumber: [
        [[1, 2, 3, 4]],
        [[35, 36, 37, 38], [39, 40, 41, 42]],
        [[43, 44, 45, 46], [47, 48, 49, 50], [51, 52, 53, 54]],
        [[55, 56, 57, 58], [59, 60, 61, 62]]
      ]
    }
  ],
  ListMyLottery: [
    {
      type: 1,
      ticket_id: '344',
      created_date: '10:39 14-11-2016',
      money: 200000,
      trans_id: 391,
      ticket_type: 2,
      state_win: 0,
      drawing_date: '18/11/2016',
      period: '00053/16',
      delivery_type: '0',
      delivery_state: '0',
      state: '0',
      enable: '1',
      board_total: 2,
      bao: 1,
      is_scheduled: 0,
      have_result: '0'
    },
    {
      type: 1,
      ticket_id: '344',
      created_date: '10:39 14-11-2016',
      money: 20000,
      trans_id: 391,
      ticket_type: 2,
      state_win: 0,
      drawing_date: '18/11/2016',
      period: '00053/16',
      delivery_type: '2',
      delivery_state: '1',
      state: '0',
      enable: '1',
      board_total: 2,
      bao: 1,
      is_scheduled: 0,
      have_result: '0'
    },
    {
      type: 1,
      ticket_id: '344',
      created_date: '10:39 14-11-2016',
      money: 100000,
      trans_id: 391,
      ticket_type: 2,
      state_win: 0,
      drawing_date: '18/11/2016',
      period: '00053/16',
      delivery_type: '1',
      delivery_state: '2',
      state: '0',
      enable: '1',
      board_total: 2,
      bao: 1,
      is_scheduled: 0,
      have_result: '0'
    },
    {
      type: 0,
      ticket_id: '344',
      created_date: '10:39 14-11-2016',
      money: 200000,
      trans_id: 391,
      ticket_type: 2,
      state_win: 0,
      drawing_date: '18/11/2016',
      period: '00053/16',
      delivery_type: '0',
      delivery_state: '0',
      state: '0',
      enable: '1',
      board_total: 2,
      bao: 1,
      is_scheduled: 0,
      have_result: '0'
    },
    {
      type: 1,
      ticket_id: '344',
      created_date: '10:39 14-11-2016',
      money: 20000,
      trans_id: 391,
      ticket_type: 2,
      state_win: 0,
      drawing_date: '18/11/2016',
      period: '00053/16',
      delivery_type: '2',
      delivery_state: '1',
      state: '0',
      enable: '1',
      board_total: 2,
      bao: 1,
      is_scheduled: 0,
      have_result: '0'
    },
    {
      type: 0,
      ticket_id: '344',
      created_date: '10:39 14-11-2016',
      money: 100000,
      trans_id: 391,
      ticket_type: 2,
      state_win: 0,
      drawing_date: '18/11/2016',
      period: '00053/16',
      delivery_type: '1',
      delivery_state: '2',
      state: '0',
      enable: '1',
      board_total: 2,
      bao: 1,
      is_scheduled: 0,
      have_result: '0'
    },
    {
      type: 0,
      ticket_id: '344',
      created_date: '10:39 14-11-2016',
      money: 200000,
      trans_id: 391,
      ticket_type: 2,
      state_win: 0,
      drawing_date: '18/11/2016',
      period: '00053/16',
      delivery_type: '0',
      delivery_state: '0',
      state: '0',
      enable: '1',
      board_total: 2,
      bao: 1,
      is_scheduled: 0,
      have_result: '0'
    },
    {
      type: 1,
      ticket_id: '344',
      created_date: '10:39 14-11-2016',
      money: 20000,
      trans_id: 391,
      ticket_type: 2,
      state_win: 0,
      drawing_date: '18/11/2016',
      period: '00053/16',
      delivery_type: '2',
      delivery_state: '1',
      state: '0',
      enable: '1',
      board_total: 2,
      bao: 1,
      is_scheduled: 0,
      have_result: '0'
    },
    {
      type: 0,
      ticket_id: '344',
      created_date: '10:39 14-11-2016',
      money: 100000,
      trans_id: 391,
      ticket_type: 2,
      state_win: 0,
      drawing_date: '18/11/2016',
      period: '00053/16',
      delivery_type: '1',
      delivery_state: '2',
      state: '0',
      enable: '1',
      board_total: 2,
      bao: 1,
      is_scheduled: 0,
      have_result: '0'
    }

  ]

}


// -  “state_win”:  0: vé không trúng thưởng, 1: vé trúng thưởng.
// - “ticket_id”: dùng khi cần xem chi tiết vé ở API bên dưới.
// - ”delivery_type”: 0: vé điện tử, 1: vé cứng (tự đến đại lý lấy), 2: vé cứng (giao tận nơi).
// - Trạng thái đi kèm khi delivery_type:  1, 2:
// + “delivery_state”: 0: chưa nhận vé, 1: đã nhận vé, -1: VPT giữ vé hộ.
// - “state”:  0/1 trạng thái vé đang in / đã in.
// - “enable”:  0/1 trạng thái vé bị hủy/ tồn tại.
// - “is_scheduled”: 0/1, trạng thái đã lập lịch nhận thưởng hay chưa với vé trúng giải.


export default data
