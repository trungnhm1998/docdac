import { combineReducers } from 'redux'
import { deviceReducer, apiReducer, accountReducer, paymentReducer, lotteryReducer, initReducer } from '../redux'

const rootReducer = combineReducers({
  deviceInfo: deviceReducer,
  apiResponse: apiReducer,
  lottery: lotteryReducer,
  accountInfo: accountReducer,
  paymentInfo: paymentReducer,
  initConfig: initReducer
})
export default rootReducer
