const PaymentGatewayMomo = {
  PUBLIC_KEY: 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAip7E4c5ylxCsH+g/GMco4/6/nhc1rqZGhGzk0E3cT11OQUeQ9F1rRGML3mk9FAirzzwjsrndxdJaOZTj8xFsxPmBf8j46oac7PgbOL9i/veDDDjUIFITPa+BRzL6DIUXG0PasiU6CLgpCKPUHZLIi7ZedAruWUideIbRoaBuzpaqDuuMqFeLiIapJj6TaAwllS1EwcZyvAD+jRBDsiOnOMRz/KvJZxZ957TtaUVn+UzBDhrYsIMpt8rcFxIFkZBgpgfWbvVaP3nLyWuUrrJMSdNsslnUlOlmfCz2JtIAq3711hfXxYhkY5FjdJwTK5HJEVrUqtC3CPUJG1STNlo1IwIDAQAB',
  BUNDLE_ID: 'vn.vuiphattai.com',
  MERCHANT_LABEL: 'MeCorp',
  MERCHANT_CODE: 'MECORP20160712',
  MERCHANT_ID: '41129898989'
}

const PaymentGatewayZalo = {
  APP_ID: "42"
}


export {
  PaymentGatewayMomo,
  PaymentGatewayZalo
}
