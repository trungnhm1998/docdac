import { Themes } from '../ui'

const ZaloPayMessageError = {
  getMessage: (code) => {
    let message = ''
    switch (code) {
      case "1":
        message = 'Nạp tiền qua ZaloPay thành công.'
        break
      case "-1" :
        message = 'Ứng dụng Zalopay chưa được cài đặt trên thiết bị.'
        break
      case "-2" :
        message = 'Không nhận được phản hồi từ Zalopay.'
        break
      case "-3" :
        message = 'Giao dịch không hơp lệ. Vui lòng thử lại sau.'
        break
      case "-4" :
        message = 'Giao dich ZaloPay đã được huỷ.'
        break
      default:
        message = 'Hệ thống đang có lỗi, vui lòng quay lại sau.'
    }
    return message
  }
}

export {
  ZaloPayMessageError
}
