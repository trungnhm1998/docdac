import { Themes } from '../ui'

const Config = {
  screen: {
    sendOTPRegister: {
      screen: 'sendOTPRegister'
    },
    home: {
      screen: 'Home'
    },
    login: {
      screen: 'Login',
      title: 'Đăng nhập',
      backButtonTitle: ''
    },
    welcome: {
      screen: 'Welcome',
      title: 'Đăng nhập',
      backButtonTitle: ''
    },
    profile: {
      screen: 'Profile',
      title: 'Hồ sơ cá nhân',
      backButtonTitle: ''
    },
    forgotPassword: {
      screen: 'ForgotPassword',
      title: 'Quên mật khẩu',
      backButtonTitle: ''
    },
    forgotPasswordAccountKit: {
      screen: 'ForgotPasswordAccountKit',
      title: 'Quên mật khẩu',
      backButtonTitle: ''
    },
    register: {
      screen: 'Register',
      title: 'Kích hoạt',
      backButtonTitle: ''
    },
    registerAccountKit: {
      screen: 'RegisterAccountKit',
      title: 'Kích hoạt',
      backButtonTitle: ''
    },
    megaPickup: {
      screen: 'PickupTicket',
      title: 'Chọn cách chơi',
      navigatorStyle: {
        screenBackgroundColor: '#fff'
      }
    },
    megaPowerPickup: {
      screen: 'PickupTicketMegaPower',
      title: 'Chọn cách chơi',
      navigatorStyle: {
        screenBackgroundColor: '#fff'
      }
    },
    megaPickupMax: {
      screen: 'PickupTicketMax',
      title: 'Chọn cách chơi',
      navigatorStyle: {
        screenBackgroundColor: '#fff'
      }
    },
    depositGatewayInput: {
      screen: 'DepositGatewayInput',
      title: 'Nạp tiền tài khoản',
      backButtonTitle: ''
    },
    depositGatewayNapasInput: {
      screen: 'DepositGatewayNapasInput',
      title: 'Nạp tiền qua Napas',
      backButtonTitle: ''
    },
    depositGatewayCard: {
      screen: 'DepositGatewayCard',
      title: 'Nạp tiền tài khoản',
      backButtonTitle: ''
    },
    depositGatewaySuccess: {
      screen: 'DepositGatewaySuccess',
      title: 'Giao dịch thành công',
      backButtonTitle: ''
    },
    depositGatewayFail: {
      screen: 'DepositGatewayFail',
      title: 'Giao dịch thất bại',
      backButtonTitle: ''
    },
    depositGatewayCheckout: {
      screen: 'DepositGatewayCheckout',
      title: 'Thanh toán',
      backButtonTitle: ''
    },
    depositGatewayWebmoneyCheckout: {
      screen: 'DepositGatewayWebmoneyCheckout',
      title: 'Thanh toán',
      backButtonTitle: ''
    },
    paymentHome: {
      screen: 'PaymentHome',
      title: 'Tài khoản',
      backButtonTitle: ''
    },
    paymentHistory: {
      screen: 'PaymentHistory',
      title: 'Lịch sử giao dịch',
      backButtonTitle: ''
    },
    selectContacts: {
      screen: 'SelectContacts',
      title: 'Chọn SĐT',
      backButtonTitle: ''
    },
    settingHome: {
      screen: 'SettingHome',
      title: 'Cài đặt',
      backButtonTitle: ''
    },
    settingHomeIntro: {
      screen: 'SettingHomeIntro',
      title: 'Cài đặt',
      backButtonTitle: ''
    },
    settingInfo: {
      screen: 'SettingInfo',
      backButtonTitle: ''
    },
    lottery645Delivery: {
      screen: 'Lottery645Delivery',
      title: 'Chi tiết vé',
      backButtonTitle: ''
    },
    lottery645Pick: {
      screen: 'Lottery645Pick',
      title: 'Chi tiết vé',
      backButtonTitle: ''
    },
    lottery645Stored: {
      screen: 'Lottery645Stored',
      title: 'Chi tiết vé',
      backButtonTitle: ''
    },
    lottery645WinForm: {
      screen: 'Lottery645WinForm',
      title: 'Đăng ký nhận thưởng',
      backButtonTitle: ''
    },
    lottery655WinForm: {
      screen: 'Lottery655WinForm',
      title: 'Đăng ký nhận thưởng',
      backButtonTitle: ''
    },
    lottery655Delivery: {
      screen: 'Lottery655Delivery',
      title: 'Chi tiết vé',
      backButtonTitle: ''
    },
    lottery655Pick: {
      screen: 'Lottery655Pick',
      title: 'Chi tiết vé',
      backButtonTitle: ''
    },
    lottery655Stored: {
      screen: 'Lottery655Stored',
      title: 'Chi tiết vé',
      backButtonTitle: ''
    },
    lotteryMax4dDelivery: {
      screen: 'LotteryMax4dDelivery',
      title: 'Chi tiết vé',
      backButtonTitle: ''
    },
    lotteryMax4dPick: {
      screen: 'LotteryMax4dPick',
      title: 'Chi tiết vé',
      backButtonTitle: ''
    },
    lotteryMax4dStored: {
      screen: 'LotteryMax4dStored',
      title: 'Chi tiết vé',
      backButtonTitle: ''
    },
    depositBankingInput: {
      screen: 'DepositBankingInput',
      title: 'Chuyển khoản ngân hàng',
      backButtonTitle: ''
    },
    withdrawGateway: {
      screen: 'WithdrawGateway',
      title: 'Rút tiền về tài khoản',
      backButtonTitle: ''
    },
    withdrawGatewayConfirm: {
      screen: 'WithdrawGatewayConfirm',
      title: 'Xác nhận rút tiền',
      backButtonTitle: ''
    },
    lotteryResult: {
      screen: 'LotteryResult',
      title: 'Kết quả xổ số',
      backButtonTitle: ''
    },
    detailMax4d: {
      screen: 'DetailMax4d',
      title: 'Chi tiết thanh toán',
      backButtonTitle: ''
    },
    confirmMax4d: {
      screen: 'ConfirmMax4d',
      title: 'Xác nhận mua vé',
      backButtonTitle: ''
    },
    confirmMega: {
      screen: 'ConfirmMega',
      title: 'Xác nhận mua vé',
      backButtonTitle: ''
    },
    confirmMegaPower: {
      screen: 'ConfirmMegaPower',
      title: 'Xác nhận mua vé',
      backButtonTitle: ''
    },
    detailMega: {
      screen: 'DetailMega',
      title: 'Chi tiết thanh toán',
      backButtonTitle: ''
    },
    detailLottery645Delivery: {
      screen: 'Lottery645Delivery',
      title: 'Chi tiết vé',
      backButtonTitle: ''
    },
    detailLottery645Pick: {
      screen: 'Lottery645Pick',
      title: 'Chi tiết vé',
      backButtonTitle: ''
    },
    detailLottery645Stored: {
      screen: 'Lottery645Stored',
      title: 'Chi tiết vé',
      backButtonTitle: ''
    },
  },
  navigatorStyle: {
    DEFAULT_NAV_STYLE: {
      navBarBackgroundColor: Themes.Colors.navColor,
      navBarNoBorder: true,
      statusBarTextColorSchemeSingleScreen: 'light',
      statusBarTextColorScheme: 'light',
      navBarButtonColor: '#fff',
      navBarTextColor: '#fff'
    },
    DEFAULT_NAV_STYLE_HIDDEN_TAB: {
      navBarBackgroundColor: Themes.Colors.navColor,
      navBarNoBorder: true,
      statusBarTextColorSchemeSingleScreen: 'light',
      statusBarTextColorScheme: 'light',
      navBarButtonColor: '#fff',
      navBarTextColor: '#fff',
      tabBarHidden: true,
      statusBarColor: Themes.Colors.navColor
    },
    DEFAULT_NAV_STYLE_CUSTOM: {
      statusBarTextColorSchemeSingleScreen: 'light',
      statusBarTextColorScheme: 'light',
      navBarHidden: true,
      navBarTranslucent: true,
      tabBarHidden: true
    }
  }
}
export default Config
