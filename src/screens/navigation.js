import { Navigation as navigator } from 'react-native-navigation'
import { Platform } from 'react-native'
import store from '../configs/store.config'
import { VERSION } from '../configs/version.config'
import { Icon, Themes } from '../ui'


const Navigation = {
  navToRoot: () => {
    navigator.startSingleScreenApp({
      appStyle: {
        orientation: 'portrait' // Available values: portrait, landscape, auto
      },
      screen: {
        screen: 'Root',
        navigatorStyle: {
          navBarHidden: true
        }
      },
      animationType: 'none'
    })
  },
  navToLogin: () => {
    navigator.startSingleScreenApp({
      screen: {
        screen: 'SendOTPRegister',
        navigatorStyle: {
          navBarHidden: true
        }
      },
      animationType: 'none',
      appStyle: {
        orientation: 'portrait' // Available values: portrait, landscape, auto
      }
    })
  },
  navToWelcome: () => {
    navigator.startSingleScreenApp({
      screen: {
        screen: 'Welcome',
        title: 'Đăng nhập'
      },
      animationType: 'none',
      appStyle: {
        orientation: 'portrait' // Available values: portrait, landscape, auto
      }
    })
  },
  navToHome: () => {
    const { accountInfo } = store.getState()
    Promise.all([
      Icon.getImageSource('nav-cup', 20, 'red'),
      Icon.getImageSource('nav-club', 20, 'red'),
      Icon.getImageSource('nav-ball', 20, 'red'),
      Icon.getImageSource('nav-note', 18, 'red'),
      Icon.getImageSource('nav-user', 20, 'red')
    ]).then((values) => {
      const tabs = [{
        label: 'Trò chơi',
        screen: 'Home',
        icon: values[0]
      }, {
        label: 'KQXS',
        screen: 'LotteryResult',
        icon: values[2],
        title: 'Kết quả xổ số'
      }, {
        label: 'Lịch sử',
        screen: 'LotteryHome',
        icon: values[3],
        title: 'Vé của bạn'
      }]
      if (accountInfo.production_version !== VERSION || Platform.OS === 'android') {
        tabs.push({
          label: 'Tài khoản',
          screen: 'PaymentHome',
          icon: values[4],
          title: 'Tài khoản'
        })
      }
      navigator.startTabBasedApp({
        tabs,
        tabsStyle: {
          tabBarButtonColor: '#bdbdbd',
          tabBarSelectedButtonColor: Themes.Colors.darkRed
        },
        appStyle: {
          tabBarButtonColor: '#bdbdbd',
          tabBarSelectedButtonColor: Themes.Colors.darkRed,
          forceTitlesDisplay: true,
          orientation: 'portrait' // Available values: portrait, landscape, auto
        },
        animationType: 'none'
      })
    })
  },
  showLoading: () => {
    navigator.showLightBox({
      screen: 'Loading',
      animationType: 'none',
      style: {
        backgroundBlur: 'dark',
        backgroundColor: Platform.OS === 'ios' ? 'rgba(255, 255, 255, 0)' : 'rgba(0, 0, 0, 0.4)'
      } 
    })
  },
  hideLoading: () => {
    navigator.dismissLightBox()
  },
  showMessage: (message, buttons, options) => {
    setTimeout(() => {
      navigator.showLightBox({
        screen: 'LightBox',
        animationType: 'none',
        style: {
          backgroundBlur: 'dark',
          backgroundColor: 'rgba(201, 32, 51, 0)'
        },
        passProps: {
          message,
          buttons,
          options
        }
      })
    }, 400)
  },

  hideMessage: () => {
    navigator.dismissLightBox()
  }
}

export {
  Navigation
}
