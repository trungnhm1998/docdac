import React from 'react'
import { View, Platform, StyleSheet, Image, TouchableOpacity } from 'react-native'
import numeral from 'numeral'
import { TextFont, Icon } from '../../ui'
import { Images } from '../../ui/themes'
import Fonts from '../../ui/themes/fonts'

const styles = StyleSheet.create({
  mainNavbar: {
    height: Platform.OS === 'ios' ? 64 : 56,
    width: '100%',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 5
  },
  jackpotNumberContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 10
  },
  jackpotNumber: {
    color: '#fff',
    fontSize: 20,
    fontFamily: Fonts.type.UTMHel
  },
  rightButton: {
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  logoContainer: {
    justifyContent: 'center',
    paddingLeft: 5,
    paddingRight: 10
  }

})
const PickupTicketNav = (props) => {
  const { jackpotNumber, onRightButton, backgroundColor, imageSource } = props
  const showNumber = jackpotNumber === 0 ? '' : jackpotNumber

  return (
    <View class="navbarName" style={[styles.mainNavbar, { backgroundColor }]}>
      <View>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity
            onPress={() => onRightButton()}
            style={styles.rightButton}
          >
            <Icon name="back" size={18} color="#fff" />
          </TouchableOpacity>
          <View style={styles.logoContainer}>
            <Image source={imageSource} style={{ width: 70, height: 37, resizeMode: 'contain' }} />
          </View>
        </View>

      </View>
      <View style={styles.jackpotNumberContainer}>
        <TextFont style={styles.jackpotNumber}>{jackpotNumber === 0 ? null : `${numeral(showNumber).format('0,0')} đ`} </TextFont>
      </View>
    </View>
  )
}

PickupTicketNav.propTypes = {
  onRightButton: React.PropTypes.func,
  jackpotNumber: React.PropTypes.number,
  backgroundColor: React.PropTypes.string,
  imageSource: React.PropTypes.any
}

PickupTicketNav.defaultProps = {
  jackpotNumber: 0,
  backgroundColor: '#c92033',
  onRightButton: () => {
    alert('back screen')
  },
  imageSource: Images.logo645
}

export default PickupTicketNav
