import React from 'react'
import { View, Text, Platform, StyleSheet, Image } from 'react-native'
import { Colors } from '../../ui/themes'

const styles = StyleSheet.create({
  navbarContainer: {
    width: '100%',
    paddingTop: Platform.OS === 'ios' ? 0 : 0,
    ...Platform.select({
      ios: {
        height: 64
      },
      android: {
        height: 56
      }
    }),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.navColor
  },
  logoStyle: {
    width: 100,
    height: 30,
    marginTop: 10,
    resizeMode: 'contain'
  }
})

const LogoNav = (props) => {
  return (
    <View style={styles.navbarContainer}>
      <Image style={styles.logoStyle} source={props.imageSouce} />
    </View>
  )
}

LogoNav.propTypes = {
  style: Text.propTypes.style,
  font: React.PropTypes.string,
  size: React.PropTypes.number
}

LogoNav.defaultProps = {
  style: {},
  color: '#000',
  font: 'SFUIText-Regular',
  size: 15
}

export default LogoNav
