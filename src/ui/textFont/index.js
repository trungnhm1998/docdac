import React, { PropTypes } from 'react'
import { Dimensions, Text, StyleSheet } from 'react-native'
import Fonts from '../../ui/themes/fonts'
const { width, height } = Dimensions.get('window')
const flattenStyle = StyleSheet.flatten
const realWidth = height > width ? width : height

const TextFont = ({ style, children, color, font, size, ...props }) => {
  const fontSize = size
  const scaledFontSize = Math.round(fontSize * realWidth / 375)

  return (
    <Text {...props} style={[{ backgroundColor: 'transparent', color: color, fontFamily: Fonts.type[font], fontSize: scaledFontSize }, style]} >
      {children}
    </Text>
  )
}

TextFont.propTypes = {
  style: Text.propTypes.style,
  font: React.PropTypes.string,
  size: React.PropTypes.number,
  color: React.PropTypes.string
}

TextFont.defaultProps = {
  style: {},
  color: '#000',
  font: 'Regular',
  size: 16
}

export default TextFont
