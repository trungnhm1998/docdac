import React, { Component, PropTypes } from 'react'
import { TouchableOpacity, View, Platform, TouchableNativeFeedback, ViewPropTypes } from 'react-native'
import _ from 'lodash'
import { TextFont, Themes } from '../../ui'


class Button extends Component {
  constructor(props) {
    super(props)
    const { onPress } = props
    if(onPress) {
      this.onPress = Platform.OS === 'ios' ? props.onPress : _.debounce(props.onPress, 100, this)
    }
  }

  render() {
    const { style, textStyle, btnColor, children } = this.props


    if (Platform.OS === 'android') {
      return (
        <View >
          <TouchableNativeFeedback
            background={TouchableNativeFeedback.SelectableBackground()}
            onPress={this.onPress}
          >
            <View style={[{ padding: 15, backgroundColor: btnColor, borderRadius: Themes.Metrics.buttonRadius }, style]}>
              <TextFont style={textStyle}>{children}</TextFont>
            </View>
          </TouchableNativeFeedback>
        </View>
      )
    }
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={[{ padding: 15, backgroundColor: btnColor, borderRadius: Themes.Metrics.buttonRadius }, style]}
        onPress={this.onPress}
      >
        <TextFont style={textStyle}>{children}</TextFont>
      </TouchableOpacity>
    )
  }
}

Button.propTypes = {
  textStyle: React.PropTypes.any,
  btnColor: React.PropTypes.any,
  style: ViewPropTypes.style
}

Button.defaultProps = {
  textStyle: {
    color: '#fff',
    textAlign: 'center'
  },
  btnColor: 'blue'
}

export default Button
