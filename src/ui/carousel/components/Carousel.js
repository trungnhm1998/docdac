import React, { Component } from 'react'
import { StyleSheet, View, ViewPropTypes } from 'react-native'
import ViewPager from './ViewPager'

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  viewPager: {
    margin: 10,
    overflow: 'visible',
    backgroundColor: 'black'
  },
  title: {
    color: 'white',
    fontSize: 12,
    textAlign: 'center'
  }
})

class CarouselComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedIndex: props.selectedIndex
    }
    this.selectedIndexChange = this.selectedIndexChange.bind(this)
  }

  componentWillReceiveProps(nexProps) {
    if (this.props.selectedIndex !== nexProps.selectedIndex) {
      this.setState({ selectedIndex: nexProps.selectedIndex })
    }
  }

  selectedIndexChange = (index) => {
    const { onSelectedIndexChange } = this.props
    // callback
    onSelectedIndexChange(index)
    this.setState({ selectedIndex: index })
  }

  render() {
    const {
      cards,
      style,
      viewPagerStyle
    } = this.props
    return (
      <View style={[styles.container, style]}>
        <ViewPager
          style={[styles.viewPager, viewPagerStyle]}
          count={cards.length}
          selectedIndex={this.state.selectedIndex}
          onSelectedIndexChange={this.selectedIndexChange}
          bounces
        >
          {cards}
        </ViewPager>
      </View>
    )
  }
}
CarouselComponent.propTypes = {
  style: ViewPropTypes.style,
  selectedIndex: React.PropTypes.number,
  onSelectedIndexChange: React.PropTypes.func,
  cards: React.PropTypes.array
}
CarouselComponent.defaultProps = {
  style: null,
  viewPagerStyle: null,
  selectedIndex: 0,
  onSelectedIndexChange: () => {},
  showPageControl: true,
  cards: []
}
export default CarouselComponent
