import React, { Component } from 'react'
import { Platform, ScrollView, StyleSheet, View, ViewPagerAndroid, ViewPropTypes } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  scrollView: {
    flex: 1,
    backgroundColor: 'transparent'
  },
  card: {
    backgroundColor: 'transparent'
  }
})
class ViewPager extends Component {

  constructor(props) {
    super(props)
    this.state = {
      width: 0,
      height: 0,
      selectedIndex: this.props.selectedIndex,
      initialSelectedIndex: this.props.selectedIndex,
      scrollingTo: null
    }
    this.handleHorizontalScroll = this.handleHorizontalScroll.bind(this)
    this.adjustCardSize = this.adjustCardSize.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedIndex !== this.state.selectedIndex) {
      if (Platform.OS === 'ios') {
        this.setState({ scrollingTo: nextProps.selectedIndex })
      } else {
        this.scrollView.setPage(nextProps.selectedIndex)
        this.setState({ selectedIndex: nextProps.selectedIndex })
      }
    }
  }

  adjustCardSize(e) {
    this.setState({
      width: e.nativeEvent.layout.width,
      height: e.nativeEvent.layout.height
    })
  }

  handleHorizontalScroll(e) {
    let selectedIndex = e.nativeEvent.position
    if (selectedIndex === undefined) {
      selectedIndex = Math.round(
        e.nativeEvent.contentOffset.x / this.state.width
      )
    }
    if (selectedIndex < 0 || selectedIndex >= this.props.count) {
      return
    }
    if (this.state.scrollingTo !== null && this.state.scrollingTo !== selectedIndex) {
      return
    }
    if (this.props.selectedIndex !== selectedIndex || this.state.scrollingTo !== null) {
      this.setState({ selectedIndex, scrollingTo: null })
      this.props.onSelectedIndexChange(selectedIndex)
    }
  }

  renderContent = () => {
    const { width, height } = this.state
    const style = Platform.OS === 'ios' && styles.card
    return React.Children.map(this.props.children, (child, i) => (
      <View style={[style, { width, height }]} key={`r_${i}`}>
        {child}
      </View>
    ))
  }

  renderIOS() {
    return (
      <ScrollView
        ref={(scrollView) => { this.scrollView = scrollView }}
        contentOffset={{
          x: this.state.width * this.state.initialSelectedIndex,
          y: 0
        }}
        style={[styles.scrollView, this.props.style]}
        horizontal
        pagingEnabled
        bounces={this.props.bounces}
        scrollsToTop={false}
        onScroll={this.handleHorizontalScroll}
        scrollEventThrottle={100}
        automaticallyAdjustContentInsets={false}
        directionalLockEnabled
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        onLayout={this.adjustCardSize}
      >
        {this.renderContent()}
      </ScrollView>
    )
  }

  renderAndroid() {
    return (
      <ViewPagerAndroid
        ref={(scrollView) => { this.scrollView = scrollView }}
        initialPage={this.state.initialSelectedIndex}
        onPageSelected={this.handleHorizontalScroll}
        style={styles.container}
      >
        {this.renderContent()}
      </ViewPagerAndroid>
    )
  }

  render() {
    if (Platform.OS === 'ios') {
      return this.renderIOS()
    }
    return this.renderAndroid()
  }
}
ViewPager.propTypes = {
  style: ViewPropTypes.style,
  count: React.PropTypes.number,
  selectedIndex: React.PropTypes.number,
  onSelectedIndexChange: React.PropTypes.func,
  children: React.PropTypes.array,
  bounces: React.PropTypes.bool
}
ViewPager.defaultProps = {
  style: null,
  viewPagerStyle: null,
  selectedIndex: 0,
  onSelectedIndexChange: () => {},
  showPageControl: true,
  bounces: true,
  children: null,
  count: 0
}
export default ViewPager

