import Carousel from './components/Carousel'
import ViewPager from './components/ViewPager'

export {
  Carousel,
  ViewPager
}

// read more at https://github.com/jacklam718/react-native-carousel-component
