import { createIconSetFromFontello } from 'react-native-vector-icons'
import fontelloConfig from '../../configs/icon.config.json'

export default Icon = createIconSetFromFontello(fontelloConfig)
