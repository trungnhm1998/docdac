import React, { Component } from 'react'
import { View, ActivityIndicator } from 'react-native'

class Loading extends Component {
  render() {
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator
          animating
          style={{ height: 80 }}
        />
      </View>
    )
  }
}

export default Loading
