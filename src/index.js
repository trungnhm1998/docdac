import React from 'react'
import { Alert } from 'react-native'
import { Provider } from 'react-redux'
import _ from 'lodash'
import store from './configs/store.config'
import { registerScreen } from './screens'
import { Navigation } from './screens/navigation'
registerScreen(store, Provider)

class App extends React.Component {
  constructor(props) {
    super(props)
    store.subscribe(this.onStoreUpdate.bind(this))
    Navigation.navToRoot()
  }

  onStoreUpdate() {
    const { apiResponse } = store.getState()
    if (apiResponse.showLoading === true) {
      Navigation.showLoading()
    } else if (apiResponse.showLoading === false) {
      Navigation.hideLoading()
    }

    if (_.isEmpty(apiResponse.message) === false && apiResponse.isDefault === true) {
      if (_.isNumber(apiResponse.code) === true) {
        switch (apiResponse.code) {
          case -1000: {
            Alert.alert('Thông báo', apiResponse.message, [{
              text: 'Thử lại',
              onPress: () => {
                store.dispatch({ type: 'LOGOUT_USER_SUCCESS' })
                store.dispatch({ type: 'CLEAR_API_RESPONSE' })
                Navigation.navToLogin()
              }
            }], { isLogout: true })
            break
          }
          case -7: {
            Alert.alert('Thông báo', apiResponse.message, [{
              text: 'Đăng nhập lại',
              onPress: () => {
                store.dispatch({ type: 'LOGOUT_USER_SUCCESS' })
                store.dispatch({ type: 'CLEAR_API_RESPONSE' })
                Navigation.navToLogin()
              }
            }], { isLogout: true })
            break
          }
          default: {
            setTimeout(() => {
              Alert.alert('Thông báo', apiResponse.message, [{
                text: 'Đóng',
                onPress: () => {
                  store.dispatch({ type: 'CLEAR_API_RESPONSE' })
                }
              }])
            }, 200)
          }
        }
      }
    } else if (apiResponse.code === -1 && apiResponse.isDefault === true) {
      store.dispatch({ type: 'CLEAR_API_RESPONSE' })
      setTimeout(() => {
        Alert.alert('Thông báo', 'Vui lòng kiểm tra kết nối mạng.', [{
          text: 'Thử lại',
          onPress: () => {
            Navigation.navToRoot()
          }
        }])
      }, 200)
    }
  }
}

// export default codePush(codePushOptions)(App)
export default App
